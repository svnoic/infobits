package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class SafePWCreationStratagy extends MyActivity {

    Context context;
    private static boolean count = false;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("SafePWCreationStratagy"));
        }
        editor.putInt("current", getActivityNumber("SafePWCreationStratagy"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        if(count){
            tv.setText(R.string.create_pw_algo);
        }else{
            tv.setText("Wie du gerade an den Auswahlmöglichkeiten bei der Frage gesehen hast, muss man immer einen Kompromiss eingehen, wenn man ein möglichst sicheres Passwort erstellen möchte. \n\nEntweder verwendest du reale Namen, Wörter, Sätze oder Daten, damit du dir das Passwort leichter merken kannst. Oder aber du denkst dir irgendeine Kombination aus. Dabei kann es aber leicht passieren, dass man diese Kombination wieder vergisst. \n\nAber bedenke, genau wie du die PIN deiner Bankkarte nicht zusammen mit der Karte aufbewaren solltest, solltest du auch deine Passwörter für deine digitalen Geräte nicht bei den digitalen Geräten aufbewahren. Es wäre also sinnvoll eine Strategie zu haben, mit der man sich Passwörter wählen kann, die man sich zum einen gut merken kann, die aber zum anderen so aussehen als wären sie erfunden.");
        }


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (count) {
                        Intent myIntent = nextActivity(getActivityNumber("SafePWCreationStratagy") + 1);
                        startActivity(myIntent);
                        finish();
                        count = false;
                    } else {
                        Intent myIntent = new Intent(context, SafePWCreationStratagy.class);
                        startActivity(myIntent);
                        finish();
                        count = true;
                    }
                }
                return false;
            }
        });
    }
}
