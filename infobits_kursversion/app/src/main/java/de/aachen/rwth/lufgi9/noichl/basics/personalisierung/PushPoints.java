package de.aachen.rwth.lufgi9.noichl.basics.personalisierung;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.Toolbar;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class PushPoints extends BaseActivity {

    int numberOfQuerys = 4;
    int[] buttonClass = {0,0,0,0,0,0};
    int currentNumberOfQuery = 1;
    double precision=0;

    static TouchBehaviour touchBehaviour = new TouchBehaviour();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basics_personalization_push_point);

        init();
    }

    public void onConfigurationChanged(Configuration configuration) {
        // Tilt of Screen in landscape, or back to normal
        super.onConfigurationChanged(configuration);
        setPosition((int) (calculateScreenWidth()*0.5), (int) (calculateScreenHeight()*0.5));
    }

    @Override
    public void onRestart() {
        init();
        super.onRestart();
    }

    private void init() {
       // initToolbar(getResources().getString(R.string.pushPointsTitle));


        currentNumberOfQuery = 1;
        for(int i = 0; i<5; i++) {
            buttonClass[i] = 0;
        }
        setPosition((int) (calculateScreenWidth()*0.5), (int) (calculateScreenHeight()*0.5));
    }

    private void calculatePrecisionClass(MotionEvent event) {
        //Calculate precision
        ImageView iw = (ImageView) findViewById(R.id.imageView);
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_push_points);
        //Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);

        int array[] = new int[2];
        iw.getLocationOnScreen(array);
        float w = array[0];
        float h = array[1];

        int array2[] = new int[2];
        layout.getLocationOnScreen(array);
        float w2 = array[0];
        float h2 = array[1];

        int upperBar = (int) calculateScreenHeight()-(layout.getHeight());
       // int toolBar = myToolbar.getHeight();
        //double distance = calculateDistance(event.getRawX(), event.getRawY()-upperBar-toolBar, iw.getX() + (iw.getWidth() / 2), iw.getY() + (iw.getHeight() / 2));
        double distance = calculateDistance(event.getRawX(), event.getRawY(), iw.getX() + (iw.getWidth() / 2), h + (iw.getHeight() / 2));
        Log.e("DISTANCE", (h2) +"");

        Log.e("DISTANCE", (event.getRawY())+"");

        Log.e("DISTANCE", (h+(iw.getWidth()/2))+"");
        precision = (precision+distance)*0.5;

		//!!!!!!!!!!!!!!!!!!!!!!Hier besser immer eine Klasse runtersetzen?!
//        if(distance <= iw.getWidth()/4) {
//            buttonClass[0] = buttonClass[0]+1;
//        } else {
            if(distance <= iw.getWidth()/2) {
                buttonClass[0] = buttonClass[0]+1;
            } else {
                if(distance <= iw.getWidth()) {
                    buttonClass[1] = buttonClass[1]+1;
                } else {
                    if(distance <= 2*iw.getWidth()) {
                        buttonClass[2] = buttonClass[2]+1;
                    } else {
                        if(distance <= 4*iw.getWidth()) {
                            buttonClass[3] = buttonClass[3]+1;
                        } else {
                            buttonClass[4] = buttonClass[4]+1;
                        }
                    }
                }
            }

    }

    private int calculateButtonHeight() {
        // calculate the corresponding buttonHeight
        int pKlasse = 0;
        for(int i=0; i <= 4; i++) {
            pKlasse = pKlasse+(i+1)* buttonClass[i];
			//!!!!!!Hier ausgeben lassen
        }
        double test = (double) pKlasse / numberOfQuerys;
        pKlasse = (int) Math.round(test);
		//!!!!!!!!!!!!Hier ausgeben lassen
        Log.e("KLASSE", pKlasse+"");
        switch (pKlasse) {
            case 1: return 100;
            case 2: return 125;
            case 3: return 150;
            case 4: return 175;
            case 5: return 175;
            default: return 125;
        }

    }

    private void storeValues() {
        // store the calculated values
        values.setButtonHeight(calculateButtonHeight());
        Log.e("KLASSE", calculateButtonHeight()+"");
        values.setPrecision((int) precision);
        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("ButtonHeight", calculateButtonHeight());
        editor.commit();
    }

    @Override
    public void actionAfterTouch() {
        //Callback when Waiting-Time after Input is over
        TouchBehaviour object = new TouchBehaviour();
        object.setCustomObjectListener(new TouchBehaviour.InputOver() {
            @Override
            public void next() {
                if (currentNumberOfQuery != numberOfQuerys) {
                    newSpawn();
                } else {
                    // Activity done
                    storeValues();
                    nextActivity();
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        actionAfterTouch();

        touchBehaviour.calculateDurationPushed(event, inputTime);
        if (event.getAction() == MotionEvent.ACTION_DOWN && !touchBehaviour.moreThanOnePush()) {
            calculatePrecisionClass(event);
        }
        return true;
    }

    private void newSpawn() {
        // Load new red cross
        currentNumberOfQuery++;
        setRandomPosition();
    }

    private void setPosition(int posX, int posY) {
        // Set redCross to position (posX, posY)
        ImageView iw = (ImageView) findViewById(R.id.imageView);
        iw.setX(posX);
        iw.setY(posY);
    }

    private void setRandomPosition() {
        // Reset the position of the cross randomly
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_push_points);
        ImageView iw = (ImageView) findViewById(R.id.imageView);

        int upperBar = (int) calculateScreenHeight()-(layout.getHeight());
       // int toolBar = myToolbar.getHeight();
        int puffer = 80;    // distance from screen

        int positionWidth = randomNumber(puffer, (int) (calculateScreenWidth()-layout.getPaddingLeft()-layout.getPaddingRight()-iw.getWidth()-puffer));
        int positionHeight = randomNumber(puffer, (int) (calculateScreenHeight()-layout.getPaddingBottom()-layout.getPaddingTop()-iw.getHeight()-upperBar-puffer));

        while (calculateDistance(positionWidth, positionHeight, iw.getX(), iw.getY()) < 200) {          //Distance of Cross should be over 200
            positionWidth = randomNumber(puffer, (int) (calculateScreenWidth()-layout.getPaddingLeft()-layout.getPaddingRight()-iw.getWidth()-puffer));
            positionHeight = randomNumber(puffer, (int) (calculateScreenHeight()-layout.getPaddingBottom()-layout.getPaddingTop()-iw.getHeight()-upperBar-puffer));
        }

        setPosition(positionWidth, positionHeight);
    }



    //Calculate Height and Width of the Screen
    private double calculateScreenWidth() {
        DisplayMetrics metrics;
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }
    private double calculateScreenHeight() {
        DisplayMetrics metrics;
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }
}