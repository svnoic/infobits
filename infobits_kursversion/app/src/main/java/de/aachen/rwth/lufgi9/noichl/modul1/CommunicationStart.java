package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the entry point of module 1.
 * The user will be informed what the topic of this module is.
 * By using the learningoutcomes-button the get a list of the learning outcomes for this module in a dialog.
 * If the user press the nextButton the next Activity (PretestInformation.class) will be started.
 * All button touch events will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class CommunicationStart extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (R.layout.activity_communication_start).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch and PretestInformation-Activity will be started.
     * <b>outcomesButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The learning outcomes will be shown in a dialog. To close the dialog the close-button has to be pressed twice.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mytextview_coloredbutton_nextbutton);

        context = this;

        Log.e("NEW_CURRENT", newCurrent + "");


        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("CommunicationStart")); //101
            editor.apply();
        }
        editor.putInt("current", getActivityNumber("CommunicationStart"));
        editor.apply();
        saveSharedPrefs();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.communication_welcome);

        final Button outcomesButton = findViewById(R.id.outcomes);
        outcomesButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, outcomesButton));

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog("Lernziele", getString(R.string.communication_learning_outcomes), context);
                }

                return true;
            }
        });

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                    //Intent myIntent = new Intent(context, PretestInformation.class);
                    //Intent myIntent = new Intent(context, DigComp.class);
                    int next = getActivityNumber("CommunicationStart") + 1;

                    Intent myIntent = nextActivity(getActivityNumber("CommunicationStart") + 1); //102
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });
    }


}
