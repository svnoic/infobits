package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the end of module 1.
 * This class gives an overview over all texts, videos and excercises of this module and the user can easily jump back to the content he/she wants to repeat.
 * The is also a possibility to watch the results of the pre- and post- "Selbsteinschätzung". If the statsButton is touched the Activity (XYPlotActivity) with the corresponding diagram will be started.
 * If the user press the nextButton the AppStart-Activity will be started.
 * All button touch events will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Recap extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;
    int currentActivity;
    boolean recapMode = false;

    /**
     * Loads the activities layout (R.layout.viewstub_finalpage_module1).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The recap variable is set to false and the AppStart-Activity will be started.
     * <b>statsButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The XYPlotActivity will be started to show the user a diagram.
     * <b>All other buttons:</b>Loggs time and touch-coordinates of users touch. Start the corresponding Activity with the content the user wants to repeat.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recap_modul1);

        context = this;


        if (myrecap){
            recapMode = true;
        }
        myrecap = true;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.recap_text);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setText(R.string.close);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(recapMode){

                }else {
                    myrecap = false;
                }
                finish();
                return false;
            }
        });

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);

        currentActivity = sharedpreferences.getInt("currentActivityM1", 100);


        final Button recap_video1 = findViewById(R.id.recap_video1);
        if (currentActivity < 104 ){
            recap_video1.setVisibility(View.GONE);
        }
        if (currentActivity == 104 && !VideoTalkAndLetter.second){
            recap_video1.setBackgroundResource(R.drawable.color_button);
        }
        recap_video1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if(currentActivity == 104 && !VideoTalkAndLetter.second){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), TalkLetterIntro.class);
                        VideoTalkAndLetter.second = false;
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_video2 = findViewById(R.id.recap_video2);
        if (currentActivity < 104 || (currentActivity == 104 && !VideoTalkAndLetter.second)){
            recap_video2.setVisibility(View.GONE);
        }
        if (currentActivity == 104 && VideoTalkAndLetter.second){
            recap_video2.setBackgroundResource(R.drawable.color_button);
        }
        recap_video2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if(currentActivity == 104 && VideoTalkAndLetter.second){
                        myrecap = false;
                    }
                        VideoTalkAndLetter.second = true;
                        Intent myIntent = new Intent(getBaseContext(), VideoTalkAndLetter.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_video3 = findViewById(R.id.recap_video3);
        if (currentActivity < 111){
            recap_video3.setVisibility(View.GONE);
        }
        if (currentActivity == 111 || currentActivity == 112){
            recap_video3.setBackgroundResource(R.drawable.color_button);
        }
        recap_video3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 111 || currentActivity == 112){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), VideoAdressIntro.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_video4 = findViewById(R.id.recap_video4);
        if (currentActivity < 114){
            recap_video4.setVisibility(View.GONE);
        }
        if (currentActivity == 114 || currentActivity == 115 || currentActivity == 116){
            recap_video4.setBackgroundResource(R.drawable.color_button);
        }
        recap_video4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 114 || currentActivity == 115 || currentActivity == 116){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), ImageUseIntro.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_video5 = findViewById(R.id.recap_video5);
        if (currentActivity < 117){
            recap_video5.setVisibility(View.GONE);
        }
        if (currentActivity == 117 || currentActivity == 118){
            recap_video5.setBackgroundResource(R.drawable.color_button);
        }
        recap_video5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 117 || currentActivity == 118){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), CreativeCommonsIntro.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_video6 = findViewById(R.id.recap_video6);
        if (currentActivity < 119){
            recap_video6.setVisibility(View.GONE);
        }
        if (currentActivity == 119 ||  currentActivity == 120){
            recap_video6.setBackgroundResource(R.drawable.color_button);
        }
        recap_video6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 119 || currentActivity == 120){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), OERIntro.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_activity1 = findViewById(R.id.recap_activity1);
        if (currentActivity < 105){
            recap_activity1.setVisibility(View.GONE);
        }
        if (currentActivity == 105){
            recap_activity1.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 105){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), DifferenceTalkLetter.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_activity2 = findViewById(R.id.recap_activity2);
        if (currentActivity < 107){
            recap_activity2.setVisibility(View.GONE);
        }
        if (currentActivity == 107 || currentActivity == 108){
            recap_activity2.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 107 || currentActivity == 108){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), CommunicationUsage.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_activity3 = findViewById(R.id.recap_activity3);
        if (currentActivity < 109){
            recap_activity3.setVisibility(View.GONE);
        }
        if (currentActivity == 109){
            recap_activity3.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 109){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), UsecaseActivity.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_activity4 = findViewById(R.id.recap_activity4);
        if (currentActivity < 110){
            recap_activity4.setVisibility(View.GONE);
        }
        if (currentActivity == 110){
            recap_activity4.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 110){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), FillTheGapActivity.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_activity5 = findViewById(R.id.recap_activity5);
        if (currentActivity < 113){
            recap_activity5.setVisibility(View.GONE);
        }
        if (currentActivity == 113){
            recap_activity5.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 113){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), MessageComponentMatch.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_activity6 = findViewById(R.id.recap_activity6);
        if (currentActivity < 121){
            recap_activity6.setVisibility(View.GONE);
        }
        if (currentActivity == 121){
            recap_activity6.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 121){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), SortPolicy.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_activity7 = findViewById(R.id.recap_activity7);
        if (currentActivity < 122){
            recap_activity7.setVisibility(View.GONE);
        }
        if (currentActivity == 122){
            recap_activity7.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity7));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 122){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), PolicyTrueFalse.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_activity8 = findViewById(R.id.recap_activity8);
        if (currentActivity < 123){
            recap_activity8.setVisibility(View.GONE);
        }
        if (currentActivity == 123){
            recap_activity8.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity8));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 123) {
myrecap = false;                    }
                        Intent myIntent = new Intent(getBaseContext(), UsecaseActivity2.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });
    }
}
