package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class UrheberrechtIntro extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

//        if(!recap && !myrecap) {
//            SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.putInt("currentActivityM1", getActivityNumber("UrheberrechtIntro"));
//            editor.apply();
//        }

        int correct = Objects.requireNonNull(getIntent().getExtras()).getInt("correct");
        TextView tv = findViewById(R.id.my_textview);
        tv.setText(getString(R.string.module1_urheberrecht_text1) + " " + correct + " " + getString(R.string.module1_urheberrecht_text2));

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                    Intent myIntent = nextActivity(getActivityNumber("UrheberrechtIntro") + 1);
                    startActivity(myIntent);
                    finish();

                return false;
            }
        });
    }
}
