package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;



import java.util.List;

/**
 * Some useful methods for views.
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class ViewUtilities {

    /**
     * Find all views which are type of <code>eClass</code> in the root view.
     *
     * @param root   The view that contains the nested views
     * @param eClass The class entry to look for nested views.
     * @param views  The list of views that you are looking for.
     * @param <E>    The type of the class entry.
     * @return The list of views that have your given class.
     */
    public static <E> List<E> findViews(View root, Class<E> eClass, List<E> views) {
        if (eClass.isAssignableFrom(root.getClass())) {
            //noinspection unchecked
            views.add((E) root);
        }

        // look for all children if they are or have a checkable view
        if (root instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) root).getChildCount(); i++) {
                findViews(((ViewGroup) root).getChildAt(i), eClass, views);
            }
        }
        return views;
    }

    /**
     * Check if a view contains a given point
     *
     * @param view The view to check
     * @param rawX The x position on screen
     * @param rawY The y position on screen
     * @return <code>true</code> if the view contains the point
     */
    public static boolean containsPoint(View view, float rawX, float rawY) {
        // get the view coordinates on screen
        int[] locationOnScreenView = new int[2];
        view.getLocationOnScreen(locationOnScreenView);
        int viewX = locationOnScreenView[0];
        int viewY = locationOnScreenView[1];

        // look in both axis if the point is inside
        boolean dimX = rawX > viewX && rawX < viewX + view.getWidth();
        boolean dimY = rawY > viewY && rawY < viewY + view.getHeight();
        return dimX && dimY;
    }

    /**
     * Creates a {@link RectF} of a view relative to screen.
     *
     * @param view The view to get its rect of
     * @return A {@link RectF} of the <code>view</code> relative to screen.
     */
    public static RectF rectScreen(View view) {
        int[] screenValues = new int[2];
        view.getLocationOnScreen(screenValues);
        return new RectF(screenValues[0], screenValues[1], screenValues[0] + view.getWidth(), screenValues[1] + view.getHeight());
    }

    /**
     * Creates a {@link RectF} of a view relative to parent.
     *
     * @param view The view to get its rect of
     * @return A {@link RectF} of the <code>view</code> relative to parent.
     */
    public static RectF rectParent(View view) {
        return new RectF(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
    }

    /**
     * Close the keyboard that is currently opened in the activity
     * @param activity Need to access services and to find the current focus
     */
    public static void closeKeyboard(Activity activity){
        // find the current view
        View view = activity.getCurrentFocus();

        // close keyboard
        if(view != null){
            closeKeyboard(activity, view);
        }
    }

    /**
     * Close the keyboard that is currently opened for <code>view</code>
     * @param context Need to access services
     * @param view The view that has the focus for the keyboard
     */
    public static void closeKeyboard(Context context, View view){
        //CustomLog.i(context, ViewUtilities.class, "system", "close keyboard if opened");
        // close the keyboard, if opened
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
