package de.aachen.rwth.lufgi9.noichl.general;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;

public class MainActivity extends AppCompatActivity {
public static Context mycontext;
int counter = 0;
String ID = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_user_code);
        mycontext = this;
        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if(ID.length() == 4) {
                        //USERID = "0000";
                        MyActivity.USERID = ID;
                        Intent myIntent = new Intent(mycontext, AppStart.class); //DataInformation.class in studienversion
                        startActivity(myIntent);
                        finish();
                    }

                }
                return false;
            }
        });

        final Button n1 = findViewById(R.id.n1);
        n1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                                tv1.setText("1");
                                ID = ID + "1";
                                break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("1");
                            ID = ID + "1";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("1");
                            ID = ID + "1";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("1");
                            ID = ID + "1";
                            break;
                    }
                }
                return false;
            }
        });

        final Button n2 = findViewById(R.id.n2);
        n2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                            tv1.setText("2");
                            ID = ID + "2";
                            break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("2");
                            ID = ID + "2";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("2");
                            ID = ID + "2";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("2");
                            ID = ID + "2";
                            break;
                    }
                }
                return false;
            }
        });

        final Button n3 = findViewById(R.id.n3);
        n3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                            tv1.setText("3");
                            ID = ID + "3";
                            break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("3");
                            ID = ID + "3";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("3");
                            ID = ID + "3";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("3");
                            ID = ID + "3";
                            break;
                    }
                }
                return false;
            }
        });

        final Button n4 = findViewById(R.id.n4);
        n4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                            tv1.setText("4");
                            ID = ID + "4";
                            break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("4");
                            ID = ID + "4";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("4");
                            ID = ID + "4";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("4");
                            ID = ID + "4";
                            break;
                    }
                }
                return false;
            }
        });

        final Button n5 = findViewById(R.id.n5);
        n5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                            tv1.setText("5");
                            ID = ID + "5";
                            break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("5");
                            ID = ID + "5";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("5");
                            ID = ID + "5";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("5");
                            ID = ID + "5";
                            break;
                    }
                }
                return false;
            }
        });

        final Button n6 = findViewById(R.id.n6);
        n6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                            tv1.setText("6");
                            ID = ID + "6";
                            break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("6");
                            ID = ID + "6";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("6");
                            ID = ID + "6";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("6");
                            ID = ID + "6";
                            break;
                    }
                }
                return false;
            }
        });

        final Button n7 = findViewById(R.id.n7);
        n7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                            tv1.setText("7");
                            ID = ID + "7";
                            break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("7");
                            ID = ID + "7";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("7");
                            ID = ID + "7";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("7");
                            ID = ID + "7";
                            break;
                    }
                }
                return false;
            }
        });

        final Button n8 = findViewById(R.id.n8);
        n8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                            tv1.setText("8");
                            ID = ID + "8";
                            break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("8");
                            ID = ID + "8";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("8");
                            ID = ID + "8";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("8");
                            ID = ID + "8";
                            break;
                    }
                }
                return false;
            }
        });

        final Button n9 = findViewById(R.id.n9);
        n9.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    counter++;
                    switch (counter) {
                        case 1: TextView tv1 = findViewById(R.id.code1);
                            tv1.setText("9");
                            ID = ID + "9";
                            break;
                        case 2: TextView tv2 = findViewById(R.id.code2);
                            tv2.setText("9");
                            ID = ID + "9";
                            break;
                        case 3: TextView tv3 = findViewById(R.id.code3);
                            tv3.setText("9");
                            ID = ID + "9";
                            break;
                        case 4: TextView tv4 = findViewById(R.id.code4);
                            tv4.setText("9");
                            ID = ID + "9";
                            break;
                    }
                }
                return false;
            }
        });

        final Button reset = findViewById(R.id.reset);
        reset.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    TextView tv1 = findViewById(R.id.code1);
                    tv1.setText("");
                    TextView tv2 = findViewById(R.id.code2);
                    tv2.setText("");
                    TextView tv3 = findViewById(R.id.code3);
                    tv3.setText("");
                    TextView tv4 = findViewById(R.id.code4);
                    tv4.setText("");
                    ID = "";
                    counter = 0;
                }
                return false;
            }
        });
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mycontext);

        // set title
        TextView mytitle = new TextView(mycontext);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        alertDialog.getWindow().getAttributes();

        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }
}
