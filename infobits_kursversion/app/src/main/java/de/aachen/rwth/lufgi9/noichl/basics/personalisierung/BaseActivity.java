package de.aachen.rwth.lufgi9.noichl.basics.personalisierung;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Random;

import de.aachen.rwth.lufgi9.noichl.R;


abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    static Values values = new Values();
    static HardwareButtonPress hardwareButtonPress = new HardwareButtonPress();
    static TouchBehaviour touchBehaviour = new TouchBehaviour();
    static ChangeActivity changeActivity = new ChangeActivity();
    static int inputTime = 300;                            // Time that has to pass till Continue-Button causes action




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected void setScreenTimeout() {
        // Set the time that passes till screen turns off
        int timeOut = 0;
        try {
            timeOut = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onKeyUp(final int keyCode, final KeyEvent event) {
        // If a HardwareButton is pressed
        HardwareButtonPress warning = new HardwareButtonPress();


        return hardwareButtonPress.onKeyUp(keyCode,this);
    }



    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        // Necessary to highlight words in a bold font
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    protected void setTextInTextView(String message) {
        int resID = getResources().getIdentifier("textView1", "id", getPackageName());
        TextView textView = (TextView) findViewById(resID);

        if(textView != null) {
            textView.setTextSize(values.getMinTextSize());
            textView.setText(fromHtml(message));
        }
    }

    protected void setTextHeightInButton(Button b) {
        // Shift text in button vertically
        if(values.getTextHeightInButton() > 0.5) {
            b.setPadding(0,(int) (b.getHeight()*(values.getTextHeightInButton()-0.5)),0,0);
        } else {
            b.setPadding(0,0,0,(int) (b.getHeight()*(0.5- values.getTextHeightInButton())));
        }
    }

    protected void initButton(String title, int buttonNumber) {
        // Initialize button 'buttonNumber'
        int resID = getResources().getIdentifier("button" + (buttonNumber), "id", getPackageName());
        final Button button = (Button) findViewById(resID);


        // Change buttonHeight if buttonHeight has already been calculated
        if (values.getButtonHeight() != 0) {
            LinearLayout.LayoutParams ll = (LinearLayout.LayoutParams) button.getLayoutParams();
            ll.gravity = Gravity.CENTER;
            //ll.height = values.getButtonHeight();
            button.setLayoutParams(ll);
            button.setMinimumHeight((int) (values.getButtonHeight() * getResources().getDisplayMetrics().density + 0.5f));
        }

        // Wait till Button fully loaded. Else getHeight() returns 0
        button.post(new Runnable() {
            @Override
            public void run() {
                setTextHeightInButton(button);
            }
        });
        button.setTextSize(values.getTextSizeButton());
        button.setOnClickListener(this);
        button.setText(title);

        // OnTouchListener to track the Touch-Behaviour (only for Continue-Button)
        if (buttonNumber == 1) {
            button.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    actionAfterTouch();
                    touchBehaviour.calculateDurationPushed(event, inputTime);
                    touchBehaviour.calculateTextHeightInButton(v, event);
                    return false;
                }
            });
        }
    }

    protected void actionAfterTouch() {
        // What happens after the TouchBehaviour was tracked, and the Listener is loaded
        TouchBehaviour object = new TouchBehaviour();
        object.setCustomObjectListener(new TouchBehaviour.InputOver() {
            @Override
            public void next() {
                if(changeActivity.getCurrentActivity() != 13) {

                    values.setTextHeightInButton(touchBehaviour.getTextHeightInButton());


                }
                nextActivity();
            }
        });
    }

    protected void initImageButtons(int numberOfButtons) {
        // Initialize all ImageButtons till 'numberOfButtons'
        for(int i=1; i<=numberOfButtons; i++) {
            int resID = getResources().getIdentifier("imageButton"+(i), "id", getPackageName());
            ImageButton button = (ImageButton) findViewById(resID);
            button.setOnClickListener(this);
        }
    }

    protected int randomNumber(int lowerBound, int upperBound) {
        // Calculate a random number n such that lowerBound <= n < upperBound
        if(lowerBound != upperBound) {
            Random r = new Random();
            return r.nextInt(upperBound - lowerBound) + lowerBound;
        } else {
            return lowerBound;
        }
    }

    protected double calculateDistance(float x1, float y1, float x2, float y2) {
        // Calculate distance between point x and point y
        return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    }

    @Override
    public void onClick(View v) {
        // Empty Method has to be implemented
    }

    protected void nextActivity() {
        // Load the next Activity
        Log.e("ACTIVITY", changeActivity.getCurrentActivity()+"");
        if(changeActivity.getCurrentActivity() == 13) {
            loadAttachedApp();
        } else {
            if(changeActivity.getCurrentActivity() == 6){
                changeActivity.loadNextActivity(this);
            }else {
                if(changeActivity.getCurrentActivity() == 2) {
                    changeActivity.loadNextActivity(this);
                }else {

                    changeActivity.loadNextActivity(this);
                }
            }
        }
    }

    protected void loadAttachedApp() {
        // Load the attached App

        // DEMO (the following seven lines)
//        int resID = getResources().getIdentifier("textView1", "id", getPackageName());
//        TextView textView = (TextView) findViewById(resID);
//        if(textView.getText().length() == getResources().getString(R.string.instructionEnding).length()) {
//            textView.setText(values.getDataWithExplanation());
//        } else {
//            changeActivity.loadActivity(this, 1);
//        }
        SharedPreferences prefsOverview = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefsOverview.edit();
        editor.putBoolean("repeat_P", false);
        editor.putBoolean("active_P", false);
        editor.putBoolean("repeat_1", true);
        editor.putBoolean("active_1", true);
        editor.commit();
        Intent intent = getPackageManager().getLaunchIntentForPackage("de.aachen.rwth.lufgi9.noichl.informatikssystem");
        Bundle extras = new Bundle();
        extras.putInt("pre", 0);
        intent.putExtras(extras);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}



/*
Buttons:
Buttons should be named "button1", "button2", "button3", etc.
"button1": Continue-Button. Next Activity loads after 'inputTime' passes.
 */

