package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is for showing two videos, one explains "Gespräch" and the other one "Brief".
 * To continue to the next Activity the user has to watch both of them completely.
 *
 * @author Svenja Noichl
 */
public class VideoTalkAndLetter extends MyActivity {

    static int videopos = 10;
    static boolean videoready;
    static boolean second = false;
    VideoView myVideo;
    Context context;
    Button nextButton;




    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_nextbutton);
        context = this;

        Log.e("_NEW_CURRENT__", this.getClass().toString());

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("VideoTalkAndLetter"));
        }
        editor.putInt("current", getActivityNumber("VideoTalkAndLetter"));
        editor.apply();
        saveSharedPrefs();

        Uri uri;
        if (second) {
            uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.v2);
        } else {
            uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.v1);
        }
        myVideo = findViewById(R.id.videoView);
        myVideo.setVideoURI(uri);
        myVideo.seekTo(videopos);
        myVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(MediaPlayer mediaPlayer) {
                        myVideo.start();
                        myVideo.pause();
                    }
                });
            }
        });
        myVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoready = true;
                nextButton.setVisibility(View.VISIBLE);
            }
        });

        myVideo.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), videoButtonEventExtension(event, myVideo, "wlan", myVideo.getCurrentPosition()));

                if (myVideo.isPlaying()) {
                    myVideo.pause();
                } else {
                    myVideo.start();
                }
                return false;
            }
        });

        MediaController mediaController = new
                MediaController(this);
        mediaController.setAnchorView(myVideo);
        myVideo.setMediaController(mediaController);


        nextButton = findViewById(R.id.next);
        nextButton.setVisibility(View.GONE);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (recap) {
                        Intent myIntent = new Intent(context, CommunicationEnd.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        if (myrecap) {
                            restartAfterRecap();
                        } else {
                            Intent myIntent;
                            if (second) {
                                second = false;
                                //myIntent = new Intent(context, DifferenceTalkLetter.class);
                                myIntent = nextActivity(getActivityNumber("VideoTalkAndLetter") + 1);
                            } else {
                                second = true;
                                myIntent = new Intent(context, VideoTalkAndLetter.class);
                            }
                            startActivity(myIntent);
                            finish();
                        }
                    }

                }
                return false;
            }
        });
    }


}
