package de.aachen.rwth.lufgi9.noichl.basics.eingaben;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.AppStart;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the last Activity of the Foundation-Input Module. The user get an information text, saying he/she reached the end of this module.
 * If the user press the nextButton the AppStart-Activity will be started.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class End extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (R.layout.activity_textview_next_button).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinated of users touch.
     * Any touch event on this button will trigger the start of AppStart-Activity.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.input_end_info_text);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                Intent myIntent = new Intent(context, AppStart.class);
                startActivity(myIntent);
                finish();

                return false;
            }
        });
    }
}
