package de.aachen.rwth.lufgi9.noichl.basics.eingaben;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is for learning how type in texts and send them using a button.
 * If the user did not send any text using the send button a dialog box is shown saying that he/she should send any text. If the user has send any text using the send button the next Activity (PwInput.class) is started.
 * The send texts will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class TextInput extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Represents the status of the given solution.
     * If the user has send any text, solution has the state <b>true</b> and on nextButton-touch-event the next activity will be started.
     * If the did not send any text, solution has the state <b>false</b> ans on nextButton-touch-event a dialog will be shown.
     */
    boolean solution;

    /**
     * Loads the activities layout (R.layout.activity_textview_viewstub2_next_button) which uses a viewstub (basics_inputs_text).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event either the next activity will be started or the dialog will be shown based on the status of the <b>solution</b> variable.
     * If solution is <b>true</b> the next activity (PwInput.class) will be started, otherwise a dialog will be shown.
     * <b>sendBtton-onTouchListener</b>Loggs time and touch-coordinates of users touch.
     * The entered text will be shown in a TextView and solution is set to <b>true</b>. The entered text will be logged using the myLog-method.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_inputs_text);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.input_text_question);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (solution) {
                        solution = false;
                        Intent myIntent = new Intent(context, PwInput.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.input_text_dialog), context);
                    }
                }

                return false;
            }
        });

        final ImageButton sendButton = findViewById(R.id.imageButton);
        sendButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageButtonEventExtension(event, sendButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager)
                            v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    EditText et = findViewById(R.id.editText);
                    String message = String.valueOf(et.getText());
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension(message));
                    et.setText("");
                    TextView tv = findViewById(R.id.textView20);
                    if (tv.getText().equals("")) {
                        tv.append(message);
                    } else {
                        tv.append("\n" + message);
                    }
                    solution = true;
                }

                return false;
            }
        });
    }


}
