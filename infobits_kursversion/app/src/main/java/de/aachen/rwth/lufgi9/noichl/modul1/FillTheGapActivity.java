package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class FillTheGapActivity extends MyActivity implements AdapterView.OnItemSelectedListener {

    private static boolean solution = false;
    Context context;
    Spinner spinner1, spinner2, spinner3, spinner4, spinner5, spinner6, spinner7, spinner8, spinner9, spinner10;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mytextview_s_viewstub_s_nextbutton);
        context = this;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_10_fillthegap_spinner);
        stub.inflate();
        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("FillTheGapActivity"));
        }
        editor.putInt("current", getActivityNumber("FillTheGapActivity"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.fill_the_gap_info);

        TextView gap1 = findViewById(R.id.c_gap_text1);
        gap1.setText(R.string.c_gap_text1);

        TextView gap2 = findViewById(R.id.c_gap_text2);
        gap2.setText(R.string.c_gap_text2);

        TextView gap3 = findViewById(R.id.c_gap_text3);
        gap3.setText(R.string.c_gap_text3);
        TextView gap31 = findViewById(R.id.c_gap_text31);
        gap31.setText(R.string.c_gap_text31);

        TextView gap4 = findViewById(R.id.c_gap_text4);
        gap4.setText(R.string.c_gap_text4);

        TextView gap5 = findViewById(R.id.c_gap_text5);
        gap5.setText(R.string.c_gap_text5);

        TextView gap6 = findViewById(R.id.c_gap_text6);
        gap6.setText(R.string.c_gap_text6);

        TextView gap7 = findViewById(R.id.c_gap_text7);
        gap7.setText(R.string.c_gap_text7);

        TextView gap8 = findViewById(R.id.c_gap_text8);
        gap8.setText(R.string.c_gap_text8);

        TextView gap81 = findViewById(R.id.c_gap_text81);
        gap81.setText(R.string.c_gap_text1);

        TextView gap9 = findViewById(R.id.c_gap_text9);
        gap9.setText(R.string.c_gap_text9);

        TextView gap10 = findViewById(R.id.c_gap_text10);
        gap10.setText(R.string.c_gap_text10);

        TextView gap11 = findViewById(R.id.c_gap_text11);
        gap11.setText(R.string.c_gap_text11);

        TextView gap12 = findViewById(R.id.c_gap_text12);
        gap12.setText(R.string.c_gap_text12);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (!solution) {
                        String item1 = spinner1.getSelectedItem().toString();
                        String item2 = spinner2.getSelectedItem().toString();
                        String item3 = spinner3.getSelectedItem().toString();
                        String item4 = spinner4.getSelectedItem().toString();
                        String item5 = spinner5.getSelectedItem().toString();
                        String item6 = spinner6.getSelectedItem().toString();
                        String item7 = spinner7.getSelectedItem().toString();
                        String item8 = spinner8.getSelectedItem().toString();
                        String item9 = spinner9.getSelectedItem().toString();
                        String item10 = spinner10.getSelectedItem().toString();

                        if (!item1.equals("...") && !item2.equals("...") && !item3.equals("...") && !item4.equals("...") && !item5.equals("...") && !item6.equals("...")
                                && !item7.equals("...") && !item8.equals("...") && !item9.equals("...") && !item10.equals("...")) {
                            if (item1.equals("asynchrone")) {
                                spinner1.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item2.equals("nicht beide")) {
                                spinner2.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item3.equals("E-Mail, Fax und Kommentare")) {
                                spinner3.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item4.equals("synchrone")) {
                                spinner4.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item5.equals("zur gleichen Zeit")) {
                                spinner5.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item6.equals("Gespräch")) {
                                spinner6.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item7.equals("Telefonie, Videotelefonie und (Live)Chat")) {
                                spinner7.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item8.equals("Instant Messenger")) {
                                spinner8.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item9.equals("synchron")) {
                                spinner9.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item10.equals("asynchron")) {
                                spinner10.setBackgroundResource(R.drawable.answer_true);
                            }

                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner1.toString(), item1.equals("asynchrone"), item1, "asynchrone"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner2.toString(), item2.equals("nicht beide"), item2, "nicht beide"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner3.toString(), item3.equals("E-Mail, Fax und Kommentare"), item3, "E-Mail, Fax und Kommentare"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner4.toString(), item4.equals("synchrone"), item4, "synchrone"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner5.toString(), item5.equals("zur gleichen Zeit"), item5, "zur gleichen Zeit"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner6.toString(), item6.equals("Gespräch"), item6, "Gespräch"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner7.toString(), item7.equals("Telefonie, Videotelefonie und (Live)Chat"), item7, "Telefonie, Videotelefonie und (Live)Chat"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner8.toString(), item8.equals("Instant Messenger"), item8, "Instant Messenger"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner9.toString(), item9.equals("synchron"), item9, "synchron"));
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(spinner10.toString(), item10.equals("asynchron"), item10, "asynchron"));

                            spinner1.setEnabled(false);
                            spinner2.setEnabled(false);
                            spinner3.setEnabled(false);
                            spinner4.setEnabled(false);
                            spinner5.setEnabled(false);
                            spinner6.setEnabled(false);
                            spinner7.setEnabled(false);
                            spinner8.setEnabled(false);
                            spinner9.setEnabled(false);
                            spinner10.setEnabled(false);
                            solution = true;
                        } else {
                            showDialog("Noch nicht fertig.", "Bitte wähle für ALLE Lücken im Text eine Antwort aus.", context);
                        }
                    } else {
                        solution = false;
                        if (recap) {
                            Intent myIntent = new Intent(context, CommunicationEnd.class);
                            startActivity(myIntent);
                            finish();
                        } else {
                            if (myrecap) {
                                restartAfterRecap();
                            } else{
                                Intent myIntent = nextActivity(getActivityNumber("FillTheGapActivity") + 1);
                            startActivity(myIntent);
                            finish();
                        }
                        }
                    }
                }
                return false;
            }
        });

        spinner1 = findViewById(R.id.c_gap_spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_1, R.layout.template_normal_text_spinner);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(this);

        spinner2 = findViewById(R.id.c_gap_spinner2);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_2, R.layout.template_normal_text_spinner);
        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener(this);

        spinner3 = findViewById(R.id.c_gap_spinner3);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_3, R.layout.template_normal_text_spinner);
        spinner3.setAdapter(adapter);
        spinner3.setOnItemSelectedListener(this);

        spinner4 = findViewById(R.id.c_gap_spinner4);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_4, R.layout.template_normal_text_spinner);
        spinner4.setAdapter(adapter);
        spinner4.setOnItemSelectedListener(this);

        spinner5 = findViewById(R.id.c_gap_spinner5);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_5, R.layout.template_normal_text_spinner);
        spinner5.setAdapter(adapter);
        spinner5.setOnItemSelectedListener(this);

        spinner6 = findViewById(R.id.c_gap_spinner6);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_6, R.layout.template_normal_text_spinner);
        spinner6.setAdapter(adapter);
        spinner6.setOnItemSelectedListener(this);

        spinner7 = findViewById(R.id.c_gap_spinner7);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_7, R.layout.template_normal_text_spinner);
        spinner7.setAdapter(adapter);
        spinner7.setOnItemSelectedListener(this);

        spinner8 = findViewById(R.id.c_gap_spinner8);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_8, R.layout.template_normal_text_spinner);
        spinner8.setAdapter(adapter);
        spinner8.setOnItemSelectedListener(this);

        spinner9 = findViewById(R.id.c_gap_spinner9);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_9, R.layout.template_normal_text_spinner);
        spinner9.setAdapter(adapter);
        spinner9.setOnItemSelectedListener(this);

        spinner10 = findViewById(R.id.c_gap_spinner10);
        adapter = ArrayAdapter.createFromResource(this, R.array.communication_gap_10, R.layout.template_normal_text_spinner);
        spinner10.setAdapter(adapter);
        spinner10.setOnItemSelectedListener(this);

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), gapEventExtension(parent + "", parent.getItemAtPosition(position).toString()));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
