package de.aachen.rwth.lufgi9.noichl.modul3;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileNotFoundException;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.AppStart;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class ConnectionEnd extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewstub_finalpage_module3);



        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM3", getActivityNumber("ConnectionEnd"));
        }
        editor.putInt("current", getActivityNumber("ConnectionEnd"));
        editor.apply();
        saveSharedPrefs();

        recap = true;


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.finish);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setText(R.string.module2_end);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                recap = false;
                try {
                    readSharedPrefs();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Intent myIntent = new Intent(getBaseContext(), AppStart.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button statsButton = findViewById(R.id.stats);
        statsButton.setText(R.string.watch_stats);
        statsButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, statsButton));
                Intent myIntent = new Intent(getBaseContext(), XYPlotActivity.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });


        final Button recap_video1 = findViewById(R.id.recap_video1);
        recap_video1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), CableVideoIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video2 = findViewById(R.id.recap_video2);
        recap_video2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), ConnectionVideoIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });




        final Button recap_text1 = findViewById(R.id.recap_text1);
        recap_text1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), ConnectionIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text2 = findViewById(R.id.recap_text2);
        recap_text2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), LanManWan.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text3 = findViewById(R.id.recap_text3);
        recap_text3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), WlanWifi.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text4 = findViewById(R.id.recap_text4);
        recap_text4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), Mobilfunk.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });



        final Button recap_activity1 = findViewById(R.id.recap_activity1);
        recap_activity1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), InternetWordsSolution.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity2 = findViewById(R.id.recap_activity2);
        recap_activity2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DifferenceWLANMobil.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity3 = findViewById(R.id.recap_activity3);
        recap_activity3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), GameBookIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity4 = findViewById(R.id.recap_activity4);
        recap_activity4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), WLANQuiz.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });



    }
}
