package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;


public class DatasecurityStart extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mytextview_coloredbutton_nextbutton);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("DatasecurityStart"));
        }
        editor.putInt("current", getActivityNumber("DatasecurityStart"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.datasecurity_welcome);



        final Button outcomesButton = findViewById(R.id.outcomes);
        outcomesButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, outcomesButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.outcomes), getString(R.string.datasecurity_learning_outcomes), context);
                }
                return true;
            }
        });

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                Intent myIntent = nextActivity(getActivityNumber("DatasecurityStart") + 1);
                startActivity(myIntent);
                finish();
                return false;
            }
        });
    }

}
