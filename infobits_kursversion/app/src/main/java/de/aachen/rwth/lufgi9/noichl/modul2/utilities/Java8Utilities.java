package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Java8-utilities implemented for all android version
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class Java8Utilities {


    /**
     * Returns the value to which the specified key is mapped, or
     * {@code defaultValue} if this map contains no mapping for the key.
     * <p>
     * The default implementation makes no guarantees about synchronization
     * or atomicity properties of this method. Any implementation providing
     * atomicity guarantees must override this method and document its
     * concurrency properties.
     *
     * @param map          The map to look at
     * @param key          the key whose associated value is to be returned
     * @param defaultValue the default mapping of the key
     * @param <E>          The type of the map values
     * @return the value to which the specified key is mapped, or
     * {@code defaultValue} if this map contains no mapping for the key
     */
    public static <E> E getOrDefault(Map<?, E> map, Object key, E defaultValue) {
        E result;
        return (((result = map.get(key)) != null) || map.containsKey(key))
                ? result
                : defaultValue;
    }

    /**
     * Interface to filter a collection.
     *
     * @param <E> The type for the attributes
     */
    public interface Filter<E> {

        /**
         * Check if the object corresponds to the filter.
         *
         * @param obj The object to check
         * @return <code>true</code>, if the object matches the filter and will be added to the resulting list
         */
        boolean match(E obj);
    }

    /**
     * Filter a collection by a given Filter instance.
     *
     * @param data   The data to filter.
     * @param filter The filter attribute that decides if an entry will be added to the results list or not
     * @param <E>    The type of the collection
     * @return new {@link ArrayList} of type <code>E</code> that contains filtered elements of <code>data</code>
     * @see Filter
     */
    public static <E> List<E> filter(Collection<E> data, Filter<E> filter) {
        List<E> results = new ArrayList<>();
        for (E d : data) {
            if (filter.match(d)) {
                results.add(d);
            }
        }
        return results;
    }

    /**
     * Get the number of elements that are matches a specific condition.
     *
     * @param data   The data to check.
     * @param filter The filter attribute that decides if an entry matches the condition or not
     * @param <E>    The type of the collection
     * @return number of elements in <code>data</code> that matches the condition <code>filter</code>
     * @see Filter
     */
    public static <E> int filterAmount(Collection<E> data, Filter<E> filter) {
        int result = 0;
        for (E d : data) {
            if (filter.match(d)) {
                result++;
            }
        }
        return result;
    }

    /**
     * Check if any value matches a specific condition.
     *
     * @param data   The data to check.
     * @param filter The filter attribute that decides if an entry matches the condition or not
     * @param <E>    The type of the collection
     * @return <code>true</code> if an entry matches the condition, <code>false</code> otherwise.
     * @see Filter
     */
    public static <E> boolean contains(Collection<E> data, Filter<E> filter) {
        boolean result = false;
        for (E d : data) {
            if (filter.match(d)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
