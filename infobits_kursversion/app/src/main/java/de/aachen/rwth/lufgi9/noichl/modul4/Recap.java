package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the end of module 1.
 * This class gives an overview over all texts, videos and excercises of this module and the user can easily jump back to the content he/she wants to repeat.
 * The is also a possibility to watch the results of the pre- and post- "Selbsteinschätzung". If the statsButton is touched the Activity (XYPlotActivity) with the corresponding diagram will be started.
 * If the user press the nextButton the AppStart-Activity will be started.
 * All button touch events will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Recap extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;
    int currentActivity;
    boolean recapMode = false;

    /**
     * Loads the activities layout (R.layout.viewstub_finalpage_module1).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The recap variable is set to false and the AppStart-Activity will be started.
     * <b>statsButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The XYPlotActivity will be started to show the user a diagram.
     * <b>All other buttons:</b>Loggs time and touch-coordinates of users touch. Start the corresponding Activity with the content the user wants to repeat.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recap_modul4);

        context = this;


        if (myrecap){
            recapMode = true;
        }
        myrecap = true;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.recap_text);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setText(R.string.close);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(recapMode){

                }else {
                    myrecap = false;
                }
                finish();
                return false;
            }
        });

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);

        currentActivity = sharedpreferences.getInt("currentActivityM4", 400);





        final Button recap_video1 = findViewById(R.id.recap_video1);
        if (currentActivity < 405){
            recap_video1.setVisibility(View.GONE);
        }
        if (currentActivity == 405 || currentActivity == 406){
            recap_video1.setBackgroundResource(R.drawable.color_button);
        }
        recap_video1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 405 || currentActivity == 406){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), SafePWInfo.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_video2 = findViewById(R.id.recap_video2);
        if (currentActivity < 414){
            recap_video2.setVisibility(View.GONE);
        }
        if (currentActivity == 414){
            recap_video2.setBackgroundResource(R.drawable.color_button);
        }
        recap_video2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 414){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), MechanicsInfo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_video3 = findViewById(R.id.recap_video3);
        if (currentActivity < 418){
            recap_video3.setVisibility(View.GONE);
        }
        if (currentActivity == 418){
            recap_video3.setBackgroundResource(R.drawable.color_button);
        }
        recap_video3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 418){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DataDefinition.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_video4 = findViewById(R.id.recap_video4);
        if (currentActivity < 421){
            recap_video4.setVisibility(View.GONE);
        }
        if (currentActivity == 421){
            recap_video4.setBackgroundResource(R.drawable.color_button);
        }
        recap_video4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 421){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), Datasecurity.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text1 = findViewById(R.id.recap_text1);
        if (currentActivity < 404){
            recap_text1.setVisibility(View.GONE);
        }
        if (currentActivity == 404){
            recap_text1.setBackgroundResource(R.drawable.color_button);
        }
        recap_text1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 404){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DefinitionPW.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text2 = findViewById(R.id.recap_text2);
        if (currentActivity < 419){
            recap_text2.setVisibility(View.GONE);
        }
        if (currentActivity == 419 || currentActivity == 420){
            recap_text2.setBackgroundResource(R.drawable.color_button);
        }
        recap_text2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 419 || currentActivity == 420){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DataMC.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });


        final Button recap_activity1 = findViewById(R.id.recap_activity1);
        if (currentActivity < 403){
            recap_activity1.setVisibility(View.GONE);
        }
        if (currentActivity == 403){
            recap_activity1.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 403){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), PrePassword.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity2 = findViewById(R.id.recap_activity2);
        if (currentActivity < 407){
            recap_activity2.setVisibility(View.GONE);
        }
        if (currentActivity == 407 || currentActivity == 408){
            recap_activity2.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 407 || currentActivity == 408){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), CreatePW.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity3 = findViewById(R.id.recap_activity3);
        if (currentActivity < 409){
            recap_activity3.setVisibility(View.GONE);
        }
        if (currentActivity == 409 || currentActivity == 410){
            recap_activity3.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 409 || currentActivity == 410){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), SafePWCreationStratagy.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity4 = findViewById(R.id.recap_activity4);
        if (currentActivity < 411){
            recap_activity4.setVisibility(View.GONE);
        }
        if (currentActivity == 411){
            recap_activity4.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 411){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), IsPWSafe.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity5 = findViewById(R.id.recap_activity5);
        if (currentActivity < 412){
            recap_activity5.setVisibility(View.GONE);
        }
        if (currentActivity == 412 || currentActivity == 413){
            recap_activity5.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 412 || currentActivity == 413){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), Saftymechanics.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity6 = findViewById(R.id.recap_activity6);
        if (currentActivity < 415){
            recap_activity6.setVisibility(View.GONE);
        }
        if (currentActivity == 415){
            recap_activity6.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 415){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), IsMechanicsSafe.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity7 = findViewById(R.id.recap_activity7);
        if (currentActivity < 416){
            recap_activity7.setVisibility(View.GONE);
        }
        if (currentActivity == 416){
            recap_activity7.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity7));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 416){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), PWmcInfo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity8 = findViewById(R.id.recap_activity8);
        if (currentActivity < 422){
            recap_activity8.setVisibility(View.GONE);
        }
        if (currentActivity == 422){
            recap_activity8.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity8));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 422){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DifferenceDD.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity9 = findViewById(R.id.recap_activity9);
        if (currentActivity < 423){
            recap_activity9.setVisibility(View.GONE);
        }
        if (currentActivity == 423){
            recap_activity9.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity9.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity9));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 423){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DatasecuritySzenarioInfo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });


    }
}
