package de.aachen.rwth.lufgi9.noichl.basics.eingaben;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is for learning how selection of options within a Spinner work. There is one question with a correct answer. The user has to select the correct answers from the list in the Spinner to understand how this works.
 * If the user do not select the correct answer a dialog box is shown saying that the answer is not yet correct. If the user select the correct answer the next Activity (End.class) is started.
 * The selected answer will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Spinner extends MyActivity implements AdapterView.OnItemSelectedListener {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Represents the Spinner with the possible answers.
     * To present the answers the layout R.layout.url_spinner_textview is used.
     */
    android.widget.Spinner spinner;

    /**
     * Loads the activities layout (R.layout.activity_textview_viewstub2_next_button) which uses a viewstub (R.layout.basics_inputs_spinner).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event the answers chosen by the user are logged and evaluated.
     * If the user has given the correct answer the next activity (End.class) will be started, otherwise a dialog will be shown.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_inputs_spinner);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.inputs_spinner_question);

        spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.inputs, R.layout.template_large_text_spinner);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    String item1 = spinner.getSelectedItem().toString();
                    if (item1.equals(getString(R.string.inputs_spinner_answer))) {
                        Intent myIntent = new Intent(context, End.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.try_again), context);
                    }
                }

                return false;
            }
        });


    }

    /**
     * Overrides method onItemSelected for the used Spinner. Loggs the selected Item.
     *
     * @param parent   The current Spinner
     * @param view     The current View
     * @param position The position of the selected Item within the Spinner
     * @param id       The id
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String sSelected = parent.getItemAtPosition(position).toString();
        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), gapEventExtension(parent + "", sSelected));

    }

    /**
     * Overrides method onNothingSelected for the used Spinner. Does nothing here.
     *
     * @param parent The current Spinner
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
