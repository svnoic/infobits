package de.aachen.rwth.lufgi9.noichl.modul3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class ConnectedSort extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        TextView tv = findViewById(R.id.my_textview);
        tv.setText("");

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM3", getActivityNumber("ConnectedSort"));
        }
        editor.putInt("current", getActivityNumber("ConnectedSort"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_usecase_textview_3_radiobutton);
        stub.inflate();



        RadioButton rb_yes = findViewById(R.id.my_true);
        rb_yes.setText(R.string.module3_cable);
        RadioButton rb_no = findViewById(R.id.my_false);
        rb_no.setText(R.string.module3_nocable);
        RadioButton rb_maybe = findViewById(R.id.my_maybe);
        rb_maybe.setText(R.string.module3_both);
        TextView usecasetv = findViewById(R.id.my_usecase_textview);
        usecasetv.setText(R.string.module3_connectedsort_question);



        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    RadioGroup rg = findViewById(R.id.true_false);
                    int selectedId = rg.getCheckedRadioButtonId();
                    if (findViewById(selectedId) != null) {
                        solution();
                        Intent myIntent = nextActivity(getActivityNumber("ConnectedSort") + 1);
                        startActivity(myIntent);
                        finish();
                    }else{
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module3_connectedsort_dialog), context);
                    }
                }
                return false;
            }
        });
    }

    private void solution() {

        RadioGroup rg = findViewById(R.id.true_false);
        int selectedId = rg.getCheckedRadioButtonId();
        RadioButton rb = findViewById(selectedId);
        String givenAnswer = (String) rb.getText();

        myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.module3_connectedsort_question), givenAnswer));




    }

}
