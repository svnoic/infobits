package de.aachen.rwth.lufgi9.noichl.basics.personalisierung;

import android.app.Activity;
import android.content.Intent;


import androidx.appcompat.app.AppCompatActivity;


public class ChangeActivity extends AppCompatActivity {

    public static int currentActivity = 2;
    Intent nextActivity;

    protected void loadActivity(final Activity activity, int activityNumber) {
        // Load the current Activity
        switch (activityNumber) {
            case 2:
                nextActivity = new Intent(activity, TextSize.class);
                break;
            case 8:
                nextActivity = new Intent(activity, Volume.class);
                break;
            case 10:
                nextActivity = new Intent(activity, PushPoints.class);
                break;
            case 9:
            case 13:
                nextActivity = new Intent(activity, Instruction.class);
                break;
            default:
                nextActivity = new Intent(activity, PersonalisationEnd.class);
                break;
        }

        activity.startActivity(nextActivity);
    }

    protected void loadNextActivity(final Activity activity) {
        // Load the next Activity. Increase current Activity.
        if(currentActivity == 2){
            currentActivity += 6;
        }else {
            if (currentActivity == 10) {
                currentActivity += 5;
            } else {
                currentActivity += 1;
            }
        }
        loadActivity(activity, currentActivity);
    }


    public int getCurrentActivity() {
        return currentActivity;
    }
}


/*
-2: Data not gathered, Don't ask again
-1: Data not gathered, Ask next time
0: Data gathered and can be used
1: ExplanatoryScreen
2: TextSize
3: Instruction
4: ShowPattern
5: Instruction
6: Colour
7: Instruction
8: Volume
9: Instruction
10: PushPoints
11: Instruction
12: TestPattern
13: Ending-Instruction
 */