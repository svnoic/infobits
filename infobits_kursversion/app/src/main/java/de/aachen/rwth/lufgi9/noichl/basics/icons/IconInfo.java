package de.aachen.rwth.lufgi9.noichl.basics.icons;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the entry point of the inputs module.
 * This class is to inform the user about what the icons module is for.
 * If the user press the nextButton the next activity (SelectIcon.class) will be started.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class IconInfo extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (R.layout.activity_textview_next_button).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinated of users touch.
     * Any touch event on this button will trigger the start of SelectIcon-Activity.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.icons_info_text);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                Intent myIntent = new Intent(context, SelectIcon.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });
    }
}
