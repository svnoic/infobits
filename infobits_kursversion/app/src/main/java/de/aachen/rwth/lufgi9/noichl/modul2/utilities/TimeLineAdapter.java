package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.github.vipulasri.timelineview.TimelineView;





import java.util.List;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.modul2.History;

/**
 * Adapter for a timeline. You can add information for timeline data by adding a date, title and description
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineViewHolder> {

    /**
     * The holder for a timeline entry
     */
    class TimeLineViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * {@link View} for the timeline part left of the item
         */
        private final TimelineView timelineView;
        /**
         * {@link View} for the title of an entry
         */
        private final TextView     textViewTitle;
        /**
         * {@link View} for the description of a timeline entry
         */
        private final TextView     textViewDescription;
        /**
         * {@link View} that contains the description and a button to read further information if there is some
         */
        private final LinearLayout linearLayoutExtendable;
        /**
         * {@link View} for the time of a timeline entry
         */
        private final TextView     textViewTime;
        /**
         * {@link View} for the expand icon
         */
        private final ImageView    imageViewExpand;
        /**
         * {@link View} for the text of the expand icon
         */
        private final TextView     textViewExpandText;
        /**
         * {@link Button} for get more information if available
         */
        private final Button       buttonFurtherInformation;

        /**
         * Normal constructor.
         *
         * @param itemView the view for a timeline item
         * @param viewType the type of the timeline
         * @see TimelineView#initLine(int)
         */
        public TimeLineViewHolder(View itemView, int viewType) {
            super(itemView);

            // install the local views
            timelineView = itemView.findViewById(R.id.item_timeline_view);
            textViewTitle = itemView.findViewById(R.id.item_timeline_title);
            textViewDescription = itemView.findViewById(R.id.item_timeline_description);
            linearLayoutExtendable = itemView.findViewById(R.id.item_timeline_expandable_view);
            textViewTime = itemView.findViewById(R.id.item_timeline_time);
            imageViewExpand = itemView.findViewById(R.id.item_timeline_expand);
            textViewExpandText = itemView.findViewById(R.id.item_timeline_expandText);
            buttonFurtherInformation = itemView.findViewById(R.id.item_timeline_furtherInformation);

            // add the listener
            buttonFurtherInformation.setOnClickListener(this);

            // set up the timeline line type
            timelineView.initLine(viewType);

            // install the listener for open or collapse information
            itemView.findViewById(R.id.item_timeline_card).setOnClickListener(this);

            // do not show the button as default
            buttonFurtherInformation.setVisibility(View.GONE);
            // make the information not visible on default
            linearLayoutExtendable.setVisibility(View.GONE);
            // use default text
            textViewExpandText.setText("Mehr");
            // get the context to access the drawable for the expandable view
            Context context = itemView.getContext();
            // load the drawable
            Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_expand_more_black_24dp, context.getTheme());
            // update the drawable
            imageViewExpand.setImageDrawable(drawable);
        }

        @Override
        public void onClick(final View v) {
            if (v.getId() == R.id.item_timeline_furtherInformation) {
                // get the item by the position
                TimeLineModel item = data.get(getAdapterPosition());
                // show more information as a dialog
                //DialogUtilities.showSimpleOKDialog(v.getContext(), "Weitere Informationen", item.getFurtherInformation());
            } else {
                // define the new visibility state
                final int visibility = linearLayoutExtendable.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE;
                // also for animation effect, set alpha
                float alpha = linearLayoutExtendable.getVisibility() == View.VISIBLE ? 0 : 1;
                // height of the content are animated, too
                float scaleY = linearLayoutExtendable.getVisibility() == View.VISIBLE ? 0 : 1;
                // define the id of the new drawable
                int drawableId = linearLayoutExtendable.getVisibility() == View.VISIBLE ? R.drawable.ic_expand_more_black_24dp : R.drawable.ic_expand_less_black_24dp;
                // define the id of the text of the expendable text view
                int textId = linearLayoutExtendable.getVisibility() == View.VISIBLE ? R.string.more : R.string.less;

                // change visibility state
                linearLayoutExtendable.animate().alpha(alpha).setDuration(500).scaleY(scaleY).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (visibility == View.GONE) {
                            linearLayoutExtendable.setVisibility(visibility);
                        } else {
                            // the element at the position
                            int position = getAdapterPosition();
                            TimeLineModel obj = data.get(position);
                            timelineView.setMarker(drawableConverter.getDrawable(obj, obj.getState(), v.getContext()));
                        }
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        if (visibility == View.VISIBLE) {
                            linearLayoutExtendable.setVisibility(visibility);
                        }
                    }
                }).start();

                // log that the user opened the entry, if visibility will switch to visible
                if (visibility == View.VISIBLE) {

                    // update the state read
                    int position = getAdapterPosition();
                    // the element at the position
                    TimeLineModel obj = data.get(position);
                    // update the object
                    obj.setState(States.addState(obj.getState(), History.HistoryState.READ.getId()));
                    // store in database as a log entry
                    //Log.i("LOG", v.getContext() +", " +this+", " + "sessionContent" + ", "+ getMessageForLog(obj.getId(), History.HistoryState.READ.getId()));
                    //DefaultAppDatabase.getInstance(v.getContext()).systemInfoDao().add(new SystemInfo(App.getUserId(), "sessionContent", getMessageForLog(obj.getId(), History.HistoryState.READ.getId())));
                }

                // get the context to access the drawable for the expandable view
                Context context = v.getContext();
                // load the drawable
                Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), drawableId, context.getTheme());
                // update the drawable
                imageViewExpand.setImageDrawable(drawable);
                // update text
                textViewExpandText.setText(textId);
            }
        }
    }

    /**
     * Use this method to get the message for the {@link TimeLineModel} object.
     * For example, to store in database and to read from database.
     *
     * @param id    The id of the entry to create a message for.
     * @param state The state of the message.
     * @return Message string for the object.
     */
    public static String getMessageForLog(String id, String state) {
        return "timeLineAdapter,readTopic:'" + id + "',state:'" + state + "'";
    }

    /**
     * Interface for get a drawable for a specific state.
     */
    public interface StateToDrawable {
        /**
         * Convert a state to a drawable for the timeline entry
         *
         * @param object  The object of the <code>state</code>
         * @param state   The state of a timeline entry
         * @param context The context can be used for access resources.
         * @return {@link Drawable} for show in the timeline entry
         */
        Drawable getDrawable(TimeLineModel object, String state, Context context);
    }

    /**
     * Default implementation of a {@link StateToDrawable}.
     * A mapping from string to the id of and drawable has to be done nevertheless.
     *
     * @see StateToDrawable
     */
    public abstract static class DefaultStateToDrawable implements StateToDrawable {
        /**
         * Map a string to an id of a drawable for a timeline entry
         *
         * @param object The object of the <code>state</code>
         * @param state  The state of a timeline entry
         * @return id of a drawable resource
         * @see StateToDrawable
         * @see StateToDrawable#getDrawable(TimeLineModel, String, Context)
         */
        public abstract int getDrawableId(TimeLineModel object, String state);

        @Override
        public Drawable getDrawable(TimeLineModel object, String state, Context context) {
            // get the id of the drawable
            int drawableId = getDrawableId(object, state);

            // load the drawable depending on the android version
            return ResourcesCompat.getDrawable(context.getResources(), drawableId, context.getTheme());
        }
    }

    /**
     * The dataSet for the timeline adapter with all entries.
     */
    private final List<TimeLineModel> data;
    /**
     * Converts a state to a drawable
     */
    private final StateToDrawable     drawableConverter;

    /**
     * Normal constructor for the timeline adapter
     *
     * @param data              The dataSet for the timeline adapter
     * @param drawableConverter A converter to get a drawable by given state. A {@link Context} parameter is given
     */
    public TimeLineAdapter(List<TimeLineModel> data, StateToDrawable drawableConverter) {
        this.data = data;
        this.drawableConverter = drawableConverter;
    }

    @NonNull
    @Override
    public TimeLineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_timeline_item, parent, false);
        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeLineViewHolder holder, int position) {
        // the item at the position
        TimeLineModel item = data.get(position);

        // update texts
        holder.textViewTitle.setText(item.getTitle());
        holder.textViewDescription.setText(item.getDescription());
        holder.textViewTime.setText(item.getTime());

        // show or hide the more information button if there are more information
        holder.buttonFurtherInformation.setVisibility(item.getFurtherInformation() != null && !item.getFurtherInformation().isEmpty() ? View.VISIBLE : View.GONE);

        // get the context for access resources by using a view
        Context context = holder.textViewTitle.getContext();

        // update the marker drawable
        holder.timelineView.setMarker(drawableConverter.getDrawable(item, item.getState(), context));
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}