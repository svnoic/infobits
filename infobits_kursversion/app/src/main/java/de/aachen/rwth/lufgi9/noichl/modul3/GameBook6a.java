package de.aachen.rwth.lufgi9.noichl.modul3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class GameBook6a extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.module3_gamebook_6a_text);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (recap) {
                        Intent myIntent = new Intent(context, ConnectionEnd.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        if (myrecap) {
                            restartAfterRecap();
                        } else {
                            Intent myIntent = nextActivity(getActivityNumber("GameBookIntro") + 1);
                            startActivity(myIntent);
                            finish();
                        }
                    }
                }
                return false;
            }
        });
    }
}
