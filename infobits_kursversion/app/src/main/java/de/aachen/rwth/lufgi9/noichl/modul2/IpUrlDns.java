package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.google.android.flexbox.FlexboxLayout;
import com.google.android.flexbox.FlexboxLayout.LayoutParams;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class IpUrlDns extends MyActivity implements View.OnDragListener, View.OnTouchListener {


    private static boolean solution = false;
    ArrayList<String> answersChecked = new ArrayList<>();
    Context context;


    @SuppressLint({"ResourceType", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM2", getActivityNumber("IpUrlDns"));
        }
        editor.putInt("current", getActivityNumber("IpUrlDns"));
        editor.apply();
        saveSharedPrefs();

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_sort_3_category);
        stub.inflate();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.module2_ipurldns_info);

        TextView left = findViewById(R.id.left);
        left.setText(R.string.module2_ip);

        TextView middle = findViewById(R.id.middle);
        middle.setText(R.string.module2_url);

        TextView right = findViewById(R.id.right);
        right.setText(R.string.module2_domain);

        ArrayList<String> answers = new ArrayList<>();
        answers.add(getString(R.string.module2_ipurldns_item1));
        answers.add(getString(R.string.module2_ipurldns_item2));
        answers.add(getString(R.string.module2_ipurldns_item3));
        answers.add(getString(R.string.module2_ipurldns_item4));
        answers.add(getString(R.string.module2_ipurldns_item5));
        answers.add(getString(R.string.module2_ipurldns_item6));

        View linearLayout = findViewById(R.id.flex_layout);

        answersChecked = answers;

        for (int i = 0; i < answersChecked.size(); i++) {
            LinearLayout mylinearLayout = new LinearLayout(this);
            mylinearLayout.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
            mylinearLayout.setPadding(8, 8, 8, 8);
            TextView valueTV = new TextView(this);
            valueTV.setText(answersChecked.get(i));
            int[] array = new int[2];
            valueTV.getLocationOnScreen(array);
            int w = valueTV.getWidth();
            int h = valueTV.getHeight();
            myLog("testuser",this.getClass()+"/displayed", "displayed", this.getClass().toString(), textExtension(answersChecked.get(i)));
            myLog("testuser", this.getClass()+"/created", "created", this.getClass().toString(), "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+ ", \"displaymetrics\": { \"element\": \"" + valueTV + "\", \"x1\": \"" + array[0] + "\", \"y1\": \"" + array[1]+ "\", \"x2\": \"" + (array[0]+w) + "\", \"y2\": \"" + (array[1]+h) + "}");

            valueTV.setTextColor(Color.BLACK);
            valueTV.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sortbox, null));
            valueTV.setId(i);
            mylinearLayout.setTag("l" + valueTV.getId());
            valueTV.setTag(i + "");
            valueTV.setPadding(8, 8, 8, 8);
            valueTV.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

            mylinearLayout.addView(valueTV);

            valueTV.setOnTouchListener(this);

            ((LinearLayout) linearLayout).addView(mylinearLayout);
        }


        implementEvents();

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (!solution) {
                            ViewGroup vg = findViewById(R.id.flex_layout);
                            if (vg.getChildCount() == 0) {
                                solution();
                                solution = true;
                            }else{
                                showDialog(getString(R.string.not_yet_ready), getString(R.string.module2_ipurldns_dialog), context);
                            }
                        } else {
                            solution = false;
                            if(recap){
                                Intent myIntent = new Intent(context, InternetEnd.class);
                                startActivity(myIntent);
                                finish();
                            }else {
                                if(myrecap){
                                    restartAfterRecap();
                                }else {
                                    Intent myIntent = nextActivity(getActivityNumber("IpUrlDns") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                            }
                        }

                }
                return false;
            }

        });
    }


    private void solution() {

        for (int i = 0; i < answersChecked.size(); i++) {

            ViewGroup parent1 = findViewById(R.id.left_layout);
            parent1.setTag(getString(R.string.module2_ip));
            ViewGroup parent2 = findViewById(R.id.middle_layout);
            parent2.setTag(getString(R.string.module2_url));
            ViewGroup parent3 = findViewById(R.id.right_layout);
            parent3.setTag(getString(R.string.module2_domain));

            TextView myview0 = findViewById(i);
            myview0.setEnabled(false);

            if (i == 1 || i == 4) {
                if (myview0.getParent().getParent() == parent1) {
                    myview0.setBackgroundResource(R.drawable.answer_true);
                    myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), true, parent1.getTag().toString(), parent1.getTag().toString()));
                } else {
                    myview0.setBackgroundResource(R.drawable.answer_false);
                    if(myview0.getParent().getParent() == parent2){
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent2.getTag().toString(), parent1.getTag().toString()));
                    }else{
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent3.getTag().toString(), parent1.getTag().toString()));
                    }
                }
            }
            if (i == 2 || i == 3) {
                if (myview0.getParent().getParent() == parent3) {
                    myview0.setBackgroundResource(R.drawable.answer_true);
                    myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), true, parent3.getTag().toString(), parent3.getTag().toString()));
                } else {
                    myview0.setBackgroundResource(R.drawable.answer_false);
                    if(myview0.getParent().getParent() == parent2){
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent2.getTag().toString(), parent3.getTag().toString()));
                    }else{
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent1.getTag().toString(), parent3.getTag().toString()));
                    }
                }
            }
            if (i == 0 || i == 5) {
                if (myview0.getParent().getParent() == parent2) {
                    myview0.setBackgroundResource(R.drawable.answer_true);
                    myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), true, parent2.getTag().toString(), parent2.getTag().toString() ));
                    }else {
                        myview0.setBackgroundResource(R.drawable.answer_false);
                        if (myview0.getParent().getParent() == parent1) {
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent1.getTag().toString(), parent2.getTag().toString()));
                        } else {
                            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent3.getTag().toString(), parent2.getTag().toString()));
                        }
                    }
            }
        }
    }


    private void implementEvents() {

        findViewById(R.id.left_layout).setOnDragListener(this);
        findViewById(R.id.middle_layout).setOnDragListener(this);
        findViewById(R.id.right_layout).setOnDragListener(this);

    }


    @Override
    public boolean onDrag(View view, DragEvent event) {
        View v = (View) event.getLocalState();
        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), dragEventExtension(event, view, ((TextView)v).getText().toString()));

        int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:

                return event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);

            case DragEvent.ACTION_DRAG_ENTERED:

                view.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                view.invalidate();

                return true;
            case DragEvent.ACTION_DRAG_LOCATION:

                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                view.getBackground().clearColorFilter();
                view.invalidate();

                return true;

            case DragEvent.ACTION_DROP:
                view.getBackground().clearColorFilter();
                view.invalidate();

                ViewGroup owner = (ViewGroup) v.getParent();
                owner.removeView(v);//remove the dragged view
                ViewGroup root = (ViewGroup) owner.getParent();
                if (view instanceof FlexboxLayout) {
                    FlexboxLayout container = (FlexboxLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                    LinearLayout rm = owner.findViewWithTag("l" + v.getId());
                    root.removeView(rm);
                    LinearLayout l = new LinearLayout(this);
                    l.setPadding(8, 8, 8, 8);
                    l.setTag("l" + v.getId());
                    l.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
                    l.addView(v);
                    container.addView(l);//Add the dragged view
                } else {
                    LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                    LinearLayout rm = owner.findViewWithTag("l" + v.getId());
                    root.removeView(rm);
                    LinearLayout l = new LinearLayout(this);
                    l.setPadding(8, 8, 8, 8);
                    l.setTag("l" + v.getId());
                    l.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
                    l.addView(v);
                    container.addView(l);//Add the dragged view
                }


                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                view.getBackground().clearColorFilter();
                view.invalidate();
                view = (View) event.getLocalState();
                view.setVisibility(View.VISIBLE);
                return true;
        }
        return false;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), viewEventExtension(motionEvent, view));
        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());


        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

        view.startDrag(data, shadowBuilder, view, 0);

        view.setVisibility(View.INVISIBLE);

        return true;
    }
}
