package de.aachen.rwth.lufgi9.noichl.modul3;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class PreTest extends MyActivity {

    private static int questionCounter = 1;
    private static int jump = 0;
    Context context;
    JSONArray myJson = null;
    JSONObject rec = null;
    public static int number = 0;
    public static  ArrayList<Number> answers = new ArrayList<Number>(Arrays.asList(0,0,0,0,0,0,0,0));

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_6_radiobutton);
        stub.inflate();

        String json = null;
        try {
            InputStream is = this.getAssets().open("pretestConnection.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (myJson != null) {
            for (int i = 0; i < myJson.length(); ++i) {
                try {
                    rec = myJson.getJSONObject(i);
                    Log.e("rec", rec + "");
                    String id = rec.getString("id");
                    String question = "pre_connection_item" + questionCounter;
                    if (id.equals(question)) {
                        jump = rec.getInt("jump");
                        TextView likertQuestion = findViewById(R.id.my_textview);
                        RadioButton likert1 = findViewById(R.id.likert1);
                        RadioButton likert2 = findViewById(R.id.likert2);
                        RadioButton likert3 = findViewById(R.id.likert3);
                        RadioButton likert4 = findViewById(R.id.likert4);
                        RadioButton likert5 = findViewById(R.id.likert5);
                        RadioButton likert6 = findViewById(R.id.likert6);
                        likertQuestion.setText(rec.getString("description"));
                        likert1.setText(R.string.likert1);
                        likert2.setText(R.string.likert2);
                        likert3.setText(R.string.likert3);
                        likert4.setText(R.string.likert4);
                        likert5.setText(R.string.likert5);
                        likert6.setText(R.string.likert6);

                        number = i;

                        final RadioGroup radioGroup = findViewById(R.id.radioGroup);
                        for (int j = 0; j < radioGroup.getChildCount(); j++) {
                            final Button rb = (Button) radioGroup.getChildAt(j);
                            rb.setOnTouchListener(new View.OnTouchListener() {
                                public boolean onTouch(View v, MotionEvent event) {
                                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, rb));
                                    return false;
                                }
                            });
                        }

                        final Button next = findViewById(R.id.next);
                        next.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, next));
                                try {
                                    if(event.getAction() == MotionEvent.ACTION_UP){nextListener(rec.getString("id"), rec.getString("description"));}
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }
                        });
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }



        private void nextListener(String id, String description) {
            RadioGroup radioGroup = findViewById(R.id.radioGroup);
            int index = radioGroup.indexOfChild(findViewById(radioGroup.getCheckedRadioButtonId()));
            int lastScore = 6 - index;

            myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(id, description, String.valueOf(lastScore)));
            answers.set(number, lastScore);
            SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putInt("m3_pre_"+number, lastScore);
            editor.apply();


            if (questionCounter <= myJson.length()-1 && lastScore < 7) {
                if (jump != 0 && lastScore < 4){
                    questionCounter = questionCounter + jump;
                }
                    questionCounter++;

                Intent myIntent = new Intent(context, PreTest.class);
                startActivity(myIntent);
                finish();
            } else {
                if (lastScore < 7) {
                    questionCounter++;
//                    try {
//                        readSharedPrefs();
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
                    saveSharedPrefs();
                    Intent myIntent = nextActivity(getActivityNumber("PretestInformation3") + 1);
                    startActivity(myIntent);
                    questionCounter = 1;
                    finish();
                }else {
                    if(lastScore == 7){

                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_pretest_dialog), context);
                    }
                }
            }
        }


}
