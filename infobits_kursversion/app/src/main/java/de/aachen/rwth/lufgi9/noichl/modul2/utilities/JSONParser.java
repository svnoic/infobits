package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.IOException;

import de.aachen.rwth.lufgi9.noichl.modul2.utilities.TimeLineModel;

/**
 * This class is used for handle json files. The containing methods creates objects of receiving json data
 * and create a json string from a given object.
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class JSONParser {

    /**
     * Takes a JSON string and converts it to an element of the given <code>eClass</code>.
     *
     * @param json   The JSON string
     * @param eClass The class of the result type
     * @param <E>    The type of the result
     * @return object of type <code>E</code>
     */
    public static <E> E toObject(String json, Class<E> eClass) {
        try {
            return new ObjectMapper().readValue(json, eClass);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Takes a JSON string and converts it to an array of TimeLineModel
     *
     * @param json The JSON string
     * @return An array of TimeLineModel objects
     */
    public static TimeLineModel[] jsonToTimeLineModel(String json) {
        return toObject(json, TimeLineModel[].class);
    }

//    /**
//     * Takes a JSON string and converts it to an array of ExerciseModel
//     *
//     * @param json The JSON string
//     * @return An array of ExerciseModel objects
//     */
//    public static ExerciseModel[] jsonToExerciseModel(String json) {
//        return toObject(json, ExerciseModel[].class);
//    }
//
//    /**
//     * Takes a JSON string and converts it to an array of EvaluationModel
//     *
//     * @param json The JSON string
//     * @return An array of EvaluationModel objects
//     */
//    public static EvaluationModel[] jsonToEvaluationModel(String json) {
//        return toObject(json, EvaluationModel[].class);
//    }
//
//    /**
//     * Takes a JSON string and converts it to a WebViewModel
//     *
//     * @param json The JSON string
//     * @return A WebViewModel object
//     */
//    public static WebViewModel jsonToWebViewModel(String json) {
//        return toObject(json, WebViewModel.class);
//    }
//
//    /**
//     * Takes a JSON string and converts it to a HTMLSlideModel
//     *
//     * @param json The JSON string
//     * @return A HTMLSlideModel object
//     */
//    public static HTMLSlideModel jsonToHTMLSlideModel(String json) {
//        return toObject(json, HTMLSlideModel.class);
//    }

    /**
     * @param obj the object that will be parsed to a json string
     * @return json string of the object
     */
    public static String toJson(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
