package de.aachen.rwth.lufgi9.noichl.modul4;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class PosttestInformation extends MyActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("PosttestInformation4"));
        }
        editor.putInt("current", getActivityNumber("PosttestInformation4"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText("Erinnerst du dich noch an die Aussagen, die du ganz am Anfang dieses Moduls bewertet hast? Wie angekündigt kommen diese jetzt noch einmal. Bitte gib eine neue Einschätzung ab.");

        final Button nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                Intent myIntent = new Intent(context,   PostTest.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });
    }
}
