package de.aachen.rwth.lufgi9.noichl.basics.eingaben;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;


/**
 * This class is for learning how CheckBoxes work. There is one question with two correct answers. The user has to tick both correct answers to understand that multiple selection is possible.
 * If the user chose not both correct answers a dialog box is shown saying that the answers are not yet correct. If the user chose both correct answers the next Activity (Spinner.class) is started.
 * The selected answers will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Check extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (R.layout.activity_textview_viewstub2_next_button) which uses a viewstub (R.layout.basics_inputs_multiplechoice).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event the answers chosen by the user are logged and evaluated.
     * If the user has given the two correct answers the next activity (Spinner.class) will be started, otherwise a dialog will be shown.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_inputs_multiplechoice);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.foundation_check_question);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    CheckBox cb1 = findViewById(R.id.checkBox);
                    CheckBox cb2 = findViewById(R.id.checkBox2);
                    CheckBox cb3 = findViewById(R.id.checkBox3);
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("An welchen Tagen feiern wir Weihnachten?", cb1.getText().toString(), cb1.isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("An welchen Tagen feiern wir Weihnachten?", cb2.getText().toString(), cb2.isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("An welchen Tagen feiern wir Weihnachten?", cb3.getText().toString(), cb3.isChecked() + ""));


                    if (cb1.isChecked() && cb2.isChecked() && !cb3.isChecked()) {
                        Intent myIntent = new Intent(context, Spinner.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.try_again), context);
                    }
                }

                return false;
            }
        });


    }


}
