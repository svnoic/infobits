package de.aachen.rwth.lufgi9.noichl.modul1;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class PolicyTrueFalse extends MyActivity {

    private static int correct = 0;
    private static int counter = 0;
    Context context;
    JSONArray myJson = null;
    String correctAnswer = "";
    String scenario;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.policy_true_false);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("PolicyTrueFalse"));
        }
        editor.putInt("current", getActivityNumber("PolicyTrueFalse"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_usecase_textview_2_radiobutton);
        stub.inflate();


        String json = null;
        try {
            InputStream is = this.getAssets().open("policyTrueFalse.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (myJson != null) {
            JSONObject rec;
            try {
                rec = myJson.getJSONObject((int) (Math.random() * 4));
                scenario = rec.getString("description");
                TextView usecase_tv = findViewById(R.id.my_usecase_textview);
                usecase_tv.setText(scenario);
                correctAnswer = rec.getString("correct");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    RadioGroup rg = findViewById(R.id.true_false);
                    int selectedId = rg.getCheckedRadioButtonId();
                    if (findViewById(selectedId) != null) {
                        solution();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_policy_true_false_dialog), context);
                    }
                }
                return false;
            }
        });
    }

    private void solution() {

        RadioGroup rg = findViewById(R.id.true_false);
        int selectedId = rg.getCheckedRadioButtonId();
        RadioButton rb = findViewById(selectedId);
        String givenAnswer = (String) rb.getText();

        myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(scenario, givenAnswer.equals(correctAnswer), givenAnswer, correctAnswer));


        if (givenAnswer.equals(correctAnswer)) {
            showDialog(getString(R.string.very_good), getString(R.string.selection_correct));
            correct = correct + 1;
        } else {
            showDialog(getString(R.string.sadly_wrong), getString(R.string.selection_wrong));
        }

    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        myLog("testuser", this.getClass() + "/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));


        // set title
        TextView mytitle = new TextView(context);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent myIntent;
                        if (counter < 1) {
                            myIntent = new Intent(context, PolicyTrueFalse.class);
                            startActivity(myIntent);
                            finish();
                            counter++;
                        } else {
                            counter = 0;
                            correct = 0;
                            if (recap) {
                                myIntent = new Intent(context, CommunicationEnd.class);
                                startActivity(myIntent);
                                finish();
                            } else {
                                if(myrecap){

                                    restartAfterRecap();
                                }else {
                                    myIntent = nextActivity(getActivityNumber("PolicyTrueFalse") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                            }
                        }
                        myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));


                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

        TextView textView = alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }
}
