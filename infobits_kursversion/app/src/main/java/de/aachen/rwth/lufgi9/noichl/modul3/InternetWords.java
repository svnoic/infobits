package de.aachen.rwth.lufgi9.noichl.modul3;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class InternetWords extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.internet_words);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM3", getActivityNumber("InternetWords "));
        }
        editor.putInt("current", getActivityNumber("InternetWords "));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.module3_internetword_info);

        final Button nextButton = findViewById(R.id.next);

        nextButton.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                    if(event.getAction() == MotionEvent.ACTION_UP) {

                        SharedPreferences Preference = getSharedPreferences("internetwords", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = Preference.edit();
                        editor.putBoolean("1", ((CheckBox) findViewById(R.id.w1)).isChecked());
                        editor.putBoolean("2", ((CheckBox) findViewById(R.id.w2)).isChecked());
                        editor.putBoolean("3", ((CheckBox) findViewById(R.id.w3)).isChecked());
                        editor.putBoolean("4", ((CheckBox) findViewById(R.id.w4)).isChecked());
                        editor.putBoolean("5", ((CheckBox) findViewById(R.id.w5)).isChecked());
                        editor.putBoolean("6", ((CheckBox) findViewById(R.id.w6)).isChecked());
                        editor.putBoolean("7", ((CheckBox) findViewById(R.id.w7)).isChecked());
                        editor.putBoolean("8", ((CheckBox) findViewById(R.id.w8)).isChecked());
                        editor.putBoolean("9", ((CheckBox) findViewById(R.id.w9)).isChecked());
                        editor.putBoolean("10", ((CheckBox) findViewById(R.id.w10)).isChecked());

                        editor.apply();

                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.lan), ((CheckBox) findViewById(R.id.w1)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.gsm), ((CheckBox) findViewById(R.id.w2)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("3", getString(R.string.man), ((CheckBox) findViewById(R.id.w3)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("4", getString(R.string.umts), ((CheckBox) findViewById(R.id.w4)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.wan), ((CheckBox) findViewById(R.id.w5)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.lte), ((CheckBox) findViewById(R.id.w6)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("3", getString(R.string.wlan), ((CheckBox) findViewById(R.id.w7)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("4", getString(R.string.fourg), ((CheckBox) findViewById(R.id.w8)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.wifi), ((CheckBox) findViewById(R.id.w9)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.mobiledata), ((CheckBox) findViewById(R.id.w10)).isChecked()+""));

                        Intent myIntent = nextActivity(getActivityNumber("InternetWords ") + 1);
                        startActivity(myIntent);
                        finish();
                    }
                    return false;
            }
        });
    }



}
