package de.aachen.rwth.lufgi9.noichl.basics.icons;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * In this class different descriptions and six possible icons are given. The user can than choose the icon he/she prefers most for the given description.
 * If the user chose an icon an press the corresponding imageButton either this activity will be started again to show the next description or the next activity (OverviewIcon.class) will be started.
 * The order of the six icons is randomized by using a shuffleList.
 * All gestures and selected icons will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class SelectIcon extends MyActivity {

    /**
     * Represents the number how often this activity was started. Depending on that number the corresponding description and icons will be shown.
     */
    public static int number = 0;

    /**
     * Represents the list of six icons for the current description.
     */
    public static List icons = new ArrayList();

    /**
     * Represents the randomized order of the icons for the current description.
     */
    public static List shuffleList = new ArrayList();

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Represents the ImageButtons for the six icons.
     */
    ImageButton i1, i2, i3, i4, i5, i6;

    /**
     * Represents the current icon.
     */
    JSONObject icon = null;

    /**
     * Loads the activities layout (basics_icons_chooseicon) and sets the current icons.
     * <b>i1,i2,i3,i4,i5,i6-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event the selected icon will be saved and either the next activity or this activity will be started depending on the <b>number</b> variable.
     * If number is <b>>=9</b> the next activity (OvervieIcon.class) will be started, otherwise this activity will be started again and number will be increased by one.
     *
     * @param savedInstanceState Android-Parameter
     */

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.basics_icons_chooseicon);
        context = this;


        JSONObject addPerson = new JSONObject();
        try {
            addPerson.put("description", context.getString(R.string.addPerson));
            addPerson.put("1", R.mipmap.ic_person_add_black_48dp);
            addPerson.put("2", R.mipmap.ic_group_add_black_48dp);
            addPerson.put("3", R.mipmap.ic_people_black_48dp);
            addPerson.put("4", R.mipmap.ic_person_black_48dp);
            addPerson.put("5", R.mipmap.ic_account_box_black_48dp);
            addPerson.put("6", R.mipmap.ic_contact_phone_black_48dp);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        JSONObject specificPlace = new JSONObject();
        try {
            specificPlace.put("description", context.getString(R.string.specificPlace));
            specificPlace.put("1", R.mipmap.ic_place_black_48dp);
            specificPlace.put("2", R.mipmap.ic_arrow_upward_black_48dp);
            specificPlace.put("3", R.mipmap.ic_all_out_black_48dp);
            specificPlace.put("4", R.mipmap.ic_home_black_48dp);
            specificPlace.put("5", R.mipmap.ic_near_me_black_48dp);
            specificPlace.put("6", R.mipmap.ic_phonelink_setup_black_48dp);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        JSONObject navigation = new JSONObject();
        try {
            navigation.put("description", context.getString(R.string.navigation));
            navigation.put("1", R.mipmap.navigation1);
            navigation.put("2", R.mipmap.navigation2);
            navigation.put("3", R.mipmap.navigation3);
            navigation.put("4", R.mipmap.navigation4);
            navigation.put("5", R.mipmap.navigation5);
            navigation.put("6", R.mipmap.navigation6);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        JSONObject alarm = new JSONObject();
        try {
            alarm.put("description", context.getString(R.string.alarm));
            alarm.put("1", R.mipmap.alarm1);
            alarm.put("2", R.mipmap.alarm2);
            alarm.put("3", R.mipmap.alarm3);
            alarm.put("4", R.mipmap.alarm4);
            alarm.put("5", R.mipmap.alarm5);
            alarm.put("6", R.mipmap.alarm6);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        JSONObject rotate = new JSONObject();
        try {
            rotate.put("description", context.getString(R.string.rotate));
            rotate.put("1", R.mipmap.ic_screen_rotation_black_48dp);
            rotate.put("2", R.mipmap.ic_sync_black_48dp);
            rotate.put("3", R.mipmap.ic_refresh_black_48dp);
            rotate.put("4", R.mipmap.ic_tablet_black_48dp);
            rotate.put("5", R.mipmap.ic_screen_lock_portrait_black_48dp);
            rotate.put("6", R.mipmap.ic_link_black_48dp);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        JSONObject send = new JSONObject();
        try {
            send.put("description", context.getString(R.string.send));
            send.put("1", R.mipmap.send1);
            send.put("2", R.mipmap.send2);
            send.put("3", R.mipmap.send3);
            send.put("4", R.mipmap.send4);
            send.put("5", R.mipmap.send5);
            send.put("6", R.mipmap.send6);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        JSONObject missedCall = new JSONObject();
        try {
            missedCall.put("description", context.getString(R.string.missedCall));
            missedCall.put("1", R.mipmap.missed1);
            missedCall.put("2", R.mipmap.missed2);
            missedCall.put("3", R.mipmap.missed3);
            missedCall.put("4", R.mipmap.missed4);
            missedCall.put("5", R.mipmap.missed5);
            missedCall.put("6", R.mipmap.missed6);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        JSONObject currentPosition = new JSONObject();
        try {
            currentPosition.put("description", context.getString(R.string.currentPosition));
            currentPosition.put("1", R.mipmap.place1);
            currentPosition.put("2", R.mipmap.place2);
            currentPosition.put("3", R.mipmap.place3);
            currentPosition.put("4", R.mipmap.place4);
            currentPosition.put("5", R.mipmap.place5);
            currentPosition.put("6", R.mipmap.place6);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        JSONObject share = new JSONObject();
        try {
            share.put("description", context.getString(R.string.share));
            share.put("1", R.mipmap.share1);
            share.put("2", R.mipmap.share2);
            share.put("3", R.mipmap.share3);
            share.put("4", R.mipmap.share4);
            share.put("5", R.mipmap.share5);
            share.put("6", R.mipmap.share6);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        shuffleList = Arrays.asList("1", "2", "3", "4", "5", "6");
        Log.d("shuffleList", shuffleList.toString());
        Collections.shuffle(shuffleList);
        Log.d("shuffleList", shuffleList.toString());


        i1 = findViewById(R.id.imageButton1);
        i2 = findViewById(R.id.imageButton2);
        i3 = findViewById(R.id.imageButton3);
        i4 = findViewById(R.id.imageButton4);
        i5 = findViewById(R.id.imageButton5);
        i6 = findViewById(R.id.imageButton6);
        TextView tv = findViewById(R.id.textView);

        switch (number) {
            case 0:
                icon = addPerson;
                break;
            case 1:
                icon = specificPlace;
                break;
            case 2:
                icon = navigation;
                break;
            case 3:
                icon = alarm;
                break;
            case 4:
                icon = rotate;
                break;
            case 5:
                icon = send;
                break;
            case 6:
                icon = missedCall;
                break;
            case 7:
                icon = currentPosition;
                break;
            case 8:
                icon = share;
                break;
        }


        try {
            i1.setImageResource(icon.getInt(shuffleList.get(0).toString()));
            i2.setImageResource(icon.getInt(shuffleList.get(1).toString()));
            i3.setImageResource(icon.getInt(shuffleList.get(2).toString()));
            i4.setImageResource(icon.getInt(shuffleList.get(3).toString()));
            i5.setImageResource(icon.getInt(shuffleList.get(4).toString()));
            i6.setImageResource(icon.getInt(shuffleList.get(5).toString()));
            tv.setText(icon.getString("description"));
            if (icons.size() % 2 == 0) {
                icons.add(icon.getInt("1"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        i1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, i1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(number + "", icon.getString("description"), shuffleList.get(0) + ""));
                        icons.add(icon.getInt(shuffleList.get(0).toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    number++;
                    if (number < 9) {
                        Intent myIntent = new Intent(context, SelectIcon.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        Intent myIntent = new Intent(context, OverviewIcon.class);
                        startActivity(myIntent);
                        finish();
                    }
                }
                return true;
            }
        });

        i2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, i2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(number + "", icon.getString("description"), shuffleList.get(1) + ""));
                        icons.add(icon.getInt(shuffleList.get(1).toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    number++;
                    if (number < 9) {
                        Intent myIntent = new Intent(context, SelectIcon.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        Intent myIntent = new Intent(context, OverviewIcon.class);
                        startActivity(myIntent);
                        finish();
                    }
                }
                return true;
            }
        });

        i3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, i3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(number + "", icon.getString("description"), shuffleList.get(2) + ""));
                        icons.add(icon.getInt(shuffleList.get(2).toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    number++;
                    if (number < 9) {
                        Intent myIntent = new Intent(context, SelectIcon.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        Intent myIntent = new Intent(context, OverviewIcon.class);
                        startActivity(myIntent);
                        finish();
                    }
                }
                return true;
            }
        });

        i4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, i4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(number + "", icon.getString("description"), shuffleList.get(3) + ""));
                        icons.add(icon.getInt(shuffleList.get(3).toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    number++;
                    if (number < 9) {
                        Intent myIntent = new Intent(context, SelectIcon.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        Intent myIntent = new Intent(context, OverviewIcon.class);
                        startActivity(myIntent);
                        finish();
                    }
                }
                return true;
            }
        });

        i5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, i5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(number + "", icon.getString("description"), shuffleList.get(4) + ""));
                        icons.add(icon.getInt(shuffleList.get(4).toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    number++;
                    if (number < 9) {
                        Intent myIntent = new Intent(context, SelectIcon.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        Intent myIntent = new Intent(context, OverviewIcon.class);
                        startActivity(myIntent);
                        finish();
                    }
                }
                return true;
            }
        });

        i6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, i6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(number + "", icon.getString("description"), shuffleList.get(5) + ""));
                        icons.add(icon.getInt(shuffleList.get(5).toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    number++;
                    if (number < 9) {
                        Intent myIntent = new Intent(context, SelectIcon.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        Intent myIntent = new Intent(context, OverviewIcon.class);
                        startActivity(myIntent);
                        finish();
                    }
                }
                return true;
            }
        });


    }


}
