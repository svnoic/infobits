package de.aachen.rwth.lufgi9.noichl.basics.personalisierung;


public class Values {

    private static int buttonHeight=0;
    private static int volume = 6;
    private static double textHeightInButton = 0.5;
    private static int minTextSize = 20;

    private static int precision = 0;


    private int textSizeButton = 25;



    public String getData() {
        // return gathered/default data
        String data = "";
        data += getButtonHeight()+"\n";
        data += getVolume()+"\n";
        data += getTextHeightInButton()+"\n";
        data += getMinTextSize()+"\n";
        data += getPrecision()+"\n";
        return data;
    }



    // GETTER-Functions



    private int getPrecision() {
        return precision;
    }



    int getButtonHeight() {
        return buttonHeight;
    }

    int getTextSizeButton() {
        return textSizeButton;
    }

    int getMinTextSize() {
        return minTextSize;
    }

    double getTextHeightInButton() {
        return textHeightInButton;
    }


    int getVolume() {
        return volume;
    }



    // SETTER-Functions


    void setPrecision(int pPrecision) {
        precision = pPrecision;
    }

    void setButtonHeight(int bH) {
        buttonHeight = bH;
    }

    private void setTextSizeButton(int pTextSize) {
        if( pTextSize >= 20 )
            textSizeButton = pTextSize;
    }

    void setMinTextSize(int pTextSize) {
        minTextSize = pTextSize;
        setTextSizeButton(minTextSize);
    }


    void setTextHeightInButton(double pRelativeHeight) {
        textHeightInButton = pRelativeHeight;
    }

    void setVolume(int pVolume) {
        volume = pVolume;
    }

}