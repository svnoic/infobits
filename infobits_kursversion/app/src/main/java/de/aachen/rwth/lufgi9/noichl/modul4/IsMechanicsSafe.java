package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class IsMechanicsSafe extends MyActivity {

    static int stepcounter = 0;
    static String syncaync = "";
    Context context;
    JSONArray myJson = null;
    String alt1, alt2, alt3, alt4;
    String scenario;
    static int index = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_usecasetextview_viewstub_s_nextbutton);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("IsMechanicsSafe"));
        }
        editor.putInt("current", getActivityNumber("IsMechanicsSafe"));
        editor.apply();
        saveSharedPrefs();

        String json = null;
        try {
            InputStream is = this.getAssets().open("isMechanicsSafe.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (myJson != null) {
            JSONObject rec;
            try {
                rec = myJson.getJSONObject(index);
                alt1 = rec.getString("alt1");
                alt2 = rec.getString("alt2");
                alt3 = rec.getString("alt3");
                alt4 = rec.getString("alt4");
                scenario = rec.getString("description");
                TextView tv = findViewById(R.id.my_usecase_textview);
                tv.setText(scenario);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        TextView tv;
        CheckBox cb;
        ViewStub stub = findViewById(R.id.viewStub);
        switch (stepcounter) {
            case 0:
                stub.setLayoutResource(R.layout.viewstub_textview_3_radiobutton);


                stub.inflate();
                RadioButton rb = findViewById(R.id.likert6);
                rb.setText(R.string.module4_ismsafe_item1);
                rb = findViewById(R.id.likert5);
                rb.setText(R.string.module4_ismsafe_item2);
                rb = findViewById(R.id.likert4);
                rb.setText(R.string.module4_ismsafe_item3);
                tv = findViewById(R.id.tv_ex);
                tv.setText(R.string.module4_ismsafe_question);

                break;
            case 1:
                stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                tv.setText(getString(R.string.usecase_reason, syncaync));
                cb = findViewById(R.id.alt1);
                cb.setText(alt1);
                cb = findViewById(R.id.alt2);
                cb.setText(alt2);
                cb = findViewById(R.id.alt3);
                cb.setText(alt3);
                cb = findViewById(R.id.alt4);
                cb.setText(alt4);
                break;
        }


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (stepcounter < 1) {
                        if (stepcounter == 0) {
                            RadioGroup myRadioGroup = findViewById(R.id.radioGroup3);
                            int selectedID = myRadioGroup.getCheckedRadioButtonId();

                            if (findViewById(selectedID) != null) {
                                RadioButton selectedView = findViewById(selectedID);
                                syncaync = (String) selectedView.getText();
                                myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.tv_ex)).getText().toString(), ((String) selectedView.getText())));
                                stepcounter++;
                                Intent myIntent = new Intent(context, IsMechanicsSafe.class);
                                startActivity(myIntent);
                                finish();
                            } else {
                                showDialog(getString(R.string.not_yet_ready), getString(R.string.module4_ismsafe_dialog), context);
                            }
                        }
                    } else {

                        CheckBox alt11 = findViewById(R.id.alt1);
                        CheckBox alt12 = findViewById(R.id.alt2);
                        CheckBox alt13 = findViewById(R.id.alt3);
                        CheckBox alt14 = findViewById(R.id.alt4);
                        if (alt11.isChecked() || alt12.isChecked() || alt13.isChecked() || alt14.isChecked()) {
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt11.getText().toString(), alt11.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt12.getText().toString(), alt12.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt13.getText().toString(), alt13.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt14.getText().toString(), alt14.isChecked() + ""));
                            stepcounter++; index++;
                            if(index == 4) {
                                if (recap) {
                                    stepcounter = 0;
                                    index = 0;
                                    Intent myIntent = new Intent(context, DataSecEnd.class);
                                    startActivity(myIntent);
                                    finish();
                                } else {
                                    if(myrecap){
                                        stepcounter = 0;
                                        index = 0;
                                        restartAfterRecap();
                                    }else {
                                        stepcounter = 0;
                                        index = 0;
                                        Intent myIntent = nextActivity(getActivityNumber("IsMechanicsSafe") + 1);
                                        startActivity(myIntent);
                                        finish();
                                    }
                                }
                            }else{
                                stepcounter = 0;
                                Intent myIntent = new Intent(context, IsMechanicsSafe.class);
                                startActivity(myIntent);
                                finish();
                            }

                        } else {
                            showDialog(getString(R.string.not_yet_ready), getString(R.string.module4_ismsafe_dialog), context);
                        }


                    }
                }
                return false;
            }
        });

    }
}

