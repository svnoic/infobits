package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.view.View;
import android.view.animation.Animation;

import androidx.annotation.DimenRes;


/**
 * A class to minimize a view on click and make it to origin size on click again.
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class ViewClickResizing<E extends View> implements View.OnClickListener, Animation.AnimationListener {

    /**
     * The view to resize
     */
    private final E view;

    /**
     * The animation for the minimizing
     */
    private final ResizeAnimation animation;

    /**
     * The with to minimize to. It is initial -3 (do not change width) but can be replaced.
     */
    private int targetWidth = -3;

    /**
     * The height to minimize to. It is initial -3 (do not change height) but can be replaced.
     */
    private int targetHeight = -3;

    /**
     * Flag to show if the current state of the view is minimized or normal
     */
    private boolean minimized = false;

    /**
     * A string for log for the class that is calling this class
     */
    private final String callerId;

    /**
     * Normal constructor
     *
     * @param callerId A string for log for the class that is calling this class
     * @param view     The view to minimize on click.
     */
    public ViewClickResizing(String callerId, E view) {
        this.callerId = callerId;
        this.view = view;
        this.view.setOnClickListener(this);
        animation = new ResizeAnimation(view);
        animation.setDuration(500);
        animation.setAnimationListener(this);
    }

    /**
     * @param targetWidth The target width where the animation ends
     * @return The object self for chaining.
     */
    public ViewClickResizing<E> setTargetWidth(int targetWidth) {
        this.targetWidth = targetWidth;

        // chaining
        return this;
    }

    /**
     * @param dimTargetWidth The dimension id of the target width where the animation ends
     * @return The object self for chaining.
     */
    public ViewClickResizing<E> setTargetWidthRes(@DimenRes int dimTargetWidth) {
        int targetWidth = (int) view.getContext().getResources().getDimension(dimTargetWidth);
        return setTargetWidth(targetWidth);
    }

    /**
     * @param targetHeight The target height where the animation ends
     * @return The object self for chaining.
     */
    public ViewClickResizing<E> setTargetHeight(int targetHeight) {
        this.targetHeight = targetHeight;

        // chaining
        return this;
    }

    /**
     * @param dimTargetHeight The dimension id of the target height where the animation ends
     * @return The object self for chaining.
     */
    public ViewClickResizing<E> setTargetHeightRes(@DimenRes int dimTargetHeight) {
        int targetHeight = (int) view.getContext().getResources().getDimension(dimTargetHeight);
        return setTargetHeight(targetHeight);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == this.view.getId()) {
            //CustomLog.i(view.getContext(), this, "userAction", callerId + ",click,view,viewClickResizing");
            // disable the view so that the view does not need to react when user click on while animation
            view.setEnabled(false);
            if (minimized) {
               // CustomLog.i(view.getContext(), this, "sessionContent", callerId + ",restored,viewClickResizing");
               // DefaultAppDatabase.getInstance(view.getContext()).systemInfoDao().add(new SystemInfo(App.getUserId(), "sessionContent", callerId + ",restored,viewClickResizing"));
                // currently minimized, go back to origin size
                minimized = false;
                animation.back();
                view.startAnimation(animation);
            } else {
               // CustomLog.i(view.getContext(), this, "sessionContent", callerId + ",minimized,viewClickResizing");
                //DefaultAppDatabase.getInstance(view.getContext()).systemInfoDao().add(new SystemInfo(App.getUserId(), "sessionContent", callerId + ",minimized,viewClickResizing"));
                // currently in origin size, minimize to target with
                minimized = true;
                if (targetWidth < 0 && targetHeight < 0) {
                    // no animation possible.. activate the view without animation
                    view.setEnabled(true);
                    return;
                }
                if (targetWidth >= 0 && targetHeight >= 0) {
                    animation.to(targetWidth, targetHeight);
                } else if (targetWidth >= 0) {
                    animation.toWidth(targetWidth);
                } else {
                    animation.toHeight(targetHeight);
                }
                view.startAnimation(animation);
            }
        }
    }

    public boolean isMinimized() {
        return minimized;
    }

    public E getView() {
        return view;
    }

    /**
     * <p>Notifies the start of the animation.</p>
     *
     * @param animation The started animation.
     */
    @Override
    public void onAnimationStart(Animation animation) {
        //CustomLog.i(view.getContext(), this, "sessionContent", callerId + ",startAnimation,viewClickResizing");
        //DefaultAppDatabase.getInstance(view.getContext()).systemInfoDao().add(new SystemInfo(App.getUserId(), "sessionContent", callerId + ",startAnimation,viewClickResizing"));
    }

    /**
     * <p>Notifies the end of the animation. This callback is not invoked
     * for animations with repeat count set to INFINITE.</p>
     *
     * @param animation The animation which reached its end.
     */
    @Override
    public void onAnimationEnd(Animation animation) {
        //CustomLog.i(view.getContext(), this, "sessionContent", callerId + ",endAnimation,viewClickResizing");
        //DefaultAppDatabase.getInstance(view.getContext()).systemInfoDao().add(new SystemInfo(App.getUserId(), "sessionContent", callerId + ",endAnimation,viewClickResizing"));
        // enable the view
        view.setEnabled(true);
    }

    /**
     * <p>Notifies the repetition of the animation.</p>
     *
     * @param animation The animation which was repeated.
     */
    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
