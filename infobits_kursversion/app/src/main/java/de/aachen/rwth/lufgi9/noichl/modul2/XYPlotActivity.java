package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.CatmullRomInterpolator;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Arrays;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;


public class XYPlotActivity extends Activity {

    private XYPlot plot;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plot_module);

        // initialize our XYPlot reference:
        plot = findViewById(R.id.plot);

        // create a couple arrays of y-values to plot:



        final FloatingActionButton nextButton = findViewById(R.id.floatingActionButton);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                Intent myIntent = new Intent(getBaseContext(), InternetEnd.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        Number[] series2Numbers = {sharedpreferences.getInt("m2_pre_0", 0),
                sharedpreferences.getInt("m2_pre_1", 0),
                sharedpreferences.getInt("m2_pre_2", 0),
                sharedpreferences.getInt("m2_pre_3", 0),
                sharedpreferences.getInt("m2_pre_4", 0),
                sharedpreferences.getInt("m2_pre_5", 0),
                sharedpreferences.getInt("m2_pre_6", 0),
                sharedpreferences.getInt("m2_pre_7", 0),
                sharedpreferences.getInt("m2_pre_8", 0),
                sharedpreferences.getInt("m2_pre_9", 0),
                sharedpreferences.getInt("m2_pre_10", 0),
                sharedpreferences.getInt("m2_pre_11", 0),
                sharedpreferences.getInt("m2_pre_12", 0),
                sharedpreferences.getInt("m2_pre_13", 0),
                sharedpreferences.getInt("m2_pre_14", 0),
                sharedpreferences.getInt("m2_pre_15", 0),
                sharedpreferences.getInt("m2_pre_16", 0),
                sharedpreferences.getInt("m2_pre_17", 0),
                sharedpreferences.getInt("m2_pre_18", 0),
                sharedpreferences.getInt("m2_pre_19", 0),
                sharedpreferences.getInt("m2_pre_20", 0)};
        Number[] series1Numbers = {sharedpreferences.getInt("m2_post_0", 0),
                sharedpreferences.getInt("m2_post_1", 0),
                sharedpreferences.getInt("m2_post_2", 0),
                sharedpreferences.getInt("m2_post_3", 0),
                sharedpreferences.getInt("m2_post_4", 0),
                sharedpreferences.getInt("m2_post_5", 0),
                sharedpreferences.getInt("m2_post_6", 0),
                sharedpreferences.getInt("m2_post_7", 0),
                sharedpreferences.getInt("m2_post_8", 0),
                sharedpreferences.getInt("m2_post_9", 0),
                sharedpreferences.getInt("m2_post_10", 0),
                sharedpreferences.getInt("m2_post_11", 0),
                sharedpreferences.getInt("m2_post_12", 0),
                sharedpreferences.getInt("m2_post_13", 0),
                sharedpreferences.getInt("m2_post_14", 0),
                sharedpreferences.getInt("m2_post_15", 0),
                sharedpreferences.getInt("m2_post_16", 0),
                sharedpreferences.getInt("m2_post_17", 0),
                sharedpreferences.getInt("m2_post_18", 0),
                sharedpreferences.getInt("m2_post_19", 0),
                sharedpreferences.getInt("m2_post_20", 0)};

        // turn the above arrays into XYSeries':
        // (Y_VALS_ONLY means use the element index as the x value)
        XYSeries series1 = new SimpleXYSeries(
                Arrays.asList(series1Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "nachher");
        XYSeries series2 = new SimpleXYSeries(
                Arrays.asList(series2Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "vorher");

        // create formatters to use for drawing a series using LineAndPointRenderer
        // and configure them from xml:
        LineAndPointFormatter series1Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels);

        LineAndPointFormatter series2Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels_2);

        // add an "dash" effect to the series2 line:
        series2Format.getLinePaint().setPathEffect(new DashPathEffect(new float[] {

                // always use DP when specifying pixel sizes, to keep things consistent across devices:
                PixelUtils.dpToPix(20),
                PixelUtils.dpToPix(15)}, 0));

        // just for fun, add some smoothing to the lines:
        // see: http://androidplot.com/smooth-curves-and-androidplot/
        series1Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));

        series2Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));
        plot.setRangeBoundaries(0,6, BoundaryMode.FIXED);
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 1);
        // add a new series' to the xyplot:
        plot.addSeries(series1, series1Format);
        plot.addSeries(series2, series2Format);

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                return toAppendTo.append("");
            }
            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });
    }
}