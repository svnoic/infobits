package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.AppCompatImageView;

import de.aachen.rwth.lufgi9.noichl.R;


/**
 * You can use this class to make your image clickable
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class ZoomOnClickImageView extends AppCompatImageView implements View.OnClickListener {

    /**
     * The id of the image to zoom
     */
    private @DrawableRes
    int zoomImageId;

    /**
     * The string title for the zoom view
     */
    private String zoomTitle;

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context     The Context the view is running in, through which it can
     *                    access the current theme, resources, etc.
     * @param zoomImageId The id of the image to zoom
     * @param zoomTitle   The string title for the zoom view
     */
    public ZoomOnClickImageView(Context context, @DrawableRes int zoomImageId, String zoomTitle) {
        super(context);
        this.zoomImageId = zoomImageId;
        this.zoomTitle = zoomTitle;
    }

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context     The Context the view is running in, through which it can
     *                    access the current theme, resources, etc.
     * @param zoomImageId The id of the image to zoom
     * @param zoomTitle   The string title for the zoom view
     */
    public ZoomOnClickImageView(Context context, @DrawableRes int zoomImageId, @StringRes int zoomTitle) {
        this(context, zoomImageId, context.getString(zoomTitle));
    }

    /**
     * The main constructor that contains also user customized attributes
     *
     * @param context Needed for parent constructor and access resources
     * @param attrs   Set of attributes given in the xml layout file
     */
    public ZoomOnClickImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // the user given custom style attributes in the layout xml file
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ZoomOnClickImageView);

        zoomTitle = a.getString(R.styleable.ZoomOnClickImageView_title);

        zoomImageId = a.getResourceId(R.styleable.ZoomOnClickImageView_srcZoom, 0);

        setOnClickListener(this);

        a.recycle();
    }

    /**
     * The main constructor that contains also user customized attributes
     *
     * @param context      Needed for parent constructor and access resources
     * @param attrs        Set of attributes given in the xml layout file
     * @param defStyleAttr The style of the view
     */
    public ZoomOnClickImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v == this) {
            //CustomLog.i(getContext(), this, "userAction", "click,ZoomOnClickImageView,zoomImage:'" + zoomImageId + "',title:'" + zoomTitle + "'");
            if (zoomImageId == 0) {
               // CustomLog.i(getContext(), this, "system", "ZoomOnClickImageView: no image resource given (zoomImageId is 0)");
            } else {
               // DialogUtilities.showImage(getContext(), zoomImageId, zoomTitle);
            }
        }
    }

    /**
     * Use this method to make the default update of the image.
     * The zoom image will be updated, too.
     *
     * @param imageId   The id of the new image
     * @param zoomTitle The title of the zoom view
     */
    public void updateImage(@DrawableRes int imageId, String zoomTitle) {
        setImageResource(imageId);

        this.zoomImageId = imageId;
        this.zoomTitle = zoomTitle;
    }

    /**
     * Use this method to make the default update of the image.
     * The zoom image will be updated, too.
     *
     * @param imageId   The id of the new image
     * @param zoomTitle The title of the zoom view
     */
    public void updateImage(@DrawableRes int imageId, @StringRes int zoomTitle) {
        updateImage(imageId, getContext().getString(zoomTitle));
    }

    /**
     * Remove the image and title
     */
    public void remove() {
        updateImage(0, "");
    }
}
