package de.aachen.rwth.lufgi9.noichl.modul1;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class ImageUseIntro extends MyActivity {

    Context context;
    JSONArray myJson = null;
    private static int counter = 0;
    private static int correct = 0;
    String scenario;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.image_use_intro);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("ImageUseIntro"));
        }
        editor.putInt("current", getActivityNumber("ImageUseIntro"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_usecase_textview_2_radiobutton);
        stub.inflate();

        String json = null;
        try {
            InputStream is = this.getAssets().open("imageUseIntro.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (myJson != null) {
            JSONObject rec;
            try {
                rec = myJson.getJSONObject(counter);
                scenario = rec.getString("description");
                TextView usecase_tv = findViewById(R.id.my_usecase_textview);
                usecase_tv.setText(scenario);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    Intent myIntent;
                    RadioButton rb1 = findViewById(R.id.my_true);
                    RadioButton rb2 = findViewById(R.id.my_false);
                    if (counter < 2 && (rb1.isChecked() || rb2.isChecked())) {
                        switch (counter) {
                            case 0:
                                if (rb1.isChecked()) {
                                    correct++;
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(scenario, true, rb1.getText().toString(), rb1.getText().toString()));
                                } else {
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(scenario, false, rb2.getText().toString(), rb1.getText().toString()));
                                }
                                break;
                            case 1:
                                if (rb2.isChecked()) {
                                    correct++;
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(scenario, true, rb2.getText().toString(), rb2.getText().toString()));
                                } else {
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(scenario, false, rb1.getText().toString(), rb2.getText().toString()));
                                }
                                break;
                        }
                        myIntent = new Intent(context, ImageUseIntro.class);
                        startActivity(myIntent);
                        finish();
                        counter++;
                    } else {
                        if (rb1.isChecked() || rb2.isChecked()) {
                            if (rb2.isChecked()) {
                                correct++;
                                myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(scenario, true, rb2.getText().toString(), rb2.getText().toString()));
                            } else {
                                myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(scenario, false, rb1.getText().toString(), rb2.getText().toString()));
                            }
                            myIntent = nextActivity(getActivityNumber("ImageUseIntro") + 1);
                            myIntent.putExtra("correct", correct);
                            startActivity(myIntent);
                            finish();
                            counter = 0;
                            correct = 0;
                        } else {
                            showDialog(getString(R.string.not_yet_ready), getString(R.string.chose_answer), context);
                        }
                    }

                }

                return false;
            }

        });
    }
}
