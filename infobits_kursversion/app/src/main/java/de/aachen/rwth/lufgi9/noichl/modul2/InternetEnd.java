package de.aachen.rwth.lufgi9.noichl.modul2;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileNotFoundException;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.AppStart;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class InternetEnd extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewstub_finalpage_module2);



        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM2", getActivityNumber("InternetEnd"));
        }
        editor.putInt("current", getActivityNumber("InternetEnd"));
        editor.apply();
        saveSharedPrefs();

        recap = true;


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.finish);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setText(R.string.module2_end);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                recap = false;
                try {
                    readSharedPrefs();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Intent myIntent = new Intent(getBaseContext(), AppStart.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button statsButton = findViewById(R.id.stats);
        statsButton.setText(R.string.watch_stats);
        statsButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, statsButton));
                Intent myIntent = new Intent(getBaseContext(), XYPlotActivity.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });


        final Button recap_video1 = findViewById(R.id.recap_video1);
        recap_video1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), Binaersystem.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video2 = findViewById(R.id.recap_video2);
        recap_video2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DPVideo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video3 = findViewById(R.id.recap_video3);
        recap_video3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), URLVideo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video4 = findViewById(R.id.recap_video4);
        recap_video4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DNSVideo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });


        final Button recap_text1 = findViewById(R.id.recap_text1);
        recap_text1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), InternetInfo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text2 = findViewById(R.id.recap_text2);
        recap_text2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DataInfo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text3 = findViewById(R.id.recap_text3);
        recap_text3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), Datapackage.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text4 = findViewById(R.id.recap_text4);
        recap_text4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), IPIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text5 = findViewById(R.id.recap_text5);
        recap_text5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), NetworkCompareHubSwitchRouterActivity.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text6 = findViewById(R.id.recap_text6);
        recap_text6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DevicesInInternetInfo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text7 = findViewById(R.id.recap_text7);
        recap_text7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text7));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), URLIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text8 = findViewById(R.id.recap_text8);
        recap_text8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text8));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DNSIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_text9 = findViewById(R.id.recap_text9);
        recap_text9.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text9));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), HistoryIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });



        final Button recap_activity1 = findViewById(R.id.recap_activity1);
        recap_activity1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DezToBin.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity2 = findViewById(R.id.recap_activity2);
        recap_activity2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), Devices.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity3 = findViewById(R.id.recap_activity3);
        recap_activity3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), URL1.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity4 = findViewById(R.id.recap_activity4);
        recap_activity4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), IpUrlDns.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity5 = findViewById(R.id.recap_activity5);
        recap_activity5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), InternetQuizInfo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });


    }
}
