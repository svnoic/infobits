package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.CatmullRomInterpolator;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Arrays;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;


@SuppressWarnings("ALL")
public class DigComp_Stats extends Activity {

    private XYPlot plot;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_plot_digcomp);

        // initialize our XYPlot reference:
        plot = findViewById(R.id.plot);

        // create a couple arrays of y-values to plot:



        final FloatingActionButton nextButton = findViewById(R.id.floatingActionButton);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                Intent myIntent = new Intent(getBaseContext(), CommunicationEnd.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        Number[] series5Numbers = {
                sharedpreferences.getInt("m0_digcomp_11", 0),
                sharedpreferences.getInt("m0_digcomp_12", 0),
                sharedpreferences.getInt("m0_digcomp_13", 0),
                sharedpreferences.getInt("m0_digcomp_21", 0),
                sharedpreferences.getInt("m0_digcomp_22", 0),
                sharedpreferences.getInt("m0_digcomp_23", 0),
                sharedpreferences.getInt("m0_digcomp_24", 0),
                sharedpreferences.getInt("m0_digcomp_25", 0),
                sharedpreferences.getInt("m0_digcomp_26", 0),
                sharedpreferences.getInt("m0_digcomp_31", 0),
                sharedpreferences.getInt("m0_digcomp_32", 0),
                sharedpreferences.getInt("m0_digcomp_33", 0),
                sharedpreferences.getInt("m0_digcomp_34", 0),
                sharedpreferences.getInt("m0_digcomp_41", 0),
                sharedpreferences.getInt("m0_digcomp_42", 0),
                sharedpreferences.getInt("m0_digcomp_43", 0),
                sharedpreferences.getInt("m0_digcomp_44", 0),
                sharedpreferences.getInt("m0_digcomp_51", 0),
                sharedpreferences.getInt("m0_digcomp_52", 0),
                sharedpreferences.getInt("m0_digcomp_53", 0),
                sharedpreferences.getInt("m0_digcomp_54", 0)};
        Number[] series4Numbers = {
                sharedpreferences.getInt("m1_digcomp_11", 0),
                sharedpreferences.getInt("m1_digcomp_12", 0),
                sharedpreferences.getInt("m1_digcomp_13", 0),
                sharedpreferences.getInt("m1_digcomp_21", 0),
                sharedpreferences.getInt("m1_digcomp_22", 0),
                sharedpreferences.getInt("m1_digcomp_23", 0),
                sharedpreferences.getInt("m1_digcomp_24", 0),
                sharedpreferences.getInt("m1_digcomp_25", 0),
                sharedpreferences.getInt("m1_digcomp_26", 0),
                sharedpreferences.getInt("m1_digcomp_31", 0),
                sharedpreferences.getInt("m1_digcomp_32", 0),
                sharedpreferences.getInt("m1_digcomp_33", 0),
                sharedpreferences.getInt("m1_digcomp_34", 0),
                sharedpreferences.getInt("m1_digcomp_41", 0),
                sharedpreferences.getInt("m1_digcomp_42", 0),
                sharedpreferences.getInt("m1_digcomp_43", 0),
                sharedpreferences.getInt("m1_digcomp_44", 0),
                sharedpreferences.getInt("m1_digcomp_51", 0),
                sharedpreferences.getInt("m1_digcomp_52", 0),
                sharedpreferences.getInt("m1_digcomp_53", 0),
                sharedpreferences.getInt("m1_digcomp_54", 0)};
        Number[] series3Numbers = {
                sharedpreferences.getInt("m2_digcomp_11", 0),
                sharedpreferences.getInt("m2_digcomp_12", 0),
                sharedpreferences.getInt("m2_digcomp_13", 0),
                sharedpreferences.getInt("m2_digcomp_21", 0),
                sharedpreferences.getInt("m2_digcomp_22", 0),
                sharedpreferences.getInt("m2_digcomp_23", 0),
                sharedpreferences.getInt("m2_digcomp_24", 0),
                sharedpreferences.getInt("m2_digcomp_25", 0),
                sharedpreferences.getInt("m2_digcomp_26", 0),
                sharedpreferences.getInt("m2_digcomp_31", 0),
                sharedpreferences.getInt("m2_digcomp_32", 0),
                sharedpreferences.getInt("m2_digcomp_33", 0),
                sharedpreferences.getInt("m2_digcomp_34", 0),
                sharedpreferences.getInt("m2_digcomp_41", 0),
                sharedpreferences.getInt("m2_digcomp_42", 0),
                sharedpreferences.getInt("m2_digcomp_43", 0),
                sharedpreferences.getInt("m2_digcomp_44", 0),
                sharedpreferences.getInt("m2_digcomp_51", 0),
                sharedpreferences.getInt("m2_digcomp_52", 0),
                sharedpreferences.getInt("m2_digcomp_53", 0),
                sharedpreferences.getInt("m2_digcomp_54", 0)};
        Number[] series2Numbers = {
                sharedpreferences.getInt("m3_digcomp_11", 0),
                sharedpreferences.getInt("m3_digcomp_12", 0),
                sharedpreferences.getInt("m3_digcomp_13", 0),
                sharedpreferences.getInt("m3_digcomp_21", 0),
                sharedpreferences.getInt("m3_digcomp_22", 0),
                sharedpreferences.getInt("m3_digcomp_23", 0),
                sharedpreferences.getInt("m3_digcomp_24", 0),
                sharedpreferences.getInt("m3_digcomp_25", 0),
                sharedpreferences.getInt("m3_digcomp_26", 0),
                sharedpreferences.getInt("m3_digcomp_31", 0),
                sharedpreferences.getInt("m3_digcomp_32", 0),
                sharedpreferences.getInt("m3_digcomp_33", 0),
                sharedpreferences.getInt("m3_digcomp_34", 0),
                sharedpreferences.getInt("m3_digcomp_41", 0),
                sharedpreferences.getInt("m3_digcomp_42", 0),
                sharedpreferences.getInt("m3_digcomp_43", 0),
                sharedpreferences.getInt("m3_digcomp_44", 0),
                sharedpreferences.getInt("m3_digcomp_51", 0),
                sharedpreferences.getInt("m3_digcomp_52", 0),
                sharedpreferences.getInt("m3_digcomp_53", 0),
                sharedpreferences.getInt("m3_digcomp_54", 0)};
        Number[] series1Numbers = {
                sharedpreferences.getInt("m4_digcomp_11", 0),
                sharedpreferences.getInt("m4_digcomp_12", 0),
                sharedpreferences.getInt("m4_digcomp_13", 0),
                sharedpreferences.getInt("m4_digcomp_21", 0),
                sharedpreferences.getInt("m4_digcomp_22", 0),
                sharedpreferences.getInt("m4_digcomp_23", 0),
                sharedpreferences.getInt("m4_digcomp_24", 0),
                sharedpreferences.getInt("m4_digcomp_25", 0),
                sharedpreferences.getInt("m4_digcomp_26", 0),
                sharedpreferences.getInt("m4_digcomp_31", 0),
                sharedpreferences.getInt("m4_digcomp_32", 0),
                sharedpreferences.getInt("m4_digcomp_33", 0),
                sharedpreferences.getInt("m4_digcomp_34", 0),
                sharedpreferences.getInt("m4_digcomp_41", 0),
                sharedpreferences.getInt("m4_digcomp_42", 0),
                sharedpreferences.getInt("m4_digcomp_43", 0),
                sharedpreferences.getInt("m4_digcomp_44", 0),
                sharedpreferences.getInt("m4_digcomp_51", 0),
                sharedpreferences.getInt("m4_digcomp_52", 0),
                sharedpreferences.getInt("m4_digcomp_53", 0),
                sharedpreferences.getInt("m4_digcomp_54", 0)
                };

        // turn the above arrays into XYSeries':
        // (Y_VALS_ONLY means use the element index as the x value)
        XYSeries series1 = new SimpleXYSeries(
                Arrays.asList(series1Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "nach Modul 4");
        XYSeries series2 = new SimpleXYSeries(
                Arrays.asList(series2Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "nach Modul 3");
        XYSeries series3 = new SimpleXYSeries(
                Arrays.asList(series3Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "nach Modul 2");
        XYSeries series4 = new SimpleXYSeries(
                Arrays.asList(series4Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "nach Modul 1");
        XYSeries series5 = new SimpleXYSeries(
                Arrays.asList(series5Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "vorher");

        // create formatters to use for drawing a series using LineAndPointRenderer
        // and configure them from xml:
        LineAndPointFormatter series1Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels);

        LineAndPointFormatter series2Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels_2);

        LineAndPointFormatter series3Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels_3);

        LineAndPointFormatter series4Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels_4);

        LineAndPointFormatter series5Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels_5);

        // add an "dash" effect
        series1Format.getLinePaint().setPathEffect(new DashPathEffect(new float[] {PixelUtils.dpToPix(5), PixelUtils.dpToPix(5)}, 0));

        series2Format.getLinePaint().setPathEffect(new DashPathEffect(new float[] {PixelUtils.dpToPix(5), PixelUtils.dpToPix(5)}, 0));

        series3Format.getLinePaint().setPathEffect(new DashPathEffect(new float[] {PixelUtils.dpToPix(5), PixelUtils.dpToPix(5)}, 0));

        series4Format.getLinePaint().setPathEffect(new DashPathEffect(new float[] {PixelUtils.dpToPix(5), PixelUtils.dpToPix(5)}, 0));

        series5Format.getLinePaint().setPathEffect(new DashPathEffect(new float[] {PixelUtils.dpToPix(5), PixelUtils.dpToPix(5)}, 0));

        plot.setRangeBoundaries(0,8, BoundaryMode.FIXED);
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 1);

        // just for fun, add some smoothing to the lines:
        // see: http://androidplot.com/smooth-curves-and-androidplot/
        series1Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));

        series2Format.setInterpolationParams(
            new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));

        series3Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));

        series4Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));

        series5Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));
        // add a new series' to the xyplot:
        plot.addSeries(series1, series1Format);
        plot.addSeries(series2, series2Format);
        plot.addSeries(series3, series3Format);
        plot.addSeries(series4, series4Format);
        plot.addSeries(series5, series5Format);

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                Math.round(((Number) obj).floatValue());
                //return toAppendTo.append(domainLabels[i]);
                return toAppendTo.append("");
            }
            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });
    }
}