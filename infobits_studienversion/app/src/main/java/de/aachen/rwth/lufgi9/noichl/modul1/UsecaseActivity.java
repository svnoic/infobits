package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class UsecaseActivity extends MyActivity {

    static int stepcounter = 0;
    static int comway, index;
    static String syncasync = "";
    Context context;
    JSONArray myJson = null;
    String alt1, alt2, alt3, alt4, alt21, alt22, alt23, alt24;
    String[] comways;
    String scenario;

    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_usecasetextview_viewstub_s_nextbutton);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("UsecaseActivity"));
        }
        editor.putInt("current", getActivityNumber("UsecaseActivity"));
        editor.apply();
        saveSharedPrefs();

        comways = new String[]{getString(R.string.call), getString(R.string.sms), getString(R.string.mms), getString(R.string.fax), getString(R.string.mail), getString(R.string.chat), getString(R.string.videocall), getString(R.string.forum), getString(R.string.comment), getString(R.string.im)};


        String json = null;
        try {
            InputStream is = this.getAssets().open("communicationTypeScenario.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (myJson != null) {
            JSONObject rec;
            if (stepcounter == 0) {
                index = (int) (Math.random() * 4);
            }
            try {
                rec = myJson.getJSONObject(index);
                alt1 = rec.getString("alt1");
                alt2 = rec.getString("alt2");
                alt3 = rec.getString("alt3");
                alt4 = rec.getString("alt4");
                alt21 = rec.getString("alt21");
                alt22 = rec.getString("alt22");
                alt23 = rec.getString("alt23");
                alt24 = rec.getString("alt24");

                scenario = rec.getString("description");
                TextView tv = findViewById(R.id.my_usecase_textview);
                tv.setText(scenario);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        TextView tv;
        CheckBox cb;
        ViewStub stub = findViewById(R.id.viewStub);
        switch (stepcounter) {
            case 0:
                stub.setLayoutResource(R.layout.viewstub_textview_2_radiobutton);
                stub.inflate();
                tv = findViewById(R.id.my_textview);
                tv.setText(R.string.usecase_sync_vs_async);
                break;
            case 1:
                stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                tv.setText(getString(R.string.usecase_reason, syncasync));
                cb = findViewById(R.id.alt1);
                cb.setText(alt1);
                cb = findViewById(R.id.alt2);
                cb.setText(alt2);
                cb = findViewById(R.id.alt3);
                cb.setText(alt3);
                cb = findViewById(R.id.alt4);
                cb.setText(alt4);
                break;
            case 2:
                stub.setLayoutResource(R.layout.viewstub_5x2_checkbox);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                tv.setText(getString(R.string.usecase_comway));
                break;
            case 3:
                stub.setLayoutResource(R.layout.viewstub_4_radiobutton_with_icon);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                tv.setText(R.string.usecase_sort);
                break;
            case 4:
                stub.setLayoutResource(R.layout.viewstub_textview_2_radiobutton);
                stub.inflate();
                tv = findViewById(R.id.my_textview);
                comway = (int) (Math.random() * 10);
                tv.setText(getString(R.string.uscase_good_bad1) + comways[comway] + getString(R.string.uscase_good_bad2));
                break;
            case 5:
                stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                String text = getString(R.string.uscase_reason_good_bad1) + comways[comway] + getString(R.string.uscase_reason_good_bad2) + syncasync;
                tv.setText(text);
                cb = findViewById(R.id.alt1);
                cb.setText(alt21);
                cb = findViewById(R.id.alt2);
                cb.setText(alt22);
                cb = findViewById(R.id.alt3);
                cb.setText(alt23);
                cb = findViewById(R.id.alt4);
                cb.setText(alt24);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + stepcounter);
        }


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (stepcounter < 5) {

                        switch (stepcounter) {
                            case 0:
                                RadioGroup myRadioGroup = findViewById(R.id.radioGroup3);
                                int selectedID = myRadioGroup.getCheckedRadioButtonId();
                                if (findViewById(selectedID) != null) {
                                    RadioButton selectedView = findViewById(selectedID);
                                    syncasync = (String) selectedView.getText();
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.my_textview)).getText().toString(), ((String) selectedView.getText())));
                                    stepcounter++;
                                    Intent myIntent = new Intent(context, UsecaseActivity.class);
                                    startActivity(myIntent);
                                    finish();
                                } else {
                                    showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_usecaseactivity_dialog1), context);
                                }
                                break;

                            case 1:
                                CheckBox alt1 = findViewById(R.id.alt1);
                                CheckBox alt2 = findViewById(R.id.alt2);
                                CheckBox alt3 = findViewById(R.id.alt3);
                                CheckBox alt4 = findViewById(R.id.alt4);
                                if (alt1.isChecked() || alt2.isChecked() || alt3.isChecked() || alt4.isChecked()) {
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt1.getText().toString(), alt1.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt2.getText().toString(), alt2.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt3.getText().toString(), alt3.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt4.getText().toString(), alt4.isChecked() + ""));

                                    Intent myIntent = new Intent(context, UsecaseActivity.class);
                                    startActivity(myIntent);
                                    finish();
                                    stepcounter++;
                                } else {
                                    showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_usecaseactivity_dialog1), context);
                                }
                                break;

                            case 2:
                                CheckBox cb_call = findViewById(R.id.call);
                                CheckBox cb_sms = findViewById(R.id.sms);
                                CheckBox cb_mms = findViewById(R.id.mms);
                                CheckBox cb_fax = findViewById(R.id.fax);
                                CheckBox cb_mail = findViewById(R.id.mail);
                                CheckBox cb_chat = findViewById(R.id.chat);
                                CheckBox cb_videocall = findViewById(R.id.videocall);
                                CheckBox cb_forum = findViewById(R.id.forum);
                                CheckBox cb_comment = findViewById(R.id.comment);
                                CheckBox cb_im = findViewById(R.id.im);
                                if (cb_call.isChecked() || cb_sms.isChecked() || cb_mms.isChecked() || cb_fax.isChecked() || cb_mail.isChecked() ||
                                        cb_chat.isChecked() || cb_videocall.isChecked() || cb_forum.isChecked() || cb_comment.isChecked() || cb_im.isChecked()) {
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.call), ((CheckBox) findViewById(R.id.call)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.sms), ((CheckBox) findViewById(R.id.sms)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("3", getString(R.string.mms), ((CheckBox) findViewById(R.id.mms)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("4", getString(R.string.fax), ((CheckBox) findViewById(R.id.fax)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("5", getString(R.string.mail), ((CheckBox) findViewById(R.id.mail)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("6", getString(R.string.chat), ((CheckBox) findViewById(R.id.chat)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("7", getString(R.string.videocall), ((CheckBox) findViewById(R.id.videocall)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("8", getString(R.string.forum), ((CheckBox) findViewById(R.id.forum)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("9", getString(R.string.comment), ((CheckBox) findViewById(R.id.comment)).isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("10", getString(R.string.im), ((CheckBox) findViewById(R.id.im)).isChecked() + ""));


                                    Intent myIntent = new Intent(context, UsecaseActivity.class);
                                    startActivity(myIntent);
                                    finish();
                                    stepcounter++;
                                } else {
                                    showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_usecaseactivity_dialog1), context);
                                }
                                break;

                            case 3:
                                RadioGroup rg1 = findViewById(R.id.true_false);
                                int selectedID1 = rg1.getCheckedRadioButtonId();
                                RadioGroup rg2 = findViewById(R.id.true_false1);
                                int selectedID2 = rg2.getCheckedRadioButtonId();
                                RadioGroup rg3 = findViewById(R.id.true_false2);
                                int selectedID3 = rg3.getCheckedRadioButtonId();
                                RadioGroup rg4 = findViewById(R.id.true_false3);
                                int selectedID4 = rg4.getCheckedRadioButtonId();
                                if (findViewById(selectedID1) != null && findViewById(selectedID2) != null && findViewById(selectedID3) != null && findViewById(selectedID4) != null) {
                                    RadioButton selectedView1 = findViewById(selectedID1);
                                    RadioButton selectedView2 = findViewById(selectedID2);
                                    RadioButton selectedView3 = findViewById(selectedID3);
                                    RadioButton selectedView4 = findViewById(selectedID4);
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.my_textview)).getText().toString(), ((String) selectedView1.getTag())));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.my_textview1)).getText().toString(), ((String) selectedView2.getTag())));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.my_textview2)).getText().toString(), ((String) selectedView3.getTag())));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.my_textview3)).getText().toString(), ((String) selectedView4.getTag())));
                                    stepcounter++;
                                    Intent myIntent = new Intent(context, UsecaseActivity.class);
                                    startActivity(myIntent);
                                    finish();
                                } else {
                                    showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_usecaseactivity_dialog2), context);
                                }

                                break;

                            case 4:
                                myRadioGroup = findViewById(R.id.radioGroup3);
                                selectedID = myRadioGroup.getCheckedRadioButtonId();
                                if (findViewById(selectedID) != null) {
                                    RadioButton selectedView = findViewById(selectedID);
                                    syncasync = (String) selectedView.getText();
                                    stepcounter++;
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.my_textview)).getText().toString(), ((String) selectedView.getText())));
                                    Intent myIntent = new Intent(context, UsecaseActivity.class);
                                    startActivity(myIntent);
                                    finish();
                                } else {
                                    showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_usecaseactivity_dialog1), context);
                                }
                                break;

                        }

                    } else {
                        CheckBox alt11 = findViewById(R.id.alt1);
                        CheckBox alt12 = findViewById(R.id.alt2);
                        CheckBox alt13 = findViewById(R.id.alt3);
                        CheckBox alt14 = findViewById(R.id.alt4);
                        if (alt11.isChecked() || alt12.isChecked() || alt13.isChecked() || alt14.isChecked()) {
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt11.getText().toString(), alt11.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt12.getText().toString(), alt12.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt13.getText().toString(), alt13.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt14.getText().toString(), alt14.isChecked() + ""));

                            stepcounter = 0;
                            if (recap) {
                                Intent myIntent = new Intent(context, CommunicationEnd.class);
                                startActivity(myIntent);
                                finish();
                            } else {
                                if(myrecap){
                                    restartAfterRecap();
                                }else {
                                    Intent myIntent = nextActivity(getActivityNumber("UsecaseActivity") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                            }
                        } else {
                            showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_usecaseactivity_dialog1), context);
                        }
                    }
                }
                return false;
            }
        });

    }
}
