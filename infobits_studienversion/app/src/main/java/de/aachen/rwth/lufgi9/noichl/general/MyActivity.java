package de.aachen.rwth.lufgi9.noichl.general;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationEnd;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationStart;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationUsage;
import de.aachen.rwth.lufgi9.noichl.modul1.CreativeCommons;
import de.aachen.rwth.lufgi9.noichl.modul1.CreativeCommonsIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.DifferenceTalkLetter;
import de.aachen.rwth.lufgi9.noichl.modul1.FillTheGapActivity;
import de.aachen.rwth.lufgi9.noichl.modul1.ImageUseIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.KUT_K_Pre;
import de.aachen.rwth.lufgi9.noichl.modul1.LinkTalkLetter;
import de.aachen.rwth.lufgi9.noichl.modul1.MessageComponentMatch;
import de.aachen.rwth.lufgi9.noichl.modul1.OER;
import de.aachen.rwth.lufgi9.noichl.modul1.OERIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.PolicyTrueFalse;
import de.aachen.rwth.lufgi9.noichl.modul1.Recap;
import de.aachen.rwth.lufgi9.noichl.modul1.SortPolicy;
import de.aachen.rwth.lufgi9.noichl.modul1.SyncVsAsync;
import de.aachen.rwth.lufgi9.noichl.modul1.TalkLetterIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.Urheberrecht;
import de.aachen.rwth.lufgi9.noichl.modul1.UrheberrechtIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.UsecaseActivity;
import de.aachen.rwth.lufgi9.noichl.modul1.UsecaseActivity2;
import de.aachen.rwth.lufgi9.noichl.modul1.VideoAdressIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.VideoAdressesActivity;
import de.aachen.rwth.lufgi9.noichl.modul1.VideoTalkAndLetter;
import de.aachen.rwth.lufgi9.noichl.modul2.Binaersystem;
import de.aachen.rwth.lufgi9.noichl.modul2.ConnectionpieceInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.DNSIntro;
import de.aachen.rwth.lufgi9.noichl.modul2.DNSVideo;
import de.aachen.rwth.lufgi9.noichl.modul2.DPVideo;
import de.aachen.rwth.lufgi9.noichl.modul2.DataInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.Datapackage;
import de.aachen.rwth.lufgi9.noichl.modul2.Devices;
import de.aachen.rwth.lufgi9.noichl.modul2.DevicesInInternetInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.DezToBin;
import de.aachen.rwth.lufgi9.noichl.modul2.DezToBin2;
import de.aachen.rwth.lufgi9.noichl.modul2.HistoryIntro;
import de.aachen.rwth.lufgi9.noichl.modul2.IPIntro;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetEnd;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetQuizInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetStart;
import de.aachen.rwth.lufgi9.noichl.modul2.IpUrlDns;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkCompareHubSwitchRouterActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkConnectedActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkConnectedActivity5;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkInfo5;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkRouter;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkSwitchConnectedActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.PosttestInformation;
import de.aachen.rwth.lufgi9.noichl.modul2.PretestInformation;
import de.aachen.rwth.lufgi9.noichl.modul2.URL1;
import de.aachen.rwth.lufgi9.noichl.modul2.URL2;
import de.aachen.rwth.lufgi9.noichl.modul2.URLIntro;
import de.aachen.rwth.lufgi9.noichl.modul2.URLVideo;
import de.aachen.rwth.lufgi9.noichl.modul3.Cable;
import de.aachen.rwth.lufgi9.noichl.modul3.CableToNoCable;
import de.aachen.rwth.lufgi9.noichl.modul3.CableVideo;
import de.aachen.rwth.lufgi9.noichl.modul3.CableVideoIntro;
import de.aachen.rwth.lufgi9.noichl.modul3.Connected;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectedSort;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionEnd;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionIntro;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionStart;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionVideo;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionVideoIntro;
import de.aachen.rwth.lufgi9.noichl.modul3.DifferenceWLANMobil;
import de.aachen.rwth.lufgi9.noichl.modul3.GameBookIntro;
import de.aachen.rwth.lufgi9.noichl.modul3.InternetWords;
import de.aachen.rwth.lufgi9.noichl.modul3.InternetWordsSolution;
import de.aachen.rwth.lufgi9.noichl.modul3.LanManWan;
import de.aachen.rwth.lufgi9.noichl.modul3.Mobilfunk;
import de.aachen.rwth.lufgi9.noichl.modul3.PreLanManWan;
import de.aachen.rwth.lufgi9.noichl.modul3.WLANQuizInfo;
import de.aachen.rwth.lufgi9.noichl.modul3.WlanWifi;
import de.aachen.rwth.lufgi9.noichl.modul4.CreatePW;
import de.aachen.rwth.lufgi9.noichl.modul4.CreatePWQA;
import de.aachen.rwth.lufgi9.noichl.modul4.CreateSafePW;
import de.aachen.rwth.lufgi9.noichl.modul4.DataDefinition;
import de.aachen.rwth.lufgi9.noichl.modul4.DataMC;
import de.aachen.rwth.lufgi9.noichl.modul4.DataSecEnd;
import de.aachen.rwth.lufgi9.noichl.modul4.DataUsage;
import de.aachen.rwth.lufgi9.noichl.modul4.Datasecurity;
import de.aachen.rwth.lufgi9.noichl.modul4.DatasecurityStart;
import de.aachen.rwth.lufgi9.noichl.modul4.DatasecuritySzenarioInfo;
import de.aachen.rwth.lufgi9.noichl.modul4.DefinitionPW;
import de.aachen.rwth.lufgi9.noichl.modul4.DifferenceDD;
import de.aachen.rwth.lufgi9.noichl.modul4.IsMechanicsSafe;
import de.aachen.rwth.lufgi9.noichl.modul4.IsPWSafe;
import de.aachen.rwth.lufgi9.noichl.modul4.MechanicsInfo;
import de.aachen.rwth.lufgi9.noichl.modul4.PWmcInfo;
import de.aachen.rwth.lufgi9.noichl.modul4.PrePassword;
import de.aachen.rwth.lufgi9.noichl.modul4.SafeMechanics;
import de.aachen.rwth.lufgi9.noichl.modul4.SafePWCreationStratagy;
import de.aachen.rwth.lufgi9.noichl.modul4.SafePWInfo;
import de.aachen.rwth.lufgi9.noichl.modul4.SafePWRequirements;
import de.aachen.rwth.lufgi9.noichl.modul4.Saftymechanics;
import de.aachen.rwth.lufgi9.noichl.modul4.SecurityIntro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.concurrent.TimeUnit;

public abstract class MyActivity extends AppCompatActivity {

    int w,h;
    public static boolean recap = false;
    public static boolean myrecap = false;
    public static String USERID = "";
    public static boolean KNOWNUSER = false;
    public static boolean isPlaying = false;
    private AudioManager audioManager = null;
    public static MediaPlayer mp = null;
    Context context;
    String currentClass = null;
    int current;
    int currentM1, currentM2, currentM3, currentM4;
    public static int newCurrent;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);





        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        int volume = sharedpreferences.getInt("Volume", 0);
        int buttonHeight = sharedpreferences.getInt("ButtonHeight", 100);
        float textHeight = sharedpreferences.getFloat("TextHeight", (float) 0.5);
        int textsize = sharedpreferences.getInt("TextSize", 20);


        Log.e("DATA", volume + "\n" + buttonHeight + "\n" + textHeight + "\n" + textsize);

        context = this;

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_READ_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        }

        //Small
        if(textsize <= 18 && buttonHeight <= 100 && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Small_100_30);}
        if(textsize <= 18 && (100 < buttonHeight && buttonHeight <= 125) && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Small_125_30);}
        if(textsize <= 18 && (125 < buttonHeight) && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Small_150_30);}
        if(textsize <= 18 && buttonHeight <= 100 && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Small_100_40);}
        if(textsize <= 18 && (100 < buttonHeight && buttonHeight <= 125) && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Small_125_40);}
        if(textsize <= 18 && (125 < buttonHeight) && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Small_150_40);}
        if(textsize <= 18 && buttonHeight <= 100 && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Small_100_50);}
        if(textsize <= 18 && (100 < buttonHeight && buttonHeight <= 125) && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Small_125_50);}
        if(textsize <= 18 && (125 < buttonHeight) && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Small_150_50);}
        if(textsize <= 18 && buttonHeight <= 100 && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Small_100_60);}
        if(textsize <= 18 && (100 < buttonHeight && buttonHeight <= 125) && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Small_125_60);}
        if(textsize <= 18 && (125 < buttonHeight) && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Small_150_60);}
        //Medium
        if((18 < textsize && textsize <= 20) && buttonHeight <= 100 && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Medium_100_30);}
        if((18 < textsize && textsize <= 20) && (100 < buttonHeight && buttonHeight <= 125) && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Medium_125_30);}
        if((18 < textsize && textsize <= 20) && (125 < buttonHeight) && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Medium_150_30);}
        if((18 < textsize && textsize <= 20) && buttonHeight <= 100 && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Medium_100_40);}
        if((18 < textsize && textsize <= 20) && (100 < buttonHeight && buttonHeight <= 125) && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Medium_125_40);}
        if((18 < textsize && textsize <= 20) && (125 < buttonHeight) && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Medium_150_40);}
        if((18 < textsize && textsize <= 20) && buttonHeight <= 100 && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Medium_100_50);}
        if((18 < textsize && textsize <= 20) && (100 < buttonHeight && buttonHeight <= 125) && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Medium_125_50);}
        if((18 < textsize && textsize <= 20) && (125 < buttonHeight) && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Medium_150_50);}
        if((18 < textsize && textsize <= 20) && buttonHeight <= 100 && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Medium_100_60);}
        if((18 < textsize && textsize <= 20) && (100 < buttonHeight && buttonHeight <= 125) && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Medium_125_60);}
        if((18 < textsize && textsize <= 20) && (125 < buttonHeight) && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Medium_150_60);}
        //Large
        if((20 < textsize && textsize <= 24) && buttonHeight <= 100 && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Large_100_30);}
        if((20 < textsize && textsize <= 24) && (100 < buttonHeight && buttonHeight <= 125) && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Large_125_30);}
        if((20 < textsize && textsize <= 24) && (125 < buttonHeight) && textHeight <= 0.3){super.setTheme(R.style.AppTheme_Large_150_30);}
        if((20 < textsize && textsize <= 24) && buttonHeight <= 100 && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Large_100_40);}
        if((20 < textsize && textsize <= 24) && (100 < buttonHeight && buttonHeight <= 125) && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Large_125_40);}
        if((20 < textsize && textsize <= 24) && (125 < buttonHeight) && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_Large_150_40);}
        if((20 < textsize && textsize <= 24) && buttonHeight <= 100 && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Large_100_50);}
        if((20 < textsize && textsize <= 24) && (100 < buttonHeight && buttonHeight <= 125) && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Large_125_50);}
        if((20 < textsize && textsize <= 24) && (125 < buttonHeight) && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_Large_150_50);}
        if((20 < textsize && textsize <= 24) && buttonHeight <= 100 && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Large_100_60);}
        if((20 < textsize && textsize <= 24) && (100 < buttonHeight && buttonHeight <= 125) && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Large_125_60);}
        if((20 < textsize && textsize <= 24) && (125 < buttonHeight) && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_Large_150_60);}
        //VeryLarge
        if((24 < textsize) && buttonHeight <= 100 && textHeight <= 0.3){super.setTheme(R.style.AppTheme_VeryLarge_100_30);}
        if((24 < textsize) && (100 < buttonHeight && buttonHeight <= 125) && textHeight <= 0.3){super.setTheme(R.style.AppTheme_VeryLarge_125_30);}
        if((24 < textsize) && (125 < buttonHeight) && textHeight <= 0.3){super.setTheme(R.style.AppTheme_VeryLarge_150_30);}
        if((24 < textsize) && buttonHeight <= 100 && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_VeryLarge_100_40);}
        if((24 < textsize) && (100 < buttonHeight && buttonHeight <= 125) && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_VeryLarge_125_40);}
        if((24 < textsize) && (125 < buttonHeight) && (0.3 < textHeight && textHeight <= 0.4)){super.setTheme(R.style.AppTheme_VeryLarge_150_40);}
        if((24 < textsize) && buttonHeight <= 100 && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_VeryLarge_100_50);}
        if((24 < textsize) && (100 < buttonHeight && buttonHeight <= 125) && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_VeryLarge_125_50);}
        if((24 < textsize) && (125 < buttonHeight) && (0.4 < textHeight && textHeight <= 0.5)){super.setTheme(R.style.AppTheme_VeryLarge_150_50);}
        if((24 < textsize) && buttonHeight <= 100 && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_VeryLarge_100_60);}
        if((24 < textsize) && (100 < buttonHeight && buttonHeight <= 125) && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_VeryLarge_125_60);}
        if((24 < textsize) && (125 < buttonHeight) && (0.5 < textHeight)){super.setTheme(R.style.AppTheme_VeryLarge_150_60);}


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        w = size.x;
        h = size.y;

        currentClass = this.getClass().toString();
        myLog("testuser", this.getClass() + "/started", "started", this.getClass().toString(), timeExtension());

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mp = MediaPlayer.create(context, R.raw.happyplace2);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 6, 0);





//        if (toolbar != null) {
//            // change home icon if you wish
//            toolbar.setLogo(R.mipmap.ic_launcher);
//            toolbar.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    if(isPlaying){
//                        mp.pause();
//                        isPlaying = false;
//
//                    }else {
//                        mp.start();
//                        isPlaying = true;
//                        myLog("testuser", this.getClass() + "/neededhelp", "neededhelp", currentClass, timeExtension());
//                    }
//
//                }
//            });
 //       }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {

                Context mContext = MainActivity.mycontext;
                SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
                current = sharedpreferences.getInt("current", 0);
                currentM1 = sharedpreferences.getInt("currentActivityM1", 100);
                final View info = findViewById(R.id.action_info);
                info.setOnTouchListener((new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            Log.e("CURRENT", current + "");
                            Log.e("CURRENTM1", currentM1 + "");
                            switch (current){
                                case 104: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 105: showDialog(getString(R.string.info), getString(R.string.info_dragdrop), context); break;
                                case 107: showDialog(getString(R.string.info), getString(R.string.info_moreinfo), context); break;
                                case 108: showDialog(getString(R.string.info), getString(R.string.info_dragdrop_all), context); break;
                                case 110: showDialog(getString(R.string.info), getString(R.string.info_gap), context); break;
                                case 112: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 113: showDialog(getString(R.string.info), getString(R.string.info_adressmatch), context); break;
                                case 116: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 118: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 120: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 209: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 216: showDialog(getString(R.string.info), "Ziehe ein Verbindungsstück zwischen die Geräte und Verbinde diese mit dem Verbindungsstück.", context); break;
                                case 221: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 223: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 308: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                case 317: showDialog(getString(R.string.info), getString(R.string.info_video), context); break;
                                default:showDialog(getString(R.string.info), getString(R.string.info_noinfo), context);
                            }

                        }
                        return true;

                    }}));

                final View recapv = findViewById(R.id.action_recap);
                recapv.setOnTouchListener((new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP && !recap) {
                            if(current > 102 && current < 124) {
                                Intent myIntent = new Intent(context, Recap.class);
                                startActivity(myIntent);
                            }
                            if(current > 202 && current < 229) {
                                Intent myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul2.Recap.class);
                                startActivity(myIntent);
                            }
                            if(current > 302 && current < 321 && current != 310 && current != 311) {
                                Intent myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul3.Recap.class);
                                startActivity(myIntent);
                            }
                            if(current > 402 && current < 424 && current != 417) {
                                Intent myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul4.Recap.class);
                                startActivity(myIntent);
                            }
                        }
                        return true;

                    }}));

                final View help = findViewById(R.id.action_help);
                help.setOnTouchListener((new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if(event.getAction() == MotionEvent.ACTION_UP)
                            if(isPlaying){
                                mp.pause();
                        isPlaying = false;

                    }else {
                        mp.start();
                        isPlaying = true;
                        myLog("testuser", this.getClass() + "/neededhelp", "neededhelp", currentClass, timeExtension());
                    }
                        return true;
                    }
                }));

            }});
        return true;}

    int MY_PERMISSION_REQUEST_READ_EXTERNAL_STORAGE = 0;
    int MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 0;

    public void myLog(String userId, String activityType, String status, String activity, String extensions) {




        FileOutputStream fileOutputStream;
        OutputStreamWriter outputStreamWriter;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            try {
                File dirApp = new File(Environment.getExternalStorageDirectory() + "/InfoBiTS");
                File dirLog = new File(dirApp + "/logging");
                File logFile = new File(dirLog + "/" + "logfile_" + USERID + ".json");

                // check if directory of app exists and create if not
                if (!dirApp.exists()) {
                    // create parent path if not available
                    dirApp.mkdirs();
                }

                // check if directory of logging exists and create if not
                if (!dirLog.exists()) {
                    // create parent path if not available
                    // this can also be done with mkdir because of lines above
                    dirLog.mkdirs();
                }

                final File file = new File(Environment.getExternalStorageDirectory() + "/InfoBiTS/SharedPrefs/prefs_"+USERID+".txt");
                // check if logFile exists and create if not
                if (!logFile.exists()) {
                    logFile.createNewFile();
                    String l = "[{\"w\":" + w + ", \"h\":" + h + "}";
                    fileOutputStream = new FileOutputStream(logFile, true);
                    outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.append(l); //Speichern der Display Breite und Höhe beim anlegen der Datei
                    Log.e("MYLOG", l);
                    KNOWNUSER=false;
                    outputStreamWriter.flush();

                }else{
                    if(file.exists()){KNOWNUSER=true;}
                }
                fileOutputStream = new FileOutputStream(logFile, true);
                outputStreamWriter = new OutputStreamWriter(fileOutputStream);

                String log = ", \n { \"actor\": { \"name\": \"" + USERID + "\"}, \"verb\": {\"id\":\"" + activityType + "\""
                        + ", \"display\": { \"en-US\": \"" + status + "\"} }, \"object\": { \"id\": \"" + activity + "\", \"extensions\": { " + extensions + " } } }";
                Log.e("MYLOG", log);

                outputStreamWriter.append(log);
                outputStreamWriter.flush();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    public void myLog(String userId, String s) {

        FileOutputStream fileOutputStream;
        OutputStreamWriter outputStreamWriter;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            try {
                File dirApp = new File(Environment.getExternalStorageDirectory() + "/InfoBits");
                File dirLog = new File(dirApp + "/logging");
                File logFile = new File(dirLog + "/" + "logfile_" + USERID + ".json");

                // check if directory of app exists and create if not
                if (!dirApp.exists()) {
                    // create parent path if not available
                    dirApp.mkdirs();
                }

                // check if directory of logging exists and create if not
                if (!dirLog.exists()) {
                    // create parent path if not available
                    // this can also be done with mkdir because of lines above
                    dirLog.mkdirs();
                }
                final File file = new File(Environment.getExternalStorageDirectory() + "/InfoBiTS/SharedPrefs/prefs_"+USERID+".txt");
                // check if logFile exists and create if not
                if (!logFile.exists()) {
                    logFile.createNewFile();
                    String l = "[{\"w\":" + w + ", \"h\":" + h + "}";
                    fileOutputStream = new FileOutputStream(logFile, true);
                    outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.append(l); //Speichern der Display Breite und Höhe beim anlegen der Datei
                    Log.e("MYLOG", l);
                    KNOWNUSER=false;
                    outputStreamWriter.flush();

                }else{if(file.exists()){KNOWNUSER=true;}}
                fileOutputStream = new FileOutputStream(logFile, true);
                outputStreamWriter = new OutputStreamWriter(fileOutputStream);


                String log = s.replace("\n", " ");
                Log.e("MYLOG", log);

                outputStreamWriter.append(log);
                outputStreamWriter.flush();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }


    public String timeExtension(){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    }

    public String textExtension(String text){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"text\": \"" + text + "\"";
    }

    public String enteredExtension(String text, String feedback){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"text\": \"" + text + "\", \"feedback\": \"" + feedback + "\"";
    }

    public String gapEventExtension(String v, String text){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"gap\": \"" + v +"\", \"selected\": \"" + text + "\"";
    }

    public String answerExtension(String question, boolean correct, String givenAnswer, String correctAnswer){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"question\": \"" + question + "\", \"correct\": " + correct + ", \"givenanswer\": \"" + givenAnswer + "\", \"correcanswer\": \"" + correctAnswer + "\"";
    }

    public String buttonEventExtension(MotionEvent event, Button b){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"button\": \"" + b +"\"";
    }

    public String imageButtonEventExtension(MotionEvent event, ImageButton b){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"button\": \"" + b +"\"";
    }

    public String imageViewEventExtension(MotionEvent event, ImageView b){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"button\": \"" + b +"\"";
    }

    public String ivEventExtension(MotionEvent event, ImageView iv){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"button\": \"" + iv +"\"";
    }

    public String viewEventExtension(MotionEvent event, View v){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"movedto\": \"" + v +"\"";
    }

    public String dragEventExtension(DragEvent event, View v, String text){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + "DragEvent" + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"movedto\": \"" + v +"\", \"text\": \"" + text + "\"";
    }

    public String dragEventTextExtension(DragEvent event, View v, String text, String comway){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + "DragEvent" + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"movedto\": \"" + v +"\", \"text\": \"" + text + "\", \"comway\": \"" + comway + "\"";
    }

    public String videoButtonEventExtension(MotionEvent event, Button b, String video, int videoPosition){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"button\": \"" + b +"\", \"video\": \"" + video + "\" , \"videoposition\": " + videoPosition;
    }

    public String videoButtonEventExtension(MotionEvent event, VideoView b, String video, int videoPosition){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"button\": \"" + b +"\", \"video\": \"" + video + "\" , \"videoposition\": " + videoPosition;
    }

    public String floatingButtonEventExtension(MotionEvent event, FloatingActionButton b){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction() + ", \"button\": \"" + b +"\"";
    }

    public String idQAExtension(String id, String q, String a){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"qid\": \"" + id + "\", \"question\" : \"" + q + "\", \"answer\": \"" + a + "\"";
    }

    public String qAExtension(String q, String a){
        return "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"question\" : \"" + q + "\", \"answer\": \"" + a + "\"";
    }

    public void displayMetricsElement(ViewGroup v){
        for(int i = 0; i < v.getChildCount(); i++){
            View child = v.getChildAt(i);
            int array[] = new int[2];
            child.getLocationOnScreen(array);
            int w = child.getWidth();
            int h = child.getHeight();

            myLog("testuser", this.getClass()+"/created", "created", this.getClass().toString(), "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+ ", \"displaymetrics\": { \"element\": \"" + child + "\", \"x1\": \"" + array[0] + "\", \"y1\": \"" + array[1]+ "\", \"x2\": \"" + (array[0]+w) + "\", \"y2\": \"" + (array[1]+h) + "\"}");

            if(child instanceof ViewGroup){
                displayMetricsElement((ViewGroup)child);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){

        myLog("testuser", this.getClass()+"/touched", "touched", this.getClass().toString(), "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+", \"event\": \"" + event + "\", \"x\" :" + event.getX() + ", \"y\": " + event.getY() + ", \"action\": " + event.getAction());

        return super.onTouchEvent(event);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        displayMetricsElement((ViewGroup)findViewById(R.id.main));
    }

    public void showDialog(String title, String message, Context context){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        myLog("testuser",this.getClass()+"/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + title));

        // set title
        TextView mytitle = new TextView(context);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        alertDialog.getWindow().getAttributes();

        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }


        @Override
    public void onBackPressed() {}


    public Intent nextActivity(int activityNumber){
        Intent myIntent;
        switch (activityNumber) {
            case 101: myIntent = new Intent(context, CommunicationStart.class);break;
            case 102: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul1.PretestInformation.class); break;
            case 103: myIntent = new Intent(context, TalkLetterIntro.class); break;
            case 104: myIntent = new Intent(context, VideoTalkAndLetter.class); break;
            case 105: myIntent = new Intent(context, DifferenceTalkLetter.class); break;
            case 106: myIntent = new Intent(context, LinkTalkLetter.class);  break;
            case 107: myIntent = new Intent(context, CommunicationUsage.class); break;
            case 108: myIntent = new Intent(context, SyncVsAsync.class); break;
            case 109: myIntent = new Intent(context, UsecaseActivity.class); break;
            case 110: myIntent = new Intent(context, FillTheGapActivity.class); break;
            case 111: myIntent = new Intent(context, VideoAdressIntro.class); break;
            case 112: myIntent = new Intent(context, VideoAdressesActivity.class); break;
            case 113: myIntent = new Intent(context, MessageComponentMatch.class); break;
            case 114: myIntent = new Intent(context, ImageUseIntro.class); break;
            case 115: myIntent = new Intent(context, UrheberrechtIntro .class); break;
            case 116: myIntent = new Intent(context, Urheberrecht.class); break;
            case 117: myIntent = new Intent(context, CreativeCommonsIntro.class); break;
            case 118: myIntent = new Intent(context, CreativeCommons.class); break;
            case 119: myIntent = new Intent(context, OERIntro.class); break;
            case 120: myIntent = new Intent(context, OER.class); break;
            case 121: myIntent = new Intent(context, SortPolicy.class); break;
            case 122: myIntent = new Intent(context, PolicyTrueFalse.class); break;
            case 123: myIntent = new Intent(context, UsecaseActivity2.class); break;
            case 124: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul1.PosttestInformation.class); break;
            case 125: myIntent = new Intent(context, CommunicationEnd.class); break;

            case 201: myIntent = new Intent(context, InternetStart.class); break;
            case 202: myIntent = new Intent(context, PretestInformation.class); break;
            case 203: myIntent = new Intent(context, InternetInfo.class); break;
            case 204: myIntent = new Intent(context, DataInfo.class); break;
            case 205: myIntent = new Intent(context, Binaersystem.class); break;
            case 206: myIntent = new Intent(context, DezToBin.class); break;
            case 207: myIntent = new Intent(context, DezToBin2.class); break;
            case 208: myIntent = new Intent(context, Datapackage.class); break;
            case 209: myIntent = new Intent(context, DPVideo.class); break;
            case 210: myIntent = new Intent(context, IPIntro.class); break;
            case 211: myIntent = new Intent(context, Devices.class); break;
            case 212: myIntent = new Intent(context, NetworkConnectedActivity.class); break;
            case 213: myIntent = new Intent(context, NetworkInfo5.class); break;
            case 214: myIntent = new Intent(context, NetworkConnectedActivity5.class); break;
            case 215: myIntent = new Intent(context, ConnectionpieceInfo.class); break;
            case 216: myIntent = new Intent(context, NetworkSwitchConnectedActivity.class); break;
            case 217: myIntent = new Intent(context, NetworkCompareHubSwitchRouterActivity.class); break;
            case 218: myIntent = new Intent(context, NetworkRouter.class); break;
            case 219: myIntent = new Intent(context, DevicesInInternetInfo.class); break;
            case 220: myIntent = new Intent(context, URLIntro .class); break;
            case 221: myIntent = new Intent(context, URLVideo.class); break;
            case 222: myIntent = new Intent(context, DNSIntro.class); break;
            case 223: myIntent = new Intent(context, DNSVideo.class); break;
            case 224: myIntent = new Intent(context, URL1.class); break;
            case 225: myIntent = new Intent(context, URL2.class); break;
            case 226: myIntent = new Intent(context, IpUrlDns.class); break;
            case 227: myIntent = new Intent(context, HistoryIntro.class); break;
            case 228: myIntent = new Intent(context, InternetQuizInfo.class); break;
            case 229: myIntent = new Intent(context, PosttestInformation.class); break;
            case 230: myIntent = new Intent(context, InternetEnd.class); break;

            case 301: myIntent = new Intent(context, ConnectionStart.class); break;
            case 302: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul3.PretestInformation.class); break;
            case 303: myIntent = new Intent(context, ConnectionIntro.class); break;
            case 304: myIntent = new Intent(context, Connected.class); break;
            case 305: myIntent = new Intent(context, ConnectedSort.class); break;
            case 306: myIntent = new Intent(context, Cable.class); break;
            case 307: myIntent = new Intent(context, CableVideoIntro.class); break;
            case 308: myIntent = new Intent(context, CableVideo.class); break;
            case 309: myIntent = new Intent(context, CableToNoCable.class); break;
            case 310: myIntent = new Intent(context, InternetWords.class); break;
            case 311: myIntent = new Intent(context, PreLanManWan.class); break;
            case 312: myIntent = new Intent(context, LanManWan.class); break;
            case 313: myIntent = new Intent(context, WlanWifi.class); break;
            case 314: myIntent = new Intent(context, Mobilfunk.class); break;
            case 315: myIntent = new Intent(context, InternetWordsSolution.class); break;
            case 316: myIntent = new Intent(context, ConnectionVideoIntro.class); break;
            case 317: myIntent = new Intent(context, ConnectionVideo.class); break;
            case 318: myIntent = new Intent(context, DifferenceWLANMobil.class); break;
            case 319: myIntent = new Intent(context, GameBookIntro.class); break;
            case 320: myIntent = new Intent(context, WLANQuizInfo.class); break;
            case 321: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul3.PosttestInformation.class); break;
            case 322: myIntent = new Intent(context, ConnectionEnd.class); break;

            case 401: myIntent = new Intent(context, DatasecurityStart.class); break;
            case 402: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul4.PretestInformation.class); break;
            case 403: myIntent = new Intent(context, PrePassword.class); break;
            case 404: myIntent = new Intent(context, DefinitionPW.class); break;
            case 405: myIntent = new Intent(context, SafePWInfo.class); break;
            case 406: myIntent = new Intent(context, SafePWRequirements.class); break;
            case 407: myIntent = new Intent(context, CreatePW.class); break;
            case 408: myIntent = new Intent(context, CreatePWQA.class); break;
            case 409: myIntent = new Intent(context, SafePWCreationStratagy.class); break;
            case 410: myIntent = new Intent(context, CreateSafePW.class); break;
            case 411: myIntent = new Intent(context, IsPWSafe.class); break;
            case 412: myIntent = new Intent(context, Saftymechanics.class); break;
            case 413: myIntent = new Intent(context, SafeMechanics.class); break;
            case 414: myIntent = new Intent(context, MechanicsInfo.class); break;
            case 415: myIntent = new Intent(context, IsMechanicsSafe.class); break;
            case 416: myIntent = new Intent(context, PWmcInfo.class); break;
            case 417: myIntent = new Intent(context, SecurityIntro.class); break;
            case 418: myIntent = new Intent(context, DataDefinition.class); break;
            case 419: myIntent = new Intent(context, DataMC.class); break;
            case 420: myIntent = new Intent(context, DataUsage.class); break;
            case 421: myIntent = new Intent(context, Datasecurity.class); break;
            case 422: myIntent = new Intent(context, DifferenceDD.class); break;
            case 423: myIntent = new Intent(context, DatasecuritySzenarioInfo .class); break;
            case 424: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul4.PosttestInformation.class); break;
            case 425: myIntent = new Intent(context, DataSecEnd.class); break;

            default: myIntent = new Intent(context, AppStart.class);
        }
        return myIntent;
    }

    public int getActivityNumber(String currentClass){
        Log.e("_NEW_CURRENT", currentClass.toString());
        int currentNumber = 100;
        if(currentClass.equals("CommunicationStart")){currentNumber = 101;}
        if(currentClass.equals("KUT_K_Pre")){currentNumber = 102;}
        if(currentClass.equals("TalkLetterIntro")){currentNumber = 103;}
        if(currentClass.equals("VideoTalkAndLetter")){currentNumber = 104;}
        if(currentClass.equals("DifferenceTalkLetter")){currentNumber = 105;}
        if(currentClass.equals("LinkTalkLetter")){currentNumber = 106;}
        if(currentClass.equals("CommunicationUsage")){currentNumber = 107;}
        if(currentClass.equals("SyncVsAsync")){currentNumber = 108;}
        if(currentClass.equals("UsecaseActivity")){currentNumber = 109;}
        if(currentClass.equals("FillTheGapActivity")){currentNumber = 110;}
        if(currentClass.equals("VideoAdressIntro")){currentNumber = 111;}
        if(currentClass.equals("VideoAdressesActivity")){currentNumber = 112;}
        if(currentClass.equals("MessageComponentMatch")){currentNumber = 113;}
        if(currentClass.equals("ImageUseIntro")){currentNumber = 114;}
        if(currentClass.equals("UrheberrechtIntro")){currentNumber = 115;}
        if(currentClass.equals("Urheberrecht")){currentNumber = 116;}
        if(currentClass.equals("CreativeCommonsIntro")){currentNumber = 117;}
        if(currentClass.equals("CreativeCommons")){currentNumber = 118;}
        if(currentClass.equals("OERIntro")){currentNumber = 119;}
        if(currentClass.equals("OER")){currentNumber = 120;}
        if(currentClass.equals("SortPolicy")){currentNumber = 121;}
        if(currentClass.equals("PolicyTrueFalse")){currentNumber = 122;}
        if(currentClass.equals("UsecaseActivity2")){currentNumber = 123;}
        if(currentClass.equals("PosttestInformation")){currentNumber = 124;}
        if(currentClass.equals("CommunicationEnd")){currentNumber = 125;}

        if(currentClass.equals("InternetStart")){currentNumber = 201;}
        if(currentClass.equals("PretestInformation2")){currentNumber = 202;}
        if(currentClass.equals("InternetInfo")){currentNumber = 203;}
        if(currentClass.equals("DataInfo")){currentNumber = 204;}
        if(currentClass.equals("Binaersystem")){currentNumber = 205;}
        if(currentClass.equals("DezToBin")){currentNumber = 206;}
        if(currentClass.equals("DezToBin2")){currentNumber = 207;}
        if(currentClass.equals("Datapackage")){currentNumber = 208;}
        if(currentClass.equals("DPVideo")){currentNumber = 209;}
        if(currentClass.equals("IPIntro")){currentNumber = 210;}
        if(currentClass.equals("Devices")){currentNumber = 211;}
        if(currentClass.equals("NetworkConnectedActivity")){currentNumber = 212;}
        if(currentClass.equals("NetworkInfo5")){currentNumber = 213;}
        if(currentClass.equals("NetworkConnectedActivity5")){currentNumber = 214;}
        if(currentClass.equals("ConnectionpieceInfo")){currentNumber = 215;}
        if(currentClass.equals("NetworkSwitchConnectedActivity")){currentNumber = 216;}
        if(currentClass.equals("NetworkCompareHubSwitchRouterActivity")){currentNumber = 217;}
        if(currentClass.equals("NetworkRouter")){currentNumber = 218;}
        if(currentClass.equals("DevicesInInternetInfo")){currentNumber = 219;}
        if(currentClass.equals("URLIntro ")){currentNumber = 220;}
        if(currentClass.equals("URLVideo")){currentNumber = 221;}
        if(currentClass.equals("DNSIntro")){currentNumber = 222;}
        if(currentClass.equals("DNSVideo")){currentNumber = 223;}
        if(currentClass.equals("URL1")){currentNumber = 224;}
        if(currentClass.equals("URL2")){currentNumber = 225;}
        if(currentClass.equals("IpUrlDns")){currentNumber = 226;}
        if(currentClass.equals("HistoryIntro")){currentNumber = 227;}
        if(currentClass.equals("InternetQuizInfo")){currentNumber = 228;}
        if(currentClass.equals("PosttestInformation2")){currentNumber = 229;}
        if(currentClass.equals("InternetEnd")){currentNumber = 230;}

        if(currentClass.equals("ConnectionStart")){currentNumber = 301;}
        if(currentClass.equals("PretestInformation3")){currentNumber = 302;}
        if(currentClass.equals("ConnectionIntro")){currentNumber = 303;}
        if(currentClass.equals("Connected")){currentNumber = 304;}
        if(currentClass.equals("ConnectedSort")){currentNumber = 305;}
        if(currentClass.equals("Cable")){currentNumber = 306;}
        if(currentClass.equals("CableVideoIntro")){currentNumber = 307;}
        if(currentClass.equals("CableVideo")){currentNumber = 308;}
        if(currentClass.equals("CableToNoCable")){currentNumber = 309;}
        if(currentClass.equals("InternetWords ")){currentNumber = 310;}
        if(currentClass.equals("PreLanManWan")){currentNumber = 311;}
        if(currentClass.equals("LanManWan")){currentNumber = 312;}
        if(currentClass.equals("WlanWifi")){currentNumber = 313;}
        if(currentClass.equals("Mobilfunk")){currentNumber = 314;}
        if(currentClass.equals("InternetWordsSolution")){currentNumber = 315;}
        if(currentClass.equals("ConnectionVideoIntro")){currentNumber = 316;}
        if(currentClass.equals("ConnectionVideo")){currentNumber = 317;}
        if(currentClass.equals("DifferenceWLANMobil")){currentNumber = 318;}
        if(currentClass.equals("GameBookIntro")){currentNumber = 319;}
        if(currentClass.equals("WLANQuizInfo")){currentNumber = 320;}
        if(currentClass.equals("PosttestInformation3")){currentNumber = 321;}
        if(currentClass.equals("ConnectionEnd")){currentNumber = 322;}

        if(currentClass.equals("DatasecurityStart")){currentNumber = 401;}
        if(currentClass.equals("PretestInformation4")){currentNumber = 402;}
        if(currentClass.equals("PrePassword")){currentNumber = 403;}
        if(currentClass.equals("DefinitionPW")){currentNumber = 404;}
        if(currentClass.equals("SafePWInfo")){currentNumber = 405;}
        if(currentClass.equals("SafePWRequirements")){currentNumber = 406;}
        if(currentClass.equals("CreatePW")){currentNumber = 407;}
        if(currentClass.equals("CreatePWQA")){currentNumber = 408;}
        if(currentClass.equals("SafePWCreationStratagy")){currentNumber = 409;}
        if(currentClass.equals("CreateSafePW")){currentNumber = 410;}
        if(currentClass.equals("IsPWSafe")){currentNumber = 411;}
        if(currentClass.equals("Saftymechanics")){currentNumber = 412;}
        if(currentClass.equals("SafeMechanics ")){currentNumber = 413;}
        if(currentClass.equals("MechanicsInfo")){currentNumber = 414;}
        if(currentClass.equals("IsMechanicsSafe")){currentNumber = 415;}
        if(currentClass.equals("PWmcInfo")){currentNumber = 416;}
        if(currentClass.equals("SecurityIntro")){currentNumber = 417;}
        if(currentClass.equals("DataDefinition")){currentNumber = 418;}
        if(currentClass.equals("DataMC")){currentNumber = 419;}
        if(currentClass.equals("DataUsage")){currentNumber = 420;}
        if(currentClass.equals("Datasecurity")){currentNumber = 421;}
        if(currentClass.equals("DifferenceDD")){currentNumber = 422;}
        if(currentClass.equals("DatasecuritySzenarioInfo")){currentNumber = 423;}
        if(currentClass.equals("PosttestInformation4")){currentNumber = 424;}
        if(currentClass.equals("DataSecEnd")){currentNumber = 425;}

        Log.e("_NEW_CURRENT", currentNumber+"");
        return currentNumber;
    }

    public void restartAfterRecap(){
        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        if(100 <= current && current < 200) {
            currentM1 = sharedpreferences.getInt("currentActivityM1", 100);
            Intent myIntent;
            switch (currentM1) {
                case 101: myIntent = new Intent(context, CommunicationStart.class);break;
                case 102: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul1.PretestInformation.class);break;
                case 103: myIntent = new Intent(context, TalkLetterIntro.class);break;
                case 104: myIntent = new Intent(context, VideoTalkAndLetter.class);break;
                case 105: myIntent = new Intent(context, DifferenceTalkLetter.class);break;
                case 106: myIntent = new Intent(context, LinkTalkLetter.class);break;
                case 107: myIntent = new Intent(context, CommunicationUsage.class);break;
                case 108: myIntent = new Intent(context, SyncVsAsync.class);break;
                case 109: myIntent = new Intent(context, UsecaseActivity.class);break;
                case 110: myIntent = new Intent(context, FillTheGapActivity.class);break;
                case 111: myIntent = new Intent(context, VideoAdressIntro.class);break;
                case 112: myIntent = new Intent(context, VideoAdressesActivity.class);break;
                case 113: myIntent = new Intent(context, MessageComponentMatch.class);break;
                case 114: myIntent = new Intent(context, ImageUseIntro.class);break;
                case 115: myIntent = new Intent(context, UrheberrechtIntro.class);break;
                case 116: myIntent = new Intent(context, Urheberrecht.class);break;
                case 117: myIntent = new Intent(context, CreativeCommonsIntro.class);break;
                case 118: myIntent = new Intent(context, CreativeCommons.class);break;
                case 119: myIntent = new Intent(context, OERIntro.class);break;
                case 120: myIntent = new Intent(context, OER.class);break;
                case 121: myIntent = new Intent(context, SortPolicy.class);break;
                case 122: myIntent = new Intent(context, PolicyTrueFalse.class);break;
                case 123: myIntent = new Intent(context, UsecaseActivity2.class);break;
                case 124: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul1.PosttestInformation.class);break;
                case 125: myIntent = new Intent(context, CommunicationEnd.class);break;
                default: myIntent = new Intent(context, CommunicationStart.class);
            }
            myrecap = false;
            startActivity(myIntent);
            finish();
        }
        if(200 <= current && current < 300) {
            currentM2 = sharedpreferences.getInt("currentActivityM2", 200);
            Intent myIntent;
            Log.e("CURRENTM2", currentM2+"");
            switch (currentM2) {
                case 201: myIntent = new Intent(context, InternetStart.class); break;
                case 202: myIntent = new Intent(context, PretestInformation.class); break;
                case 203: myIntent = new Intent(context, InternetInfo.class); break;
                case 204: myIntent = new Intent(context, DataInfo.class); break;
                case 205: myIntent = new Intent(context, Binaersystem.class); break;
                case 206: myIntent = new Intent(context, DezToBin.class); break;
                case 207: myIntent = new Intent(context, DezToBin2.class); break;
                case 208: myIntent = new Intent(context, Datapackage.class); break;
                case 209: myIntent = new Intent(context, DPVideo.class); break;
                case 210: myIntent = new Intent(context, IPIntro.class); break;
                case 211: myIntent = new Intent(context, Devices.class); break;
                case 212: myIntent = new Intent(context, NetworkConnectedActivity.class); break;
                case 213: myIntent = new Intent(context, NetworkInfo5.class); break;
                case 214: myIntent = new Intent(context, NetworkConnectedActivity5.class); break;
                case 215: myIntent = new Intent(context, ConnectionpieceInfo.class); break;
                case 216: myIntent = new Intent(context, NetworkSwitchConnectedActivity.class); break;
                case 217: myIntent = new Intent(context, NetworkCompareHubSwitchRouterActivity.class); break;
                case 218: myIntent = new Intent(context, NetworkRouter.class); break;
                case 219: myIntent = new Intent(context, DevicesInInternetInfo.class); break;
                case 220: myIntent = new Intent(context, URLIntro.class); break;
                case 221: myIntent = new Intent(context, URLVideo.class); break;
                case 222: myIntent = new Intent(context, DNSIntro.class); break;
                case 223: myIntent = new Intent(context, DNSVideo.class); break;
                case 224: myIntent = new Intent(context, URL1.class); break;
                case 225: myIntent = new Intent(context, URL2.class); break;
                case 226: myIntent = new Intent(context, IpUrlDns.class); break;
                case 227: myIntent = new Intent(context, HistoryIntro.class); break;
                case 228: myIntent = new Intent(context, InternetQuizInfo.class); break;
                case 229: myIntent = new Intent(context, PosttestInformation.class); break;
                case 230: myIntent = new Intent(context, InternetEnd.class); break;
                default: myIntent = new Intent(context, InternetStart.class);
            }
            myrecap = false;
            startActivity(myIntent);
            finish();
        }
        if(300 <= current && current < 400) {
            currentM3= sharedpreferences.getInt("currentActivityM3", 300);
            Intent myIntent;
            Log.e("CURRENTM3", currentM3+"");
            switch (currentM3) {
                case 301: myIntent = new Intent(context, ConnectionStart.class); break;
                case 302: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul3.PretestInformation.class); break;
                case 303: myIntent = new Intent(context, ConnectionIntro.class); break;
                case 304: myIntent = new Intent(context, Connected.class); break;
                case 305: myIntent = new Intent(context, ConnectedSort.class); break;
                case 306: myIntent = new Intent(context, Cable.class); break;
                case 307: myIntent = new Intent(context, CableVideoIntro.class); break;
                case 308: myIntent = new Intent(context, CableVideo.class); break;
                case 309: myIntent = new Intent(context, CableToNoCable.class); break;
                case 310: myIntent = new Intent(context, InternetWords.class); break;
                case 311: myIntent = new Intent(context, PreLanManWan.class); break;
                case 312: myIntent = new Intent(context, LanManWan.class); break;
                case 313: myIntent = new Intent(context, WlanWifi.class); break;
                case 314: myIntent = new Intent(context, Mobilfunk.class); break;
                case 315: myIntent = new Intent(context, InternetWordsSolution.class); break;
                case 316: myIntent = new Intent(context, ConnectionVideoIntro.class); break;
                case 317: myIntent = new Intent(context, ConnectionVideo.class); break;
                case 318: myIntent = new Intent(context, DifferenceWLANMobil.class); break;
                case 319: myIntent = new Intent(context, GameBookIntro.class); break;
                case 320: myIntent = new Intent(context, WLANQuizInfo.class); break;
                case 321: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul3.PosttestInformation.class); break;
                case 322: myIntent = new Intent(context, ConnectionEnd.class); break;
                default: myIntent = new Intent(context, ConnectionStart.class);
            }
            myrecap = false;
            startActivity(myIntent);
            finish();
        }
        if(400 <= current && current < 500) {
            currentM4= sharedpreferences.getInt("currentActivityM4", 400);
            Intent myIntent;
            Log.e("CURRENTM4", currentM4+"");
            switch (currentM4) {
                case 401: myIntent = new Intent(context, DatasecurityStart.class); break;
                case 402: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul4.PosttestInformation.class); break;
                case 403: myIntent = new Intent(context, PrePassword.class); break;
                case 404: myIntent = new Intent(context, DefinitionPW.class); break;
                case 405: myIntent = new Intent(context, SafePWInfo.class); break;
                case 406: myIntent = new Intent(context, SafePWRequirements.class); break;
                case 407: myIntent = new Intent(context, CreatePW.class); break;
                case 408: myIntent = new Intent(context, CreatePWQA.class); break;
                case 409: myIntent = new Intent(context, SafePWCreationStratagy.class); break;
                case 410: myIntent = new Intent(context, CreateSafePW.class); break;
                case 411: myIntent = new Intent(context, IsPWSafe.class); break;
                case 412: myIntent = new Intent(context, Saftymechanics.class); break;
                case 413: myIntent = new Intent(context, SafeMechanics.class); break;
                case 414: myIntent = new Intent(context, MechanicsInfo.class); break;
                case 415: myIntent = new Intent(context, IsMechanicsSafe.class); break;
                case 416: myIntent = new Intent(context, PWmcInfo.class); break;
                case 417: myIntent = new Intent(context, SecurityIntro.class); break;
                case 418: myIntent = new Intent(context, DataDefinition.class); break;
                case 419: myIntent = new Intent(context, DataMC.class); break;
                case 420: myIntent = new Intent(context, DataUsage.class); break;
                case 421: myIntent = new Intent(context, Datasecurity.class); break;
                case 422: myIntent = new Intent(context, DifferenceDD.class); break;
                case 423: myIntent = new Intent(context, DatasecuritySzenarioInfo.class); break;
                case 424: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul4.PosttestInformation.class); break;
                case 425: myIntent = new Intent(context, DataSecEnd.class); break;
                default: myIntent = new Intent(context, DatasecurityStart.class);
            }
            myrecap = false;
            startActivity(myIntent);
            finish();
        }
    }

    public void readSharedPrefs() throws FileNotFoundException, NumberFormatException{
        FileInputStream is;
        BufferedReader reader;
        final File file = new File(Environment.getExternalStorageDirectory() + "/InfoBiTS/SharedPrefs/prefs_"+USERID+".txt");

        if (file.exists()) {
            is = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(is));
            String line = null;
            int counter = 0;
            SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            while(counter <= 296){
                Log.e("StackOverflow", line + "; " + counter);
                try {
                    line = reader.readLine();
                    if (line == null){
                        line = "0";
                    }
                    switch (counter){
                        case 0: editor.putInt("Volume", Integer.parseInt(line)); break;
                        case 1: editor.putInt("ButtonHeight", Integer.parseInt(line)); break;
                        case 2: float f = (Float.parseFloat(line)/100); editor.putFloat("TextHeight", f);break;
                        case 3: editor.putInt("TextSize", Integer.parseInt(line)); break;
                        case 4: editor.putInt("current", Integer.parseInt(line)); break;
                        case 5: editor.putInt("currentActivityM1", Integer.parseInt(line)); break;
                        case 6: editor.putInt("currentActivityM2", Integer.parseInt(line)); break;
                        case 7: editor.putInt("currentActivityM3", Integer.parseInt(line)); break;
                        case 8: editor.putInt("currentActivityM4", Integer.parseInt(line)); break;
                        case 9: editor.putInt("m1_pre_0", Integer.parseInt(line)); break;
                        case 10: editor.putInt("m1_pre_1", Integer.parseInt(line)); break;
                        case 11: editor.putInt("m1_pre_2", Integer.parseInt(line)); break;
                        case 12: editor.putInt("m1_pre_3", Integer.parseInt(line)); break;
                        case 13: editor.putInt("m1_pre_4", Integer.parseInt(line)); break;
                        case 14: editor.putInt("m1_pre_5", Integer.parseInt(line)); break;
                        case 15: editor.putInt("m1_pre_6", Integer.parseInt(line)); break;
                        case 16: editor.putInt("m1_pre_7", Integer.parseInt(line)); break;
                        case 17: editor.putInt("m1_pre_8", Integer.parseInt(line)); break;
                        case 18: editor.putInt("m1_pre_9", Integer.parseInt(line)); break;
                        case 19: editor.putInt("m1_pre_10", Integer.parseInt(line)); break;
                        case 20: editor.putInt("m1_pre_11", Integer.parseInt(line)); break;
                        case 21: editor.putInt("m1_pre_12", Integer.parseInt(line)); break;
                        case 22: editor.putInt("m1_pre_13", Integer.parseInt(line)); break;
                        case 23: editor.putInt("m1_pre_14", Integer.parseInt(line)); break;
                        case 24: editor.putInt("m1_pre_15", Integer.parseInt(line)); break;
                        case 25: editor.putInt("m1_post_0", Integer.parseInt(line)); break;
                        case 26: editor.putInt("m1_post_1", Integer.parseInt(line)); break;
                        case 27: editor.putInt("m1_post_2", Integer.parseInt(line)); break;
                        case 28: editor.putInt("m1_post_3", Integer.parseInt(line)); break;
                        case 29: editor.putInt("m1_post_4", Integer.parseInt(line)); break;
                        case 30: editor.putInt("m1_post_5", Integer.parseInt(line)); break;
                        case 31: editor.putInt("m1_post_6", Integer.parseInt(line)); break;
                        case 32: editor.putInt("m1_post_7", Integer.parseInt(line)); break;
                        case 33: editor.putInt("m1_post_8", Integer.parseInt(line)); break;
                        case 34: editor.putInt("m1_post_9", Integer.parseInt(line)); break;
                        case 35: editor.putInt("m1_post_10", Integer.parseInt(line)); break;
                        case 36: editor.putInt("m1_post_11", Integer.parseInt(line)); break;
                        case 37: editor.putInt("m1_post_12", Integer.parseInt(line)); break;
                        case 38: editor.putInt("m1_post_13", Integer.parseInt(line)); break;
                        case 39: editor.putInt("m1_post_14", Integer.parseInt(line)); break;
                        case 40: editor.putInt("m1_post_15", Integer.parseInt(line)); break;
                        case 41: editor.putInt("m2_pre_0", Integer.parseInt(line)); break;
                        case 42: editor.putInt("m2_pre_1", Integer.parseInt(line)); break;
                        case 43: editor.putInt("m2_pre_2", Integer.parseInt(line)); break;
                        case 44: editor.putInt("m2_pre_3", Integer.parseInt(line)); break;
                        case 45: editor.putInt("m2_pre_4", Integer.parseInt(line)); break;
                        case 46: editor.putInt("m2_pre_5", Integer.parseInt(line)); break;
                        case 47: editor.putInt("m2_pre_6", Integer.parseInt(line)); break;
                        case 48: editor.putInt("m2_pre_7", Integer.parseInt(line)); break;
                        case 49: editor.putInt("m2_pre_8", Integer.parseInt(line)); break;
                        case 50: editor.putInt("m2_pre_9", Integer.parseInt(line)); break;
                        case 51: editor.putInt("m2_pre_10", Integer.parseInt(line)); break;
                        case 52: editor.putInt("m2_pre_11", Integer.parseInt(line)); break;
                        case 53: editor.putInt("m2_pre_12", Integer.parseInt(line)); break;
                        case 54: editor.putInt("m2_pre_13", Integer.parseInt(line)); break;
                        case 55: editor.putInt("m2_pre_14", Integer.parseInt(line)); break;
                        case 56: editor.putInt("m2_pre_15", Integer.parseInt(line)); break;
                        case 57: editor.putInt("m2_pre_16", Integer.parseInt(line)); break;
                        case 58: editor.putInt("m2_pre_17", Integer.parseInt(line)); break;
                        case 59: editor.putInt("m2_pre_18", Integer.parseInt(line)); break;
                        case 60: editor.putInt("m2_pre_19", Integer.parseInt(line)); break;
                        case 61: editor.putInt("m2_pre_20", Integer.parseInt(line)); break;
                        case 62: editor.putInt("m2_post_0", Integer.parseInt(line)); break;
                        case 63: editor.putInt("m2_post_1", Integer.parseInt(line)); break;
                        case 64: editor.putInt("m2_post_2", Integer.parseInt(line)); break;
                        case 65: editor.putInt("m2_post_3", Integer.parseInt(line)); break;
                        case 66: editor.putInt("m2_post_4", Integer.parseInt(line)); break;
                        case 67: editor.putInt("m2_post_5", Integer.parseInt(line)); break;
                        case 68: editor.putInt("m2_post_6", Integer.parseInt(line)); break;
                        case 69: editor.putInt("m2_post_7", Integer.parseInt(line)); break;
                        case 70: editor.putInt("m2_post_8", Integer.parseInt(line)); break;
                        case 71: editor.putInt("m2_post_9", Integer.parseInt(line)); break;
                        case 72: editor.putInt("m2_post_10", Integer.parseInt(line)); break;
                        case 73: editor.putInt("m2_post_11", Integer.parseInt(line)); break;
                        case 74: editor.putInt("m2_post_12", Integer.parseInt(line)); break;
                        case 75: editor.putInt("m2_post_13", Integer.parseInt(line)); break;
                        case 76: editor.putInt("m2_post_14", Integer.parseInt(line)); break;
                        case 77: editor.putInt("m2_post_15", Integer.parseInt(line)); break;
                        case 78: editor.putInt("m2_post_16", Integer.parseInt(line)); break;
                        case 79: editor.putInt("m2_post_17", Integer.parseInt(line)); break;
                        case 80: editor.putInt("m2_post_18", Integer.parseInt(line)); break;
                        case 81: editor.putInt("m2_post_19", Integer.parseInt(line)); break;
                        case 82: editor.putInt("m2_post_20", Integer.parseInt(line)); break;
                        case 83: editor.putInt("m3_pre_0", Integer.parseInt(line)); break;
                        case 84: editor.putInt("m3_pre_1", Integer.parseInt(line)); break;
                        case 85: editor.putInt("m3_pre_2", Integer.parseInt(line)); break;
                        case 86: editor.putInt("m3_pre_3", Integer.parseInt(line)); break;
                        case 87: editor.putInt("m3_pre_4", Integer.parseInt(line)); break;
                        case 88: editor.putInt("m3_pre_5", Integer.parseInt(line)); break;
                        case 89: editor.putInt("m3_pre_6", Integer.parseInt(line)); break;
                        case 90: editor.putInt("m3_pre_7", Integer.parseInt(line)); break;
                        case 91: editor.putInt("m3_post_0", Integer.parseInt(line)); break;
                        case 92: editor.putInt("m3_post_1", Integer.parseInt(line)); break;
                        case 93: editor.putInt("m3_post_2", Integer.parseInt(line)); break;
                        case 94: editor.putInt("m3_post_3", Integer.parseInt(line)); break;
                        case 95: editor.putInt("m3_post_4", Integer.parseInt(line)); break;
                        case 96: editor.putInt("m3_post_5", Integer.parseInt(line)); break;
                        case 97: editor.putInt("m3_post_6", Integer.parseInt(line)); break;
                        case 98: editor.putInt("m3_post_7", Integer.parseInt(line)); break;
                        case 99: editor.putInt("m4_pre_0", Integer.parseInt(line)); break;
                        case 100: editor.putInt("m4_pre_1", Integer.parseInt(line)); break;
                        case 101: editor.putInt("m4_pre_2", Integer.parseInt(line)); break;
                        case 102: editor.putInt("m4_pre_3", Integer.parseInt(line)); break;
                        case 103: editor.putInt("m4_pre_4", Integer.parseInt(line)); break;
                        case 104: editor.putInt("m4_pre_5", Integer.parseInt(line)); break;
                        case 105: editor.putInt("m4_pre_6", Integer.parseInt(line)); break;
                        case 106: editor.putInt("m4_pre_7", Integer.parseInt(line)); break;
                        case 107: editor.putInt("m4_pre_8", Integer.parseInt(line)); break;
                        case 108: editor.putInt("m4_pre_9", Integer.parseInt(line)); break;
                        case 109: editor.putInt("m4_pre_10", Integer.parseInt(line)); break;
                        case 110: editor.putInt("m4_pre_11", Integer.parseInt(line)); break;
                        case 111: editor.putInt("m4_pre_12", Integer.parseInt(line)); break;
                        case 112: editor.putInt("m4_pre_13", Integer.parseInt(line)); break;
                        case 113: editor.putInt("m4_post_0", Integer.parseInt(line)); break;
                        case 114: editor.putInt("m4_post_1", Integer.parseInt(line)); break;
                        case 115: editor.putInt("m4_post_2", Integer.parseInt(line)); break;
                        case 116: editor.putInt("m4_post_3", Integer.parseInt(line)); break;
                        case 117: editor.putInt("m4_post_4", Integer.parseInt(line)); break;
                        case 118: editor.putInt("m4_post_5", Integer.parseInt(line)); break;
                        case 119: editor.putInt("m4_post_6", Integer.parseInt(line)); break;
                        case 120: editor.putInt("m4_post_7", Integer.parseInt(line)); break;
                        case 121: editor.putInt("m4_post_8", Integer.parseInt(line)); break;
                        case 122: editor.putInt("m4_post_9", Integer.parseInt(line)); break;
                        case 123: editor.putInt("m4_post_10", Integer.parseInt(line)); break;
                        case 124: editor.putInt("m4_post_11", Integer.parseInt(line)); break;
                        case 125: editor.putInt("m4_post_12", Integer.parseInt(line)); break;
                        case 126: editor.putInt("m4_post_13", Integer.parseInt(line)); break;
                        case 127: editor.putInt("m1_kut_pre_0", Integer.parseInt(line)); break;
                        case 128: editor.putInt("m1_kut_pre_1", Integer.parseInt(line)); break;
                        case 129: editor.putInt("m1_kut_pre_2", Integer.parseInt(line)); break;
                        case 130: editor.putInt("m1_kut_pre_3", Integer.parseInt(line)); break;
                        case 131: editor.putInt("m1_kut_pre_4", Integer.parseInt(line)); break;
                        case 132: editor.putInt("m1_kut_pre_5", Integer.parseInt(line)); break;
                        case 133: editor.putInt("m1_kut_pre_6", Integer.parseInt(line)); break;
                        case 134: editor.putInt("m1_kut_pre_7", Integer.parseInt(line)); break;
                        case 135: editor.putInt("m1_kut_post_0", Integer.parseInt(line)); break;
                        case 136: editor.putInt("m1_kut_post_1", Integer.parseInt(line)); break;
                        case 137: editor.putInt("m1_kut_post_2", Integer.parseInt(line)); break;
                        case 138: editor.putInt("m1_kut_post_3", Integer.parseInt(line)); break;
                        case 139: editor.putInt("m1_kut_post_4", Integer.parseInt(line)); break;
                        case 140: editor.putInt("m1_kut_post_5", Integer.parseInt(line)); break;
                        case 141: editor.putInt("m1_kut_post_6", Integer.parseInt(line)); break;
                        case 142: editor.putInt("m1_kut_post_7", Integer.parseInt(line)); break;
                        case 143: editor.putInt("m2_kut_pre_0", Integer.parseInt(line)); break;
                        case 144: editor.putInt("m2_kut_pre_1", Integer.parseInt(line)); break;
                        case 145: editor.putInt("m2_kut_pre_2", Integer.parseInt(line)); break;
                        case 146: editor.putInt("m2_kut_pre_3", Integer.parseInt(line)); break;
                        case 147: editor.putInt("m2_kut_pre_4", Integer.parseInt(line)); break;
                        case 148: editor.putInt("m2_kut_pre_5", Integer.parseInt(line)); break;
                        case 149: editor.putInt("m2_kut_pre_6", Integer.parseInt(line)); break;
                        case 150: editor.putInt("m2_kut_pre_7", Integer.parseInt(line)); break;
                        case 151: editor.putInt("m2_kut_post_0", Integer.parseInt(line)); break;
                        case 152: editor.putInt("m2_kut_post_1", Integer.parseInt(line)); break;
                        case 153: editor.putInt("m2_kut_post_2", Integer.parseInt(line)); break;
                        case 154: editor.putInt("m2_kut_post_3", Integer.parseInt(line)); break;
                        case 155: editor.putInt("m2_kut_post_4", Integer.parseInt(line)); break;
                        case 156: editor.putInt("m2_kut_post_5", Integer.parseInt(line)); break;
                        case 157: editor.putInt("m2_kut_post_6", Integer.parseInt(line)); break;
                        case 158: editor.putInt("m2_kut_post_7", Integer.parseInt(line)); break;
                        case 159: editor.putInt("m3_kut_pre_0", Integer.parseInt(line)); break;
                        case 160: editor.putInt("m3_kut_pre_1", Integer.parseInt(line)); break;
                        case 161: editor.putInt("m3_kut_pre_2", Integer.parseInt(line)); break;
                        case 162: editor.putInt("m3_kut_pre_3", Integer.parseInt(line)); break;
                        case 163: editor.putInt("m3_kut_pre_4", Integer.parseInt(line)); break;
                        case 164: editor.putInt("m3_kut_pre_5", Integer.parseInt(line)); break;
                        case 165: editor.putInt("m3_kut_pre_6", Integer.parseInt(line)); break;
                        case 166: editor.putInt("m3_kut_pre_7", Integer.parseInt(line)); break;
                        case 167: editor.putInt("m3_kut_post_0", Integer.parseInt(line)); break;
                        case 168: editor.putInt("m3_kut_post_1", Integer.parseInt(line)); break;
                        case 169: editor.putInt("m3_kut_post_2", Integer.parseInt(line)); break;
                        case 170: editor.putInt("m3_kut_post_3", Integer.parseInt(line)); break;
                        case 171: editor.putInt("m3_kut_post_4", Integer.parseInt(line)); break;
                        case 172: editor.putInt("m3_kut_post_5", Integer.parseInt(line)); break;
                        case 173: editor.putInt("m3_kut_post_6", Integer.parseInt(line)); break;
                        case 174: editor.putInt("m3_kut_post_7", Integer.parseInt(line)); break;
                        case 175: editor.putInt("m4_kut_pre_0", Integer.parseInt(line)); break;
                        case 176: editor.putInt("m4_kut_pre_1", Integer.parseInt(line)); break;
                        case 177: editor.putInt("m4_kut_pre_2", Integer.parseInt(line)); break;
                        case 178: editor.putInt("m4_kut_pre_3", Integer.parseInt(line)); break;
                        case 179: editor.putInt("m4_kut_pre_4", Integer.parseInt(line)); break;
                        case 180: editor.putInt("m4_kut_pre_5", Integer.parseInt(line)); break;
                        case 181: editor.putInt("m4_kut_pre_6", Integer.parseInt(line)); break;
                        case 182: editor.putInt("m4_kut_pre_7", Integer.parseInt(line)); break;
                        case 183: editor.putInt("m4_kut_post_0", Integer.parseInt(line)); break;
                        case 184: editor.putInt("m4_kut_post_1", Integer.parseInt(line)); break;
                        case 185: editor.putInt("m4_kut_post_2", Integer.parseInt(line)); break;
                        case 186: editor.putInt("m4_kut_post_3", Integer.parseInt(line)); break;
                        case 187: editor.putInt("m4_kut_post_4", Integer.parseInt(line)); break;
                        case 188: editor.putInt("m4_kut_post_5", Integer.parseInt(line)); break;
                        case 189: editor.putInt("m4_kut_post_6", Integer.parseInt(line)); break;
                        case 190: editor.putInt("m4_kut_post_7", Integer.parseInt(line)); break;
                        case 191: editor.putInt("m0_digcomp_11", Integer.parseInt(line)); break;
                        case 192: editor.putInt("m0_digcomp_12", Integer.parseInt(line)); break;
                        case 193: editor.putInt("m0_digcomp_13", Integer.parseInt(line)); break;
                        case 194: editor.putInt("m0_digcomp_21", Integer.parseInt(line)); break;
                        case 195: editor.putInt("m0_digcomp_22", Integer.parseInt(line)); break;
                        case 196: editor.putInt("m0_digcomp_23", Integer.parseInt(line)); break;
                        case 197: editor.putInt("m0_digcomp_24", Integer.parseInt(line)); break;
                        case 198: editor.putInt("m0_digcomp_25", Integer.parseInt(line)); break;
                        case 199: editor.putInt("m0_digcomp_26", Integer.parseInt(line)); break;
                        case 200: editor.putInt("m0_digcomp_31", Integer.parseInt(line)); break;
                        case 201: editor.putInt("m0_digcomp_32", Integer.parseInt(line)); break;
                        case 202: editor.putInt("m0_digcomp_33", Integer.parseInt(line)); break;
                        case 203: editor.putInt("m0_digcomp_34", Integer.parseInt(line)); break;
                        case 204: editor.putInt("m0_digcomp_41", Integer.parseInt(line)); break;
                        case 205: editor.putInt("m0_digcomp_42", Integer.parseInt(line)); break;
                        case 206: editor.putInt("m0_digcomp_43", Integer.parseInt(line)); break;
                        case 207: editor.putInt("m0_digcomp_44", Integer.parseInt(line)); break;
                        case 208: editor.putInt("m0_digcomp_51", Integer.parseInt(line)); break;
                        case 209: editor.putInt("m0_digcomp_52", Integer.parseInt(line)); break;
                        case 210: editor.putInt("m0_digcomp_53", Integer.parseInt(line)); break;
                        case 211: editor.putInt("m0_digcomp_54", Integer.parseInt(line)); break;
                        case 212: editor.putInt("m1_digcomp_11", Integer.parseInt(line)); break;
                        case 213: editor.putInt("m1_digcomp_12", Integer.parseInt(line)); break;
                        case 214: editor.putInt("m1_digcomp_13", Integer.parseInt(line)); break;
                        case 215: editor.putInt("m1_digcomp_21", Integer.parseInt(line)); break;
                        case 216: editor.putInt("m1_digcomp_22", Integer.parseInt(line)); break;
                        case 217: editor.putInt("m1_digcomp_23", Integer.parseInt(line)); break;
                        case 218: editor.putInt("m1_digcomp_24", Integer.parseInt(line)); break;
                        case 219: editor.putInt("m1_digcomp_25", Integer.parseInt(line)); break;
                        case 220: editor.putInt("m1_digcomp_26", Integer.parseInt(line)); break;
                        case 221: editor.putInt("m1_digcomp_31", Integer.parseInt(line)); break;
                        case 222: editor.putInt("m1_digcomp_32", Integer.parseInt(line)); break;
                        case 223: editor.putInt("m1_digcomp_33", Integer.parseInt(line)); break;
                        case 224: editor.putInt("m1_digcomp_34", Integer.parseInt(line)); break;
                        case 225: editor.putInt("m1_digcomp_41", Integer.parseInt(line)); break;
                        case 226: editor.putInt("m1_digcomp_42", Integer.parseInt(line)); break;
                        case 227: editor.putInt("m1_digcomp_43", Integer.parseInt(line)); break;
                        case 228: editor.putInt("m1_digcomp_44", Integer.parseInt(line)); break;
                        case 229: editor.putInt("m1_digcomp_51", Integer.parseInt(line)); break;
                        case 230: editor.putInt("m1_digcomp_52", Integer.parseInt(line)); break;
                        case 231: editor.putInt("m1_digcomp_53", Integer.parseInt(line)); break;
                        case 232: editor.putInt("m1_digcomp_54", Integer.parseInt(line)); break;
                        case 233: editor.putInt("m2_digcomp_11", Integer.parseInt(line)); break;
                        case 234: editor.putInt("m2_digcomp_12", Integer.parseInt(line)); break;
                        case 235: editor.putInt("m2_digcomp_13", Integer.parseInt(line)); break;
                        case 236: editor.putInt("m2_digcomp_21", Integer.parseInt(line)); break;
                        case 237: editor.putInt("m2_digcomp_22", Integer.parseInt(line)); break;
                        case 238: editor.putInt("m2_digcomp_23", Integer.parseInt(line)); break;
                        case 239: editor.putInt("m2_digcomp_24", Integer.parseInt(line)); break;
                        case 240: editor.putInt("m2_digcomp_25", Integer.parseInt(line)); break;
                        case 241: editor.putInt("m2_digcomp_26", Integer.parseInt(line)); break;
                        case 242: editor.putInt("m2_digcomp_31", Integer.parseInt(line)); break;
                        case 243: editor.putInt("m2_digcomp_32", Integer.parseInt(line)); break;
                        case 244: editor.putInt("m2_digcomp_33", Integer.parseInt(line)); break;
                        case 245: editor.putInt("m2_digcomp_34", Integer.parseInt(line)); break;
                        case 246: editor.putInt("m2_digcomp_41", Integer.parseInt(line)); break;
                        case 247: editor.putInt("m2_digcomp_42", Integer.parseInt(line)); break;
                        case 248: editor.putInt("m2_digcomp_43", Integer.parseInt(line)); break;
                        case 249: editor.putInt("m2_digcomp_44", Integer.parseInt(line)); break;
                        case 250: editor.putInt("m2_digcomp_51", Integer.parseInt(line)); break;
                        case 251: editor.putInt("m2_digcomp_52", Integer.parseInt(line)); break;
                        case 252: editor.putInt("m2_digcomp_53", Integer.parseInt(line)); break;
                        case 253: editor.putInt("m2_digcomp_54", Integer.parseInt(line)); break;
                        case 254: editor.putInt("m3_digcomp_11", Integer.parseInt(line)); break;
                        case 255: editor.putInt("m3_digcomp_12", Integer.parseInt(line)); break;
                        case 256: editor.putInt("m3_digcomp_13", Integer.parseInt(line)); break;
                        case 257: editor.putInt("m3_digcomp_21", Integer.parseInt(line)); break;
                        case 258: editor.putInt("m3_digcomp_22", Integer.parseInt(line)); break;
                        case 259: editor.putInt("m3_digcomp_23", Integer.parseInt(line)); break;
                        case 260: editor.putInt("m3_digcomp_24", Integer.parseInt(line)); break;
                        case 261: editor.putInt("m3_digcomp_25", Integer.parseInt(line)); break;
                        case 262: editor.putInt("m3_digcomp_26", Integer.parseInt(line)); break;
                        case 263: editor.putInt("m3_digcomp_31", Integer.parseInt(line)); break;
                        case 264: editor.putInt("m3_digcomp_32", Integer.parseInt(line)); break;
                        case 265: editor.putInt("m3_digcomp_33", Integer.parseInt(line)); break;
                        case 266: editor.putInt("m3_digcomp_34", Integer.parseInt(line)); break;
                        case 267: editor.putInt("m3_digcomp_41", Integer.parseInt(line)); break;
                        case 268: editor.putInt("m3_digcomp_42", Integer.parseInt(line)); break;
                        case 269: editor.putInt("m3_digcomp_43", Integer.parseInt(line)); break;
                        case 270: editor.putInt("m3_digcomp_44", Integer.parseInt(line)); break;
                        case 271: editor.putInt("m3_digcomp_51", Integer.parseInt(line)); break;
                        case 273: editor.putInt("m3_digcomp_52", Integer.parseInt(line)); break;
                        case 274: editor.putInt("m3_digcomp_53", Integer.parseInt(line)); break;
                        case 275: editor.putInt("m3_digcomp_54", Integer.parseInt(line)); break;
                        case 276: editor.putInt("m4_digcomp_11", Integer.parseInt(line)); break;
                        case 277: editor.putInt("m4_digcomp_12", Integer.parseInt(line)); break;
                        case 278: editor.putInt("m4_digcomp_13", Integer.parseInt(line)); break;
                        case 279: editor.putInt("m4_digcomp_21", Integer.parseInt(line)); break;
                        case 280: editor.putInt("m4_digcomp_22", Integer.parseInt(line)); break;
                        case 281: editor.putInt("m4_digcomp_23", Integer.parseInt(line)); break;
                        case 282: editor.putInt("m4_digcomp_24", Integer.parseInt(line)); break;
                        case 283: editor.putInt("m4_digcomp_25", Integer.parseInt(line)); break;
                        case 284: editor.putInt("m4_digcomp_26", Integer.parseInt(line)); break;
                        case 285: editor.putInt("m4_digcomp_31", Integer.parseInt(line)); break;
                        case 286: editor.putInt("m4_digcomp_32", Integer.parseInt(line)); break;
                        case 287: editor.putInt("m4_digcomp_33", Integer.parseInt(line)); break;
                        case 288: editor.putInt("m4_digcomp_34", Integer.parseInt(line)); break;
                        case 289: editor.putInt("m4_digcomp_41", Integer.parseInt(line)); break;
                        case 290: editor.putInt("m4_digcomp_42", Integer.parseInt(line)); break;
                        case 291: editor.putInt("m4_digcomp_43", Integer.parseInt(line)); break;
                        case 292: editor.putInt("m4_digcomp_44", Integer.parseInt(line)); break;
                        case 293: editor.putInt("m4_digcomp_51", Integer.parseInt(line)); break;
                        case 294: editor.putInt("m4_digcomp_52", Integer.parseInt(line)); break;
                        case 295: editor.putInt("m4_digcomp_53", Integer.parseInt(line)); break;
                        case 296: editor.putInt("m4_digcomp_54", Integer.parseInt(line)); break;
                        }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                editor.apply();
                counter++;
            }

        }
    }

    public void saveSharedPrefs(){
        FileOutputStream fileOutputStream;
        OutputStreamWriter outputStreamWriter;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            try {
                File dirApp = new File(Environment.getExternalStorageDirectory() + "/InfoBits");
                File dirLog = new File(dirApp + "/SharedPrefs");
                File logFile = new File(dirLog + "/" + "prefs_" + USERID + ".txt");

                // check if directory of app exists and create if not
                if (!dirApp.exists()) {
                    // create parent path if not available
                    dirApp.mkdirs();
                }

                // check if directory of logging exists and create if not
                if (!dirLog.exists()) {
                    // create parent path if not available
                    // this can also be done with mkdir because of lines above
                    dirLog.mkdirs();
                }

                if(logFile.exists()){
                    logFile.delete();
                }
                // check if logFile exists and create if not
                if (!logFile.exists()) {
                    logFile.createNewFile();
                }
                Context mContext = MainActivity.mycontext;
                SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
                int volume = sharedpreferences.getInt("Volume", 0);
                int buttonHeight = sharedpreferences.getInt("ButtonHeight", 100);
                float textHeight = sharedpreferences.getFloat("TextHeight", (float) 0.5);
                int textsize = sharedpreferences.getInt("TextSize", 20);
                int cm1 = sharedpreferences.getInt("currentActivityM1", 100);
                int cm2 = sharedpreferences.getInt("currentActivityM2", 200);
                int cm3 = sharedpreferences.getInt("currentActivityM3", 300);
                int cm4 = sharedpreferences.getInt("currentActivityM4", 400);
                int m1_pre_0 = sharedpreferences.getInt("m1_pre_0", 0);
                int m1_pre_1 = sharedpreferences.getInt("m1_pre_1", 0);
                int m1_pre_2 = sharedpreferences.getInt("m1_pre_2", 0);
                int m1_pre_3 = sharedpreferences.getInt("m1_pre_3", 0);
                int m1_pre_4 = sharedpreferences.getInt("m1_pre_4", 0);
                int m1_pre_5 = sharedpreferences.getInt("m1_pre_5", 0);
                int m1_pre_6 = sharedpreferences.getInt("m1_pre_6", 0);
                int m1_pre_7 = sharedpreferences.getInt("m1_pre_7", 0);
                int m1_pre_8 = sharedpreferences.getInt("m1_pre_8", 0);
                int m1_pre_9 = sharedpreferences.getInt("m1_pre_9", 0);
                int m1_pre_10 = sharedpreferences.getInt("m1_pre_10", 0);
                int m1_pre_11 = sharedpreferences.getInt("m1_pre_11", 0);
                int m1_pre_12 = sharedpreferences.getInt("m1_pre_12", 0);
                int m1_pre_13 = sharedpreferences.getInt("m1_pre_13", 0);
                int m1_pre_14 = sharedpreferences.getInt("m1_pre_14", 0);
                int m1_pre_15 = sharedpreferences.getInt("m1_pre_15", 0);
                int m1_post_0 = sharedpreferences.getInt("m1_post_0", 0);
                int m1_post_1 = sharedpreferences.getInt("m1_post_1", 0);
                int m1_post_2 = sharedpreferences.getInt("m1_post_2", 0);
                int m1_post_3 = sharedpreferences.getInt("m1_post_3", 0);
                int m1_post_4 = sharedpreferences.getInt("m1_post_4", 0);
                int m1_post_5 = sharedpreferences.getInt("m1_post_5", 0);
                int m1_post_6 = sharedpreferences.getInt("m1_post_6", 0);
                int m1_post_7 = sharedpreferences.getInt("m1_post_7", 0);
                int m1_post_8 = sharedpreferences.getInt("m1_post_8", 0);
                int m1_post_9 = sharedpreferences.getInt("m1_post_9", 0);
                int m1_post_10 = sharedpreferences.getInt("m1_post_10", 0);
                int m1_post_11 = sharedpreferences.getInt("m1_post_11", 0);
                int m1_post_12 = sharedpreferences.getInt("m1_post_12", 0);
                int m1_post_13 = sharedpreferences.getInt("m1_post_13", 0);
                int m1_post_14 = sharedpreferences.getInt("m1_post_14", 0);
                int m1_post_15 = sharedpreferences.getInt("m1_post_15", 0);
                int m2_pre_0 = sharedpreferences.getInt("m2_pre_0", 0);
                int m2_pre_1 = sharedpreferences.getInt("m2_pre_1", 0);
                int m2_pre_2 = sharedpreferences.getInt("m2_pre_2", 0);
                int m2_pre_3 = sharedpreferences.getInt("m2_pre_3", 0);
                int m2_pre_4 = sharedpreferences.getInt("m2_pre_4", 0);
                int m2_pre_5 = sharedpreferences.getInt("m2_pre_5", 0);
                int m2_pre_6 = sharedpreferences.getInt("m2_pre_6", 0);
                int m2_pre_7 = sharedpreferences.getInt("m2_pre_7", 0);
                int m2_pre_8 = sharedpreferences.getInt("m2_pre_8", 0);
                int m2_pre_9 = sharedpreferences.getInt("m2_pre_9", 0);
                int m2_pre_10 = sharedpreferences.getInt("m2_pre_10", 0);
                int m2_pre_11 = sharedpreferences.getInt("m2_pre_11", 0);
                int m2_pre_12 = sharedpreferences.getInt("m2_pre_12", 0);
                int m2_pre_13 = sharedpreferences.getInt("m2_pre_13", 0);
                int m2_pre_14 = sharedpreferences.getInt("m2_pre_14", 0);
                int m2_pre_15 = sharedpreferences.getInt("m2_pre_15", 0);
                int m2_pre_16 = sharedpreferences.getInt("m2_pre_16", 0);
                int m2_pre_17 = sharedpreferences.getInt("m2_pre_17", 0);
                int m2_pre_18 = sharedpreferences.getInt("m2_pre_18", 0);
                int m2_pre_19 = sharedpreferences.getInt("m2_pre_19", 0);
                int m2_pre_20 = sharedpreferences.getInt("m2_pre_20", 0);
                int m2_post_0 = sharedpreferences.getInt("m2_post_0", 0);
                int m2_post_1 = sharedpreferences.getInt("m2_post_1", 0);
                int m2_post_2 = sharedpreferences.getInt("m2_post_2", 0);
                int m2_post_3 = sharedpreferences.getInt("m2_post_3", 0);
                int m2_post_4 = sharedpreferences.getInt("m2_post_4", 0);
                int m2_post_5 = sharedpreferences.getInt("m2_post_5", 0);
                int m2_post_6 = sharedpreferences.getInt("m2_post_6", 0);
                int m2_post_7 = sharedpreferences.getInt("m2_post_7", 0);
                int m2_post_8 = sharedpreferences.getInt("m2_post_8", 0);
                int m2_post_9 = sharedpreferences.getInt("m2_post_9", 0);
                int m2_post_10 = sharedpreferences.getInt("m2_post_10", 0);
                int m2_post_11 = sharedpreferences.getInt("m2_post_11", 0);
                int m2_post_12 = sharedpreferences.getInt("m2_post_12", 0);
                int m2_post_13 = sharedpreferences.getInt("m2_post_13", 0);
                int m2_post_14 = sharedpreferences.getInt("m2_post_14", 0);
                int m2_post_15 = sharedpreferences.getInt("m2_post_15", 0);
                int m2_post_16 = sharedpreferences.getInt("m2_post_16", 0);
                int m2_post_17 = sharedpreferences.getInt("m2_post_17", 0);
                int m2_post_18 = sharedpreferences.getInt("m2_post_18", 0);
                int m2_post_19 = sharedpreferences.getInt("m2_post_19", 0);
                int m2_post_20 = sharedpreferences.getInt("m2_post_20", 0);
                int m3_pre_0 = sharedpreferences.getInt("m3_pre_0", 0);
                int m3_pre_1 = sharedpreferences.getInt("m3_pre_1", 0);
                int m3_pre_2 = sharedpreferences.getInt("m3_pre_2", 0);
                int m3_pre_3 = sharedpreferences.getInt("m3_pre_3", 0);
                int m3_pre_4 = sharedpreferences.getInt("m3_pre_4", 0);
                int m3_pre_5 = sharedpreferences.getInt("m3_pre_5", 0);
                int m3_pre_6 = sharedpreferences.getInt("m3_pre_6", 0);
                int m3_pre_7 = sharedpreferences.getInt("m3_pre_7", 0);
                int m3_post_0 = sharedpreferences.getInt("m3_post_0", 0);
                int m3_post_1 = sharedpreferences.getInt("m3_post_1", 0);
                int m3_post_2 = sharedpreferences.getInt("m3_post_2", 0);
                int m3_post_3 = sharedpreferences.getInt("m3_post_3", 0);
                int m3_post_4 = sharedpreferences.getInt("m3_post_4", 0);
                int m3_post_5 = sharedpreferences.getInt("m3_post_5", 0);
                int m3_post_6 = sharedpreferences.getInt("m3_post_6", 0);
                int m3_post_7 = sharedpreferences.getInt("m3_post_7", 0);
                int m4_pre_0 = sharedpreferences.getInt("m4_pre_0", 0);
                int m4_pre_1 = sharedpreferences.getInt("m4_pre_1", 0);
                int m4_pre_2 = sharedpreferences.getInt("m4_pre_2", 0);
                int m4_pre_3 = sharedpreferences.getInt("m4_pre_3", 0);
                int m4_pre_4 = sharedpreferences.getInt("m4_pre_4", 0);
                int m4_pre_5 = sharedpreferences.getInt("m4_pre_5", 0);
                int m4_pre_6 = sharedpreferences.getInt("m4_pre_6", 0);
                int m4_pre_7 = sharedpreferences.getInt("m4_pre_7", 0);
                int m4_pre_8 = sharedpreferences.getInt("m4_pre_8", 0);
                int m4_pre_9 = sharedpreferences.getInt("m4_pre_9", 0);
                int m4_pre_10 = sharedpreferences.getInt("m4_pre_10", 0);
                int m4_pre_11 = sharedpreferences.getInt("m4_pre_11", 0);
                int m4_pre_12 = sharedpreferences.getInt("m4_pre_12", 0);
                int m4_pre_13 = sharedpreferences.getInt("m4_pre_13", 0);
                int m4_post_0 = sharedpreferences.getInt("m4_post_0", 0);
                int m4_post_1 = sharedpreferences.getInt("m4_post_1", 0);
                int m4_post_2 = sharedpreferences.getInt("m4_post_2", 0);
                int m4_post_3 = sharedpreferences.getInt("m4_post_3", 0);
                int m4_post_4 = sharedpreferences.getInt("m4_post_4", 0);
                int m4_post_5 = sharedpreferences.getInt("m4_post_5", 0);
                int m4_post_6 = sharedpreferences.getInt("m4_post_6", 0);
                int m4_post_7 = sharedpreferences.getInt("m4_post_7", 0);
                int m4_post_8 = sharedpreferences.getInt("m4_post_8", 0);
                int m4_post_9 = sharedpreferences.getInt("m4_post_9", 0);
                int m4_post_10 = sharedpreferences.getInt("m4_post_10", 0);
                int m4_post_11 = sharedpreferences.getInt("m4_post_11", 0);
                int m4_post_12 = sharedpreferences.getInt("m4_post_12", 0);
                int m4_post_13 = sharedpreferences.getInt("m4_post_13", 0);
                int m1_kut_pre_0 = sharedpreferences.getInt("m1_kut_pre_0", 0);
                int m1_kut_pre_1 = sharedpreferences.getInt("m1_kut_pre_1", 0);
                int m1_kut_pre_2 = sharedpreferences.getInt("m1_kut_pre_2", 0);
                int m1_kut_pre_3 = sharedpreferences.getInt("m1_kut_pre_3", 0);
                int m1_kut_pre_4 = sharedpreferences.getInt("m1_kut_pre_4", 0);
                int m1_kut_pre_5 = sharedpreferences.getInt("m1_kut_pre_5", 0);
                int m1_kut_pre_6 = sharedpreferences.getInt("m1_kut_pre_6", 0);
                int m1_kut_pre_7 = sharedpreferences.getInt("m1_kut_pre_7", 0);
                int m1_kut_post_0 = sharedpreferences.getInt("m1_kut_post_0", 0);
                int m1_kut_post_1 = sharedpreferences.getInt("m1_kut_post_1", 0);
                int m1_kut_post_2 = sharedpreferences.getInt("m1_kut_post_2", 0);
                int m1_kut_post_3 = sharedpreferences.getInt("m1_kut_post_3", 0);
                int m1_kut_post_4 = sharedpreferences.getInt("m1_kut_post_4", 0);
                int m1_kut_post_5 = sharedpreferences.getInt("m1_kut_post_5", 0);
                int m1_kut_post_6 = sharedpreferences.getInt("m1_kut_post_6", 0);
                int m1_kut_post_7 = sharedpreferences.getInt("m1_kut_post_7", 0);
                int m2_kut_pre_0 = sharedpreferences.getInt("m2_kut_pre_0", 0);
                int m2_kut_pre_1 = sharedpreferences.getInt("m2_kut_pre_1", 0);
                int m2_kut_pre_2 = sharedpreferences.getInt("m2_kut_pre_2", 0);
                int m2_kut_pre_3 = sharedpreferences.getInt("m2_kut_pre_3", 0);
                int m2_kut_pre_4 = sharedpreferences.getInt("m2_kut_pre_4", 0);
                int m2_kut_pre_5 = sharedpreferences.getInt("m2_kut_pre_5", 0);
                int m2_kut_pre_6 = sharedpreferences.getInt("m2_kut_pre_6", 0);
                int m2_kut_pre_7 = sharedpreferences.getInt("m2_kut_pre_7", 0);
                int m2_kut_post_0 = sharedpreferences.getInt("m2_kut_post_0", 0);
                int m2_kut_post_1 = sharedpreferences.getInt("m2_kut_post_1", 0);
                int m2_kut_post_2 = sharedpreferences.getInt("m2_kut_post_2", 0);
                int m2_kut_post_3 = sharedpreferences.getInt("m2_kut_post_3", 0);
                int m2_kut_post_4 = sharedpreferences.getInt("m2_kut_post_4", 0);
                int m2_kut_post_5 = sharedpreferences.getInt("m2_kut_post_5", 0);
                int m2_kut_post_6 = sharedpreferences.getInt("m2_kut_post_6", 0);
                int m2_kut_post_7 = sharedpreferences.getInt("m2_kut_post_7", 0);
                int m3_kut_pre_0 = sharedpreferences.getInt("m3_kut_pre_0", 0);
                int m3_kut_pre_1 = sharedpreferences.getInt("m3_kut_pre_1", 0);
                int m3_kut_pre_2 = sharedpreferences.getInt("m3_kut_pre_2", 0);
                int m3_kut_pre_3 = sharedpreferences.getInt("m3_kut_pre_3", 0);
                int m3_kut_pre_4 = sharedpreferences.getInt("m3_kut_pre_4", 0);
                int m3_kut_pre_5 = sharedpreferences.getInt("m3_kut_pre_5", 0);
                int m3_kut_pre_6 = sharedpreferences.getInt("m3_kut_pre_6", 0);
                int m3_kut_pre_7 = sharedpreferences.getInt("m3_kut_pre_7", 0);
                int m3_kut_post_0 = sharedpreferences.getInt("m3_kut_post_0", 0);
                int m3_kut_post_1 = sharedpreferences.getInt("m3_kut_post_1", 0);
                int m3_kut_post_2 = sharedpreferences.getInt("m3_kut_post_2", 0);
                int m3_kut_post_3 = sharedpreferences.getInt("m3_kut_post_3", 0);
                int m3_kut_post_4 = sharedpreferences.getInt("m3_kut_post_4", 0);
                int m3_kut_post_5 = sharedpreferences.getInt("m3_kut_post_5", 0);
                int m3_kut_post_6 = sharedpreferences.getInt("m3_kut_post_6", 0);
                int m3_kut_post_7 = sharedpreferences.getInt("m3_kut_post_7", 0);
                int m4_kut_pre_0 = sharedpreferences.getInt("m4_kut_pre_0", 0);
                int m4_kut_pre_1 = sharedpreferences.getInt("m4_kut_pre_1", 0);
                int m4_kut_pre_2 = sharedpreferences.getInt("m4_kut_pre_2", 0);
                int m4_kut_pre_3 = sharedpreferences.getInt("m4_kut_pre_3", 0);
                int m4_kut_pre_4 = sharedpreferences.getInt("m4_kut_pre_4", 0);
                int m4_kut_pre_5 = sharedpreferences.getInt("m4_kut_pre_5", 0);
                int m4_kut_pre_6 = sharedpreferences.getInt("m4_kut_pre_6", 0);
                int m4_kut_pre_7 = sharedpreferences.getInt("m4_kut_pre_7", 0);
                int m4_kut_post_0 = sharedpreferences.getInt("m4_kut_post_0", 0);
                int m4_kut_post_1 = sharedpreferences.getInt("m4_kut_post_1", 0);
                int m4_kut_post_2 = sharedpreferences.getInt("m4_kut_post_2", 0);
                int m4_kut_post_3 = sharedpreferences.getInt("m4_kut_post_3", 0);
                int m4_kut_post_4 = sharedpreferences.getInt("m4_kut_post_4", 0);
                int m4_kut_post_5 = sharedpreferences.getInt("m4_kut_post_5", 0);
                int m4_kut_post_6 = sharedpreferences.getInt("m4_kut_post_6", 0);
                int m4_kut_post_7 = sharedpreferences.getInt("m4_kut_post_7", 0);
                int m0_digcomp_11 = sharedpreferences.getInt("m0_digcomp_11", 0);
                int m0_digcomp_12 = sharedpreferences.getInt("m0_digcomp_12", 0);
                int m0_digcomp_13 = sharedpreferences.getInt("m0_digcomp_13", 0);
                int m0_digcomp_21 = sharedpreferences.getInt("m0_digcomp_21", 0);
                int m0_digcomp_22 = sharedpreferences.getInt("m0_digcomp_22", 0);
                int m0_digcomp_23 = sharedpreferences.getInt("m0_digcomp_23", 0);
                int m0_digcomp_24 = sharedpreferences.getInt("m0_digcomp_24", 0);
                int m0_digcomp_25 = sharedpreferences.getInt("m0_digcomp_25", 0);
                int m0_digcomp_26 = sharedpreferences.getInt("m0_digcomp_26", 0);
                int m0_digcomp_31 = sharedpreferences.getInt("m0_digcomp_31", 0);
                int m0_digcomp_32 = sharedpreferences.getInt("m0_digcomp_32", 0);
                int m0_digcomp_33 = sharedpreferences.getInt("m0_digcomp_33", 0);
                int m0_digcomp_34 = sharedpreferences.getInt("m0_digcomp_34", 0);
                int m0_digcomp_41 = sharedpreferences.getInt("m0_digcomp_41", 0);
                int m0_digcomp_42 = sharedpreferences.getInt("m0_digcomp_42", 0);
                int m0_digcomp_43 = sharedpreferences.getInt("m0_digcomp_43", 0);
                int m0_digcomp_44 = sharedpreferences.getInt("m0_digcomp_44", 0);
                int m0_digcomp_51 = sharedpreferences.getInt("m0_digcomp_51", 0);
                int m0_digcomp_52 = sharedpreferences.getInt("m0_digcomp_52", 0);
                int m0_digcomp_53 = sharedpreferences.getInt("m0_digcomp_53", 0);
                int m0_digcomp_54 = sharedpreferences.getInt("m0_digcomp_54", 0);
                int m1_digcomp_11 = sharedpreferences.getInt("m1_digcomp_11", 0);
                int m1_digcomp_12 = sharedpreferences.getInt("m1_digcomp_12", 0);
                int m1_digcomp_13 = sharedpreferences.getInt("m1_digcomp_13", 0);
                int m1_digcomp_21 = sharedpreferences.getInt("m1_digcomp_21", 0);
                int m1_digcomp_22 = sharedpreferences.getInt("m1_digcomp_22", 0);
                int m1_digcomp_23 = sharedpreferences.getInt("m1_digcomp_23", 0);
                int m1_digcomp_24 = sharedpreferences.getInt("m1_digcomp_24", 0);
                int m1_digcomp_25 = sharedpreferences.getInt("m1_digcomp_25", 0);
                int m1_digcomp_26 = sharedpreferences.getInt("m1_digcomp_26", 0);
                int m1_digcomp_31 = sharedpreferences.getInt("m1_digcomp_31", 0);
                int m1_digcomp_32 = sharedpreferences.getInt("m1_digcomp_32", 0);
                int m1_digcomp_33 = sharedpreferences.getInt("m1_digcomp_33", 0);
                int m1_digcomp_34 = sharedpreferences.getInt("m1_digcomp_34", 0);
                int m1_digcomp_41 = sharedpreferences.getInt("m1_digcomp_41", 0);
                int m1_digcomp_42 = sharedpreferences.getInt("m1_digcomp_42", 0);
                int m1_digcomp_43 = sharedpreferences.getInt("m1_digcomp_43", 0);
                int m1_digcomp_44 = sharedpreferences.getInt("m1_digcomp_44", 0);
                int m1_digcomp_51 = sharedpreferences.getInt("m1_digcomp_51", 0);
                int m1_digcomp_52 = sharedpreferences.getInt("m1_digcomp_52", 0);
                int m1_digcomp_53 = sharedpreferences.getInt("m1_digcomp_53", 0);
                int m1_digcomp_54 = sharedpreferences.getInt("m1_digcomp_54", 0);
                int m2_digcomp_11 = sharedpreferences.getInt("m2_digcomp_11", 0);
                int m2_digcomp_12 = sharedpreferences.getInt("m2_digcomp_12", 0);
                int m2_digcomp_13 = sharedpreferences.getInt("m2_digcomp_13", 0);
                int m2_digcomp_21 = sharedpreferences.getInt("m2_digcomp_21", 0);
                int m2_digcomp_22 = sharedpreferences.getInt("m2_digcomp_22", 0);
                int m2_digcomp_23 = sharedpreferences.getInt("m2_digcomp_23", 0);
                int m2_digcomp_24 = sharedpreferences.getInt("m2_digcomp_24", 0);
                int m2_digcomp_25 = sharedpreferences.getInt("m2_digcomp_25", 0);
                int m2_digcomp_26 = sharedpreferences.getInt("m2_digcomp_26", 0);
                int m2_digcomp_31 = sharedpreferences.getInt("m2_digcomp_31", 0);
                int m2_digcomp_32 = sharedpreferences.getInt("m2_digcomp_32", 0);
                int m2_digcomp_33 = sharedpreferences.getInt("m2_digcomp_33", 0);
                int m2_digcomp_34 = sharedpreferences.getInt("m2_digcomp_34", 0);
                int m2_digcomp_41 = sharedpreferences.getInt("m2_digcomp_41", 0);
                int m2_digcomp_42 = sharedpreferences.getInt("m2_digcomp_42", 0);
                int m2_digcomp_43 = sharedpreferences.getInt("m2_digcomp_43", 0);
                int m2_digcomp_44 = sharedpreferences.getInt("m2_digcomp_44", 0);
                int m2_digcomp_51 = sharedpreferences.getInt("m2_digcomp_51", 0);
                int m2_digcomp_52 = sharedpreferences.getInt("m2_digcomp_52", 0);
                int m2_digcomp_53 = sharedpreferences.getInt("m2_digcomp_53", 0);
                int m2_digcomp_54 = sharedpreferences.getInt("m2_digcomp_54", 0);
                int m3_digcomp_11 = sharedpreferences.getInt("m3_digcomp_11", 0);
                int m3_digcomp_12 = sharedpreferences.getInt("m3_digcomp_12", 0);
                int m3_digcomp_13 = sharedpreferences.getInt("m3_digcomp_13", 0);
                int m3_digcomp_21 = sharedpreferences.getInt("m3_digcomp_21", 0);
                int m3_digcomp_22 = sharedpreferences.getInt("m3_digcomp_22", 0);
                int m3_digcomp_23 = sharedpreferences.getInt("m3_digcomp_23", 0);
                int m3_digcomp_24 = sharedpreferences.getInt("m3_digcomp_24", 0);
                int m3_digcomp_25 = sharedpreferences.getInt("m3_digcomp_25", 0);
                int m3_digcomp_26 = sharedpreferences.getInt("m3_digcomp_26", 0);
                int m3_digcomp_31 = sharedpreferences.getInt("m3_digcomp_31", 0);
                int m3_digcomp_32 = sharedpreferences.getInt("m3_digcomp_32", 0);
                int m3_digcomp_33 = sharedpreferences.getInt("m3_digcomp_33", 0);
                int m3_digcomp_34 = sharedpreferences.getInt("m3_digcomp_34", 0);
                int m3_digcomp_41 = sharedpreferences.getInt("m3_digcomp_41", 0);
                int m3_digcomp_42 = sharedpreferences.getInt("m3_digcomp_42", 0);
                int m3_digcomp_43 = sharedpreferences.getInt("m3_digcomp_43", 0);
                int m3_digcomp_44 = sharedpreferences.getInt("m3_digcomp_44", 0);
                int m3_digcomp_51 = sharedpreferences.getInt("m3_digcomp_51", 0);
                int m3_digcomp_52 = sharedpreferences.getInt("m3_digcomp_52", 0);
                int m3_digcomp_53 = sharedpreferences.getInt("m3_digcomp_53", 0);
                int m3_digcomp_54 = sharedpreferences.getInt("m3_digcomp_54", 0);
                int m4_digcomp_11 = sharedpreferences.getInt("m4_digcomp_11", 0);
                int m4_digcomp_12 = sharedpreferences.getInt("m4_digcomp_12", 0);
                int m4_digcomp_13 = sharedpreferences.getInt("m4_digcomp_13", 0);
                int m4_digcomp_21 = sharedpreferences.getInt("m4_digcomp_21", 0);
                int m4_digcomp_22 = sharedpreferences.getInt("m4_digcomp_22", 0);
                int m4_digcomp_23 = sharedpreferences.getInt("m4_digcomp_23", 0);
                int m4_digcomp_24 = sharedpreferences.getInt("m4_digcomp_24", 0);
                int m4_digcomp_25 = sharedpreferences.getInt("m4_digcomp_25", 0);
                int m4_digcomp_26 = sharedpreferences.getInt("m4_digcomp_26", 0);
                int m4_digcomp_31 = sharedpreferences.getInt("m4_digcomp_31", 0);
                int m4_digcomp_32 = sharedpreferences.getInt("m4_digcomp_32", 0);
                int m4_digcomp_33 = sharedpreferences.getInt("m4_digcomp_33", 0);
                int m4_digcomp_34 = sharedpreferences.getInt("m4_digcomp_34", 0);
                int m4_digcomp_41 = sharedpreferences.getInt("m4_digcomp_41", 0);
                int m4_digcomp_42 = sharedpreferences.getInt("m4_digcomp_42", 0);
                int m4_digcomp_43 = sharedpreferences.getInt("m4_digcomp_43", 0);
                int m4_digcomp_44 = sharedpreferences.getInt("m4_digcomp_44", 0);
                int m4_digcomp_51 = sharedpreferences.getInt("m4_digcomp_51", 0);
                int m4_digcomp_52 = sharedpreferences.getInt("m4_digcomp_52", 0);
                int m4_digcomp_53 = sharedpreferences.getInt("m4_digcomp_53", 0);
                int m4_digcomp_54 = sharedpreferences.getInt("m4_digcomp_54", 0);
                    String l = volume + "\n"
                            + buttonHeight + "\n"
                            + (int)(textHeight*100) + "\n"
                            + textsize + "\n"
                            + current + "\n"
                            + cm1 + "\n"
                            + cm2 + "\n"
                            + cm3 + "\n"
                            + cm4 + "\n"
                            + m1_pre_0 + "\n"
                            + m1_pre_1 + "\n"
                            + m1_pre_2 + "\n"
                            + m1_pre_3 + "\n"
                            + m1_pre_4 + "\n"
                            + m1_pre_5 + "\n"
                            + m1_pre_6 + "\n"
                            + m1_pre_7 + "\n"
                            + m1_pre_8 + "\n"
                            + m1_pre_9 + "\n"
                            + m1_pre_10 + "\n"
                            + m1_pre_11 + "\n"
                            + m1_pre_12 + "\n"
                            + m1_pre_13 + "\n"
                            + m1_pre_14 + "\n"
                            + m1_pre_15 + "\n"
                            + m1_post_0 + "\n"
                            + m1_post_1 + "\n"
                            + m1_post_2 + "\n"
                            + m1_post_3 + "\n"
                            + m1_post_4 + "\n"
                            + m1_post_5 + "\n"
                            + m1_post_6 + "\n"
                            + m1_post_7 + "\n"
                            + m1_post_8 + "\n"
                            + m1_post_9 + "\n"
                            + m1_post_10 + "\n"
                            + m1_post_11 + "\n"
                            + m1_post_12 + "\n"
                            + m1_post_13 + "\n"
                            + m1_post_14 + "\n"
                            + m1_post_15 + "\n"
                            + m2_pre_0 + "\n"
                            + m2_pre_1 + "\n"
                            + m2_pre_2 + "\n"
                            + m2_pre_3 + "\n"
                            + m2_pre_4 + "\n"
                            + m2_pre_5 + "\n"
                            + m2_pre_6 + "\n"
                            + m2_pre_7 + "\n"
                            + m2_pre_8 + "\n"
                            + m2_pre_9 + "\n"
                            + m2_pre_10 + "\n"
                            + m2_pre_11 + "\n"
                            + m2_pre_12 + "\n"
                            + m2_pre_13 + "\n"
                            + m2_pre_14 + "\n"
                            + m2_pre_15 + "\n"
                            + m2_pre_16 + "\n"
                            + m2_pre_17 + "\n"
                            + m2_pre_18 + "\n"
                            + m2_pre_19 + "\n"
                            + m2_pre_20 + "\n"
                            + m2_post_0 + "\n"
                            + m2_post_1 + "\n"
                            + m2_post_2 + "\n"
                            + m2_post_3 + "\n"
                            + m2_post_4 + "\n"
                            + m2_post_5 + "\n"
                            + m2_post_6 + "\n"
                            + m2_post_7 + "\n"
                            + m2_post_8 + "\n"
                            + m2_post_9 + "\n"
                            + m2_post_10 + "\n"
                            + m2_post_11 + "\n"
                            + m2_post_12 + "\n"
                            + m2_post_13 + "\n"
                            + m2_post_14 + "\n"
                            + m2_post_15 + "\n"
                            + m2_post_16 + "\n"
                            + m2_post_17 + "\n"
                            + m2_post_18 + "\n"
                            + m2_post_19 + "\n"
                            + m2_post_20 + "\n"
                            + m3_pre_0 + "\n"
                            + m3_pre_1 + "\n"
                            + m3_pre_2 + "\n"
                            + m3_pre_3 + "\n"
                            + m3_pre_4 + "\n"
                            + m3_pre_5 + "\n"
                            + m3_pre_6 + "\n"
                            + m3_pre_7 + "\n"
                            + m3_post_0 + "\n"
                            + m3_post_1 + "\n"
                            + m3_post_2 + "\n"
                            + m3_post_3 + "\n"
                            + m3_post_4 + "\n"
                            + m3_post_5 + "\n"
                            + m3_post_6 + "\n"
                            + m3_post_7 + "\n"
                            + m4_pre_0 + "\n"
                            + m4_pre_1 + "\n"
                            + m4_pre_2 + "\n"
                            + m4_pre_3 + "\n"
                            + m4_pre_4 + "\n"
                            + m4_pre_5 + "\n"
                            + m4_pre_6 + "\n"
                            + m4_pre_7 + "\n"
                            + m4_pre_8 + "\n"
                            + m4_pre_9 + "\n"
                            + m4_pre_10 + "\n"
                            + m4_pre_11 + "\n"
                            + m4_pre_12 + "\n"
                            + m4_pre_13 + "\n"
                            + m4_post_0 + "\n"
                            + m4_post_1 + "\n"
                            + m4_post_2 + "\n"
                            + m4_post_3 + "\n"
                            + m4_post_4 + "\n"
                            + m4_post_5 + "\n"
                            + m4_post_6 + "\n"
                            + m4_post_7 + "\n"
                            + m4_post_8 + "\n"
                            + m4_post_9 + "\n"
                            + m4_post_10 + "\n"
                            + m4_post_11 + "\n"
                            + m4_post_12 + "\n"
                            + m4_post_13 + "\n"
                            + m1_kut_pre_0 + "\n"
                            + m1_kut_pre_1 + "\n"
                            + m1_kut_pre_2 + "\n"
                            + m1_kut_pre_3 + "\n"
                            + m1_kut_pre_4 + "\n"
                            + m1_kut_pre_5 + "\n"
                            + m1_kut_pre_6 + "\n"
                            + m1_kut_pre_7 + "\n"
                            + m1_kut_post_0 + "\n"
                            + m1_kut_post_1 + "\n"
                            + m1_kut_post_2 + "\n"
                            + m1_kut_post_3 + "\n"
                            + m1_kut_post_4 + "\n"
                            + m1_kut_post_5 + "\n"
                            + m1_kut_post_6 + "\n"
                            + m1_kut_post_7 + "\n"
                            + m2_kut_pre_0 + "\n"
                            + m2_kut_pre_1 + "\n"
                            + m2_kut_pre_2 + "\n"
                            + m2_kut_pre_3 + "\n"
                            + m2_kut_pre_4 + "\n"
                            + m2_kut_pre_5 + "\n"
                            + m2_kut_pre_6 + "\n"
                            + m2_kut_pre_7 + "\n"
                            + m2_kut_post_0 + "\n"
                            + m2_kut_post_1 + "\n"
                            + m2_kut_post_2 + "\n"
                            + m2_kut_post_3 + "\n"
                            + m2_kut_post_4 + "\n"
                            + m2_kut_post_5 + "\n"
                            + m2_kut_post_6 + "\n"
                            + m2_kut_post_7 + "\n"
                            + m3_kut_pre_0 + "\n"
                            + m3_kut_pre_1 + "\n"
                            + m3_kut_pre_2 + "\n"
                            + m3_kut_pre_3 + "\n"
                            + m3_kut_pre_4 + "\n"
                            + m3_kut_pre_5 + "\n"
                            + m3_kut_pre_6 + "\n"
                            + m3_kut_pre_7 + "\n"
                            + m3_kut_post_0 + "\n"
                            + m3_kut_post_1 + "\n"
                            + m3_kut_post_2 + "\n"
                            + m3_kut_post_3 + "\n"
                            + m3_kut_post_4 + "\n"
                            + m3_kut_post_5 + "\n"
                            + m3_kut_post_6 + "\n"
                            + m3_kut_post_7 + "\n"
                            + m4_kut_pre_0 + "\n"
                            + m4_kut_pre_1 + "\n"
                            + m4_kut_pre_2 + "\n"
                            + m4_kut_pre_3 + "\n"
                            + m4_kut_pre_4 + "\n"
                            + m4_kut_pre_5 + "\n"
                            + m4_kut_pre_6 + "\n"
                            + m4_kut_pre_7 + "\n"
                            + m4_kut_post_0 + "\n"
                            + m4_kut_post_1 + "\n"
                            + m4_kut_post_2 + "\n"
                            + m4_kut_post_3 + "\n"
                            + m4_kut_post_4 + "\n"
                            + m4_kut_post_5 + "\n"
                            + m4_kut_post_6 + "\n"
                            + m4_kut_post_7 + "\n"
                            + m0_digcomp_11 + "\n"
                            + m0_digcomp_12 + "\n"
                            + m0_digcomp_13 + "\n"
                            + m0_digcomp_21 + "\n"
                            + m0_digcomp_22 + "\n"
                            + m0_digcomp_23 + "\n"
                            + m0_digcomp_24 + "\n"
                            + m0_digcomp_25 + "\n"
                            + m0_digcomp_26 + "\n"
                            + m0_digcomp_31 + "\n"
                            + m0_digcomp_32 + "\n"
                            + m0_digcomp_33 + "\n"
                            + m0_digcomp_34 + "\n"
                            + m0_digcomp_41 + "\n"
                            + m0_digcomp_42 + "\n"
                            + m0_digcomp_43 + "\n"
                            + m0_digcomp_44 + "\n"
                            + m0_digcomp_51 + "\n"
                            + m0_digcomp_52 + "\n"
                            + m0_digcomp_53 + "\n"
                            + m0_digcomp_54 + "\n"
                            + m1_digcomp_11 + "\n"
                            + m1_digcomp_12 + "\n"
                            + m1_digcomp_13 + "\n"
                            + m1_digcomp_21 + "\n"
                            + m1_digcomp_22 + "\n"
                            + m1_digcomp_23 + "\n"
                            + m1_digcomp_24 + "\n"
                            + m1_digcomp_25 + "\n"
                            + m1_digcomp_26 + "\n"
                            + m1_digcomp_31 + "\n"
                            + m1_digcomp_32 + "\n"
                            + m1_digcomp_33 + "\n"
                            + m1_digcomp_34 + "\n"
                            + m1_digcomp_41 + "\n"
                            + m1_digcomp_42 + "\n"
                            + m1_digcomp_43 + "\n"
                            + m1_digcomp_44 + "\n"
                            + m1_digcomp_51 + "\n"
                            + m1_digcomp_52 + "\n"
                            + m1_digcomp_53 + "\n"
                            + m1_digcomp_54 + "\n"
                            + m2_digcomp_11 + "\n"
                            + m2_digcomp_12 + "\n"
                            + m2_digcomp_13 + "\n"
                            + m2_digcomp_21 + "\n"
                            + m2_digcomp_22 + "\n"
                            + m2_digcomp_23 + "\n"
                            + m2_digcomp_24 + "\n"
                            + m2_digcomp_25 + "\n"
                            + m2_digcomp_26 + "\n"
                            + m2_digcomp_31 + "\n"
                            + m2_digcomp_32 + "\n"
                            + m2_digcomp_33 + "\n"
                            + m2_digcomp_34 + "\n"
                            + m2_digcomp_41 + "\n"
                            + m2_digcomp_42 + "\n"
                            + m2_digcomp_43 + "\n"
                            + m2_digcomp_44 + "\n"
                            + m2_digcomp_51 + "\n"
                            + m2_digcomp_52 + "\n"
                            + m2_digcomp_53 + "\n"
                            + m2_digcomp_54 + "\n"
                            + m3_digcomp_11 + "\n"
                            + m3_digcomp_12 + "\n"
                            + m3_digcomp_13 + "\n"
                            + m3_digcomp_21 + "\n"
                            + m3_digcomp_22 + "\n"
                            + m3_digcomp_23 + "\n"
                            + m3_digcomp_24 + "\n"
                            + m3_digcomp_25 + "\n"
                            + m3_digcomp_26 + "\n"
                            + m3_digcomp_31 + "\n"
                            + m3_digcomp_32 + "\n"
                            + m3_digcomp_33 + "\n"
                            + m3_digcomp_34 + "\n"
                            + m3_digcomp_41 + "\n"
                            + m3_digcomp_42 + "\n"
                            + m3_digcomp_43 + "\n"
                            + m3_digcomp_44 + "\n"
                            + m3_digcomp_51 + "\n"
                            + m3_digcomp_52 + "\n"
                            + m3_digcomp_53 + "\n"
                            + m3_digcomp_54 + "\n"
                            + m4_digcomp_11 + "\n"
                            + m4_digcomp_12 + "\n"
                            + m4_digcomp_13 + "\n"
                            + m4_digcomp_21 + "\n"
                            + m4_digcomp_22 + "\n"
                            + m4_digcomp_23 + "\n"
                            + m4_digcomp_24 + "\n"
                            + m4_digcomp_25 + "\n"
                            + m4_digcomp_26 + "\n"
                            + m4_digcomp_31 + "\n"
                            + m4_digcomp_32 + "\n"
                            + m4_digcomp_33 + "\n"
                            + m4_digcomp_34 + "\n"
                            + m4_digcomp_41 + "\n"
                            + m4_digcomp_42 + "\n"
                            + m4_digcomp_43 + "\n"
                            + m4_digcomp_44 + "\n"
                            + m4_digcomp_51 + "\n"
                            + m4_digcomp_52 + "\n"
                            + m4_digcomp_53 + "\n"
                            + m4_digcomp_54 + "\n";
                    fileOutputStream = new FileOutputStream(logFile, true);
                    outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    //outputStreamWriter.append(l);
                    outputStreamWriter.write(l);
                    Log.e("SharedPrefs", l);
                    outputStreamWriter.flush();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }


}
