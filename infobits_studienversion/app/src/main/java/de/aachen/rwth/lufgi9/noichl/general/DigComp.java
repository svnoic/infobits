package de.aachen.rwth.lufgi9.noichl.general;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationEnd;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetEnd;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionEnd;
import de.aachen.rwth.lufgi9.noichl.modul4.DataSecEnd;

import static de.aachen.rwth.lufgi9.noichl.general.SUS.currentModule;

public class DigComp extends MyActivity {

    private static int competenceCounter = 11;
    private static int levelCounter = 1;
    String preQuestionText = "";
    static int prevC = 0;
    int c = 0;
    Context context;
    JSONArray myJson = null;
    JSONObject rec = null;
    CheckBox likert1, likert2, likert3, likert4;
    int numberOfAnswers = 0;
    static ArrayList<Number> digcomp_init = new ArrayList<Number>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
    static ArrayList<Number> digcomp_1 = new ArrayList<Number>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
    static ArrayList<Number> digcomp_2 = new ArrayList<Number>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
    static ArrayList<Number> digcomp_3 = new ArrayList<Number>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
    static ArrayList<Number> digcomp_4 = new ArrayList<Number>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
    ArrayList<Number> currentC;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_usecasetextview_s_viewstub_s_nextbutton);
        context = this;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_4_checkbox);
        stub.inflate();


        Log.e("Counter: ", "" + competenceCounter + "-" + levelCounter);

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        digcomp_init.set(0, sharedpreferences.getInt("m0_digcomp_11", 0));
        digcomp_init.set(1, sharedpreferences.getInt("m0_digcomp_12", 0));
        digcomp_init.set(2, sharedpreferences.getInt("m0_digcomp_13", 0));
        digcomp_init.set(3, sharedpreferences.getInt("m0_digcomp_21", 0));
        digcomp_init.set(4, sharedpreferences.getInt("m0_digcomp_22", 0));
        digcomp_init.set(5, sharedpreferences.getInt("m0_digcomp_23", 0));
        digcomp_init.set(6, sharedpreferences.getInt("m0_digcomp_24", 0));
        digcomp_init.set(7, sharedpreferences.getInt("m0_digcomp_25", 0));
        digcomp_init.set(8, sharedpreferences.getInt("m0_digcomp_26", 0));
        digcomp_init.set(9, sharedpreferences.getInt("m0_digcomp_31", 0));
        digcomp_init.set(10, sharedpreferences.getInt("m0_digcomp_32", 0));
        digcomp_init.set(11, sharedpreferences.getInt("m0_digcomp_33", 0));
        digcomp_init.set(12, sharedpreferences.getInt("m0_digcomp_34", 0));
        digcomp_init.set(13, sharedpreferences.getInt("m0_digcomp_41", 0));
        digcomp_init.set(14, sharedpreferences.getInt("m0_digcomp_42", 0));
        digcomp_init.set(15, sharedpreferences.getInt("m0_digcomp_43", 0));
        digcomp_init.set(16, sharedpreferences.getInt("m0_digcomp_44", 0));
        digcomp_init.set(17, sharedpreferences.getInt("m0_digcomp_51", 0));
        digcomp_init.set(18, sharedpreferences.getInt("m0_digcomp_52", 0));
        digcomp_init.set(19, sharedpreferences.getInt("m0_digcomp_53", 0));
        digcomp_init.set(20, sharedpreferences.getInt("m0_digcomp_54", 0));
        digcomp_1.set(0, sharedpreferences.getInt("m1_digcomp_11", 0));
        digcomp_1.set(1, sharedpreferences.getInt("m1_digcomp_12", 0));
        digcomp_1.set(2, sharedpreferences.getInt("m1_digcomp_13", 0));
        digcomp_1.set(3, sharedpreferences.getInt("m1_digcomp_21", 0));
        digcomp_1.set(4, sharedpreferences.getInt("m1_digcomp_22", 0));
        digcomp_1.set(5, sharedpreferences.getInt("m1_digcomp_23", 0));
        digcomp_1.set(6, sharedpreferences.getInt("m1_digcomp_24", 0));
        digcomp_1.set(7, sharedpreferences.getInt("m1_digcomp_25", 0));
        digcomp_1.set(8, sharedpreferences.getInt("m1_digcomp_26", 0));
        digcomp_1.set(9, sharedpreferences.getInt("m1_digcomp_31", 0));
        digcomp_1.set(10, sharedpreferences.getInt("m1_digcomp_32", 0));
        digcomp_1.set(11, sharedpreferences.getInt("m1_digcomp_33", 0));
        digcomp_1.set(12, sharedpreferences.getInt("m1_digcomp_34", 0));
        digcomp_1.set(13, sharedpreferences.getInt("m1_digcomp_41", 0));
        digcomp_1.set(14, sharedpreferences.getInt("m1_digcomp_42", 0));
        digcomp_1.set(15, sharedpreferences.getInt("m1_digcomp_43", 0));
        digcomp_1.set(16, sharedpreferences.getInt("m1_digcomp_44", 0));
        digcomp_1.set(17, sharedpreferences.getInt("m1_digcomp_51", 0));
        digcomp_1.set(18, sharedpreferences.getInt("m1_digcomp_52", 0));
        digcomp_1.set(19, sharedpreferences.getInt("m1_digcomp_53", 0));
        digcomp_1.set(20, sharedpreferences.getInt("m1_digcomp_54", 0));
        digcomp_2.set(0, sharedpreferences.getInt("m2_digcomp_11", 0));
        digcomp_2.set(1, sharedpreferences.getInt("m2_digcomp_12", 0));
        digcomp_2.set(2, sharedpreferences.getInt("m2_digcomp_13", 0));
        digcomp_2.set(3, sharedpreferences.getInt("m2_digcomp_21", 0));
        digcomp_2.set(4, sharedpreferences.getInt("m2_digcomp_22", 0));
        digcomp_2.set(5, sharedpreferences.getInt("m2_digcomp_23", 0));
        digcomp_2.set(6, sharedpreferences.getInt("m2_digcomp_24", 0));
        digcomp_2.set(7, sharedpreferences.getInt("m2_digcomp_25", 0));
        digcomp_2.set(8, sharedpreferences.getInt("m2_digcomp_26", 0));
        digcomp_2.set(9, sharedpreferences.getInt("m2_digcomp_31", 0));
        digcomp_2.set(10, sharedpreferences.getInt("m2_digcomp_32", 0));
        digcomp_2.set(11, sharedpreferences.getInt("m2_digcomp_33", 0));
        digcomp_2.set(12, sharedpreferences.getInt("m2_digcomp_34", 0));
        digcomp_2.set(13, sharedpreferences.getInt("m2_digcomp_41", 0));
        digcomp_2.set(14, sharedpreferences.getInt("m2_digcomp_42", 0));
        digcomp_2.set(15, sharedpreferences.getInt("m2_digcomp_43", 0));
        digcomp_2.set(16, sharedpreferences.getInt("m2_digcomp_44", 0));
        digcomp_2.set(17, sharedpreferences.getInt("m2_digcomp_51", 0));
        digcomp_2.set(18, sharedpreferences.getInt("m2_digcomp_52", 0));
        digcomp_2.set(19, sharedpreferences.getInt("m2_digcomp_53", 0));
        digcomp_2.set(20, sharedpreferences.getInt("m2_digcomp_54", 0));
        digcomp_3.set(0, sharedpreferences.getInt("m3_digcomp_11", 0));
        digcomp_3.set(1, sharedpreferences.getInt("m3_digcomp_12", 0));
        digcomp_3.set(2, sharedpreferences.getInt("m3_digcomp_13", 0));
        digcomp_3.set(3, sharedpreferences.getInt("m3_digcomp_21", 0));
        digcomp_3.set(4, sharedpreferences.getInt("m3_digcomp_22", 0));
        digcomp_3.set(5, sharedpreferences.getInt("m3_digcomp_23", 0));
        digcomp_3.set(6, sharedpreferences.getInt("m3_digcomp_24", 0));
        digcomp_3.set(7, sharedpreferences.getInt("m3_digcomp_25", 0));
        digcomp_3.set(8, sharedpreferences.getInt("m3_digcomp_26", 0));
        digcomp_3.set(9, sharedpreferences.getInt("m3_digcomp_31", 0));
        digcomp_3.set(10, sharedpreferences.getInt("m3_digcomp_32", 0));
        digcomp_3.set(11, sharedpreferences.getInt("m3_digcomp_33", 0));
        digcomp_3.set(12, sharedpreferences.getInt("m3_digcomp_34", 0));
        digcomp_3.set(13, sharedpreferences.getInt("m3_digcomp_41", 0));
        digcomp_3.set(14, sharedpreferences.getInt("m3_digcomp_42", 0));
        digcomp_3.set(15, sharedpreferences.getInt("m3_digcomp_43", 0));
        digcomp_3.set(16, sharedpreferences.getInt("m3_digcomp_44", 0));
        digcomp_3.set(17, sharedpreferences.getInt("m3_digcomp_51", 0));
        digcomp_3.set(18, sharedpreferences.getInt("m3_digcomp_52", 0));
        digcomp_3.set(19, sharedpreferences.getInt("m3_digcomp_53", 0));
        digcomp_3.set(20, sharedpreferences.getInt("m3_digcomp_54", 0));
        digcomp_4.set(0, sharedpreferences.getInt("m4_digcomp_11", 0));
        digcomp_4.set(1, sharedpreferences.getInt("m4_digcomp_12", 0));
        digcomp_4.set(2, sharedpreferences.getInt("m4_digcomp_13", 0));
        digcomp_4.set(3, sharedpreferences.getInt("m4_digcomp_21", 0));
        digcomp_4.set(4, sharedpreferences.getInt("m4_digcomp_22", 0));
        digcomp_4.set(5, sharedpreferences.getInt("m4_digcomp_23", 0));
        digcomp_4.set(6, sharedpreferences.getInt("m4_digcomp_24", 0));
        digcomp_4.set(7, sharedpreferences.getInt("m4_digcomp_25", 0));
        digcomp_4.set(8, sharedpreferences.getInt("m4_digcomp_26", 0));
        digcomp_4.set(9, sharedpreferences.getInt("m4_digcomp_31", 0));
        digcomp_4.set(10, sharedpreferences.getInt("m4_digcomp_32", 0));
        digcomp_4.set(11, sharedpreferences.getInt("m4_digcomp_33", 0));
        digcomp_4.set(12, sharedpreferences.getInt("m4_digcomp_34", 0));
        digcomp_4.set(13, sharedpreferences.getInt("m4_digcomp_41", 0));
        digcomp_4.set(14, sharedpreferences.getInt("m4_digcomp_42", 0));
        digcomp_4.set(15, sharedpreferences.getInt("m4_digcomp_43", 0));
        digcomp_4.set(16, sharedpreferences.getInt("m4_digcomp_44", 0));
        digcomp_4.set(17, sharedpreferences.getInt("m4_digcomp_51", 0));
        digcomp_4.set(18, sharedpreferences.getInt("m4_digcomp_52", 0));
        digcomp_4.set(19, sharedpreferences.getInt("m4_digcomp_53", 0));
        digcomp_4.set(20, sharedpreferences.getInt("m4_digcomp_54", 0));

        context = this;
        String json = null;
        try {
            InputStream is = this.getAssets().open("digcomp.json");
            Log.e("DONE", "DONE");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (
                IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        switch (competenceCounter) {
            case 11:
                preQuestionText = "Bei Kompetenz 1.1 geht es um die Beschaffung von Informationen mit Hilfe von digitalen Medien:";
                c = 0;
                break;
            case 12:
                preQuestionText = "Bei Kompetenz 1.2 geht es um die Bewertung von Informationen aus digitalen Medien:";
                c = 1;
                break;
            case 13:
                preQuestionText = "Bei Kompetenz 1.3 geht es um die Organisation und Speicherung von Informationen in digitalen Medien:";
                c = 2;
                break;
            case 21:
                preQuestionText = "Bei Kompetenz 2.1 geht es um die Kommunikation mit Hilfe von digitalen Medien:";
                c = 3;
                break;
            case 22:
                preQuestionText = "Bei Kompetenz 2.2 geht es um das Teilen von Daten mit Hilfe von digitalen Medien, sowie die Angabe der Herkunft der Daten:";
                c = 4;
                break;
            case 23:
                preQuestionText = "Bei Kompetenz 2.3 geht es um digitale Dienste zur Teilhabe an der Gesellschaft:";
                c = 5;
                break;
            case 24:
                preQuestionText = "Bei Kompetenz 2.4 geht es um Zusammenarbeit mit anderen mit Hilfe von digitalen Werkzeugen:";
                c = 6;
                break;
            case 25:
                preQuestionText = "Bei Kompetenz 2.5 geht es um das eigene Verhalten in digitalen Umgebungen:";
                c = 7;
                break;
            case 26:
                preQuestionText = "Bei Kompetenz 2.6 geht es um die eigene Identität und den eigenen Ruf in digitalen Umgebungen:";
                c = 8;
                break;
            case 31:
                preQuestionText = "Bei Kompetenz 3.1 geht es um die Erstellung und Bearbeitung von Inhalten mit digitalen Medien:";
                c = 9;
                break;
            case 32:
                preQuestionText = "Bei Kompetenz 3.2 geht es um die Veränderung digitaler Inhalte, um krative neue Inhalte zu Erstellen:";
                c = 10;
                break;
            case 33:
                preQuestionText = "Bei Kompetenz 3.3 geht es um Urheberrecht und Lizensierung in digitalen Umgbungen:";
                c = 11;
                break;
            case 34:
                preQuestionText = "Bei Kompetenz 3.4 geht es um die Erstellung von Anweisungen oder Anleitungen, um Probleme mit Hilfe von digitalen Medien zu lösen:";
                c = 12;
                break;
            case 41:
                preQuestionText = "Bei Kompetenz 4.1 geht es um den Schutz der eigenen digitalen Geräte und Inhalte:";
                c = 13;
                break;
            case 42:
                preQuestionText = "Bei Kompetenz 4.2 geht es um den Schutz der eigenen Privatsphäre in digitalen Umgebungen:";
                c = 14;
                break;
            case 43:
                preQuestionText = "Bei Kompetenz 4.3 geht es um den Umgang mit Gefahren in digitalen Umgebungen:";
                c = 15;
                break;
            case 44:
                preQuestionText = "Bei Kompetenz 4.4 geht es um die Auswirkungen digitaler Medien auf die Umwelt:";
                c = 16;
                break;
            case 51:
                preQuestionText = "Bei Kompetenz 5.1 geht es um technische Probleme bei der Nutzung von digitalen Medien:";
                c = 17;
                break;
            case 52:
                preQuestionText = "Bei Kompetenz 5.2 geht es um Bedürfnisse und Anforderungen an digitale Medien:";
                c = 18;
                break;
            case 53:
                preQuestionText = "Bei Kompetenz 5.3 geht es um digitale Werkzeuge und Technologien, die zur Erzeugung von Wissen und zur Erneuerung von Prozessen und Produkten genutzt werden können:";
                c = 19;
                break;
            case 54:
                preQuestionText = "Bei Kompetenz 5.4 geht es um die Reflektion der eigenen digitalen Kompetenzen:";
                c = 20;
                break;
        }

        if ((currentModule >= 1 && currentModule <= 4) && prevC != competenceCounter) {
            levelCounter = (int) digcomp_init.get(c);
            levelCounter++;
            if (levelCounter == 9) {
                c++;
            }
            prevC = competenceCounter;
            Log.e("DIGCOMP_STATUS", "c = " + c + "; l = " + levelCounter);
        }

        Log.e("DIGCOMP_STATUS", "cl = " + digcomp_init.get(c) + "; l = " + levelCounter + "; cc = " + competenceCounter);


        if (myJson != null) {
            for (int i = 0; i < myJson.length(); ++i) {
                try {
                    rec = myJson.getJSONObject(i);
                    Log.e("rec", rec + "");

                    String id = rec.getString("id");


                    String question = "c" + competenceCounter + "-" + levelCounter;
                    if (id.equals(question)) {
                        TextView likertQuestion = findViewById(R.id.my_usecase_textview);
                        likert1 = findViewById(R.id.alt1);
                        likert2 = findViewById(R.id.alt2);
                        likert3 = findViewById(R.id.alt3);
                        likert4 = findViewById(R.id.alt4);
                        likert1.setVisibility(View.GONE);
                        likert2.setVisibility(View.GONE);
                        likert3.setVisibility(View.GONE);
                        likert4.setVisibility(View.GONE);
                        String questionText = preQuestionText + "<br><b>" + rec.getString("question") + "</b>";
                        likertQuestion.setText(Html.fromHtml(questionText));
                        for (int j = 0; j < rec.getJSONArray("answers").length(); j++) {
                            Log.e("answers", j + "");
                            switch (j) {
                                case 0:
                                    likert1.setText(rec.getJSONArray("answers").getString(j));
                                    likert1.setVisibility(View.VISIBLE);
                                    likert1.setOnTouchListener(new View.OnTouchListener() {
                                        public boolean onTouch(View v, MotionEvent event) {
                                            myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, likert1));
                                            return false;
                                        }
                                    });
                                    break;
                                case 1:
                                    likert2.setText(rec.getJSONArray("answers").getString(j));
                                    likert2.setVisibility(View.VISIBLE);
                                    likert2.setOnTouchListener(new View.OnTouchListener() {
                                        public boolean onTouch(View v, MotionEvent event) {
                                            myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, likert2));
                                            return false;
                                        }
                                    });
                                    break;
                                case 2:
                                    likert3.setText(rec.getJSONArray("answers").getString(j));
                                    likert3.setVisibility(View.VISIBLE);
                                    likert3.setOnTouchListener(new View.OnTouchListener() {
                                        public boolean onTouch(View v, MotionEvent event) {
                                            myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, likert3));
                                            return false;
                                        }
                                    });
                                    break;
                                case 3:
                                    likert4.setText(rec.getJSONArray("answers").getString(j));
                                    likert4.setVisibility(View.VISIBLE);
                                    likert4.setOnTouchListener(new View.OnTouchListener() {
                                        public boolean onTouch(View v, MotionEvent event) {
                                            myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, likert4));
                                            return false;
                                        }
                                    });
                                    break;
                                default:
                                    break;
                            }
                            numberOfAnswers = j;
                        }


                        final Button next = findViewById(R.id.next);
                        next.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, next));
                                try {
                                    if (event.getAction() == MotionEvent.ACTION_UP) {
                                        nextListener(rec.getString("id"), rec.getString("question"));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }
                        });
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }


    private void nextListener(String id, String description) {

        boolean check1 = true;
        boolean check2 = true;
        boolean check3 = true;
        boolean check4 = true;

        String answers = "";


        for (int i = 0; i <= numberOfAnswers; i++) {
            switch (i) {
                case 0:
                    check1 = likert1.isChecked();
                    answers = answers + check1;
                    break;
                case 1:
                    check2 = likert2.isChecked();
                    answers = answers + "; " + check2;
                    break;
                case 2:
                    check3 = likert3.isChecked();
                    answers = answers + "; " + check3;
                    break;
                case 3:
                    check4 = likert4.isChecked();
                    answers = answers + "; " + check4;
                    break;
            }
        }


        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(id, description, answers));


        if (levelCounter < 8 && check1 && check2 && check3 && check4) {
            Log.e("STATUS", "Aktuelles Level für Kompetenz " + competenceCounter + ": " + levelCounter);
            levelCounter++;
            Intent myIntent = new Intent(context, DigComp.class);
            startActivity(myIntent);
            finish();
        } else {
            Log.e("STATUS", "Finales Level für Kompetenz " + competenceCounter + ": " + levelCounter);

            if(levelCounter == 8){
                levelCounter++;
            }

            SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            switch (currentModule) {
                case 1:
                    currentC = digcomp_1;
                    editor.putInt("m1_digcomp_" + competenceCounter, levelCounter - 1);
                    break;
                case 2:
                    currentC = digcomp_2;
                    editor.putInt("m2_digcomp_" + competenceCounter, levelCounter - 1);
                    break;
                case 3:
                    currentC = digcomp_3;
                    editor.putInt("m3_digcomp_" + competenceCounter, levelCounter - 1);
                    break;
                case 4:
                    currentC = digcomp_4;
                    editor.putInt("m4_digcomp_" + competenceCounter, levelCounter - 1);
                    break;
                default:
                    currentC = digcomp_init;
                    editor.putInt("m0_digcomp_" + competenceCounter, levelCounter - 1);
                    break;
            }
            editor.apply();

            switch (competenceCounter) {
                case 11:
                    currentC.set(0, levelCounter);
                    competenceCounter = 12;
                    break;
                case 12:
                    currentC.set(1, levelCounter - 1);
                    competenceCounter = 13;
                    break;
                case 13:
                    currentC.set(2, levelCounter - 1);
                    competenceCounter = 21;
                    break;
                case 21:
                    currentC.set(3, levelCounter - 1);
                    competenceCounter = 22;
                    break;
                case 22:
                    currentC.set(4, levelCounter - 1);
                    competenceCounter = 23;
                    break;
                case 23:
                    currentC.set(5, levelCounter - 1);
                    competenceCounter = 24;
                    break;
                case 24:
                    currentC.set(6, levelCounter - 1);
                    competenceCounter = 25;
                    break;
                case 25:
                    currentC.set(7, levelCounter - 1);
                    competenceCounter = 26;
                    break;
                case 26:
                    currentC.set(8, levelCounter - 1);
                    competenceCounter = 31;
                    break;
                case 31:
                    currentC.set(9, levelCounter - 1);
                    competenceCounter = 32;
                    break;
                case 32:
                    currentC.set(10, levelCounter - 1);
                    competenceCounter = 33;
                    break;
                case 33:
                    currentC.set(11, levelCounter - 1);
                    competenceCounter = 34;
                    break;
                case 34:
                    currentC.set(12, levelCounter - 1);
                    competenceCounter = 41;
                    break;
                case 41:
                    currentC.set(13, levelCounter - 1);
                    competenceCounter = 42;
                    break;
                case 42:
                    currentC.set(14, levelCounter - 1);
                    competenceCounter = 43;
                    break;
                case 43:
                    currentC.set(15, levelCounter - 1);
                    competenceCounter = 44;
                    break;
                case 44:
                    currentC.set(16, levelCounter - 1);
                    competenceCounter = 51;
                    break;
                case 51:
                    currentC.set(17, levelCounter - 1);
                    competenceCounter = 52;
                    break;
                case 52:
                    currentC.set(18, levelCounter - 1);
                    competenceCounter = 53;
                    break;
                case 53:
                    currentC.set(19, levelCounter - 1);
                    competenceCounter = 54;
                    break;
                case 54:
                    currentC.set(20, levelCounter - 1);
                    competenceCounter = 0;
                    break;
            }


            switch (currentModule) {
                case 1:
                    digcomp_1 = currentC;
                    break;
                case 2:
                    digcomp_2 = currentC;
                    break;
                case 3:
                    digcomp_3 = currentC;
                    break;
                case 4:
                    digcomp_4 = currentC;
                    break;
                default:
                    digcomp_init = currentC;
                    break;
            }


            Log.e("DIGCOMP_INIT", digcomp_init.toString());
            Log.e("DIGCOMP_1", digcomp_1.toString());
            Log.e("DIGCOMP_2", digcomp_2.toString());
            Log.e("DIGCOMP_3", digcomp_3.toString());
            Log.e("DIGCOMP_4", digcomp_4.toString());

            levelCounter = 1;

            if (competenceCounter != 0) {
                Intent myIntent = new Intent(context, DigComp.class);
                startActivity(myIntent);
                finish();
            } else {
                Intent myIntent = null;
                switch (currentModule) {
                    case 1:
                        myIntent = new Intent(context, CommunicationEnd.class);
                        break;
                    case 2:
                        myIntent = new Intent(context, InternetEnd.class);
                        break;
                    case 3:
                        myIntent = new Intent(context, ConnectionEnd.class);
                        break;
                    case 4:
                        myIntent = new Intent(context, DataSecEnd.class);
                        break;
                    default:
                        myIntent = new Intent(context, AppStart.class);
                        break;
                }
                levelCounter = 1;
                competenceCounter = 11;
                startActivity(myIntent);
                finish();
            }

        }
    }


}
