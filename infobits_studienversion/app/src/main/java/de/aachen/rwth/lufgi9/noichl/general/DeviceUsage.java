package de.aachen.rwth.lufgi9.noichl.general;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.modul4.DatasecurityStart;

public class DeviceUsage extends MyActivity {

    Context context;
    int index = -1;
    int index2 = -1;
    TextView tv1, tv2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("COUNTER", QuestionairAgeAndUse.counter+"");
        setContentView(R.layout.questionair_device_usage);
        context = this;
        tv1 = findViewById(R.id.textView5);
        tv2 = findViewById(R.id.textView6);


        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);

        String s1= "";
        String s2= "";
        if(QuestionairAgeAndUse.counter == 0){s1 = "c_where"; s2 = "c_time";}
        if(QuestionairAgeAndUse.counter == 1){s1 = "s_where"; s2 = "s_time";}
        if(QuestionairAgeAndUse.counter == 2){s1 = "t_where"; s2 = "t_time";}

        switch (QuestionairAgeAndUse.counter){
            case 0:
                if(QuestionairAgeAndUse.c){
                tv1.setText(R.string.computer_privat_or_work);
                tv2.setText(R.string.computer_usage_time);
                    int i1 = sharedpreferences.getInt(s1, -1);
                    if(i1 >= 0) {
                        RadioGroup rg = findViewById(R.id.use_where);
                        RadioButton rb = (RadioButton) rg.getChildAt(i1);
                        rb.setChecked(true);
                    }
                    int i2 = sharedpreferences.getInt(s2, -1);
                    if(i2 >= 0) {
                        RadioGroup rg = findViewById(R.id.use_time);
                        RadioButton rb = (RadioButton) rg.getChildAt(i2);
                        rb.setChecked(true);
                    }
                    QuestionairAgeAndUse.counter++;
                    break;
            }else{
                QuestionairAgeAndUse.counter++;
            }


            case 1:
                if(QuestionairAgeAndUse.s){
                    tv1.setText(R.string.smartphone_privat_or_work);
                    tv2.setText(R.string.smartphone_usage_time);
                    int i1 = sharedpreferences.getInt(s1, -1);
                    if(i1 >= 0) {
                        RadioGroup rg = findViewById(R.id.use_where);
                        RadioButton rb = (RadioButton) rg.getChildAt(i1);
                        rb.setChecked(true);
                    }
                    int i2 = sharedpreferences.getInt(s2, -1);
                    if(i2 >= 0) {
                        RadioGroup rg = findViewById(R.id.use_time);
                        RadioButton rb = (RadioButton) rg.getChildAt(i2);
                        rb.setChecked(true);
                    }
                    QuestionairAgeAndUse.counter++;
                    break;
                }else{
                    QuestionairAgeAndUse.counter++;
                }

            case 2:
                if(QuestionairAgeAndUse.t){
                    tv1.setText(R.string.tablet);
                    tv2.setText(R.string.tablet_usage_time);
                    int i1 = sharedpreferences.getInt(s1, -1);
                    if(i1 >= 0) {
                        RadioGroup rg = findViewById(R.id.use_where);
                        RadioButton rb = (RadioButton) rg.getChildAt(i1);
                        rb.setChecked(true);
                    }
                    int i2 = sharedpreferences.getInt(s2, -1);
                    if(i2 >= 0) {
                        RadioGroup rg = findViewById(R.id.use_time);
                        RadioButton rb = (RadioButton) rg.getChildAt(i2);
                        rb.setChecked(true);
                    }
                    QuestionairAgeAndUse.counter++;
                    break;
                }else{
                    QuestionairAgeAndUse.counter++;
                }

                default:Intent myIntent = new Intent(context,   PreCompInformation.class);
                    startActivity(myIntent);
                    finish();
        }

        Button nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    RadioGroup radioGroup = findViewById(R.id.use_where);
                    index = radioGroup.indexOfChild(findViewById(radioGroup.getCheckedRadioButtonId()));

                    RadioGroup radioGroup2 = findViewById(R.id.use_time);
                    index2 = radioGroup2.indexOfChild(findViewById(radioGroup2.getCheckedRadioButtonId()));

                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("usage_place", tv1.getText()+"", index + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("duration", tv2.getText()+"", index2 + ""));

                    String s1= "";
                    String s2= "";
                    if(QuestionairAgeAndUse.counter == 1){s1 = "c_where"; s2 = "c_time";}
                    if(QuestionairAgeAndUse.counter == 2){s1 = "s_where"; s2 = "s_time";}
                    if(QuestionairAgeAndUse.counter == 3){s1 = "t_where"; s2 = "t_time";}

                    SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt(s1, index);
                    editor.putInt(s2, index2);
                    editor.apply();


                    if(index != -1 && index2 != -1) {
                        Intent myIntent = new Intent(context, UseDeviceFor.class);
                        startActivity(myIntent);
                        finish();
                    }else{showDialog("Noch nicht fertig","Bitte beantworte beide Fragen durch antippen einer Antwortmöglichkeit.", context);}
                }

                return false;
            }
        });

        Button backButton = (Button) findViewById(R.id.back);
        if(QuestionairAgeAndUse.counter==1){
            backButton.setVisibility(View.GONE);
        }
        if(QuestionairAgeAndUse.counter==2 && !QuestionairAgeAndUse.c){
            backButton.setVisibility(View.GONE);
        }
        if(QuestionairAgeAndUse.counter==3 && !QuestionairAgeAndUse.c && !QuestionairAgeAndUse.s){
            backButton.setVisibility(View.GONE);
        }

        backButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    QuestionairAgeAndUse.counter--;
                        Intent myIntent = new Intent(context, UseDeviceFor.class);
                        startActivity(myIntent);
                        finish();
                    }


                return false;
            }
        });
    }
}
