package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the end of module 1.
 * This class gives an overview over all texts, videos and excercises of this module and the user can easily jump back to the content he/she wants to repeat.
 * The is also a possibility to watch the results of the pre- and post- "Selbsteinschätzung". If the statsButton is touched the Activity (XYPlotActivity) with the corresponding diagram will be started.
 * If the user press the nextButton the AppStart-Activity will be started.
 * All button touch events will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Recap extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;
    int currentActivity;
    boolean recapMode = false;

    /**
     * Loads the activities layout (R.layout.viewstub_finalpage_module1).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The recap variable is set to false and the AppStart-Activity will be started.
     * <b>statsButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The XYPlotActivity will be started to show the user a diagram.
     * <b>All other buttons:</b>Loggs time and touch-coordinates of users touch. Start the corresponding Activity with the content the user wants to repeat.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recap_modul2);

        context = this;


        if (myrecap){
            recapMode = true;
        }
        myrecap = true;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.recap_text);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setText(R.string.close);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(recapMode){

                }else {
                    myrecap = false;
                }
                finish();
                return false;
            }
        });

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);

        currentActivity = sharedpreferences.getInt("currentActivityM2", 200);





        final Button recap_video1 = findViewById(R.id.recap_video1);
        if (currentActivity < 205){
            recap_video1.setVisibility(View.GONE);
        }
        if (currentActivity == 205){
            recap_video1.setBackgroundResource(R.drawable.color_button);
        }
        recap_video1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 205){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), Binaersystem.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_video2 = findViewById(R.id.recap_video2);
        if (currentActivity < 209){
            recap_video2.setVisibility(View.GONE);
        }
        if (currentActivity == 209){
            recap_video2.setBackgroundResource(R.drawable.color_button);
        }
        recap_video2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 209){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DPVideo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_video3 = findViewById(R.id.recap_video3);
        if (currentActivity < 221){
            recap_video3.setVisibility(View.GONE);
        }
        if (currentActivity == 221){
            recap_video3.setBackgroundResource(R.drawable.color_button);
        }
        recap_video3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 221){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), URLVideo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_video4 = findViewById(R.id.recap_video4);
        if (currentActivity < 223){
            recap_video4.setVisibility(View.GONE);
        }
        if (currentActivity == 223){
            recap_video4.setBackgroundResource(R.drawable.color_button);
        }
        recap_video4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 223){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DNSVideo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text1 = findViewById(R.id.recap_text1);
        if (currentActivity < 203){
            recap_text1.setVisibility(View.GONE);
        }
        if (currentActivity == 203){
            recap_text1.setBackgroundResource(R.drawable.color_button);
        }
        recap_text1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 203){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), InternetInfo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text2 = findViewById(R.id.recap_text2);
        if (currentActivity < 204){
            recap_text2.setVisibility(View.GONE);
        }
        if (currentActivity == 204){
            recap_text2.setBackgroundResource(R.drawable.color_button);
        }
        recap_text2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 204){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DataInfo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text3 = findViewById(R.id.recap_text3);
        if (currentActivity < 208){
            recap_text3.setVisibility(View.GONE);
        }
        if (currentActivity == 208){
            recap_text3.setBackgroundResource(R.drawable.color_button);
        }
        recap_text3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 208){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), Datapackage.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text4 = findViewById(R.id.recap_text4);
        if (currentActivity < 210){
            recap_text4.setVisibility(View.GONE);
        }
        if (currentActivity == 210){
            recap_text4.setBackgroundResource(R.drawable.color_button);
        }
        recap_text4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 210){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), IPIntro.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text5 = findViewById(R.id.recap_text5);
        if (currentActivity < 217){
            recap_text5.setVisibility(View.GONE);
        }
        if (currentActivity == 217 || currentActivity == 218){
            recap_text5.setBackgroundResource(R.drawable.color_button);
        }
        recap_text5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 217 || currentActivity == 218){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), NetworkCompareHubSwitchRouterActivity.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text6 = findViewById(R.id.recap_text6);
        if (currentActivity < 219){
            recap_text6.setVisibility(View.GONE);
        }
        if (currentActivity == 219){
            recap_text6.setBackgroundResource(R.drawable.color_button);
        }
        recap_text6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 219){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DevicesInInternetInfo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text7 = findViewById(R.id.recap_text7);
        if (currentActivity < 220){
            recap_text7.setVisibility(View.GONE);
        }
        if (currentActivity == 220){
            recap_text7.setBackgroundResource(R.drawable.color_button);
        }
        recap_text7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text7));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 220){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), URLIntro.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text8 = findViewById(R.id.recap_text8);
        if (currentActivity < 222){
            recap_text8.setVisibility(View.GONE);
        }
        if (currentActivity == 222){
            recap_text8.setBackgroundResource(R.drawable.color_button);
        }
        recap_text8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text8));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 222){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DNSIntro.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text9 = findViewById(R.id.recap_text9);
        if (currentActivity < 227){
            recap_text9.setVisibility(View.GONE);
        }
        if (currentActivity == 227){
            recap_text9.setBackgroundResource(R.drawable.color_button);
        }
        recap_text9.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text9));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 227){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), HistoryIntro.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity1 = findViewById(R.id.recap_activity1);
        if (currentActivity < 206){
            recap_activity1.setVisibility(View.GONE);
        }
        if (currentActivity == 206 || currentActivity == 207){
            recap_activity1.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 206 || currentActivity == 207){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DezToBin.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity2 = findViewById(R.id.recap_activity2);
        if (currentActivity < 211){
            recap_activity2.setVisibility(View.GONE);
        }
        if (currentActivity == 211 || currentActivity == 212 || currentActivity == 213 || currentActivity == 214 || currentActivity == 215 || currentActivity == 216){
            recap_activity2.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 211 || currentActivity == 212 || currentActivity == 213 || currentActivity == 214 || currentActivity == 215 || currentActivity == 216){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), Devices.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity3 = findViewById(R.id.recap_activity3);
        if (currentActivity < 224){
            recap_activity3.setVisibility(View.GONE);
        }
        if (currentActivity == 224 || currentActivity == 225){
            recap_activity3.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 224 || currentActivity == 225){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), URL1.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity4 = findViewById(R.id.recap_activity4);
        if (currentActivity < 226){
            recap_activity4.setVisibility(View.GONE);
        }
        if (currentActivity == 226){
            recap_activity4.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 226){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), IpUrlDns.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity5 = findViewById(R.id.recap_activity5);
        if (currentActivity < 228){
            recap_activity5.setVisibility(View.GONE);
        }
        if (currentActivity == 228){
            recap_activity5.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 228){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), InternetQuizInfo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });


    }
}
