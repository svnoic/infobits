package de.aachen.rwth.lufgi9.noichl.basics.gesten;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the entry point of the inputs module.
 * This class is for learning the Tap gesture. The user has to tap on at least on of the provided ImageViews (plus and minus). A counter is provided for feedback.
 * If the counter was not changed a dialog box is shown saying that the user should use on of the images to change the counter.
 * If the counter was changed the next Activity (Scroll.class) is started.
 * All gestures will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Tap extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Represents the current value of the counter. Can be changed by tapping the ImageViews minus or plus.
     */
    int counter = 0;

    /**
     * Represents the status of the given solution.
     * If the counter was changed, solution has the state <b>true</b> and on nextButton-touch-event the next activity will be started.
     * If the counter was not changed, solution has the state <b>false</b> and on nextButton-touch-event a dialog will be shown.
     */
    boolean solution;

    /**
     * Loads the activities layout (R.layout.activity_textview_viewstub2_next_button) which uses a viewstub (basics_gestures_tap).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event either the next activity will be started or the dialog will be shown based on the status of the <b>solution</b> variable.
     * If solution is <b>true</b> the next activity (End.class) will be started, otherwise a dialog will be shown.
     * <b>plus-onTouchListener:</b>Updates the counter by adding one. The current counter value is logged with the myLog-method and solution is set to true.
     * <b>minus-onTouchListener:</b>Updates the counter by subtracting one. The current counter value is logged with the myLog-method and solution is set to true.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_s_mytextview_s_viewstub_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_gestures_tap);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.description_text_antippen);

        TextView c = findViewById(R.id.tv_counter);
        c.setText(String.valueOf(counter));


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (solution) {
                        solution = false;
                        Intent myIntent = new Intent(context, Scroll.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.gestures_tap_dialog), context);
                    }
                }

                return false;
            }
        });


        final ImageView plus = findViewById(R.id.imageView4);
        plus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, plus));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    counter++;
                    solution = true;
                    TextView tv = findViewById(R.id.tv_counter);
                    tv.setText(String.valueOf(counter));
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension(getString(R.string.gestures_current_value) + counter));
                }

                return false;
            }
        });

        final ImageView minus = findViewById(R.id.imageView3);
        minus.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, minus));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    counter--;
                    solution = true;
                    TextView tv = findViewById(R.id.tv_counter);
                    tv.setText(String.valueOf(counter));
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension(getString(R.string.gestures_current_value) + counter));
                }

                return false;
            }
        });

    }
}
