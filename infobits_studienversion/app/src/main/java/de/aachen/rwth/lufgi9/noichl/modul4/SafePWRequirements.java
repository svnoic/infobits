package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class SafePWRequirements extends MyActivity {

    private static boolean count = false;
    Context context;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("SafePWRequirements"));
        }
        editor.putInt("current", getActivityNumber("SafePWRequirements"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        if(count){
            tv.setText(R.string.safe_pw_reqlist);
        }else{
            tv.setText(R.string.safe_pw_req);
        }


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (count) {
                        if (recap) {
                            Intent myIntent = new Intent(context, DataSecEnd.class);
                            startActivity(myIntent);
                            finish();
                        } else {
                            if(myrecap){
                                restartAfterRecap();
                            }else {
                                Intent myIntent = new Intent(context, CreatePW.class);
                                startActivity(myIntent);
                                finish();
                            }
                        }
                        count = false;
                    } else {
                        Intent myIntent = nextActivity(getActivityNumber("SafePWRequirements") + 1);
                        startActivity(myIntent);
                        finish();
                        count = true;
                    }
                }
                return false;
            }
        });
    }
}
