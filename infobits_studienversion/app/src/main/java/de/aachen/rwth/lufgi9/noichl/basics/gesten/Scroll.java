package de.aachen.rwth.lufgi9.noichl.basics.gesten;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is for learning the Scroll gesture. The user has to scroll down until he/she reached the nextButton.
 * If the nextButton is pressed the next Activity (Fling.class) is started.
 * All gestures will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Scroll extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (R.layout.basics_gestures_scroll).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event either the next activity will be started.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.basics_gestures_scroll);


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.description_text_scroll);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(context, Fling.class);
                    startActivity(myIntent);
                    finish();
                }

                return false;
            }
        });
    }
}
