package de.aachen.rwth.lufgi9.noichl.modul1;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.Draw;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class LinkTalkLetter extends MyActivity implements View.OnTouchListener {

    static int counter = 0;
    Context context;
    private Draw draw;
    Button nextButton;
    View component1, component2, component3, component4;
    LinearLayout box1, box2;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.template_drawline_4_component);
        stub.inflate();

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("LinkTalkLetter"));
        }
        editor.putInt("current", getActivityNumber("LinkTalkLetter"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.transfer_to_digital);

        nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (counter > 0) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(motionEvent, nextButton));
                    counter = 0;
                    if (recap) {
                        Intent myIntent = new Intent(context, CommunicationEnd.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        if (myrecap) {
                            restartAfterRecap();
                        } else {
                            //Intent myIntent = new Intent(context, CommunicationUsage.class);
                            Intent myIntent = nextActivity(getActivityNumber("LinkTalkLetter") + 1);
                            startActivity(myIntent);
                            finish();
                        }
                    }
                } else {

                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_link_talk_letter_dialog), context);
                    }
                }
                return false;
            }
        });

        draw = findViewById(R.id.draw);
        draw.setOnTouchListener(this);

        component1 = findViewById(R.id.component1);
        component2 = findViewById(R.id.component2);
        component3 = findViewById(R.id.component3);
        component4 = findViewById(R.id.component4);
        box1 = findViewById(R.id.box1);
        box2 = findViewById(R.id.box2);

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        //draw.addUndirectedViewConnection(component1, component2);
        //draw.addUndirectedViewConnection(component3, component4);
        box1.setBackgroundColor(getResources().getColor(R.color.primaryColor));
        box2.setBackgroundColor(getResources().getColor(R.color.primaryDarkColor));
        draw.invalidate();
        counter++;
        return false;
    }
}
