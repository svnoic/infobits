package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.aachen.rwth.lufgi9.noichl.R;

/**
 * Custom view for a network component in the network component bar.
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class NetworkComponent extends ConstraintLayout {

    /**
     * Enabled means that the component is clickable. Disabled means that the user cannot use the component anymore.
     */
    private boolean enabled;

    /**
     * The view for the image
     */
    private final ImageView image;

    /**
     * The layout id of the image
     */
    private int imageSrc;

    /**
     * The view for the text
     */
    private final TextView text;

    /**
     * The value of the {@link #text}
     */
    private final String textValue;

    /**
     * The main constructor that contains also user customized attributes
     *
     * @param context Needed for parent constructor and access resources
     * @param attrs   Set of attributes given in the xml layout file
     */
    public NetworkComponent(Context context, AttributeSet attrs) {
        super(context, attrs);

        // the user given custom style attributes in the layout xml file
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NetworkComponent);

        // load and add the main view for this custom navigation button view
        LayoutInflater.from(getContext()).inflate(R.layout.template_network_component, this, true);

        this.text = findViewById(R.id.network_component_text);
        // set custom text size
        //FontSizeUtilities.setTextSize(context, text);
        this.image = findViewById(R.id.network_component_image);

        textValue = a.getString(R.styleable.NetworkComponent_text);

        imageSrc = a.getResourceId(R.styleable.NetworkComponent_src, 0);

        this.enabled = a.getBoolean(R.styleable.NetworkComponent_enabled, true);

        // set button background
        setBackgroundResource(R.drawable.background_network_connected_component_bar_item);
        setClickable(true);
        setFocusable(true);

        updateView();

        a.recycle();
    }

    /**
     * Update the view based on local attributes
     */
    private void updateView() {
        // update the text
        text.setText(textValue);

        // update the image
        if (imageSrc != 0) {
            Drawable src = ResourcesCompat.getDrawable(getContext().getResources(), imageSrc, getContext().getTheme());
            if (src != null) {
                image.setImageDrawable(src);
            }
        }

        findViewById(R.id.network_component_disabled_view).setVisibility(enabled ? View.GONE : View.VISIBLE);
    }

    /**
     * Enable or disable the component
     *
     * @param enabled the new value if the component is enabled for the user or not
     */
    public void setCustomEnabled(boolean enabled) {
        this.enabled = enabled;

        findViewById(R.id.network_component_disabled_view).setVisibility(enabled ? View.GONE : View.VISIBLE);
    }

    /**
     * Update the text.
     *
     * @param text The text that will be displayed in the view
     */
    public void setText(String text) {
        this.text.setText(text);
    }

    /**
     * @return String that is a serialized representation of this object
     */
    public String serialize() {
        return "text=" + text.getText()
                + ",imageSrc=" + imageSrc
                + ",enabled=" + enabled;
    }

    /**
     * Deserialize a state stored in a string
     *
     * @param str Contains all needed states
     */
    public void deserialize(String str) {
        Pattern pattern = Pattern.compile("text=(([^,]|$)*)");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            text.setText(matcher.group(1));
        }

        pattern = Pattern.compile("imageSrc=(([^,]|$)*)");
        matcher = pattern.matcher(str);
        if (matcher.find()) {
            imageSrc = Integer.parseInt(matcher.group(1));
        }

        pattern = Pattern.compile("enabled=(([^,]|$)*)");
        matcher = pattern.matcher(str);
        if (matcher.find()) {
            enabled = Boolean.parseBoolean(matcher.group(1));
        }

        updateView();
    }

    @Override
    public Parcelable onSaveInstanceState() {
        // get the parent state
        Parcelable superState = super.onSaveInstanceState();

        // create a new state
        SavedState result = new SavedState(superState);
        result.state = serialize();

        return result;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        // if the state is not a custom state, call only parent method
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        // otherwise cast to custom sate
        SavedState savedState = (SavedState) state;

        // restore parent state
        super.onRestoreInstanceState(savedState.getSuperState());

        // parse the string to this object
        deserialize(savedState.state);
    }


    /**
     * State class for a string representation of a {@link NetworkComponent} instance.
     */
    static class SavedState extends BaseSavedState {
        /**
         * The string representation of the current state of the {@link NetworkComponent} instance.
         */
        private String state;

        /**
         * Constructor for deserialize. Constructor used when reading from a parcel. Reads the state of the superclass.
         *
         * @param source parcel to read from
         */
        public SavedState(Parcel source) {
            super(source);
            this.state = source.readString();
        }

        /**
         * Constructor called by derived classes when creating their SavedState objects
         *
         * @param superState The state of the superclass of this view
         */
        public SavedState(Parcelable superState) {
            super(superState);
        }

        public void setState(String state) {
            this.state = state;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);

            // add the state to the parcel
            out.writeString(state);
        }
    }
}
