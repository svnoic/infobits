package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.text.TextUtils;

import java.util.Arrays;
import java.util.List;

/**
 * This class contains useful methods for state handling.
 * For example, you can use this class to concatenate states to a single string or search for a state in a string.
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class States {
    /**
     * @param states The states that will be combined to a single string.
     * @return single string with all states concatenated by a delimiter
     */
    public static String encode(String... states){
        return TextUtils.join(",", states);
    }

    /**
     * @param str the string that contains a list of states
     * @return list of states as string list
     */
    public static String[] decode(String str){
        return TextUtils.split(str, ",");
    }

    /**
     * Check if the given <code>state</code> is in the list of states, that are in the string <code>states</code>.
     *
     * @param states List of states joined in a string
     * @param state the state to check
     * @return <code>true</code> if the <code>state</code> occurs as single string in <code>states</code>
     */
    public static boolean contains(String states, String state){
        // check only if both not empty
        if(states == null || state==null){
            return false;
        }

        // get a state list
        String[] decodedStates = decode(states);
        // convert to a list just for look up the state entry
        List<String> decodedStateArray = Arrays.asList(decodedStates);

        // look up the state entry
        return decodedStateArray.contains(state);
    }

    /**
     * This methods adds the <code>state</code>, if it is not already in
     * @param states List of states joined in a string
     * @param state the state to add
     * @return List of states joined in a string with the new <code>state</code>
     */
    public static String addState(String states, String state){
        // if the state is already in, do nothing
        if(contains(states, state)) return states;

        // return the state itself, if the current list of states is empty
        if(states == null || states.length() == 0){
            return state;
        }

        // otherwise, concatenate
        return states + "," + state;
    }
}
