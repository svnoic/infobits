package de.aachen.rwth.lufgi9.noichl.modul3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class GameBook2a extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_button_button);
        context = this;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.module3_gamebook_2a_text);

        final Button nextButton1 = findViewById(R.id.next_one);
        nextButton1.setText(R.string.module3_gamebook_2a_next_1);
        nextButton1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton1));
                Intent myIntent = new Intent(context, GameBook3.class);
                startActivity(myIntent);
                finish();

                return false;
            }
        });
        final Button nextButton2 = findViewById(R.id.next_two);
        nextButton2.setText(R.string.module3_gamebook_2a_next_2);
        nextButton2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton2));
                Intent myIntent = new Intent(context, GameBook2b.class);
                startActivity(myIntent);
                finish();

                return false;
            }
        });
    }
}
