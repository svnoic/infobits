package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class DezToBin extends MyActivity {

    public static int binary = 0;
    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM2", getActivityNumber("DezToBin"));
        }
        editor.putInt("current", getActivityNumber("DezToBin"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_dez_to_bin);
        stub.inflate();

        TextView tv = findViewById(R.id.textView);
        tv.setText(R.string.compute_binary);
        TextView number = findViewById(R.id.number);
        number.setText(R.string._23);

        final Button b16 = findViewById(R.id.button16);
        b16.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, b16));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (b16.getText().equals("1")) {
                        binary -= 16;
                        b16.setText("0");
                    } else {
                        binary += 16;
                        b16.setText("1");
                    }
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension("Current binary: " + binary));
                }
                return false;
            }
        });

        final Button b8 = findViewById(R.id.button8);
        b8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, b8));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (b8.getText().equals("1")) {
                        binary -= 8;
                        b8.setText("0");
                    } else {
                        binary += 8;
                        b8.setText("1");
                    }
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension("Current binary: " + binary));
                }
                return false;
            }
        });

        final Button b4 = findViewById(R.id.button4);
        b4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, b4));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (b4.getText().equals("1")) {
                        binary -= 4;
                        b4.setText("0");
                    } else {
                        binary += 4;
                        b4.setText("1");
                    }
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension("Current binary: " + binary));
                }
                return false;
            }
        });

        final Button b2 = findViewById(R.id.button2);
        b2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, b2));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (b2.getText().equals("1")) {
                        binary -= 2;
                        b2.setText("0");
                    } else {
                        binary += 2;
                        b2.setText("1");
                    }
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension("Current binary: " + binary));
                }
                return false;
            }
        });

        final Button b1 = findViewById(R.id.button1);
        b1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, b1));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (b1.getText().equals("1")) {
                        binary -= 1;
                        b1.setText("0");
                    } else {
                        binary += 1;
                        b1.setText("1");
                    }
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension("Current binary: " + binary));
                }
                return false;
            }
        });


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(binary == 23) {
                        Intent myIntent = new Intent(context, DezToBin2.class);
                        startActivity(myIntent);
                        binary = 0;
                        finish();
                    }else{showDialog(getString(R.string.module2_destobin_dialog_title), getString(R.string.module2_destobin_dialog_message) + binary, context);}
                }
                return false;
            }
        });

        final Button skipButton = findViewById(R.id.skip);
        skipButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, skipButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                        Intent myIntent = nextActivity(getActivityNumber("DezToBin") + 1);
                        startActivity(myIntent);
                        binary = 0;
                        finish();
                }
                return false;
            }
        });
    }
}
