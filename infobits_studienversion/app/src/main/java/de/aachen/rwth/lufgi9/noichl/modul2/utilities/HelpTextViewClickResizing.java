package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.view.animation.Animation;
import android.widget.TextView;

/**
 * A class to minimize a help text view and make it to origin size on click again.
 *
 * @author Karl Ricken
 * @version 1.0
 * @see ViewClickResizing
 * @since 1.0
 */
public class HelpTextViewClickResizing extends ViewClickResizing<TextView> {

    /**
     * The backup for the text, when the user changes the size.
     */
    private CharSequence textBackup;

    /**
     * Normal constructor
     *
     * @param callerId A string for log for the class that is calling this class
     * @param view     The view to minimize on click.
     */
    public HelpTextViewClickResizing(String callerId, TextView view) {
        super(callerId, view);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        super.onAnimationStart(animation);
        if (!isMinimized()) {
            getView().setText(textBackup);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        super.onAnimationEnd(animation);
        if (isMinimized()) {
            textBackup = getView().getText();
            getView().setText(null);
        }
    }
}
