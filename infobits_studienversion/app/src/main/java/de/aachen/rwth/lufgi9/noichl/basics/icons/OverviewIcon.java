package de.aachen.rwth.lufgi9.noichl.basics.icons;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.AppStart;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

import static de.aachen.rwth.lufgi9.noichl.basics.icons.SelectIcon.*;

/**
 * This class is the last Activity of the Foundation-Icons Module. Here the use gets an overview over the icons he/she has choose and which icons were used in many apps.
 * If the user press the nextButton the AppStart-Activity will be started.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class OverviewIcon extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (activity_textview_viewstub2_next_button) which uses a viewstub (basics_icons_overview).
     * The imageviews with even numbers show the often used icons, the odd ones the icons selected by the user.
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinated of users touch.
     * Any touch event on this button will trigger the start of AppStart-Activity.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_icons_overview);
        stub.inflate();

        context = this;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.overview_text);

        ImageView iv1 = findViewById(R.id.iv1);
        ImageView iv2 = findViewById(R.id.iv2);
        ImageView iv3 = findViewById(R.id.iv3);
        ImageView iv4 = findViewById(R.id.iv4);
        ImageView iv5 = findViewById(R.id.iv5);
        ImageView iv6 = findViewById(R.id.iv6);
        ImageView iv7 = findViewById(R.id.iv7);
        ImageView iv8 = findViewById(R.id.iv8);
        ImageView iv9 = findViewById(R.id.iv9);
        ImageView iv10 = findViewById(R.id.iv10);
        ImageView iv11 = findViewById(R.id.iv11);
        ImageView iv12 = findViewById(R.id.iv12);
        ImageView iv13 = findViewById(R.id.iv13);
        ImageView iv14 = findViewById(R.id.iv14);
        ImageView iv15 = findViewById(R.id.iv15);
        ImageView iv16 = findViewById(R.id.iv16);
        ImageView iv17 = findViewById(R.id.iv17);
        ImageView iv18 = findViewById(R.id.iv18);

        icons.set(6, R.mipmap.ic_alarm_on_black_48dp);

        iv1.setImageResource((Integer) icons.get(1));
        iv2.setImageResource((Integer) icons.get(0));
        iv3.setImageResource((Integer) icons.get(3));
        iv4.setImageResource((Integer) icons.get(2));
        iv5.setImageResource((Integer) icons.get(5));
        iv6.setImageResource((Integer) icons.get(4));
        iv7.setImageResource((Integer) icons.get(7));
        iv8.setImageResource((Integer) icons.get(6));
        iv9.setImageResource((Integer) icons.get(9));
        iv10.setImageResource((Integer) icons.get(8));
        iv11.setImageResource((Integer) icons.get(11));
        iv12.setImageResource((Integer) icons.get(10));
        iv13.setImageResource((Integer) icons.get(13));
        iv14.setImageResource((Integer) icons.get(12));
        iv15.setImageResource((Integer) icons.get(15));
        iv16.setImageResource((Integer) icons.get(14));
        iv17.setImageResource((Integer) icons.get(17));
        iv18.setImageResource((Integer) icons.get(16));

        final Button next = findViewById(R.id.next);
        next.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, next));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    number = 0;
                    icons.clear();
                    Intent myIntent = new Intent(context, AppStart.class);
                    startActivity(myIntent);
                    finish();
                }
                return true;
            }
        });
    }

}
