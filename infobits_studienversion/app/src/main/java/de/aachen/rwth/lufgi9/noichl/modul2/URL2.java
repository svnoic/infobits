package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class URL2 extends MyActivity implements AdapterView.OnItemSelectedListener{

    private static boolean solution = false;
    Context context;
    Spinner url1, url2, url3, url4;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM2", getActivityNumber("URL2"));
        }
        editor.putInt("current", getActivityNumber("URL2"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_4_spinner_in_line);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.module2_url2_info);



        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (!solution) {
                        String item1 = url1.getSelectedItem().toString();
                        String item2 = url2.getSelectedItem().toString();
                        String item3 = url3.getSelectedItem().toString();
                        String item4 = url4.getSelectedItem().toString();

                        if (!item1.equals("...") && !item2.equals("...") && !item3.equals("...") && !item4.equals("...") ) {
                            if (item1.equals(getString(R.string.module2_url2_item1))) {
                                url1.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item2.equals(getString(R.string.module2_url2_item2))) {
                                url2.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item3.equals(getString(R.string.module2_url2_item3))) {
                                url3.setBackgroundResource(R.drawable.answer_true);
                            }
                            if (item4.equals(getString(R.string.module2_url2_item4))) {
                                url4.setBackgroundResource(R.drawable.answer_true);
                            }


                            myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(url1.toString(), item1.equals(getString(R.string.module2_url2_item1)), item1, getString(R.string.module2_url2_item1)));
                            myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(url2.toString(), item2.equals(getString(R.string.module2_url2_item2)), item2, getString(R.string.module2_url2_item2)));
                            myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(url3.toString(), item3.equals(getString(R.string.module2_url2_item3)), item3, getString(R.string.module2_url2_item3)));
                            myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(url4.toString(), item4.equals(getString(R.string.module2_url2_item4)), item4, getString(R.string.module2_url2_item4)));

                            url1.setEnabled(false);
                            url2.setEnabled(false);
                            url3.setEnabled(false);
                            url4.setEnabled(false);
                            TextView tvSolution = findViewById(R.id.tv_solution);
                            tvSolution.setText(R.string.module2_url2_solution);
                            solution = true;
                        }else {
                            showDialog(getString(R.string.not_yet_ready), getString(R.string.module2_url1_dialog), context);
                        }
                        } else {
                        TextView tvSolution = findViewById(R.id.tv_solution);
                        tvSolution.setText("");

                        solution = false;
                        if(recap){
                            Intent myIntent = new Intent(context, InternetEnd.class);
                            startActivity(myIntent);
                            finish();
                        }else {
                            if(myrecap){
                                restartAfterRecap();
                            }else {
                                Intent myIntent = nextActivity(getActivityNumber("URL2") + 1);
                                startActivity(myIntent);
                                finish();
                            }
                        }
                    }
                }
                return false;
            }
        });

        url1 = findViewById(R.id.url1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.url2, R.layout.template_large_text_spinner);
        url1.setAdapter(adapter);
        url1.setOnItemSelectedListener(this);

        url2 = findViewById(R.id.url2);
        adapter = ArrayAdapter.createFromResource(this,R.array.url2, R.layout.template_large_text_spinner);
        url2.setAdapter(adapter);
        url2.setOnItemSelectedListener(this);

        url3 = findViewById(R.id.url3);
        adapter = ArrayAdapter.createFromResource(this,R.array.url2, R.layout.template_large_text_spinner);
        url3.setAdapter(adapter);
        url3.setOnItemSelectedListener(this);

        url4 = findViewById(R.id.url4);
        adapter = ArrayAdapter.createFromResource(this,R.array.url2, R.layout.template_large_text_spinner);
        url4.setAdapter(adapter);
        url4.setOnItemSelectedListener(this);



    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
          myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), gapEventExtension(parent+"", parent.getItemAtPosition(position).toString()));

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
