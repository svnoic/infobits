package de.aachen.rwth.lufgi9.noichl.basics.gesten;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is for learning the Fling gesture. The user has to use the fling gesture to swipe through images.
 * If the gesture was not used a dialog box is shown saying that the user should use the gesture to have a look on the other images.
 * If the gesture was used the next Activity (End.class) is started.
 * All gestures will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Fling extends MyActivity {

    /**
     * Represents the current image. Value can be between 1 and 3.
     */
    private static int picture = 1;

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Represents the x-coordinate of the MotionEvent.
     */
    float x = 0;

    /**
     * Represents the difference of the x-coordinate from the last MotionEvent (stored in x) and the currend MotionEvent. Is used to evaluate if the user make a fling to the left or the right.
     */
    float diffX = 0;

    /**
     * Represents the status of the given solution.
     * If fling gesture was used, solution has the state <b>true</b> and on nextButton-touch-event the next activity will be started.
     * If fling gesture was not used, solution has the state <b>false</b> and on nextButton-touch-event a dialog will be shown.
     */
    boolean solution;

    /**
     * Loads the activities layout (R.layout.activity_textview_viewstub2_next_button) which uses a viewstub (basics_gestures_fling).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event either the next activity will be started or the dialog will be shown based on the status of the <b>solution</b> variable.
     * If solution is <b>true</b> the next activity (Zoom.class) will be started, otherwise a dialog will be shown.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_s_mytextview_s_viewstub_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_gestures_fling);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.description_text_wischen);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (solution) {
                        solution = false;
                        Intent myIntent = new Intent(context, Zoom.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.gestures_fling_dialog), context);
                    }
                }

                return false;
            }
        });
    }


    /**
     * Triggered by the ACTION_MOVE event the direction of the movement is computed by subtracting the current x-coordinate from the one before the event.
     * Triggered by the ACTION_UP event the shown image will be changed based on the movements direction.
     * Triggered by the ACTION_Down event the value of <b>x</b> will be set to the x-coordinate of the current event x-coordinate.
     *
     * @param event MotionEvent
     * @return boolean super.onTouchEvent(event)
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            diffX = x - event.getX();
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (diffX > 100) {
                ImageView iv = findViewById(R.id.iv);
                solution = true;
                switch (picture) {
                    case 1:
                        iv.setImageResource(R.drawable.b2);
                        picture = 2;
                        break;
                    case 2:
                        iv.setImageResource(R.drawable.b3);
                        picture = 3;
                        break;
                    case 3:
                        iv.setImageResource(R.drawable.b1);
                        picture = 1;
                        break;
                }
            }
            if (diffX < -100) {
                ImageView iv = findViewById(R.id.iv);
                solution = true;
                switch (picture) {
                    case 1:
                        iv.setImageResource(R.drawable.b3);
                        picture = 3;
                        break;
                    case 2:
                        iv.setImageResource(R.drawable.b1);
                        picture = 1;
                        break;
                    case 3:
                        iv.setImageResource(R.drawable.b2);
                        picture = 2;
                        break;
                }
            }

        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            x = event.getX();
        }


        return super.onTouchEvent(event);
    }

}