package de.aachen.rwth.lufgi9.noichl.basics.eingaben;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is for learning how Password Inputs work. The user have to type in the given password in the corresponding inputfield.
 * If the entered password is incorrect a dialog box is shown saying that the password is not yet correct and the word to be typed is given.
 * If the user enters the password correct the next Activity (Radio.class) is started.
 * The entered password will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class PwInput extends MyActivity {


    /**
     * Represents the context of the current Activity.
     */
    Context context;


    /**
     * Represents the status of the given solution.
     * If the entered password is correct, solution has the state <b>true</b> and on nextButton-touch-event the next activity will be started.
     * If the entered password is incorrect, solution has the state <b>false</b> ans on nextButton-touch-event a dialog will be shown.
     */
    boolean solution;

    /**
     * Loads the activities layout (R.layout.activity_textview_viewstub2_next_button) which uses a viewstub (R.layout.basics_inputs_password).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event either the next activity will be started or the dialog will be shown based on the status of the <b>solution</b> variable.
     * If solution is <b>true</b> the next activity (Spinner.class) will be started, otherwise a dialog will be shown.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_inputs_password);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.pwinput);


        ImageView image = findViewById(R.id.imageView8);
        image.setVisibility(View.INVISIBLE);
        handleInput();


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (solution) {
                        solution = false;
                        Intent myIntent = new Intent(context, Radio.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.input_incorrectpw), context);
                    }
                }

                return false;
            }
        });


    }

    /**
     * Handles the input in the EditText field.
     * If the input is done by the user, observed by a onEditorActionListener, the entered word is checked and logged with the myLog-method.
     * If the entered word is equals the mystery the check-image indicating the user that the answer was correct will be set visible, solution will be set to true and the EditText will be reseted.
     * If the entered word is not equals the mystery the check-image will be set invisible, solution will be set to false and a dialog will be shown.
     */
    private void handleInput() {
        final EditText edittext = findViewById(R.id.editTextP);
        final Context context = this;

        edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert in != null;
                    in.hideSoftInputFromWindow(v
                                    .getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    ImageView image = findViewById(R.id.imageView8);
                    myLog("testuser", this.getClass() + "/entered", "entered", this.getClass().toString(), textExtension(edittext.getText().toString()));
                    if (edittext.getText().toString().equals(getString(R.string.mystery))) {
                        image.setVisibility(View.VISIBLE);
                        solution = true;
                        edittext.setText("");
                    } else {
                        image.setVisibility(View.INVISIBLE);
                        solution = false;
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.input_incorrectpw), context);
                    }
                    return true;
                }
                return false;
            }
        });
    }

}
