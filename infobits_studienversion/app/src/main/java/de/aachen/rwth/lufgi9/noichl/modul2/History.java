package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fasterxml.jackson.annotation.JsonValue;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.Assets;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.JSONParser;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.States;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.TimeLineAdapter;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.TimeLineModel;

public class History extends MyActivity {

    ArrayList<String> read = new ArrayList<>();
    /**
     * The state enum for history entries.
     */
    public enum HistoryState {
        /**
         * This is the default case. The history was not read by the user
         */
        UNREAD("unread"),
        /**
         * State for the read entry.
         */
        READ("read");


        /**
         * The id is needed for JSON parsing
         */
        private final String id;



        /**
         * Normal constructor
         *
         * @param id The unique identifier
         */
        HistoryState(String id) {
            this.id = id;
        }

        @JsonValue
        public String getId() {
            return id;
        }



    }

    /**
     * The adapter for a history entry (as a timeline entry).
     */
    private TimeLineAdapter mAdapter;
    /**
     * The dataSet with all entries for the history activity.
     */
    private List<TimeLineModel> mData;

    Context context;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recyclerview_nextbutton);

        context = this;

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (recap) {
                        Intent myIntent = new Intent(context, InternetEnd.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        if(myrecap){
                            restartAfterRecap();
                        }else {
                            if (read.size() >= 10) {

                                Intent myIntent = nextActivity(getActivityNumber("HistoryIntro") + 1);
                                startActivity(myIntent);
                                finish();

                            } else {
                                showDialog(getString(R.string.not_yet_ready), getString(R.string.module2_history_dialog), context);
                            }
                        }
                    }
                }
                return false;
            }
        });

        // install the data
        mData = new ArrayList<>();

        // use a normal linear layout for the recycler view
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        // install the recycler view. This view represents all entries.
        RecyclerView recyclerView = findViewById(R.id.history_recyclerView);
        // set up the layout
        recyclerView.setLayoutManager(linearLayoutManager);
        // use a divider
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        // the converter for the adapter
        TimeLineAdapter.StateToDrawable stateConverter = new TimeLineAdapter.DefaultStateToDrawable() {
            @Override
            public int getDrawableId(TimeLineModel object, String state) {
                if (States.contains(state, HistoryState.READ.getId())) {
                    myLog("testuser", this.getClass()+"/readed", "readed", this.getClass().toString(), textExtension(object.getTitle()));
                    if(!read.contains(object.getTitle())){
                        read.add(object.getTitle());
                    }
                    return R.drawable.ic_check_circle_black_24dp;
                }
                // default is a black circle
                return R.drawable.ic_remove_circle_outline_black_24dp;
            }
        };
        // install the adapter
        mAdapter = new TimeLineAdapter(mData, stateConverter);
        recyclerView.setAdapter(mAdapter);
    }

    // use the onResume to reload the list, maybe you added a new entry
    @Override
    protected void onResume() {
        super.onResume();
        reloadData();
    }

    /**
     * Reload the timeline data for the history
     */
    private void reloadData() {

        // use a async task to upload file data when ready
        AsyncTask<String, Void, TimeLineModel[]> asyncTask = new HistoryAsyncTask(this, mAdapter, mData);

        // execute the task with the given file
        String assetFile = "historyEntries.json";
        asyncTask.execute(assetFile);
    }


    /**
     * The class that handles the reading of the json file in background.
     * The UI will be available for the user all the time.
     * Data are updated if everything is loaded.
     */
    private static class HistoryAsyncTask extends AsyncTask<String, Void, TimeLineModel[]> {
        /**
         * The context is used for access resources. The weak reference are useful to stop the complete activity.
         * Typically, using the normal context attribute can lead to a longer live of the async task
         */
        private final WeakReference<Context> weakContextReference;

        /**
         * The adapter for handle history events. The adapter will be notified if the process finished reading.
         */
        private final TimeLineAdapter     mAdapter;
        /**
         * The list of history entries. It will be updated when reread the file content.
         */
        private final List<TimeLineModel> mData;

        /**
         * Normal constructor
         *
         * @param context  Needed for access resources
         * @param mAdapter The adapter that should be updated when read data
         * @param mData    The list of data that are updated
         */
        private HistoryAsyncTask(Context context, TimeLineAdapter mAdapter, List<TimeLineModel> mData) {
            this.weakContextReference = new WeakReference<>(context);
            this.mAdapter = mAdapter;
            this.mData = mData;
        }

        @Override
        protected TimeLineModel[] doInBackground(String... params) {
            if (params != null && params.length > 0) {
                // read the content of the history entry file
                String historyFileContent = Assets.readAsset(weakContextReference.get(), params[0]);

                // parse the data to a list of timeline data
                return JSONParser.jsonToTimeLineModel(historyFileContent);
            }
            return null;
        }


        protected void onPostExecute(TimeLineModel[] entries) {
            if (entries != null) {
                // install a new empty array for fill. Update the main array at the end. Better performance handling.
                List<TimeLineModel> data = Arrays.asList(entries);

                // copy to the main array
                mData.clear();
                mData.addAll(data);

                // notify the adapter for change
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
