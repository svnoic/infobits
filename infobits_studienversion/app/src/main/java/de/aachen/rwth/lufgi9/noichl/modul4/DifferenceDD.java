package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.google.android.flexbox.FlexboxLayout;
import com.google.android.flexbox.FlexboxLayout.LayoutParams;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class DifferenceDD extends MyActivity implements View.OnDragListener, View.OnTouchListener {


    private TextView  valueTV;
    private LinearLayout mylinearLayout;
    Context context;
    private static boolean solution = false;
    ArrayList<String> answersSelection = new ArrayList<>();
    ArrayList<String> myAnswers = new ArrayList<>();

    @SuppressLint({"ResourceType", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_sort_2_category);
        stub.inflate();

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("DifferenceDD"));
        }
        editor.putInt("current", getActivityNumber("DifferenceDD"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText("Welche Unterschiede können wir bei diesen beiden Arten der Kommunikation feststellen?\n(Ordne die Aussagen zu, indem du sie per Drag and Drop in die entsprechende Kategorie verschiebst.)");


        final Button nextButton = (Button) findViewById(R.id.next);

        TextView tv1 = findViewById(R.id.tv1);
        tv1.setText("Datenschutz");
        TextView tv2 = findViewById(R.id.tv2);
        tv2.setText("Datensicherheit");

        myAnswers.add("Erstellung eines Persönlichkeitsprofils verhindern");
        myAnswers.add("Nutzer vor unbefugter Speicherung ihrer Daten schützen");
        myAnswers.add("Schutz der Nutzer");
        myAnswers.add("Nur befugte haben Zugriff auf die Daten");
        myAnswers.add("Unbefugte können keine Daten ändern");
        myAnswers.add("Die eigenen Daten sind immer verfügbar");
        myAnswers.add("Schutz der Daten");


    answersSelection.addAll(myAnswers);
        showAnswer(answersSelection);




        implementEvents();


        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {


                        if (!solution) {

                            if (!(answersSelection.size() == 0)) {

                                showDialog("Noch nicht fertig.", "Bitte sortiere ALLE Aussagen in eine der beiden Kategorien ein, indem du die Geste Drag and Drop benutzt.", context);

                            } else {

                                ViewGroup parent1 = findViewById(R.id.top_layout);
                                parent1.setTag("Datenschutz");
                                ViewGroup parent2 = findViewById(R.id.right_layout);
                                parent2.setTag("Datensicherheit");

                                TextView myview0 = findViewById(0);
                                TextView myview1 = findViewById(1);
                                TextView myview2 = findViewById(2);
                                TextView myview3 = findViewById(3);
                                TextView myview4 = findViewById(4);
                                TextView myview5 = findViewById(5);
                                TextView myview6 = findViewById(6);

                                if (myview0.getParent().getParent() == parent1) {
                                    myview0.setBackgroundResource(R.drawable.answer_true);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), true, parent1.getTag().toString(), parent1.getTag().toString()));
                                } else {
                                    myview0.setBackgroundResource(R.drawable.answer_false);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent2.getTag().toString(), parent1.getTag().toString()));
                                }

                                if (myview1.getParent().getParent() == parent1) {
                                    myview1.setBackgroundResource(R.drawable.answer_true);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview1.getText().toString(), true, parent1.getTag().toString(), parent1.getTag().toString()));
                                } else {
                                    myview1.setBackgroundResource(R.drawable.answer_false);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview1.getText().toString(), false, parent2.getTag().toString(), parent1.getTag().toString()));
                                }

                                if (myview2.getParent().getParent() == parent1) {
                                    myview2.setBackgroundResource(R.drawable.answer_true);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview2.getText().toString(), true, parent1.getTag().toString(), parent1.getTag().toString()));
                                } else {
                                    myview2.setBackgroundResource(R.drawable.answer_false);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview2.getText().toString(), false, parent2.getTag().toString(), parent1.getTag().toString()));
                                }

                                if (myview3.getParent().getParent() == parent2) {
                                    myview3.setBackgroundResource(R.drawable.answer_true);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview3.getText().toString(), true, parent2.getTag().toString(), parent2.getTag().toString()));
                                } else {
                                    myview3.setBackgroundResource(R.drawable.answer_false);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview3.getText().toString(), false, parent1.getTag().toString(), parent2.getTag().toString()));
                                }

                                if (myview4.getParent().getParent() == parent2) {
                                    myview4.setBackgroundResource(R.drawable.answer_true);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview4.getText().toString(), true, parent2.getTag().toString(), parent2.getTag().toString()));
                                } else {
                                    myview4.setBackgroundResource(R.drawable.answer_false);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview4.getText().toString(), false, parent1.getTag().toString(), parent2.getTag().toString()));
                                }

                                if (myview5.getParent().getParent() == parent2) {
                                    myview5.setBackgroundResource(R.drawable.answer_true);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview5.getText().toString(), true, parent2.getTag().toString(), parent2.getTag().toString()));
                                } else {
                                    myview5.setBackgroundResource(R.drawable.answer_false);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview5.getText().toString(), false, parent1.getTag().toString(), parent2.getTag().toString()));
                                }

                                if (myview6.getParent().getParent() == parent2) {
                                    myview6.setBackgroundResource(R.drawable.answer_true);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview6.getText().toString(), true, parent2.getTag().toString(), parent2.getTag().toString()));
                                } else {
                                    myview6.setBackgroundResource(R.drawable.answer_false);
                                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview6.getText().toString(), false, parent1.getTag().toString(), parent2.getTag().toString()));
                                }

                                deactivateEvents();
                                solution = true;
                            }
                        } else {
                            solution = false;
                            if (recap) {
                                Intent myIntent = new Intent(context, DataSecEnd.class);
                                startActivity(myIntent);
                                finish();
                            } else {
                                if(myrecap){
                                    restartAfterRecap();
                                }else {
                                    Intent myIntent = nextActivity(getActivityNumber("DifferenceDD") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                        }
                    }
                }
                return false;
            }
        });
    }

    private void showAnswer(ArrayList<String> answers) {

        View linearLayout = findViewById(R.id.flex_layout);
        ViewGroup vg = findViewById(R.id.flex_layout);
        if(!(answers.size()==0) && vg.getChildCount() == 0) {

            mylinearLayout = new LinearLayout(this);
            mylinearLayout.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
            mylinearLayout.setPadding(8, 8, 8, 8);
            valueTV = new TextView(this);
            int i = 0;
            if (answers.size() > 1) {
                Random r = new Random();
                i = r.nextInt(answers.size() - 1);
            } else {
                if (answers.size() == 1) {
                    i = 0;
                }
            }


            int id = -1;

            for (int j = 0; j < myAnswers.size(); j++){
                if(myAnswers.get(j).equals(answers.get(i))) {
                    Log.e(answers.get(i), myAnswers.get(j));
                    Log.e(i+"", j+"");
                    valueTV.setText(myAnswers.get(j));
                    int array[] = new int[2];
                    valueTV.getLocationOnScreen(array);
                    int w = valueTV.getWidth();
                    int h = valueTV.getHeight();
                    myLog("testuser",this.getClass()+"/displayed", "displayed", this.getClass().toString(), textExtension(myAnswers.get(j)));
                    myLog("testuser", this.getClass()+"/created", "created", this.getClass().toString(), "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+ ", \"displaymetrics\": { \"element\": \"" + valueTV + "\", \"x1\": \"" + array[0] + "\", \"y1\": \"" + array[1]+ "\", \"x2\": \"" + (array[0]+w) + "\", \"y2\": \"" + (array[1]+h) + "}");
                            valueTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
                    valueTV.setTextColor(Color.BLACK);
                    valueTV.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sortbox, null));
                    id = j;
                    break;
                }
            }
            valueTV.setId(id);
            Log.e("ID", id +"");
            mylinearLayout.setTag("l" + valueTV.getId());
            valueTV.setTag(i + "");
            valueTV.setPadding(8, 8, 8, 8);
            valueTV.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            LayoutParams params = (LayoutParams) valueTV.getLayoutParams();
            //params.setMargins(8,8,8,8);
            answersSelection.remove(i);


            mylinearLayout.addView(valueTV);

            //mylinearLayout.setOnTouchListener(this);
            valueTV.setOnTouchListener(this);

            ((FlexboxLayout) linearLayout).addView(mylinearLayout);
        }
    }


    private void implementEvents() {


        findViewById(R.id.top_layout).setOnDragListener(this);
        findViewById(R.id.right_layout).setOnDragListener(this);
        //findViewById(R.id.flex_layout).setOnDragListener(this);
    }

    private void deactivateEvents(){
        findViewById(R.id.top_layout).setEnabled(false);
        findViewById(R.id.right_layout).setEnabled(false);
        TextView myview0 = findViewById(0);
        myview0.setEnabled(false);
        @SuppressLint("ResourceType") TextView myview1 = findViewById(1);
        myview1.setEnabled(false);
        @SuppressLint("ResourceType") TextView myview2 = findViewById(2);
        myview2.setEnabled(false);
        @SuppressLint("ResourceType") TextView myview3 = findViewById(3);
        myview3.setEnabled(false);
        @SuppressLint("ResourceType") TextView myview4 = findViewById(4);
        myview4.setEnabled(false);
        @SuppressLint("ResourceType") TextView myview5 = findViewById(5);
        myview5.setEnabled(false);
        @SuppressLint("ResourceType") TextView myview6 = findViewById(5);
        myview6.setEnabled(false);
    }



    @Override
    public boolean onDrag(View view, DragEvent event) {
        View v = (View) event.getLocalState();
        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), dragEventExtension(event, view, ((TextView)v).getText().toString()));
        int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:

                if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {

                    return true;
                }

                return false;

            case DragEvent.ACTION_DRAG_ENTERED:

                view.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                view.invalidate();

                return true;
            case DragEvent.ACTION_DRAG_LOCATION:

                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                view.getBackground().clearColorFilter();
                view.invalidate();

                return true;

            case DragEvent.ACTION_DROP:
                view.getBackground().clearColorFilter();
                view.invalidate();

                ViewGroup owner = (ViewGroup) v.getParent();
                owner.removeView(v);//remove the dragged view
                ViewGroup root = (ViewGroup) owner.getParent();
                if(view instanceof FlexboxLayout) {
                    FlexboxLayout container = (FlexboxLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                    //v.setLayoutParams(param);
                    LinearLayout rm = owner.findViewWithTag("l"+v.getId());
                    root.removeView(rm);
                    LinearLayout l = new LinearLayout(this);
                    l.setPadding(8,8,8,8);
                    l.setTag("l"+v.getId());
                    l.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
                    l.addView(v);
                    container.addView(l);//Add the dragged view
                }else{
                    LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                    //v.setLayoutParams(param);
                    LinearLayout rm = owner.findViewWithTag("l"+v.getId());
                    root.removeView(rm);
                    LinearLayout l = new LinearLayout(this);
                    l.setPadding(8,8,8,8);
                    l.setTag("l"+v.getId());
                    l.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
                    l.addView(v);
                    container.addView(l);//Add the dragged view


                }
                showAnswer(answersSelection);

                //v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                view.getBackground().clearColorFilter();
                view.invalidate();
                view = (View) event.getLocalState();
                view.setVisibility(View.VISIBLE);
                return true;
        }
        return false;
    }

    ViewGroup.LayoutParams param;

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), viewEventExtension(motionEvent, view));

        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());


        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

        view.startDrag(data, shadowBuilder, view, 0);

        view.setVisibility(View.INVISIBLE);

        return true;
    }
}
