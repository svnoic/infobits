package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.Draw;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.GraphUtilities;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.Java8Utilities;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.ViewUtilities;

/**
 * This is the first interactive activity of the network topic.
 * Let the user connect two components.
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class NetworkConnectedActivity5 extends MyActivity implements OnTouchListener {

    Context context;

    /**
     * The state enum for the current action mode of the {@link #selectedClient}.
     * This means, that a user can, for example, move a client on screen to another position or connect a client to another.
     */
    public enum ClientActionState {
        /**
         * This is the default case. The user can move a client to another position.
         */
        MOVING,
        /**
         * State for connect the current selected client to another client.
         */
        CONNECTING,
        /**
         * State for connect the current selected client to another client.
         */
        CREATING
    }

    /**
     * The list of clients which can be connected
     */
    private final List<View> clients = new ArrayList<>();

    /**
     * List of connection between client views
     */
    private final List<Pair<View, View>> connectedClients = new ArrayList<>();

    /**
     * The view where the user plays on with clients, server and connections
     */
    private Draw playgroundView;

    /**
     * The view of the client that is selected at the moment.
     */
    private View selectedClient;

    /**
     * State to signalize if the move-event in MotionEvent is to create a connection between two clients or to move the current client
     */
    private ClientActionState clientActionState = ClientActionState.MOVING;

    /**
     * The start raw screen position of the user touch when the user touches down
     */
    private final PointF startRawTouch = new PointF();

    /**
     * The start position of the client in his parent view when the user touches down
     */
    private final PointF originClientPosition = new PointF();

    /**
     * The help view for the user with hints what to do
     */
    private TextView helpLayout;

    /**
     * The target number of clients that the user should place and connect
     */
    private int targetNumberOfClients;

    /**
     * A state to show hints for the solution.
     * If state is 0, no hint is needed.
     * If state is 1, show the user blinking computer that has not enough edges.
     * If state is 2, show the user pairs of blinking computer that are not connected.
     */
    private int solutionHintState = 0;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_s_viewstub_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM2", getActivityNumber("NetworkConnectedActivity5"));
        }
        editor.putInt("current", getActivityNumber("NetworkConnectedActivity5"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
                stub.setLayoutResource(R.layout.viewstub_draw_network_empty);
                stub.inflate();

        playgroundView = findViewById(R.id.network_connected_playground);
        playgroundView.setOnTouchListener(this);

        // initialize the help view
        helpLayout = findViewById(R.id.my_textview);
        helpLayout.setText(R.string.msgHelpNetworkConnectClickFirst);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (clients.size() < targetNumberOfClients) {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module2_networkcon5_dialog1), context);
                        return false;
                    }

                    if (!completeConnection()) {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module2_networkcon5_dialog2), context);

                        solutionHintState++;
                        showSolutionHint(solutionHintState);
                        return false;
                    }

                    Intent myIntent = nextActivity(getActivityNumber("NetworkConnectedActivity5") + 1);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        /*// make the help view to a view click resizing
        new HelpTextViewClickResizing("networkConnected", helpLayout)
                .setTargetWidthRes(R.dimen.width_help_layout_minimized)
                .setTargetHeightRes(R.dimen.height_help_layout_minimized);*/

        // clear the list if the activity is started
        if (savedInstanceState == null) {
            clients.clear();
            connectedClients.clear();
            clientActionState = ClientActionState.MOVING;

            // get the target number of clients given in the activity intent, default is 2
            targetNumberOfClients = 4;//getIntent().getExtras().getInt(KEY_TARGET_NUMBER_OF_CLIENTS, 2);

            solutionHintState = 0;

            // show the user the task
          //  DialogUtilities.showSimpleOKDialog(this, R.string.titleTask, getResources().getQuantityString(R.plurals.msgNetworkConnectedTask, targetNumberOfClients, targetNumberOfClients));
        }
    }


    /**
     * Called when a touch event is dispatched to a view. This allows listeners to
     * get a chance to respond before the target view.
     *
     * @param v     The view the touch event has been dispatched to.
     * @param event The MotionEvent object containing full information about
     *              the event.
     * @return True if the listener has consumed the event, false otherwise.
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // install the start position
            startRawTouch.set(event.getRawX(), event.getRawY());

            if (v.getId() == R.id.network_connected_playground) {
                // add only a new client if the maximum number of clients is not reached.
                if (clients.size() < targetNumberOfClients) {
                    final int x = (int) event.getX();
                    final int y = (int) event.getY();

                    final View client = LayoutInflater.from(this).inflate(R.layout.template_network_connected_client, (ViewGroup) v, false);
                    client.setOnTouchListener(this);
                    TextView label = client.findViewById(R.id.network_connected_client_label);
                    label.setText(getString(R.string.nameNetworkConnectedClient, "" + (clients.size() + 1)));

                    // hide view, since the correct position can not be calculated (because sizes not available at this moment)
                    client.setVisibility(View.INVISIBLE);

                    ((ViewGroup) v).addView(client);
                    client.post(new Runnable() {
                        @Override
                        public void run() {
                            int width = client.getWidth();
                            int height = client.getHeight();

                            int clientX = Math.max(0, x - width / 2);
                            int clientY = Math.max(0, y - height / 2);

                            // change to correct position
                            client.setX(clientX);
                            client.setY(clientY);
                            originClientPosition.set(clientX, clientY);

                            // show the view
                            client.setVisibility(View.VISIBLE);
                        }
                    });

                    // set the action state to creating to signalize the creation mode
                    clientActionState = ClientActionState.CREATING;

                    selectedClient = client;
                }

                return true;
            } else {
                // enable the connection mode so that the user can connect two clients by drawing a line
                clientActionState = ClientActionState.CONNECTING;

                // look into clients
                if (clients.contains(v)) {
                    // set selectable background
                    v.setBackgroundResource(R.drawable.background_network_connected_component_selected);
                    selectedClient = v;
                    return true;
                }
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            if (selectedClient != null && clientActionState == ClientActionState.CREATING) {
                // the end of creating mode. Check if position is valid
                if (isValidPosition(selectedClient)) {
                    clients.add(selectedClient);
                    // show the user a dialog that he added a client successfully
                    if (clients.size() == 1) {
                       // DialogUtilities.showSimpleOKDialog(this, R.string.titleNetworkConnected, R.string.msgNetworkConnectedSuccessfullyAddedFirst);
                        helpLayout.setText(R.string.msgHelpNetworkConnectedAddSecondClient);
                    } else if (clients.size() == targetNumberOfClients) {
                      //  DialogUtilities.showSimpleOKDialog(this, R.string.titleNetworkConnected, R.string.msgNetworkConnectedSuccessfullyAddedSecond);
                        helpLayout.setText(R.string.msgHelpNetworkConnectedConnectClients);
                    }

                    // go to moving mode
                    clientActionState = ClientActionState.MOVING;
                } else {
                    playgroundView.removeView(selectedClient);
                    selectedClient = null;
                   // DialogUtilities.showSimpleOKDialog(this, R.string.titleNetworkConnected, R.string.msgNetworkConnectedInvalidPosition);
                }
            }

            // make all clients not selected
            for (View client : clients) {
                client.setBackgroundResource(R.drawable.background_network_connected_component);
            }

            // if there was a selection before and now the event is inside another client, connect them
            if (selectedClient != null && clientActionState == ClientActionState.CONNECTING) {
                View connectedClient = null;
                for (int i = clients.size() - 1; i >= 0; i--) { // start at the end... top elements first
                    View client = clients.get(i);
                    if (client == selectedClient) continue;  // skip selected client
                    if (ViewUtilities.containsPoint(client, event.getRawX(), event.getRawY())) {
                        // found a second client
                        connectedClient = client;
                        break;
                    }
                }

                // if a connected client is found, connect them
                if (connectedClient != null) {
                    // found a second client. Connect both.
                    playgroundView.addUndirectedViewConnection(selectedClient, connectedClient);
                    playgroundView.invalidate();

                    // reset the hint timer
                    solutionHintState = 0;

                    if (!contains(connectedClients, selectedClient, connectedClient)) {
                        connectedClients.add(new Pair<>(selectedClient, connectedClient));

                    }
                } else {
                    // show the user a small hint otherwise
                    Toast.makeText(this, R.string.msgNetworkConnectedNoConnectedClient, Toast.LENGTH_LONG).show();
                }

                // deselect the client
                selectedClient = null;

                // remove all lines from selectedClient to current touch position
                playgroundView.clearUndirectedPointConnections();
                playgroundView.invalidate();
            }

            v.performClick();
        } else if (event.getAction() == MotionEvent.ACTION_MOVE && selectedClient != null) {
            if (clientActionState == ClientActionState.CONNECTING) {
                // check if another client is selected
                for (int i = clients.size() - 1; i >= 0; i--) { // start at the end... top elements first
                    View client = clients.get(i);
                    if (client == selectedClient) continue;  // skip selected client

                    if (ViewUtilities.containsPoint(client, event.getRawX(), event.getRawY())) {
                        // highlight the view
                        client.setBackgroundResource(R.drawable.background_network_connected_component_selected);
                        break;
                    } else {
                        // set normal background otherwise
                        client.setBackgroundResource(R.drawable.background_network_connected_component);
                    }
                }

                // remove all lines from selectedClient to current touch position
                playgroundView.clearUndirectedPointConnections();
                // add the current position
                playgroundView.addUndirectedPointConnection(
                        selectedClient.getX() + selectedClient.getWidth() / 2F,
                        selectedClient.getY() + selectedClient.getHeight() / 2F,
                        selectedClient.getX() + event.getX(),
                        selectedClient.getY() + event.getY());
                playgroundView.invalidate();
            } else if (clientActionState == ClientActionState.MOVING || clientActionState == ClientActionState.CREATING) {
                float deltaX = event.getRawX() - startRawTouch.x;
                float deltaY = event.getRawY() - startRawTouch.y;
                selectedClient.setX(originClientPosition.x + deltaX);
                selectedClient.setY(originClientPosition.y + deltaY);
            }
        }

        return false;
    }

    /**
     * If state is 0, no hint is needed.
     * If state is 1, show the user blinking computer that has not enough edges.
     * If state is 2, show the user pairs of blinking computer that are not connected.
     *
     * @param state The state to show hints for the solution.
     */
    private void showSolutionHint(int state) {
        if (state == 1) {
            final List<View> views = Java8Utilities.filter(clients, new Java8Utilities.Filter<View>() {
                @Override
                public boolean match(final View view) {
                    return Java8Utilities.filterAmount(connectedClients, new Java8Utilities.Filter<Pair<View, View>>() {
                        @Override
                        public boolean match(Pair<View, View> obj) {
                            return obj.first == view || obj.second == view;
                        }
                    }) < clients.size() - 1;
                }
            });
            if (views.size() > 0) {
                List<List<View>> convertedViews = new ArrayList<>();
                for (View view : views) {
                    convertedViews.add(Collections.singletonList(view));
                }
                letViewsBlink(this, convertedViews, 300, 2, 2, 600);
            }
        } else if (state >= 2) {
            // collect each pear
            List<List<View>> notConnectedPairs = new ArrayList<>();
            for (int i = 0; i < clients.size() - 1; i++) {
                View viewA = clients.get(i);
                for (int j = i + 1; j < clients.size(); j++) {
                    View viewB = clients.get(j);

                    if (!contains(connectedClients, viewA, viewB)) {
                        notConnectedPairs.add(Arrays.asList(viewA, viewB));
                    }
                }
            }
            if (notConnectedPairs.size() > 0) {
                letViewsBlink(this, notConnectedPairs, 300, 2, 2, 600);
            }
        }
    }

    /**
     * Let views blink for a given time. For example, show the user a hint which view is not finished.
     *
     * @param context                  Need to access the {@link android.os.Looper}
     * @param data                     The list of lists of views which will be alpha animated
     * @param duration                 The time for a single animation
     * @param repeatCount              The number of rounds the animation will be executed
     * @param rounds                   The number of rounds each animation-set will be executed
     *                                 (for example, 2 means the first <code>data</code> entry will be blinked 2 rounds)
     * @param timeBetweenBlinkingItems The time between the hint (blinking item).
     *                                 This means, that if item A blinks, item B starts after item A finished blinking + this variable
     */
    @SuppressWarnings("SameParameterValue")
    private static void letViewsBlink(Context context, final List<? extends Collection<View>> data, final long duration, final int repeatCount, final int rounds, long timeBetweenBlinkingItems) {
        final long animationDiff = duration * repeatCount + timeBetweenBlinkingItems;
        final Handler handler = new Handler(context.getMainLooper());
        handler.post(new Runnable() {

            /**
             * The current index of the <code>data</code> entry which elements will be alpha animated
             */
            int index = 0;

            /**
             * The current round in which each set of views in <code>data</code> will be alpha animated
             */
            int round = 0;

            @Override
            public void run() {
                // repeat until the number of rounds reached the limit
                if (round < rounds) {
                    Collection<View> views = data.get(index);
                    for (View view : views) {
                        // use a alpha animation to make the view visible and transparent
                        Animation animation = new AlphaAnimation(0, 1);
                        animation.setDuration(duration);
                        animation.setRepeatMode(Animation.REVERSE);
                        animation.setRepeatCount(repeatCount);
                        view.startAnimation(animation);
                    }
                    if (index == data.size() - 1) {
                        round++;
                        index = 0;
                    } else {
                        index++;
                    }
                    handler.postDelayed(this, animationDiff);
                }
            }
        });
    }

    /**
     * Check if both objects are in the collection as a pair entry. First or second entry does not matter.
     *
     * @param collection The collection that can contain the object
     * @param objA       The object to check
     * @param objB       The object to check
     * @param <E>        The generic type of the objects
     * @return <code>true</code> if the collection contains both objects in one pair entry
     */
    @SuppressWarnings({"BooleanMethodIsAlwaysInverted", "RedundantSuppression"})
    public static <E> boolean contains(Collection<Pair<E, E>> collection, final E objA, final E objB) {
        return Java8Utilities.contains(collection, new Java8Utilities.Filter<Pair<E, E>>() {
            @Override
            public boolean match(Pair<E, E> pair) {
                return (pair.first == objA && pair.second == objB) || (pair.first == objB && pair.second == objA);
            }
        });
    }

    /**
     * Checks if all components are connected
     *
     * @return <code>true</code> if all clients are connected to each other
     */
    private boolean completeConnection() {
        // on a complete connected graph, the number of edges is the triangle-number
        return connectedClients.size() == GraphUtilities.edgesInCompleteGraph(targetNumberOfClients);
    }

    /**
     * Checks the validity of the client current position.
     * A position is valid if the complete view is inside the playground frame and do not intersect with other clients.
     *
     * @param client The client view to check
     * @return <code>true</code> if the client current position is valid
     */
    private boolean isValidPosition(View client) {
        return isValidPosition(playgroundView, clients, client);
    }

    /**
     * Checks the validity of the client current position.
     * A position is valid if the complete view is inside the playground frame and do not intersect with other clients.
     *
     * @param parent     The parent view which bounds will be used to check if the current position of the component is completely inside.
     * @param components The list of current available components in the parent view.
     * @param component  The client view to check.
     * @return <code>true</code> if the client current position is valid.
     */
    public static boolean isValidPosition(View parent, List<View> components, View component) {
        // consider: All positions are relative to screen
        RectF rectClient = ViewUtilities.rectScreen(component);
        RectF rectFrame = ViewUtilities.rectScreen(parent);
        if (!rectFrame.contains(rectClient)) return false;
        boolean noIntersect = true;
        for (View v : components) {
            if (v == component) continue;
            RectF rectV = ViewUtilities.rectScreen(v);
            if (rectClient.intersects(rectV.left, rectV.top, rectV.right, rectV.bottom)) {
                noIntersect = false;
                break;
            }
        }
        return noIntersect;
    }

}
