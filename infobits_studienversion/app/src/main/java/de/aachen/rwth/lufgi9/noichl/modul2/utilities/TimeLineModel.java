package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

/**
 * The model for a timeline entry
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class TimeLineModel {

    /**
     * The id of the entry.
     */
    private String id;

    /**
     * The title for an entry.
     */
    private String title;

    /**
     * The state of an entry. It can have multiple states, that are separated by a ','.
     */
    private String state;

    /**
     * The description of a timeline entry. The description contains the complete text.
     */
    private String description;

    /**
     * The timestamp for a timeline entry.
     */
    private String time;

    /**
     * Further information of an entity of a timeline entry. It is reachable for experts or who wants to read further information.
     */
    private String furtherInformation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFurtherInformation() {
        return furtherInformation;
    }

    public void setFurtherInformation(String furtherInformation) {
        this.furtherInformation = furtherInformation;
    }
}