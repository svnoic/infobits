package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class UsecaseActivity2 extends MyActivity {

    static int stepcounter = 0;
    static String syncasync = "";
    Context context;
    JSONArray myJson = null;
    String alt1, alt2, alt3, alt4;
    String scenario;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_usecasetextview_viewstub_s_nextbutton);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("UsecaseActivity2"));
        }
        editor.putInt("current", getActivityNumber("UsecaseActivity2"));
        editor.apply();
        saveSharedPrefs();

        String json = null;
        try {
            InputStream is = this.getAssets().open("policyUseScenario.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (myJson != null) {
            JSONObject rec;
            try {
                rec = myJson.getJSONObject(1);
                alt1 = rec.getString("alt1");
                alt2 = rec.getString("alt2");
                alt3 = rec.getString("alt3");
                alt4 = rec.getString("alt4");
                scenario = rec.getString("description");
                TextView tv = findViewById(R.id.my_usecase_textview);
                tv.setText(scenario);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        TextView tv;
        CheckBox cb;
        ViewStub stub = findViewById(R.id.viewStub);
        switch (stepcounter) {
            case 0:
                stub.setLayoutResource(R.layout.viewstub_textview_2_radiobutton);


                stub.inflate();
                RadioButton rb = findViewById(R.id.likert6);
                rb.setText(getString(R.string.yes));
                rb = findViewById(R.id.likert5);
                rb.setText(R.string.no);
                tv = findViewById(R.id.my_textview);
                tv.setText(R.string.module1_uc2_sync_vs_async);

                break;
            case 1:
                stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                tv.setText(getString(R.string.usecase_reason, syncasync));
                cb = findViewById(R.id.alt1);
                cb.setText(alt1);
                cb = findViewById(R.id.alt2);
                cb.setText(alt2);
                cb = findViewById(R.id.alt3);
                cb.setText(alt3);
                cb = findViewById(R.id.alt4);
                cb.setText(alt4);
                break;
        }


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (stepcounter < 1) {
                        if (stepcounter == 0) {
                            RadioGroup myRadioGroup = findViewById(R.id.radioGroup3);
                            int selectedID = myRadioGroup.getCheckedRadioButtonId();

                            if (findViewById(selectedID) != null) {
                                RadioButton selectedView = findViewById(selectedID);
                                syncasync = (String) selectedView.getText();
                                myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.my_textview)).getText().toString(), ((String) selectedView.getText())));
                                stepcounter++;
                                Intent myIntent = new Intent(context, UsecaseActivity2.class);
                                startActivity(myIntent);
                                finish();
                            } else {
                                showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_usecaseactivity_dialog1), context);
                            }
                        }
                    } else {

                        CheckBox alt11 = findViewById(R.id.alt1);
                        CheckBox alt12 = findViewById(R.id.alt2);
                        CheckBox alt13 = findViewById(R.id.alt3);
                        CheckBox alt14 = findViewById(R.id.alt4);
                        if (alt11.isChecked() || alt12.isChecked() || alt13.isChecked() || alt14.isChecked()) {
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt11.getText().toString(), alt11.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt12.getText().toString(), alt12.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt13.getText().toString(), alt13.isChecked() + ""));
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt14.getText().toString(), alt14.isChecked() + ""));
                            stepcounter++;
                            if (recap) {
                                stepcounter = 0;
                                Intent myIntent = new Intent(context, CommunicationEnd.class);
                                startActivity(myIntent);
                                finish();
                            } else {
                                if(myrecap){
                                    stepcounter = 0;
                                    restartAfterRecap();
                                }else {
                                    stepcounter = 0;
                                    Intent myIntent = nextActivity(getActivityNumber("UsecaseActivity2") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                            }

                        } else {
                            showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_usecaseactivity_dialog1), context);
                        }


                    }
                }
                return false;
            }
        });

    }
}

