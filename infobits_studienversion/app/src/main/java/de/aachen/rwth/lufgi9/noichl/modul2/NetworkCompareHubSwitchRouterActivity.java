package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;



import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;


public class NetworkCompareHubSwitchRouterActivity extends MyActivity{



    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM2", getActivityNumber("NetworkCompareHubSwitchRouterActivity"));
        }
        editor.putInt("current", getActivityNumber("NetworkCompareHubSwitchRouterActivity"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_textview_2_gif_textview);
        stub.inflate();

        context = this;


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                Intent myIntent = nextActivity(getActivityNumber("NetworkCompareHubSwitchRouterActivity") + 1);
                startActivity(myIntent);
                finish();
                return false;
            }
        });
    }
}
