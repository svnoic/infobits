package de.aachen.rwth.lufgi9.noichl.general;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;
import de.aachen.rwth.lufgi9.noichl.R;

public class Draw extends FrameLayout {

    /**
     * A list of view pairs which get a connection line
     */
    private final List<Pair<View, View>> undirectedViewConnections = new ArrayList<>();

    /**
     * A list of point pairs which get a connection line
     */
    private final List<Pair<PointF, PointF>> undirectedPointConnections = new ArrayList<>();

    /**
     * The paint for the connection line between components
     */
    private final Paint paintConnection = new Paint();

    /**
     * The paint for the connection line between two points
     */
    private final Paint paintPointConnection = new Paint();

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in, through which it can
     *                access the current theme, resources, etc.
     */
    public Draw(Context context) {
        super(context);

        install();
    }

    /**
     * The main constructor that contains also user customized attributes
     *
     * @param context Needed for parent constructor and access resources
     * @param attrs   Set of attributes given in the xml layout file
     */
    public Draw(Context context, AttributeSet attrs) {
        super(context, attrs);

        install();
    }

    /**
     * The main constructor that contains also user customized attributes
     *
     * @param context  Needed for parent constructor and access resources
     * @param attrs    Set of attributes given in the xml layout file
     * @param defStyle The style of the view
     */
    public Draw(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        install();
    }

    /**
     * Install for this view needed elements like the paint variables
     */
    private void install() {
        // install the paints

        paintConnection.setColor(ContextCompat.getColor(getContext(), R.color.secondaryTextColor));
        paintConnection.setStrokeWidth(3);

        paintPointConnection.setColor(ContextCompat.getColor(getContext(), R.color.secondaryTextColor));
        paintPointConnection.setStrokeWidth(3);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

       /* // draw a connection line between each view pair
        for (Pair<View, View> pair : undirectedViewConnections) {
            float centerXFirst = pair.first.getX() + pair.first.getWidth() / 2F;
            float centerYFirst = pair.first.getY() + pair.first.getHeight() / 2F;
            float centerXSecond = pair.second.getX() + pair.second.getWidth() / 2F;
            float centerYSecond = pair.second.getY() + pair.second.getHeight() / 2F;
            canvas.drawLine(centerXFirst, centerYFirst, centerXSecond, centerYSecond, paintConnection);
        }

        // draw lines between custom points
        for (Pair<PointF, PointF> pair : undirectedPointConnections) {
            canvas.drawLine(pair.first.x, pair.first.y, pair.second.x, pair.second.y, paintPointConnection);
        }
        Log.e("DRAWLINE onDraw", "DONE!");*/

    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        // draw a connection line between each view pair
        for (Pair<View, View> pair : undirectedViewConnections) {
            //float centerXFirst = pair.first.getX() + pair.first.getWidth() / 2F;
            float centerXFirst = pair.first.getX() + pair.first.getWidth();
            float centerYFirst = pair.first.getY() + pair.first.getHeight() / 2F;
            //float centerXSecond = pair.second.getX() + pair.second.getWidth() / 2F;
            float centerXSecond = pair.second.getX();
            float centerYSecond = pair.second.getY() + pair.second.getHeight() / 2F;
            canvas.drawLine(centerXFirst, centerYFirst, centerXSecond, centerYSecond, paintConnection);
        }

        // draw lines between custom points
        for (Pair<PointF, PointF> pair : undirectedPointConnections) {
            canvas.drawLine(pair.first.x, pair.first.y, pair.second.x, pair.second.y, paintPointConnection);
        }
        Log.e("DRAWLINE dispatchDraw", "DONE!");

    }

    /**
     * Draw a single line between a view and the current position
     *
     * @param view  The source of the line
     * @param event The target of the line
     */
    public void drawLineFromViewToTouch(View view, MotionEvent event) {
        undirectedPointConnections.clear();

        PointF pointView = new PointF(view.getX() + view.getWidth() / 2F, view.getY() + view.getHeight() / 2F);
        PointF pointTouch = new PointF(event.getX(), event.getY());
        undirectedPointConnections.add(new Pair<>(pointView, pointTouch));

        Log.e("DRAWLINE", "DONE!");
    }

    /**
     * Adds a new connection between two views.
     *
     * @param a The first view
     * @param b The second view
     */
    public void addUndirectedViewConnection(View a, View b) {
        // if the pair is already drawn, do nothing
        for (Pair<View, View> pair : undirectedViewConnections) {
            if ((pair.first == a && pair.second == b)
                    || (pair.first == b && pair.second == a)) {
                return;
            }
        }
        undirectedViewConnections.add(new Pair<>(a, b));
        Log.e("DRAWCONNECTION", "DONE!");
    }

    /**
     * Removes a connection between two views.
     *
     * @param a The first view
     * @param b The second view
     */
    public void removeUndirectedViewConnection(View a, View b) {
        for (int i = undirectedViewConnections.size() - 1; i >= 0; i--) {
            Pair<View, View> pair = undirectedViewConnections.get(i);
            if ((pair.first == a && pair.second == b) || (pair.first == b && pair.second == a)) {
                undirectedViewConnections.remove(i);
            }
        }
    }

    /**
     * Adds a new connection between two points.
     *
     * @param pX The raw x value of the first point
     * @param pY The raw y value of the first point
     * @param qX The raw x value of the second point
     * @param qY The raw y value of the second point
     */
    public void addUndirectedPointConnection(float pX, float pY, float qX, float qY) {
        PointF p = new PointF(pX, pY);
        PointF q = new PointF(qX, qY);
        // if the pair is already drawn, do nothing
        for (Pair<PointF, PointF> pair : undirectedPointConnections) {
            if ((pair.first.equals(p) && pair.second.equals(q))
                    || (pair.first.equals(q) && pair.second.equals(p))) {
                return;
            }
        }
        undirectedPointConnections.add(new Pair<>(new PointF(pX, pY), new PointF(qX, qY)));
    }

    /**
     * Remove all point connections
     */
    public void clearUndirectedPointConnections() {
        undirectedPointConnections.clear();
    }

    /**
     * Remove all view connections
     */
    public void clearUndirectedViewConnections() {
        undirectedViewConnections.clear();
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }

}