package de.aachen.rwth.lufgi9.noichl.general;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;

public class DataInformation extends MyActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        if(KNOWNUSER){
            Intent myIntent = new Intent(context, AppStart.class);
            startActivity(myIntent);
            finish();
        }
        TextView tv = findViewById(R.id.my_textview);
        tv.setText("Diese App speichert alle deine Nutzerinteraktionen auf dem dem Gerät und zeichnet den Bildschirm auf. Alle Daten sind vollständig anonym und können nicht auf eine bestimmte Person zurückgeführt werden.\n\nMit der Nutzung der App erklärst du dich dazu bereit, dass diese Daten während deiner Nutzung erhoben, gespeichert und für wissenschaftliche Zwecke ausgewertet werden.");

        final Button nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                Intent myIntent = new Intent(context,   QuestionairAgeAndUse.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });
    }
}
