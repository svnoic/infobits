package de.aachen.rwth.lufgi9.noichl.modul1;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class VideoAdressesActivity extends MyActivity {


    static int videopos = 10;
    static boolean videoready;
    VideoView myVideo;
    Context context;
    Button nextButton;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("VideoAdressesActivity"));
        }
        editor.putInt("current", getActivityNumber("VideoAdressesActivity"));
        editor.apply();
        saveSharedPrefs();


        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.v3);
        myVideo = findViewById(R.id.videoView);
        myVideo.setVideoURI(uri);
        myVideo.seekTo(videopos);
        myVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(MediaPlayer mediaPlayer) {
                        myVideo.start();
                        myVideo.pause();
                    }
                });
            }
        });
        myVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoready = true;
                nextButton.setVisibility(View.VISIBLE);
            }
        });

        myVideo.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), videoButtonEventExtension(event, myVideo, "wlan", myVideo.getCurrentPosition()));

                if (myVideo.isPlaying()) {
                    myVideo.pause();
                } else {
                    myVideo.start();
                }
                return false;
            }
        });

        MediaController mediaController = new
                MediaController(this);
        mediaController.setAnchorView(myVideo);
        myVideo.setMediaController(mediaController);


        nextButton = findViewById(R.id.next);
        nextButton.setVisibility(View.GONE);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (recap) {
                        Intent myIntent = new Intent(context, CommunicationEnd.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        if(myrecap){
                            restartAfterRecap();
                        }else {
                            Intent myIntent = nextActivity(getActivityNumber("VideoAdressesActivity") + 1);
                            startActivity(myIntent);
                            finish();
                        }
                    }

                }
                return false;
            }
        });
    }




}
