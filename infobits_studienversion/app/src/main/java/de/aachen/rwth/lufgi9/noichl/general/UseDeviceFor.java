package de.aachen.rwth.lufgi9.noichl.general;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationStart;

public class UseDeviceFor extends MyActivity {

    Context context;
    String device ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.questionair_use_device_for);

        context = this;


        TextView tv1 = findViewById(R.id.textView7);

        final CheckBox cb1 = findViewById(R.id.checkBox15);
        final CheckBox cb2 = findViewById(R.id.checkBox16);
        final CheckBox cb3 = findViewById(R.id.checkBox17);
        final CheckBox cb4 = findViewById(R.id.checkBox18);
        final CheckBox cb5 = findViewById(R.id.checkBox19);
        final CheckBox cb6 = findViewById(R.id.checkBox20);
        final CheckBox cb7 = findViewById(R.id.checkBox21);
        final CheckBox cb8 = findViewById(R.id.checkBox22);
        final CheckBox cb9 = findViewById(R.id.checkBox23);
        final CheckBox cb10 = findViewById(R.id.checkBox24);
        final CheckBox cb11 = findViewById(R.id.checkBox25);
        final CheckBox cb12 = findViewById(R.id.checkBox26);
        final CheckBox cb13 = findViewById(R.id.checkBox27);
        final CheckBox cb14 = findViewById(R.id.checkBox28);
        final CheckBox cb15 = findViewById(R.id.checkBox29);
        final CheckBox cb16 = findViewById(R.id.checkBox30);
        final CheckBox cb17 = findViewById(R.id.checkBox31);
        final CheckBox cb18 = findViewById(R.id.checkBox32);
        final CheckBox cb19 = findViewById(R.id.checkBox33);
        final CheckBox cb20 = findViewById(R.id.checkBox34);
        final CheckBox cb21 = findViewById(R.id.checkBox35);
        final CheckBox cb22 = findViewById(R.id.checkBox36);
        final CheckBox cb23 = findViewById(R.id.checkBox37);
        final CheckBox cb24 = findViewById(R.id.checkBox38);

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);

        String s1= "";
        if(QuestionairAgeAndUse.counter == 1){s1 = "c_";}
        if(QuestionairAgeAndUse.counter == 2){s1 = "s_";}
        if(QuestionairAgeAndUse.counter == 3){s1 = "t_";}

        if(sharedpreferences.getBoolean(s1 + "cb1", false)){cb1.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb2", false)){cb2.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb3", false)){cb3.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb4", false)){cb4.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb5", false)){cb5.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb6", false)){cb6.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb7", false)){cb7.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb8", false)){cb8.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb9", false)){cb9.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb10", false)){cb10.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb11", false)){cb11.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb12", false)){cb12.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb13", false)){cb13.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb14", false)){cb14.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb15", false)){cb15.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb16", false)){cb16.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb17", false)){cb17.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb18", false)){cb18.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb19", false)){cb19.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb20", false)){cb20.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb21", false)){cb21.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb22", false)){cb22.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb23", false)){cb23.setChecked(true);}
        if(sharedpreferences.getBoolean(s1 + "cb24", false)){cb24.setChecked(true);}


        switch (QuestionairAgeAndUse.counter){
            case 1:
                if(QuestionairAgeAndUse.c){
                    tv1.setText(R.string.ex_for_device);
                    device = "Computer";
                    break;
                }


            case 2:
                if(QuestionairAgeAndUse.s){
                    tv1.setText(R.string.ex_for_device_s);
                    device = "Smartphone";
                    break;
                }

            case 3:
                if(QuestionairAgeAndUse.t){
                    tv1.setText(R.string.ex_for_device_t);
                    device = "Tablet";
                    break;
                }

            default:Intent myIntent = new Intent(context, AppStart.class);
                startActivity(myIntent);
                finish();
        }


        Button nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {



                    String s1= "";
                    if(QuestionairAgeAndUse.counter == 1){s1 = "c_";}
                    if(QuestionairAgeAndUse.counter == 2){s1 = "s_";}
                    if(QuestionairAgeAndUse.counter == 3){s1 = "t_";}
                    SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean(s1+"cb1", cb1.isChecked());
                    editor.putBoolean(s1+"cb2", cb2.isChecked());
                    editor.putBoolean(s1+"cb3", cb3.isChecked());
                    editor.putBoolean(s1+"cb4", cb4.isChecked());
                    editor.putBoolean(s1+"cb5", cb5.isChecked());
                    editor.putBoolean(s1+"cb6", cb6.isChecked());
                    editor.putBoolean(s1+"cb7", cb7.isChecked());
                    editor.putBoolean(s1+"cb8", cb8.isChecked());
                    editor.putBoolean(s1+"cb9", cb9.isChecked());
                    editor.putBoolean(s1+"cb10", cb10.isChecked());
                    editor.putBoolean(s1+"cb11", cb11.isChecked());
                    editor.putBoolean(s1+"cb12", cb12.isChecked());
                    editor.putBoolean(s1+"cb13", cb13.isChecked());
                    editor.putBoolean(s1+"cb14", cb14.isChecked());
                    editor.putBoolean(s1+"cb15", cb15.isChecked());
                    editor.putBoolean(s1+"cb16", cb16.isChecked());
                    editor.putBoolean(s1+"cb17", cb17.isChecked());
                    editor.putBoolean(s1+"cb18", cb18.isChecked());
                    editor.putBoolean(s1+"cb19", cb19.isChecked());
                    editor.putBoolean(s1+"cb20", cb20.isChecked());
                    editor.putBoolean(s1+"cb21", cb21.isChecked());
                    editor.putBoolean(s1+"cb22", cb22.isChecked());
                    editor.putBoolean(s1+"cb23", cb23.isChecked());
                    editor.putBoolean(s1+"cb24", cb24.isChecked());
                    editor.apply();

                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb1.getText().toString(), cb1.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb2.getText().toString(), cb2.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb3.getText().toString(), cb3.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb4.getText().toString(), cb4.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb5.getText().toString(), cb5.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb6.getText().toString(), cb6.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb7.getText().toString(), cb7.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb8.getText().toString(), cb8.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb9.getText().toString(), cb9.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb10.getText().toString(), cb10.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb11.getText().toString(), cb11.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb12.getText().toString(), cb12.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb13.getText().toString(), cb13.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb14.getText().toString(), cb14.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb15.getText().toString(), cb15.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb16.getText().toString(), cb16.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb17.getText().toString(), cb17.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb18.getText().toString(), cb18.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb19.getText().toString(), cb19.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb20.getText().toString(), cb20.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb21.getText().toString(), cb21.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb22.getText().toString(), cb22.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb23.getText().toString(), cb23.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(device, cb24.getText().toString(), cb24.isChecked()+""));


                    Intent myIntent = new Intent(context, DeviceUsage.class);
                        startActivity(myIntent);
                        finish();

                }

                return false;
            }
        });

        Button backButton = (Button) findViewById(R.id.back);
        backButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    QuestionairAgeAndUse.counter--;
                    Intent myIntent = new Intent(context, DeviceUsage.class);
                    startActivity(myIntent);
                    finish();
                }


                return false;
            }
        });

    }
}
