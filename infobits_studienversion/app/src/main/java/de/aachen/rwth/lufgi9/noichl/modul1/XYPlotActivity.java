package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.*;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.androidplot.util.PixelUtils;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.*;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.*;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;


@SuppressWarnings("ALL")
public class XYPlotActivity extends Activity {

    private XYPlot plot;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plot_module);

        // initialize our XYPlot reference:
        plot = findViewById(R.id.plot);

        // create a couple arrays of y-values to plot:



        final FloatingActionButton nextButton = findViewById(R.id.floatingActionButton);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                Intent myIntent = new Intent(getBaseContext(), CommunicationEnd.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        Number[] series2Numbers = {sharedpreferences.getInt("m1_pre_0", 0),
                sharedpreferences.getInt("m1_pre_1", 0),
                sharedpreferences.getInt("m1_pre_2", 0),
                sharedpreferences.getInt("m1_pre_3", 0),
                sharedpreferences.getInt("m1_pre_4", 0),
                sharedpreferences.getInt("m1_pre_5", 0),
                sharedpreferences.getInt("m1_pre_6", 0),
                sharedpreferences.getInt("m1_pre_7", 0),
                sharedpreferences.getInt("m1_pre_8", 0),
                sharedpreferences.getInt("m1_pre_9", 0),
                sharedpreferences.getInt("m1_pre_10", 0),
                sharedpreferences.getInt("m1_pre_11", 0),
                sharedpreferences.getInt("m1_pre_12", 0),
                sharedpreferences.getInt("m1_pre_13", 0),
                sharedpreferences.getInt("m1_pre_14", 0),
                sharedpreferences.getInt("m1_pre_15", 0)};
        Number[] series1Numbers = {sharedpreferences.getInt("m1_post_0", 0),
                sharedpreferences.getInt("m1_post_1", 0),
                sharedpreferences.getInt("m1_post_2", 0),
                sharedpreferences.getInt("m1_post_3", 0),
                sharedpreferences.getInt("m1_post_4", 0),
                sharedpreferences.getInt("m1_post_5", 0),
                sharedpreferences.getInt("m1_post_6", 0),
                sharedpreferences.getInt("m1_post_7", 0),
                sharedpreferences.getInt("m1_post_8", 0),
                sharedpreferences.getInt("m1_post_9", 0),
                sharedpreferences.getInt("m1_post_10", 0),
                sharedpreferences.getInt("m1_post_11", 0),
                sharedpreferences.getInt("m1_post_12", 0),
                sharedpreferences.getInt("m1_post_13", 0),
                sharedpreferences.getInt("m1_post_14", 0),
                sharedpreferences.getInt("m1_post_15", 0)};

        // turn the above arrays into XYSeries':
        // (Y_VALS_ONLY means use the element index as the x value)
        XYSeries series1 = new SimpleXYSeries(
                Arrays.asList(series1Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.post));
        XYSeries series2 = new SimpleXYSeries(
                Arrays.asList(series2Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.pre));

        // create formatters to use for drawing a series using LineAndPointRenderer
        // and configure them from xml:
        LineAndPointFormatter series1Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels);

        LineAndPointFormatter series2Format =
                new LineAndPointFormatter(this, R.xml.line_point_formatter_with_labels_2);

        // add an "dash" effect to the series2 line:
        series2Format.getLinePaint().setPathEffect(new DashPathEffect(new float[] {

                // always use DP when specifying pixel sizes, to keep things consistent across devices:
                PixelUtils.dpToPix(20),
                PixelUtils.dpToPix(15)}, 0));

        // just for fun, add some smoothing to the lines:
        // see: http://androidplot.com/smooth-curves-and-androidplot/
        series1Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));

        series2Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(20, CatmullRomInterpolator.Type.Centripetal));
        plot.setRangeBoundaries(0,6, BoundaryMode.FIXED);
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 1);
        // add a new series' to the xyplot:
        plot.addSeries(series1, series1Format);
        plot.addSeries(series2, series2Format);

        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                Math.round(((Number) obj).floatValue());
                //return toAppendTo.append(domainLabels[i]);
                return toAppendTo.append("");
            }
            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });
    }
}