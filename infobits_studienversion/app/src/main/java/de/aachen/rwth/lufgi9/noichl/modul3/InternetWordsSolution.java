package de.aachen.rwth.lufgi9.noichl.modul3;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.Objects;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;


public class InternetWordsSolution extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.internet_words);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM3", getActivityNumber("InternetWordsSolution"));
        }
        editor.putInt("current", getActivityNumber("InternetWordsSolution"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.module3_internetwordssolution_info);

        final Button nextButton = findViewById(R.id.next);



        nextButton.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                    if(event.getAction() == MotionEvent.ACTION_UP) {

                        SharedPreferences Preference = getSharedPreferences("internetwords", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = Preference.edit();
                        editor.putBoolean("1", ((CheckBox) findViewById(R.id.w1)).isChecked());
                        editor.putBoolean("2", ((CheckBox) findViewById(R.id.w2)).isChecked());
                        editor.putBoolean("3", ((CheckBox) findViewById(R.id.w3)).isChecked());
                        editor.putBoolean("4", ((CheckBox) findViewById(R.id.w4)).isChecked());
                        editor.putBoolean("5", ((CheckBox) findViewById(R.id.w5)).isChecked());
                        editor.putBoolean("6", ((CheckBox) findViewById(R.id.w6)).isChecked());
                        editor.putBoolean("7", ((CheckBox) findViewById(R.id.w7)).isChecked());
                        editor.putBoolean("8", ((CheckBox) findViewById(R.id.w8)).isChecked());
                        editor.putBoolean("9", ((CheckBox) findViewById(R.id.w9)).isChecked());
                        editor.putBoolean("10", ((CheckBox) findViewById(R.id.w10)).isChecked());

                        editor.apply();

                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.lan), ((CheckBox) findViewById(R.id.w1)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.gsm), ((CheckBox) findViewById(R.id.w2)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("3", getString(R.string.man), ((CheckBox) findViewById(R.id.w3)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("4", getString(R.string.umts), ((CheckBox) findViewById(R.id.w4)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.wan), ((CheckBox) findViewById(R.id.w5)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.lte), ((CheckBox) findViewById(R.id.w6)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("3", getString(R.string.wlan), ((CheckBox) findViewById(R.id.w7)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("4", getString(R.string.fourg), ((CheckBox) findViewById(R.id.w8)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.wifi), ((CheckBox) findViewById(R.id.w9)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.mobiledata), ((CheckBox) findViewById(R.id.w10)).isChecked()+""));



                        showDialog(getString(R.string.module3_internetwordssolution_dialog_title),getString(R.string.module3_internetwordssolution_dialog));

                    }
                    return false;
            }
        });
    }







private void showDialog(String title, String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
        context);

        myLog("testuser",this.getClass()+"/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));

        // set title
        TextView mytitle = new TextView(context);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
        .setMessage(message)
        .setCancelable(false)
        .setPositiveButton("ok",new DialogInterface.OnClickListener() {
public void onClick(DialogInterface dialog,int id) {
        Intent myIntent;

        if(recap){

        myIntent = new Intent(context, ConnectionEnd.class);
            startActivity(myIntent);
            finish();
        }else {
            if(myrecap){
                restartAfterRecap();
            }else {
                myIntent = nextActivity(getActivityNumber("InternetWordsSolution") + 1);
                startActivity(myIntent);
                finish();
            }
        }

        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));

        }
        });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

        TextView textView = alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        }



}
