package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class PWmc extends MyActivity {

    static int index = 0;
    Context context;
    JSONArray myJson = null;
    String alt1, alt2, alt3, alt4;
    boolean a1, a2, a3, a4;
    String scenario;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_usecasetextview_viewstub_s_nextbutton);

        context = this;

        String json = null;
        try {
            InputStream is = this.getAssets().open("pwMC.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (myJson != null) {
            JSONObject rec;
            try {
                rec = myJson.getJSONObject(index);
                alt1 = rec.getString("alt1");
                alt2 = rec.getString("alt2");
                alt3 = rec.getString("alt3");
                alt4 = rec.getString("alt4");
                a1 = rec.getBoolean("correct1");
                a2 = rec.getBoolean("correct2");
                a3 = rec.getBoolean("correct3");
                a4 = rec.getBoolean("correct4");
                scenario = rec.getString("description");
                TextView tv = findViewById(R.id.my_usecase_textview);
                tv.setText(scenario);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        TextView tv;
        CheckBox cb;
        ViewStub stub = findViewById(R.id.viewStub);

        stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
        stub.inflate();
        tv = findViewById(R.id.tv_ex);
        tv.setText(R.string.module4_pwmc_ex);
        cb = findViewById(R.id.alt1);
        cb.setText(alt1);
        cb = findViewById(R.id.alt2);
        cb.setText(alt2);
        cb = findViewById(R.id.alt3);
        cb.setText(alt3);
        cb = findViewById(R.id.alt4);
        cb.setText(alt4);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    CheckBox alt11 = findViewById(R.id.alt1);
                    CheckBox alt12 = findViewById(R.id.alt2);
                    CheckBox alt13 = findViewById(R.id.alt3);
                    CheckBox alt14 = findViewById(R.id.alt4);
                    if (alt11.isChecked() || alt12.isChecked() || alt13.isChecked() || alt14.isChecked()) {
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt11.getText().toString(), alt11.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt12.getText().toString(), alt12.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt13.getText().toString(), alt13.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt14.getText().toString(), alt14.isChecked() + ""));
                        index++;
                        solution(alt11.isChecked(), alt12.isChecked(), alt13.isChecked(), alt14.isChecked());

                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module4_pwmc_dialog), context);
                    }

                }
                return false;
            }
        });
    }
        private void solution(boolean alt11, boolean alt12, boolean alt13, boolean alt14) {

            if(alt11 == a1 && alt12 == a2 && alt13 == a3 && alt14 == a4){
                showDialog(getString(R.string.very_good),getString(R.string.module4_pwmc_dialog_true));
            }else{
                TextView tv = findViewById(R.id.my_usecase_textview);
                String correctAnswer = "\n\n";
                if(a1){correctAnswer = correctAnswer + alt1 + "\n\n";}
                if(a2){correctAnswer = correctAnswer + alt2 + "\n\n";}
                if(a3){correctAnswer = correctAnswer + alt3 + "\n\n";}
                if(a4){correctAnswer = correctAnswer + alt4 + "\n\n";}
                showDialog(getString(R.string.sadly_wrong), getString(R.string.module4_pwmc_dialog_false1) + tv.getText() + getString(R.string.module4_pwmc_dialog_false2) + correctAnswer);
            }

        }

        private void showDialog(String title, String message){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context);

            myLog("testuser",this.getClass()+"/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));

            // set title
            TextView mytitle = new TextView(context);
            mytitle.setText(title);
            mytitle.setPadding(10, 10, 10, 10);
            mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
            alertDialogBuilder.setCustomTitle(mytitle);


            // set dialog message
            alertDialogBuilder
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("ok",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            Intent myIntent;
                            if (index == 5) {
                                if (recap) {
                                    index = 0;
                                    myIntent = new Intent(context, DataSecEnd.class);
                                    startActivity(myIntent);
                                    finish();
                                } else {
                                    if (myrecap) {
                                        index = 0;
                                        restartAfterRecap();
                                    } else{
                                        index = 0;
                                        myIntent = nextActivity(getActivityNumber("PWmcInfo") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                                }
                            } else {
                                myIntent = new Intent(context, PWmc.class);
                                startActivity(myIntent);
                                finish();
                            }
                            myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));

                        }
                    });

            // create alert dialog

            AlertDialog alertDialog = alertDialogBuilder.create();


            // show it
            alertDialog.show();

            Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

            TextView textView = alertDialog.findViewById(android.R.id.message);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
            Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        }
    }
