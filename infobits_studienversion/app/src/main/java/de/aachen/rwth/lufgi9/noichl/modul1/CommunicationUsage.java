package de.aachen.rwth.lufgi9.noichl.modul1;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is to get information from the user. The user should tick all digital communication possibilities he/she have used before.
 * In case the user do not know what a specific communication possibility is about he/she can use the ?-box behind the word to get more information in a dialog box.
 * If the user press the nextButton the  next Activity (SyncVsAsync.class) will be started.
 * All button touch events and user inputs will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class CommunicationUsage extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (R.layout.activity_communication_usage).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Checks whether each of the checkboxes is checked or not and loggs all these inputs individually with the myLog-method.
     * <b>iv1,iv2,iv3,iv4,iv5,iv6,iv7,iv8,iv9,iv10-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Opens a dialog box with further information about the corresponding communication possibility.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_10_checkbox_question_1_checkbox_input);
        stub.inflate();


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("CommunicationUsage"));
        }
        editor.putInt("current", getActivityNumber("CommunicationUsage"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.module1_communication_usage_question);

        final ImageView iv1 = findViewById(R.id.iv_call);
        iv1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.call), getString(R.string.call_info), context);
                }
                return true;
            }
        });
        final ImageView iv2 = findViewById(R.id.iv_sms);
        iv2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.sms), getString(R.string.sms_info), context);
                }
                return true;
            }
        });
        final ImageView iv3 = findViewById(R.id.iv_mms);
        iv3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.mms), getString(R.string.mms_info), context);
                }
                return true;
            }
        });
        final ImageView iv4 = findViewById(R.id.iv_fax);
        iv4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.fax), getString(R.string.fax_info), context);
                }
                return true;
            }
        });
        final ImageView iv5 = findViewById(R.id.iv_mail);
        iv5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.mail), getString(R.string.mail_info), context);
                }
                return true;
            }
        });
        final ImageView iv6 = findViewById(R.id.iv_chat);
        iv6.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.chat), getString(R.string.chat_info), context);
                }
                return true;
            }
        });
        final ImageView iv7 = findViewById(R.id.iv_videocall);
        iv7.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv7));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.videocall), getString(R.string.videocall_info), context);
                }
                return true;
            }
        });
        final ImageView iv8 = findViewById(R.id.iv_forum);
        iv8.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv8));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.forum), getString(R.string.forum_info), context);
                }
                return true;
            }
        });

        final ImageView iv9 = findViewById(R.id.iv_comment);
        iv9.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv9));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.comment), getString(R.string.comment_info), context);
                }
                return true;
            }
        });
        final ImageView iv10 = findViewById(R.id.iv_im);
        iv10.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), ivEventExtension(event, iv10));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showDialog(getString(R.string.im), getString(R.string.im_info), context);
                }
                return true;
            }
        });

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    SharedPreferences Preference = getSharedPreferences("communication_usage", Activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = Preference.edit();
                    editor.putBoolean("1", ((CheckBox) findViewById(R.id.call)).isChecked());
                    editor.putBoolean("2", ((CheckBox) findViewById(R.id.sms)).isChecked());
                    editor.putBoolean("3", ((CheckBox) findViewById(R.id.mms)).isChecked());
                    editor.putBoolean("4", ((CheckBox) findViewById(R.id.fax)).isChecked());
                    editor.putBoolean("5", ((CheckBox) findViewById(R.id.mail)).isChecked());
                    editor.putBoolean("6", ((CheckBox) findViewById(R.id.chat)).isChecked());
                    editor.putBoolean("7", ((CheckBox) findViewById(R.id.videocall)).isChecked());
                    editor.putBoolean("8", ((CheckBox) findViewById(R.id.forum)).isChecked());
                    editor.putBoolean("9", ((CheckBox) findViewById(R.id.comment)).isChecked());
                    editor.putBoolean("10", ((CheckBox) findViewById(R.id.im)).isChecked());
                    editor.apply();

                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.call), ((CheckBox) findViewById(R.id.call)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.sms), ((CheckBox) findViewById(R.id.sms)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("3", getString(R.string.mms), ((CheckBox) findViewById(R.id.mms)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("4", getString(R.string.fax), ((CheckBox) findViewById(R.id.fax)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("5", getString(R.string.mail), ((CheckBox) findViewById(R.id.mail)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("6", getString(R.string.chat), ((CheckBox) findViewById(R.id.chat)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("7", getString(R.string.videocall), ((CheckBox) findViewById(R.id.videocall)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("8", getString(R.string.forum), ((CheckBox) findViewById(R.id.forum)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("9", getString(R.string.comment), ((CheckBox) findViewById(R.id.comment)).isChecked() + ""));
                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("10", getString(R.string.im), ((CheckBox) findViewById(R.id.im)).isChecked() + ""));

                    Intent myIntent = nextActivity(getActivityNumber("CommunicationUsage") + 1);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });
    }


}
