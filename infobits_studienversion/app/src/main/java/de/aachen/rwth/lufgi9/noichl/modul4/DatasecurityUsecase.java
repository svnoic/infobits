package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class DatasecurityUsecase extends MyActivity {

    static int stepcounter = 0;
    static int index;
    static String syncaync = "";
    Context context;
    JSONArray myJson = null;
    String alt1, alt2, alt3, alt4, alt21, alt22, alt23, alt24, sol1, sol2, sol3;
    String scenario;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_usecasetextview_viewstub_s_nextbutton);

        context = this;

        String json = null;
        try {
            InputStream is = this.getAssets().open("datasecuritySzenario.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (myJson != null) {
            JSONObject rec;
            if (stepcounter == 0) {
                index = (int) (Math.random() * 2);
            }
            try {
                rec = myJson.getJSONObject(index);
                alt1 = rec.getString("alt1");
                alt2 = rec.getString("alt2");
                alt3 = rec.getString("alt3");
                alt4 = rec.getString("alt4");
                alt21 = rec.getString("alt21");
                alt22 = rec.getString("alt22");
                alt23 = rec.getString("alt23");
                alt24 = rec.getString("alt24");
                sol1 = rec.getString("sol1");
                sol2 = rec.getString("sol2");
                sol3 = rec.getString("sol3");

                scenario = rec.getString("description");
                TextView tv = findViewById(R.id.my_usecase_textview);
                tv.setText(scenario);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        TextView tv;
        CheckBox cb;
        ViewStub stub = findViewById(R.id.viewStub);
        switch (stepcounter) {
            case 0:
                stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                tv.setText(R.string.module4_datsec_case1);
                cb = findViewById(R.id.alt1);
                cb.setText(alt1);
                cb = findViewById(R.id.alt2);
                cb.setText(alt2);
                cb = findViewById(R.id.alt3);
                cb.setText(alt3);
                cb = findViewById(R.id.alt4);
                cb.setText(alt4);
                break;
            case 1:
                stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                tv.setText(R.string.module4_datsec_case2);
                cb = findViewById(R.id.alt1);
                cb.setText(alt21);
                cb = findViewById(R.id.alt2);
                cb.setText(alt22);
                cb = findViewById(R.id.alt3);
                cb.setText(alt23);
                cb = findViewById(R.id.alt4);
                cb.setText(alt24);
                break;
            case 2:
                stub.setLayoutResource(R.layout.viewstub_textview_2_radiobutton);
                stub.inflate();
                tv = findViewById(R.id.my_textview);
                tv.setText(R.string.module4_datsec_case3);
                tv = findViewById(R.id.likert6);
                tv.setText(R.string.more_yes);
                tv = findViewById(R.id.likert5);
                tv.setText(R.string.more_no);
                break;

        }


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (stepcounter < 2) {

                        switch (stepcounter) {
                            case 0:
                                CheckBox alt1 = findViewById(R.id.alt1);
                                CheckBox alt2 = findViewById(R.id.alt2);
                                CheckBox alt3 = findViewById(R.id.alt3);
                                CheckBox alt4 = findViewById(R.id.alt4);
                                if (alt1.isChecked() || alt2.isChecked() || alt3.isChecked() || alt4.isChecked()) {
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt1.getText().toString(), alt1.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt2.getText().toString(), alt2.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt3.getText().toString(), alt3.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt4.getText().toString(), alt4.isChecked() + ""));
                                    showDialog("Auflösung",sol1);

                                } else {
                                    showDialog(getString(R.string.not_yet_ready), getString(R.string.module4_datsec_case_dialog), context);
                                }
                                break;

                            case 1:
                                alt1 = findViewById(R.id.alt1);
                                alt2 = findViewById(R.id.alt2);
                                alt3 = findViewById(R.id.alt3);
                                alt4 = findViewById(R.id.alt4);
                                if (alt1.isChecked() || alt2.isChecked() || alt3.isChecked() || alt4.isChecked()) {
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt1.getText().toString(), alt1.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt2.getText().toString(), alt2.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt3.getText().toString(), alt3.isChecked() + ""));
                                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt4.getText().toString(), alt4.isChecked() + ""));

                                    Intent myIntent = new Intent(context, DatasecurityUsecase.class);
                                    startActivity(myIntent);
                                    finish();
                                    stepcounter++;
                                } else {
                                    showDialog(getString(R.string.not_yet_ready), getString(R.string.module4_datsec_case_dialog), context);
                                }
                                break;
                        }

                    } else {
                        RadioGroup myRadioGroup = findViewById(R.id.radioGroup3);
                        int selectedID = myRadioGroup.getCheckedRadioButtonId();
                        if (findViewById(selectedID) != null) {
                            RadioButton selectedView = findViewById(selectedID);
                            syncaync = (String) selectedView.getText();
                            myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, ((TextView) findViewById(R.id.my_textview)).getText().toString(), ((String) selectedView.getText())));
                            showDialog(getString(R.string.solution),sol2 + "\n\n" + sol3);

                        } else {
                            showDialog(getString(R.string.not_yet_ready), getString(R.string.module4_datsec_case_dialog), context);
                        }
                    }
                }
                return false;
            }
        });
    }


    private void showDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        myLog("testuser", this.getClass() + "/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));

        // set title
        TextView mytitle = new TextView(context);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (stepcounter < 2) {
                            Intent myIntent = new Intent(context, DatasecurityUsecase.class);
                            startActivity(myIntent);
                            finish();
                            stepcounter++;
                        } else{
                            stepcounter = 0;
                            index = 0;
                            if (recap) {
                            Intent myIntent = new Intent(context, DataSecEnd.class);
                            startActivity(myIntent);
                            finish();
                            } else {
                                if(myrecap){
                                    stepcounter = 0;
                                    index = 0;
                                    restartAfterRecap();
                                }else {
                                    stepcounter = 0;
                                    index = 0;
                                    Intent myIntent = nextActivity(getActivityNumber("DatasecuritySzenarioInfo") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                            }}
                        myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        finish();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

        TextView textView = alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }



}
