package de.aachen.rwth.lufgi9.noichl.basics.eingaben;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is for learning how RadioButtons work. There is one question with a correct answer. The user has to tick the correct answer to understand that just one selection is possible.
 * If the user chose not the correct answer a dialog box is shown saying that the answer are not yet correct. If the user chose the correct answer the next Activity (Check.class) is started.
 * The selected answer will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Radio extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (R.layout.activity_textview_viewstub2_next_button) which uses a viewstub (R.layout.basics_inputs_singlechoice).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event the answer chosen by the user is logged and evaluated.
     * If the user has given the correct answer the next activity (Check.class) will be started, otherwise a dialog will be shown.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_inputs_singlechoice);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.input_radio_question);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    RadioGroup rg = findViewById(R.id.radioGroup);
                    int selectedId = rg.getCheckedRadioButtonId();
                    RadioButton rb = findViewById(selectedId);
                    String givenAnswer = (String) rb.getText();
                    myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(getString(R.string.input_radio_question), givenAnswer.equals(getString(R.string.input_radio_solution)), givenAnswer, getString(R.string.input_radio_solution)));

                    if (givenAnswer.equals(getString(R.string.input_radio_solution))) {
                        Intent myIntent = new Intent(context, Check.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.try_again), context);
                    }
                }

                return false;
            }
        });


    }

}
