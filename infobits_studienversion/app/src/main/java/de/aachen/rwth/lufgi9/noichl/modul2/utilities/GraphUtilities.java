package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

/**
 * Utility class for operations on mathematical graphs
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class GraphUtilities {

    /**
     * Calculates the number of edges in a complete graph
     *
     * @param nodes The number of nodes in the graph that can be connected to each other
     * @return The number of edges in a complete graph
     */
    public static long edgesInCompleteGraph(long nodes){
        return nodes * (nodes - 1) / 2;
    }
}
