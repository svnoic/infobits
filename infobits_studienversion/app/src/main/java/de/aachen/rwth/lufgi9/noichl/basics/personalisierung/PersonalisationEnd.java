package de.aachen.rwth.lufgi9.noichl.basics.personalisierung;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.AppStart;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class PersonalisationEnd extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText("Super! Du hast die Personalisierung abgeschlossen, die App passt ihr aussehen jetzt an deine Bedürfnisse an, um dir eine möglichst einfache Interaktion zu ermöglichen. Tippe auf WEITER um zur Modul-Auswahl zurückzukehren.");

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                Intent myIntent = new Intent(context,   AppStart.class);
                ChangeActivity.currentActivity = 2;
                saveSharedPrefs();
                startActivity(myIntent);
                finish();
                return false;
            }
        });
    }
}
