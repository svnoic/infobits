package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class CreatePW extends MyActivity {

    Context context;
    private boolean solved;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("CreatePW"));
        }
        editor.putInt("current", getActivityNumber("CreatePW"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_textview_edittext);
        stub.inflate();

        /***********************************************************************************************
         *
         * Listener
         *
         **********************************************************************************************/
        /*
          Checks if the entered password fulfils the required criteria. Depending on the results a
          corresponding info text is show. If a criteria is violated a description for the error is
          given.
         */
        final TextView editTextPasswordCreation = findViewById(R.id.et);
        final TextView textViewPasswordCreationFeedback = findViewById(R.id.tv_ex);
        @SuppressLint("CutPasteId") EditText et = findViewById(R.id.et);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // receive and store the password entered by the user
                String password = editTextPasswordCreation.getText().toString();
                //SharedPreferencesHelper.saveString(PW_Creation.PW_CREATION_PASSWORD_KEY, password);
                //Logging.info(this, "PASSWORD_QUALITY", password);

                // if no password was entered show an error message
                if (password.length() == 0) {
                    //textViewPasswordCreationFeedback.setBackgroundColor(getResources().getColor(R.color.pw_creation_background));
                    textViewPasswordCreationFeedback.setText(R.string.module4_createpw_give_pw);
                    return;
                }

                // check all the criteria for a good password add an error message
                int rating = 0;
                String feedbackExtended = "";
                if (!password.matches(".*[A-Z].*")) {
                    rating++;
                    feedbackExtended += getString(R.string.pw_creation_password_criteria_captital);
                }
                if (!password.matches(".*[a-z].*")) {
                    rating++;
                    feedbackExtended += getString(R.string.pw_creation_password_criteria_small);
                }
                if (!password.matches(".*[0-9].*")) {
                    rating++;
                    feedbackExtended += getString(R.string.pw_creation_password_criteria_number);
                }
                if (!password.matches(".*[!?@%&+-].*")) {
                    rating++;
                    feedbackExtended += getString(R.string.pw_creation_password_criteria_special);
                }
                if (password.length() < 8) {
                    rating++;
                    feedbackExtended += getString(R.string.pw_creation_password_criteria_length);
                }
                if (!password.matches("[a-zA-Z0-9!?@%&+-]*")) {
                    rating++;
                    feedbackExtended += getString(R.string.pw_creation_password_criteria_illegal);
                }

                // when criteria were violated show a corresponding error message
                if (rating == 0) {
                    if (password.length() >= 12) {
                        textViewPasswordCreationFeedback.setBackgroundColor(getResources().getColor(R.color.green));
                        textViewPasswordCreationFeedback.setText(R.string.pw_creation_password_quality_good);
                        myLog("testuser", this.getClass()+"/entered", "entered", this.getClass().toString(), enteredExtension(password, getResources().getString(R.string.pw_creation_password_quality_good)));
                        solved = true;
                    } else if (password.length() >= 8) {
                        textViewPasswordCreationFeedback.setBackgroundColor(getResources().getColor(R.color.yellow));
                        textViewPasswordCreationFeedback.setText(R.string.pw_creation_password_quality_neutral);
                        myLog("testuser", this.getClass()+"/entered", "entered", this.getClass().toString(), enteredExtension(password, getResources().getString(R.string.pw_creation_password_quality_neutral)));
                        solved = true;
                    } else {
                        textViewPasswordCreationFeedback.setBackgroundColor(getResources().getColor(R.color.red));
                        textViewPasswordCreationFeedback.setText(R.string.pw_creation_password_quality_bad);
                        myLog("testuser", this.getClass()+"/entered", "entered", this.getClass().toString(), enteredExtension(password, getResources().getString(R.string.pw_creation_password_quality_bad)));
                        solved = false;
                    }
                } else {
                    textViewPasswordCreationFeedback.setBackgroundColor(getResources().getColor(R.color.red));
                    textViewPasswordCreationFeedback.setText(getResources().getString(R.string.pw_creation_password_quality_not_all_criteria) + feedbackExtended);
                    myLog("testuser", this.getClass()+"/entered", "entered", this.getClass().toString(), enteredExtension(password, getResources().getString(R.string.pw_creation_password_quality_not_all_criteria) + feedbackExtended));
                    solved = false;
                }


            }});



            TextView tv = findViewById(R.id.my_textview);
            tv.setText(R.string.create_pw);

            final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                    if(event.getAction()==MotionEvent.ACTION_UP) {
                        if(solved) {
                            Intent myIntent = nextActivity(getActivityNumber("CreatePW") + 1);
                            startActivity(myIntent);
                            finish();
                        }else{
                            showDialog(getString(R.string.not_yet_ready), getString(R.string.module4_createpw_dialog), context);
                        }
                    }
                    return false;
                }
            });

    }
}
