package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;

import androidx.core.content.res.ResourcesCompat;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.google.android.flexbox.FlexboxLayout.LayoutParams;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class SyncVsAsync extends MyActivity implements View.OnDragListener, View.OnTouchListener {


    private static boolean complete = false;
    private static boolean solution = false;
    private static boolean solution0 = false;
    int notChecked = 0;
    ArrayList<String> answersChecked = new ArrayList<>();
    Context context;


    @SuppressLint({"ResourceType", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("SyncVsAsync"));
        }
        editor.putInt("current", getActivityNumber("SyncVsAsync"));
        editor.apply();
        saveSharedPrefs();

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_sort_3_category);
        stub.inflate();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.sync_vs_async_exercise_known);

        ArrayList<String> answers = new ArrayList<>();
        answers.add(getString(R.string.call));
        answers.add(getString(R.string.sms));
        answers.add(getString(R.string.mms));
        answers.add(getString(R.string.fax));
        answers.add(getString(R.string.mail));
        answers.add(getString(R.string.videocall));
        answers.add(getString(R.string.chat));
        answers.add(getString(R.string.forum));
        answers.add(getString(R.string.comment));
        answers.add(getString(R.string.im));

        View linearLayout = findViewById(R.id.flex_layout);


        SharedPreferences Preference = getSharedPreferences("communication_usage", Activity.MODE_PRIVATE);
        for (int i = 0; i < answers.size(); i++) {
            Log.e(i + "", Preference.getBoolean((i + 1) + "", false) + "");
            if (!complete) {
                if (Preference.getBoolean((i + 1) + "", false)) {
                    answersChecked.add(answers.get(i));
                }else{
                    notChecked++;
                }
            } else {
                tv.setText(R.string.module1_sync_vs_async_question);
                if (!Preference.getBoolean((i + 1) + "", false)) {
                    answersChecked.add(answers.get(i));
                }

            }

        }

        if (answersChecked.size() == 0){
            complete = true;
            Intent myIntent = new Intent(context, SyncVsAsync.class);
            startActivity(myIntent);
            finish();
        }

        for (int i = 0; i < answersChecked.size(); i++) {
            LinearLayout mylinearLayout = new LinearLayout(this);
            mylinearLayout.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
            mylinearLayout.setPadding(8, 8, 8, 8);
            TextView valueTV = new TextView(this);
            valueTV.setText(answersChecked.get(i));
            int[] array = new int[2];
            valueTV.getLocationOnScreen(array);
            int w = valueTV.getWidth();
            int h = valueTV.getHeight();
            myLog("testuser",this.getClass()+"/displayed", "displayed", this.getClass().toString(), textExtension(answersChecked.get(i)));
            myLog("testuser", this.getClass()+"/created", "created", this.getClass().toString(), "\"timestamp\": " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+ ", \"displaymetrics\": { \"element\": \"" + valueTV + "\", \"x1\": \"" + array[0] + "\", \"y1\": \"" + array[1]+ "\", \"x2\": \"" + (array[0]+w) + "\", \"y2\": \"" + (array[1]+h) + "}");

            valueTV.setTextColor(Color.BLACK);
            valueTV.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sortbox, null));
            valueTV.setId(i);
            mylinearLayout.setTag("l" + valueTV.getId());
            valueTV.setTag(i + "");
            valueTV.setPadding(8, 8, 8, 8);
            valueTV.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

            mylinearLayout.addView(valueTV);

            valueTV.setOnTouchListener(this);

            ((LinearLayout) linearLayout).addView(mylinearLayout);
        }


        implementEvents();

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (complete || notChecked == 0) {
                        if (!solution) {
                            ViewGroup vg = findViewById(R.id.flex_layout);
                            if (vg.getChildCount() == 0) {
                                solution();
                                solution = true;
                            }else{
                                showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_sync_vs_async_dialog), context);
                            }
                        } else {
                            complete = false;
                            solution = false;
                            solution0 = false;
                            if(recap){
                                Intent myIntent = new Intent(context, CommunicationEnd.class);
                                startActivity(myIntent);
                                finish();
                            }else {
                                if(myrecap){
                                    restartAfterRecap();
                                }else
                                        {
                                        Intent myIntent = nextActivity(getActivityNumber("SyncVsAsync") + 1);
                                        startActivity(myIntent);
                                        finish();
                                    }
                                }
                            }

                    } else {
                        if (!solution0) {
                            ViewGroup vg = findViewById(R.id.flex_layout);
                            if (vg.getChildCount() == 0) {
                                solution();
                                solution0 = true;
                            }else{
                                showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_sync_vs_async_dialog), context);
                            }
                        } else {
                            complete = true;
                            Intent myIntent = new Intent(context, SyncVsAsync.class);
                            startActivity(myIntent);
                            finish();
                        }
                    }
                }
                return false;
            }

        });
    }


    private void solution() {

        for (int i = 0; i < answersChecked.size(); i++) {

            ViewGroup parent1 = findViewById(R.id.left_layout);
            parent1.setTag(R.string.sync);
            ViewGroup parent2 = findViewById(R.id.middle_layout);
            parent2.setTag(R.string.mix);
            ViewGroup parent3 = findViewById(R.id.right_layout);
            parent3.setTag(R.string.async);

            TextView myview0 = findViewById(i);
            myview0.setEnabled(false);

            if (i == 0 || i == 5 || i == 6) {
                if (myview0.getParent().getParent() == parent1) {
                    myview0.setBackgroundResource(R.drawable.answer_true);
                    myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), true, parent1.getTag().toString(), parent1.getTag().toString()));
                } else {
                    myview0.setBackgroundResource(R.drawable.answer_false);
                    if(myview0.getParent().getParent() == parent2){
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent2.getTag().toString(), parent1.getTag().toString()));
                    }else{
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent3.getTag().toString(), parent1.getTag().toString()));
                    }
                }
            }
            if (i == 3 || i == 4 || i == 7 || i == 8) {
                if (myview0.getParent().getParent() == parent3) {
                    myview0.setBackgroundResource(R.drawable.answer_true);
                    myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), true, parent3.getTag().toString(), parent3.getTag().toString()));
                } else {
                    myview0.setBackgroundResource(R.drawable.answer_false);
                    if(myview0.getParent().getParent() == parent2){
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent2.getTag().toString(), parent3.getTag().toString()));
                    }else{
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent1.getTag().toString(), parent3.getTag().toString()));
                    }
                }
            }
            if (i == 9 || i == 1 || i == 2) {
                if (myview0.getParent().getParent() == parent2 || myview0.getParent().getParent() == parent3) {
                    myview0.setBackgroundResource(R.drawable.answer_true);
                    if(myview0.getParent().getParent() == parent2){
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), true, parent2.getTag().toString(), parent2.getTag().toString() + ", " + parent3.getTag().toString()));
                    }else{
                        myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), true, parent3.getTag().toString(), parent2.getTag().toString() + ", " + parent3.getTag().toString()));
                    }
                } else {
                    myview0.setBackgroundResource(R.drawable.answer_false);
                    myLog("testuser",this.getClass()+"/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), false, parent1.getTag().toString(), parent2.getTag().toString() + ", " + parent3.getTag().toString()));
                }
            }
        }
    }


    private void implementEvents() {


        findViewById(R.id.left_layout).setOnDragListener(this);
        findViewById(R.id.middle_layout).setOnDragListener(this);
        findViewById(R.id.right_layout).setOnDragListener(this);
    }


    @Override
    public boolean onDrag(View view, DragEvent event) {
        View v = (View) event.getLocalState();
        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), dragEventExtension(event, view, ((TextView)v).getText().toString()));

        int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:

                return event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);

            case DragEvent.ACTION_DRAG_ENTERED:

                view.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                view.invalidate();

                return true;
            case DragEvent.ACTION_DRAG_LOCATION:

                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                view.getBackground().clearColorFilter();
                view.invalidate();

                return true;

            case DragEvent.ACTION_DROP:
                view.getBackground().clearColorFilter();
                view.invalidate();

                ViewGroup owner = (ViewGroup) v.getParent();
                owner.removeView(v);//remove the dragged view
                ViewGroup root = (ViewGroup) owner.getParent();
                if (view instanceof FlexboxLayout) {
                    FlexboxLayout container = (FlexboxLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                    LinearLayout rm = owner.findViewWithTag("l" + v.getId());
                    root.removeView(rm);
                    LinearLayout l = new LinearLayout(this);
                    l.setPadding(8, 8, 8, 8);
                    l.setTag("l" + v.getId());
                    l.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
                    l.addView(v);
                    container.addView(l);//Add the dragged view
                } else {
                    LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                    LinearLayout rm = owner.findViewWithTag("l" + v.getId());
                    root.removeView(rm);
                    LinearLayout l = new LinearLayout(this);
                    l.setPadding(8, 8, 8, 8);
                    l.setTag("l" + v.getId());
                    l.setLayoutParams((new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)));
                    l.addView(v);
                    container.addView(l);//Add the dragged view
                }

                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                view.getBackground().clearColorFilter();
                view.invalidate();
                view = (View) event.getLocalState();
                view.setVisibility(View.VISIBLE);
                return true;
        }
        return false;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), viewEventExtension(motionEvent, view));
        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());


        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

        view.startDrag(data, shadowBuilder, view, 0);

        view.setVisibility(View.INVISIBLE);

        return true;
    }

}
