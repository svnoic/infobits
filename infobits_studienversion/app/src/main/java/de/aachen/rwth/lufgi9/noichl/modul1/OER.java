package de.aachen.rwth.lufgi9.noichl.modul1;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

public class OER extends MyActivity {

    static int videopos = 10;
    VideoView myVideo;
    Context context;
    Button nextButton;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("OER"));
        }
        editor.putInt("current", getActivityNumber("OER"));
        editor.apply();
        saveSharedPrefs();


        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.v6);
        myVideo = findViewById(R.id.videoView);
        myVideo.setVideoURI(uri);
        myVideo.seekTo(videopos);
        myVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(MediaPlayer mediaPlayer) {
                        myVideo.start();
                        myVideo.pause();
                    }
                });
            }
        });


        myVideo.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), videoButtonEventExtension(event, myVideo, "wlan", myVideo.getCurrentPosition()));

                if (myVideo.isPlaying()) {
                    myVideo.pause();
                } else {
                    myVideo.start();
                }
                return false;
            }
        });

        MediaController mediaController = new
                MediaController(this);
        mediaController.setAnchorView(myVideo);
        myVideo.setMediaController(mediaController);


        nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (recap) {
                        Intent myIntent = new Intent(context, CommunicationEnd.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        if(myrecap){
                            restartAfterRecap();
                        }else {
                            Intent myIntent = nextActivity(getActivityNumber("OER") + 1);
                            startActivity(myIntent);
                            finish();
                        }
                    }

                }
                return false;
            }
        });
    }




}