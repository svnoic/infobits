package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.Objects;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationEnd;

public class DataMC extends MyActivity {

    Context context;
    String scenario;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("DataMC"));
        }
        editor.putInt("current", getActivityNumber("DataMC"));
        editor.apply();
        saveSharedPrefs();

        TextView tv;
        CheckBox cb;
        ViewStub stub = findViewById(R.id.viewStub);

        stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
        stub.inflate();
        tv = findViewById(R.id.tv_ex);
        tv.setText(R.string.question);
        cb = findViewById(R.id.alt1);
        cb.setText(R.string.module4_datamc_item1);
        cb = findViewById(R.id.alt2);
        cb.setText(R.string.module4_datamc_item2);
        cb = findViewById(R.id.alt3);
        cb.setText(R.string.module4_datamc_item3);
        cb = findViewById(R.id.alt4);
        cb.setText(R.string.module4_datamc_item4);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    CheckBox alt11 = findViewById(R.id.alt1);
                    CheckBox alt12 = findViewById(R.id.alt2);
                    CheckBox alt13 = findViewById(R.id.alt3);
                    CheckBox alt14 = findViewById(R.id.alt4);

                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt11.getText().toString(), alt11.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt12.getText().toString(), alt12.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt13.getText().toString(), alt13.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt14.getText().toString(), alt14.isChecked() + ""));

                        showDialog(getString(R.string.module4_datamc_dialog_title), getString(R.string.module4_datamc_dialog));

                }
                return false;
            }
        });
    }


        private void showDialog(String title, String message){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context);

            myLog("testuser",this.getClass()+"/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));

            // set title
            TextView mytitle = new TextView(context);
            mytitle.setText(title);
            mytitle.setPadding(10, 10, 10, 10);
            mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
            alertDialogBuilder.setCustomTitle(mytitle);


            // set dialog message
            alertDialogBuilder
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("ok",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            Intent myIntent;
                                if (recap) {
                                    myIntent = new Intent(context, CommunicationEnd.class);
                                    startActivity(myIntent);
                                    finish();
                                } else {
                                    if(myrecap){
                                        restartAfterRecap();
                                    }else {
                                        myIntent = nextActivity(getActivityNumber("DataMC") + 1);
                                        startActivity(myIntent);
                                        finish();
                                    }
                                }

                            myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));

                        }
                    });

            // create alert dialog

            AlertDialog alertDialog = alertDialogBuilder.create();


            // show it
            alertDialog.show();

            Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

            TextView textView = alertDialog.findViewById(android.R.id.message);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
            Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        }
    }
