package de.aachen.rwth.lufgi9.noichl.general;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;

public class PreCompInformation extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mytextview_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText("Im folgenden werden deine digitalen Kompetenzen ermittelt. Dafür wird das EU-Rahmenmodell für digitale Kompetenzen, verwendet. Dieses Modell besteht aus insgesamt 21 Kompetenzen, die in 5 verschiedene Bereiche einsortiert sind." +
                "\nFür jede diser Kompetenzen wird deine persönliche Kompetenzstufe ermittelt. Diese Stufe kann zwischen 0 und 8 liegen, wobei 8 die höchste Stufe darstellt." +
                "\nAber keine Sorge, wenn du nicht die höchste Stufe erreichst. Es wird vermutet, dass Personen, die keine spezielle Ausbildung im Bereich digitaler Medien besitzen, maximal die Stufen 3 bis 4 erreichen." +
                "\n\nUm deine Kompetenzstufen festzustellen, wirst du im folgenden eine kurze Erläuterung sehen, worum es bei der jeweiligen Kompetenz geht, gefolgt von einem Satz, den du dann fortsetzen musst." +
                " Dafür siehst du bis zu vier Antwortmöglichkeiten. Es ist wichtig, dass du immer ALLE Antwortmöglichkeiten auswählst, die auf dich zutreffen, bevor du auf den Weiter-Knopf drückst." +
                " Achte dabei immer auf den Anfang des Satzes, jenach Kompetenzstufe ändert sich dieser nämlich!" +
                "\n\nAm Ende der Module kannst du dir die Entwicklung deiner digitalen Kompetenzen ansehen. Bei der ersten Nutzung der App werden deine digitalen Kompetenzen vollständig ermittelt. Am Ende jedes Moduls, werden diese dann aktualisiert." +
                "\nSo kannst du nach jedem Modul sehen, welche digitalen Kompetenzen sich durch das jeweilige Modul verbessert haben, natürlich werden sich nicht alle Kompetenzen verbessern, da nicht jedes Modul alle Themen behandelt." +
                "\n\nWie immer gilt: Antworte bitte ehrlich und wähle alle Antworten aus, die auf die zutreffen.");

        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                Intent myIntent = new Intent(context,   DigComp.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });
    }

}
