package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.InputStream;

/**
 * Utility class for access asset files.
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class Assets {

    /**
     * Open and return the content of a text file as string.
     *
     * @param context  Used for access resources
     * @param filename The name of the file in the asset directory
     * @return string of complete text content
     */
    public static String readAsset(Context context, String filename) {
        String result = "";

        // use the asset manager to get access to the asset files
        AssetManager assetManager = context.getAssets();
        // load the text file
        InputStream inputStream;
        try {
            inputStream = assetManager.open(filename);

            // get the size to read the complete file at once
            int size = inputStream.available();

            // buffer to store the content
            byte[] buffer = new byte[size];

            // load the file content into the buffer
            //noinspection ResultOfMethodCallIgnored
            inputStream.read(buffer);

            // close the stream
            inputStream.close();

            // parse to string
            result = new String(buffer);
        } catch (Exception e) {
            //CustomLog.w(context, Assets.class, "exception", "exception while read asset for file '" + filename + "'");
            e.printStackTrace();
        }

        return result;
    }
}
