package de.aachen.rwth.lufgi9.noichl.general;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationEnd;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetEnd;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionEnd;
import de.aachen.rwth.lufgi9.noichl.modul4.DataSecEnd;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class SUS extends MyActivity {

    private static int questionCounter = 1;
    Context context;
    JSONArray myJson = null;
    JSONObject rec = null;
    public static int currentModule=0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_5_radiobutton);
        stub.inflate();

Log.e("Counter: ", ""+questionCounter);

        String json = null;
        try {
            InputStream is = this.getAssets().open("sus.json");
            Log.e("DONE", "DONE");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            myJson = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (myJson != null) {
            for (int i = 0; i < myJson.length(); ++i) {
                try {
                    rec = myJson.getJSONObject(i);
                    Log.e("rec", rec + "");

                    String id = rec.getString("id");
                    String question = "sus_item" + questionCounter;
                    if (id.equals(question)) {
                        TextView likertQuestion = findViewById(R.id.my_textview);
                        RadioButton likert1 = findViewById(R.id.likert1);
                        RadioButton likert2 = findViewById(R.id.likert2);
                        RadioButton likert3 = findViewById(R.id.likert3);
                        RadioButton likert4 = findViewById(R.id.likert4);
                        RadioButton likert5 = findViewById(R.id.likert5);
                        likertQuestion.setText(rec.getString("description"));
                        likert1.setText(R.string.likert1);
                        likert2.setText(R.string.likert2);
                        likert3.setText("teils/teils");
                        likert4.setText(R.string.likert5);
                        likert5.setText(R.string.likert6);

                        final RadioGroup radioGroup = findViewById(R.id.radioGroup);
                        for (int j = 0; j < radioGroup.getChildCount(); j++) {
                            final Button rb = (Button) radioGroup.getChildAt(j);
                            rb.setOnTouchListener(new View.OnTouchListener() {
                                public boolean onTouch(View v, MotionEvent event) {
                                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, rb));
                                    return false;
                                }
                            });
                        }

                        final Button next = findViewById(R.id.next);
                        next.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, next));
                                try {
                                    if(event.getAction() == MotionEvent.ACTION_UP){nextListener(rec.getString("id"), rec.getString("description"));}
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }
                        });
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }



        private void nextListener(String id, String description) {
            RadioGroup radioGroup = findViewById(R.id.radioGroup);
            int index = radioGroup.indexOfChild(findViewById(radioGroup.getCheckedRadioButtonId()));
            int lastScore = 5 - index;


            myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension(id, description, String.valueOf(lastScore)));



            if (questionCounter <= myJson.length()-1 && lastScore < 6) {
                questionCounter++;
                Intent myIntent = new Intent(context, SUS.class);
                startActivity(myIntent);
                finish();
            } else {
                saveSharedPrefs();
                if (lastScore < 6) {
                    questionCounter++;
                    Intent myIntent = null;
                    switch (currentModule){
                        case 1:  myIntent = new Intent(context, PreCompInformation.class); break;
                        case 2:  myIntent = new Intent(context, PreCompInformation.class); break;
                        case 3:  myIntent = new Intent(context, PreCompInformation.class); break;
                        case 4:  myIntent = new Intent(context, PreCompInformation.class); break;
                    }
                    questionCounter=1;
                    startActivity(myIntent);
                    finish();
                }else {
                    if(lastScore == 6){

                        showDialog("Noch nicht fertig","Bitte wähle durch antippen eine Antwort aus bevor du weiter machst.", context);
                    }
                }
            }
        }


}
