package de.aachen.rwth.lufgi9.noichl.modul4;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileNotFoundException;


import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.AppStart;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;


public class DataSecEnd extends MyActivity {

    Context context;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewstub_finalpage_module4);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("DataSecEnd"));
        }
        editor.putInt("current", getActivityNumber("DataSecEnd"));
        editor.apply();
        saveSharedPrefs();

        recap = true;


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.finish);


        final Button nextButton = (Button) findViewById(R.id.next);
        nextButton.setText("Modul Beenden");
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                //myLog("testuser", "]");
                recap = false;
                try {
                    readSharedPrefs();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Intent myIntent = new Intent(getBaseContext(), AppStart.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button statsButton = (Button) findViewById(R.id.stats);
        statsButton.setText(R.string.watch_stats);
        statsButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, statsButton));
                Intent myIntent = new Intent(getBaseContext(), XYPlotActivity.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button kutStatsButton = findViewById(R.id.kutstats);
        kutStatsButton.setText(R.string.kut_stats);
        kutStatsButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, kutStatsButton));
                Intent myIntent = new Intent(getBaseContext(), KUT_K_Stats.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button digcompStatsButton = findViewById(R.id.digcompstats);
        digcompStatsButton.setText(R.string.digcomp_stats);
        digcompStatsButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, digcompStatsButton));
                Intent myIntent = new Intent(getBaseContext(), DigComp_Stats.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });


        final Button recap_video1 = (Button) findViewById(R.id.recap_text1);
        recap_video1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DefinitionPW.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video2 = (Button) findViewById(R.id.recap_video1);
        recap_video2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), SafePWInfo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video3 = (Button) findViewById(R.id.recap_video2);
        recap_video3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), MechanicsInfo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video4 = (Button) findViewById(R.id.recap_video3);
        recap_video4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DataDefinition.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video5 = (Button) findViewById(R.id.recap_text2);
        recap_video5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DataMC.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video6 = (Button) findViewById(R.id.recap_video4);
        recap_video6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), Datasecurity.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });




        final Button recap_activity1 = (Button) findViewById(R.id.recap_activity1);
        recap_activity1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), PrePassword.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity2 = (Button) findViewById(R.id.recap_activity2);
        recap_activity2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), CreatePW.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity3 = (Button) findViewById(R.id.recap_activity3);
        recap_activity3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), SafePWCreationStratagy.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity4 = (Button) findViewById(R.id.recap_activity4);
        recap_activity4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), IsPWSafe.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity5 = (Button) findViewById(R.id.recap_activity5);
        recap_activity5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), Saftymechanics.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity6 = (Button) findViewById(R.id.recap_activity6);
        recap_activity6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), IsMechanicsSafe.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity7 = (Button) findViewById(R.id.recap_activity7);
        recap_activity7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity7));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), PWmcInfo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        //????????????????????
        final Button recap_activity8 = (Button) findViewById(R.id.recap_activity8);
        recap_activity8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity8));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DifferenceDD.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity9 = (Button) findViewById(R.id.recap_activity9);
        recap_activity9.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity9));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DatasecuritySzenarioInfo.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });
    }
}
