package de.aachen.rwth.lufgi9.noichl.basics.personalisierung;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class TextSize extends BaseActivity implements View.OnClickListener {

    int minimalTextSize = 18;
    int maximalTextSize = 24; //war ursprünglich 40

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basics_personalization_textsize);

        init();
    }

    @Override
    public void onRestart() {
        init();
        super.onRestart();
    }

    private void init() {
        //initToolbar(getResources().getString(R.string.textSizeTitle));
        setTextInTextView(getResources().getString(R.string.instructionTextSize));
        initButton(getResources().getString(R.string.pContinue), 1);
        initButton(getResources().getString(R.string.shrink), 2);
        initButton(getResources().getString(R.string.enlarge), 3);
        loadTextSizes();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button2:      //shrink TextSize
                if(values.getMinTextSize() > minimalTextSize && values.getMinTextSize() <= maximalTextSize)
                    values.setMinTextSize(values.getMinTextSize() - 1);
                Context mContext = MainActivity.mycontext;
                SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("TextSize", values.getMinTextSize());
                editor.commit();
                break;

            case R.id.button3:      //enlarge TextSize
                if(values.getMinTextSize() >= minimalTextSize && values.getMinTextSize() < maximalTextSize)
                    values.setMinTextSize(values.getMinTextSize() + 1);
                sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
                editor = sharedpreferences.edit();
                editor.putInt("TextSize", values.getMinTextSize());
                editor.commit();
                break;

            default:
                break;
        }
        loadTextSizes();
    }

    private void loadTextSizes() {
        // Update Button- and TextView-TextSize
        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("TextSize", values.getMinTextSize());
        editor.commit();
        TextView tw = (TextView) findViewById(R.id.textView);
        tw.setTextSize(values.getMinTextSize());
        final Button b1 = (Button) findViewById(R.id.button1);
        final Button b2 = (Button) findViewById(R.id.button2);
        final Button b3 = (Button) findViewById(R.id.button3);
        b1.setTextSize(values.getTextSizeButton());
        b2.setTextSize(values.getTextSizeButton());
        b3.setTextSize(values.getTextSizeButton());
    }
}