package de.aachen.rwth.lufgi9.noichl.modul1;

import androidx.core.content.res.ResourcesCompat;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;

import java.util.ArrayList;

public class MessageComponentMatch extends MyActivity implements View.OnDragListener, View.OnTouchListener {

    private static boolean solution = false;
    private static int counter = 0;
    Context context;
    private LinearLayout mylinearLayout;
    View linearLayout;
    private TextView valueTV;
    private ArrayList<String> answers;
    private int random;
    String comway;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_component_match);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("MessageComponentMatch"));
        }
        editor.putInt("current", getActivityNumber("MessageComponentMatch"));
        editor.apply();
        saveSharedPrefs();

        String comways[] = {getString(R.string.call), getString(R.string.sms), getString(R.string.mail), getString(R.string.im), getString(R.string.chat), getString(R.string.videocall)};

        random = (int) (Math.random() * 6);

        comway = comways[random];

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(getString(R.string.message_component_task, comway));

        answers = new ArrayList<>();
        answers.add(getString(R.string.module1_message_component_match_adress1));
        answers.add(getString(R.string.module1_message_component_match_adress2));
        answers.add(getString(R.string.module1_message_component_match_adress3));
        answers.add(getString(R.string.module1_message_component_match_message1));
        answers.add(getString(R.string.module1_message_component_match_message2));

        linearLayout = findViewById(R.id.flex_layout);

        for (int i = 0; i < answers.size(); i++) {
            mylinearLayout = new LinearLayout(this);
            mylinearLayout.setLayoutParams((new FlexboxLayout.LayoutParams(FlexboxLayout.LayoutParams.WRAP_CONTENT, FlexboxLayout.LayoutParams.WRAP_CONTENT)));
            mylinearLayout.setPadding(8, 8, 8, 8);
            valueTV = new TextView(this);
            valueTV.setText(answers.get(i));
            valueTV.setTextColor(Color.BLACK);
            valueTV.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sortbox, null));
            valueTV.setId(i);
            mylinearLayout.setTag("l" + valueTV.getId());
            valueTV.setTag(i + "");
            valueTV.setPadding(8, 8, 8, 8);
            valueTV.setLayoutParams(new FlexboxLayout.LayoutParams(FlexboxLayout.LayoutParams.WRAP_CONTENT, FlexboxLayout.LayoutParams.WRAP_CONTENT));

            mylinearLayout.addView(valueTV);

            valueTV.setOnTouchListener(this);

            ((FlexboxLayout) linearLayout).addView(mylinearLayout);

            implementEvents();


            final Button nextButton = findViewById(R.id.next);
            nextButton.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (!solution) {
                            ViewGroup vg1 = findViewById(R.id.top_layout);
                            ViewGroup vg2 = findViewById(R.id.bottom_layout);
                            if ((vg1.getChildCount() == 1) && (vg2.getChildCount() == 1)) {
                                solution();
                                solution = true;
                            } else {
                                showDialog(getString(R.string.not_yet_ready), getString(R.string.module1_message_component_match_dialog), context);
                            }
                        } else {
                            solution = false;
                            Intent myIntent;
                            if (counter < 3) {
                                myIntent = new Intent(context, MessageComponentMatch.class);
                                startActivity(myIntent);
                                finish();
                                counter++;
                            } else {
                                solution = false;
                                counter = 0;
                                if (recap) {
                                    myIntent = new Intent(context, CommunicationEnd.class);
                                    startActivity(myIntent);
                                    finish();
                                } else {
                                    if(myrecap){
                                        restartAfterRecap();
                                    }
                                    myIntent = nextActivity(getActivityNumber("MessageComponentMatch") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                            }


                        }

                    }
                    return false;
                }
            });
        }
    }

    private void implementEvents() {


        findViewById(R.id.top_layout).setOnDragListener(this);
        findViewById(R.id.bottom_layout).setOnDragListener(this);
        findViewById(R.id.flex_layout).setOnDragListener(this);
    }


    @Override
    public boolean onDrag(View view, DragEvent event) {

        View v = (View) event.getLocalState();
        myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), dragEventTextExtension(event, view, ((TextView) v).getText().toString(), comway));

        int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:

                if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {

                    return true;
                }

                return false;

            case DragEvent.ACTION_DRAG_ENTERED:

                view.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                view.invalidate();

                return true;
            case DragEvent.ACTION_DRAG_LOCATION:

                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                view.getBackground().clearColorFilter();
                view.invalidate();

                return true;

            case DragEvent.ACTION_DROP:
                view.getBackground().clearColorFilter();
                view.invalidate();

                ViewGroup myview = (ViewGroup) view;
                if (myview.getChildCount() > 0 && !(myview.equals(linearLayout))) {
                } else {

                    ViewGroup owner = (ViewGroup) v.getParent();
                    owner.removeView(v);//remove the dragged view
                    ViewGroup root = (ViewGroup) owner.getParent();
                    if (view instanceof FlexboxLayout) {
                        FlexboxLayout container = (FlexboxLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                        //v.setLayoutParams(param);
                        LinearLayout rm = owner.findViewWithTag("l" + v.getId());
                        root.removeView(rm);
                        LinearLayout l = new LinearLayout(this);
                        l.setPadding(8, 8, 8, 8);
                        l.setTag("l" + v.getId());
                        l.setLayoutParams((new FlexboxLayout.LayoutParams(FlexboxLayout.LayoutParams.WRAP_CONTENT, FlexboxLayout.LayoutParams.WRAP_CONTENT)));
                        l.addView(v);
                        container.addView(l);//Add the dragged view
                    } else {


                        LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                        //v.setLayoutParams(param);
                        LinearLayout rm = owner.findViewWithTag("l" + v.getId());
                        root.removeView(rm);
                        LinearLayout l = new LinearLayout(this);
                        l.setPadding(8, 8, 8, 8);
                        l.setTag("l" + v.getId());
                        l.setLayoutParams((new FlexboxLayout.LayoutParams(FlexboxLayout.LayoutParams.WRAP_CONTENT, FlexboxLayout.LayoutParams.WRAP_CONTENT)));
                        l.addView(v);
                        container.addView(l);//Add the dragged view

                    }
                }


                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                view.getBackground().clearColorFilter();
                view.invalidate();
                view = (View) event.getLocalState();
                view.setVisibility(View.VISIBLE);
                return true;
        }
        return false;
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), viewEventExtension(motionEvent, view));

        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());


        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

        view.startDrag(data, shadowBuilder, view, 0);

        view.setVisibility(View.INVISIBLE);

        return true;
    }

    private void solution() {

        for (int i = 0; i < answers.size(); i++) {

            ViewGroup parent1 = findViewById(R.id.top_layout);
            ViewGroup parent2 = findViewById(R.id.bottom_layout);
            ViewGroup parent3 = findViewById(R.id.flex_layout);

            parent1.setTag(getString(R.string.receiver));
            parent2.setTag(getString(R.string.message));
            parent3.setTag("null");
            ViewGroup myparent = parent3;
            boolean mytruefalse = true;

            TextView myview0 = findViewById(i);
            myview0.setEnabled(false);

            if ((random == 0 || random == 1 || random == 3 || random == 5) && i == 0) {
                if (myview0.getParent().getParent() == parent1) {
                    myview0.setBackgroundResource(R.drawable.answer_true);
                    myparent = parent1;
                }
            } else {

                if ((random == 2) && i == 1) {
                    if (myview0.getParent().getParent() == parent1) {
                        myview0.setBackgroundResource(R.drawable.answer_true);
                        myparent = parent1;
                    }
                } else {


                    if ((random == 4) && i == 2) {
                        if (myview0.getParent().getParent() == parent1) {
                            myview0.setBackgroundResource(R.drawable.answer_true);
                            myparent = parent1;
                        }
                    } else {


                        if ((random == 0 || random == 1 || random == 3 || random == 4 || random == 5) && i == 4) {
                            if (myview0.getParent().getParent() == parent2) {
                                myview0.setBackgroundResource(R.drawable.answer_true);
                                myparent = parent2;
                            }
                        } else {


                            if ((random == 2) && i == 3) {
                                if (myview0.getParent().getParent() == parent2) {
                                    myview0.setBackgroundResource(R.drawable.answer_true);
                                    myparent = parent1;
                                }
                            } else {
                                if (myview0.getParent().getParent() != parent3) {
                                    myview0.setBackgroundResource(R.drawable.answer_false);
                                    mytruefalse = false;
                                }
                            }
                        }
                    }
                }
            }
            ViewGroup vg = (ViewGroup) myview0.getParent().getParent();
            myLog("testuser", this.getClass() + "/answered", "answered", this.getClass().toString(), answerExtension(myview0.getText().toString(), mytruefalse, vg.getTag().toString(), myparent.getTag().toString()));

        }
    }


}
