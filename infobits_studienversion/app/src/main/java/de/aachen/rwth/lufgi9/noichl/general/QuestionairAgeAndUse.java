package de.aachen.rwth.lufgi9.noichl.general;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;

public class QuestionairAgeAndUse extends MyActivity {

    public static boolean c = false;
    public static boolean s = false;
    public static boolean t = false;
    public static int counter = 0;
    Context context;
    public static int index = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;

        setContentView(R.layout.questionair_age_and_use);



        Button nextButton = (Button) findViewById(R.id.next);

        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {


                if(event.getAction() == MotionEvent.ACTION_UP) {
                    TextView tv = findViewById(R.id.age);
                    RadioGroup radioGroup = findViewById(R.id.agegroup);
                    int index = radioGroup.indexOfChild(findViewById(radioGroup.getCheckedRadioButtonId()));
                    String description = (String) tv.getText();

                    CheckBox computer = findViewById(R.id.computer);
                    CheckBox smartphone = findViewById(R.id.smartphone);
                    CheckBox tablet = findViewById(R.id.tablet);

                    myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("age", description, index + ""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension("computer", computer.getText().toString(), computer.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension("smartphone", smartphone.getText().toString(), smartphone.isChecked()+""));
                    myLog("testuser",this.getClass()+"/selected", "selected", this.getClass().toString(), idQAExtension("tablet", tablet.getText().toString(), tablet.isChecked()+""));

                    if(computer.isChecked()){
                        c = true;
                    }

                    if(smartphone.isChecked()){
                        s = true;
                    }

                    if(tablet.isChecked()){
                        t = true;
                    }

                    if(index != -1) {
                        Intent myIntent = new Intent(context, DeviceUsage.class);
                        startActivity(myIntent);
                        finish();
                    }else{
                        showDialog("Noch nicht fertig","Bitte wähle durch antippen dein Alter aus bevor du weiter machst.", context);
                    }
                }
                return false;
            }
        });
    }
}


