package de.aachen.rwth.lufgi9.noichl.modul2.utilities;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * An animation for resize a view.
 * Modified by https://stackoverflow.com/questions/8063466/how-to-expand-a-layout-height-with-animation/33095268
 * Access date: 2019-06-30 19-11
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class ResizeAnimation extends Animation {
    /**
     * The height where animation starts
     */
    int startHeight;
    /**
     * The width where animation starts
     */
    int startWidth;
    /**
     * The height where the animation stops
     */
    int targetHeight;
    /**
     * The width where the animation stops
     */
    int targetWidth;
    /**
     * The view to change size with an animation
     */
    final View view;

    /**
     * The origin height to go back
     */
    int backupHeight;
    /**
     * The origin width to go back
     */
    int backupWidth;

    /**
     * Normal constructor
     *
     * @param view The view to change size with an animation
     */
    public ResizeAnimation(View view) {
        this.view = view;
    }

    /**
     * Store the current size attributes of the {@link #view}.
     */
    public void backup() {
        backupHeight = view.getHeight();
        backupWidth = view.getWidth();
    }

    /**
     * Use this method to change the size from origin to new size.
     * It calls the {@link #backup()} method for you.
     *
     * @param targetWidth  The width where the animation stops
     * @param targetHeight The height where the animation stops
     */
    public void to(int targetWidth, int targetHeight) {
        // backup first
        backup();

        // setup the animation, second
        setup(targetWidth, targetHeight);
    }

    /**
     * This method updates the variables for the animation.
     *
     * @param targetWidth  The width where the animation stops
     * @param targetHeight The height where the animation stops
     */
    private void setup(int targetWidth, int targetHeight) {
        startHeight = view.getHeight();
        this.targetHeight = targetHeight;
        startWidth = view.getWidth();
        this.targetWidth = targetWidth;
    }

    /**
     * Use this method to change the width from origin to new width
     *
     * @param targetWidth The width where the animation stops
     * @see #to(int, int)
     */
    public void toWidth(int targetWidth) {
        to(targetWidth, view.getHeight());
    }

    /**
     * Use this method to change the height from origin to new height
     *
     * @param targetHeight The height where the animation stops
     * @see #to(int, int)
     */
    public void toHeight(int targetHeight) {
        to(view.getWidth(), targetHeight);
    }

    /**
     * Use this method to change the size back to origin
     */
    public void back() {
        to(backupWidth, backupHeight);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        /*
         * Calculate the new sizes.
         * consider the following formula: width = start + (target - start) * interpolatedTime
         */
        int width = (int) (startWidth + (targetWidth - startWidth) * interpolatedTime);
        int height = (int) (startHeight + (targetHeight - startHeight) * interpolatedTime);

        // update the layout params
        view.getLayoutParams().width = width;
        view.getLayoutParams().height = height;

        // request the layout to apply the changes on UI
        view.requestLayout();
    }

}