package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;


import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class CreatePWQA extends MyActivity {


    Context context;
String scenario="";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("CreatePWQA"));
        }
        editor.putInt("current", getActivityNumber("CreatePWQA"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        TextView tv;
        CheckBox cb;

                stub.setLayoutResource(R.layout.viewstub_textview_4_checkbox);
                stub.inflate();
                tv = findViewById(R.id.tv_ex);
                scenario = getString(R.string.module4_createpwqa_question);
                tv.setText(scenario);
                cb = findViewById(R.id.alt1);
                cb.setText(R.string.module4_createpwqa_item1);
                cb = findViewById(R.id.alt2);
                cb.setText(R.string.module4_createpwqa_item2);
                cb = findViewById(R.id.alt3);
                cb.setText(R.string.module4_createpwqa_item3);
                cb = findViewById(R.id.alt4);
                cb.setText(R.string.module4_createpwqa_item4);




        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if (event.getAction() == MotionEvent.ACTION_UP) {


                    CheckBox alt1 = findViewById(R.id.alt1);
                    CheckBox alt2 = findViewById(R.id.alt2);
                    CheckBox alt3 = findViewById(R.id.alt3);
                    CheckBox alt4 = findViewById(R.id.alt4);
                    if (alt1.isChecked() || alt2.isChecked() || alt3.isChecked() || alt4.isChecked()) {
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt1.getText().toString(), alt1.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt2.getText().toString(), alt2.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt3.getText().toString(), alt3.isChecked() + ""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension(scenario, alt4.getText().toString(), alt4.isChecked() + ""));

                        if (recap) {
                            Intent myIntent = new Intent(context, DataSecEnd.class);
                            startActivity(myIntent);
                            finish();
                        } else {
                            if(myrecap){
                                restartAfterRecap();
                            }else {
                                Intent myIntent = nextActivity(getActivityNumber("CreatePWQA") + 1);
                                startActivity(myIntent);
                                finish();
                            }
                        }
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module4_createpwqa_dialog), context);
                    }
                }

                    return false;

        }});

    }
}
