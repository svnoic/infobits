package de.aachen.rwth.lufgi9.noichl.general;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.os.Environment;
import android.os.Handler;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Objects;

import de.aachen.rwth.lufgi9.noichl.R;

import de.aachen.rwth.lufgi9.noichl.basics.eingaben.TelInput;
import de.aachen.rwth.lufgi9.noichl.basics.gesten.Tap;
import de.aachen.rwth.lufgi9.noichl.basics.icons.IconInfo;
import de.aachen.rwth.lufgi9.noichl.basics.personalisierung.TextSize;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationEnd;
import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationStart;

import de.aachen.rwth.lufgi9.noichl.modul1.CommunicationUsage;
import de.aachen.rwth.lufgi9.noichl.modul1.CreativeCommons;
import de.aachen.rwth.lufgi9.noichl.modul1.CreativeCommonsIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.DifferenceTalkLetter;
import de.aachen.rwth.lufgi9.noichl.modul1.FillTheGapActivity;
import de.aachen.rwth.lufgi9.noichl.modul1.ImageUseIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.LinkTalkLetter;
import de.aachen.rwth.lufgi9.noichl.modul1.MessageComponentMatch;
import de.aachen.rwth.lufgi9.noichl.modul1.OER;
import de.aachen.rwth.lufgi9.noichl.modul1.OERIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.PolicyTrueFalse;
import de.aachen.rwth.lufgi9.noichl.modul1.SortPolicy;
import de.aachen.rwth.lufgi9.noichl.modul1.SyncVsAsync;
import de.aachen.rwth.lufgi9.noichl.modul1.TalkLetterIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.Urheberrecht;
import de.aachen.rwth.lufgi9.noichl.modul1.UrheberrechtIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.UsecaseActivity;
import de.aachen.rwth.lufgi9.noichl.modul1.UsecaseActivity2;
import de.aachen.rwth.lufgi9.noichl.modul1.VideoAdressIntro;
import de.aachen.rwth.lufgi9.noichl.modul1.VideoAdressesActivity;
import de.aachen.rwth.lufgi9.noichl.modul1.VideoTalkAndLetter;
import de.aachen.rwth.lufgi9.noichl.modul2.Binaersystem;
import de.aachen.rwth.lufgi9.noichl.modul2.ConnectionpieceInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.DNSIntro;
import de.aachen.rwth.lufgi9.noichl.modul2.DNSVideo;
import de.aachen.rwth.lufgi9.noichl.modul2.DPVideo;
import de.aachen.rwth.lufgi9.noichl.modul2.DataInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.Datapackage;
import de.aachen.rwth.lufgi9.noichl.modul2.Devices;
import de.aachen.rwth.lufgi9.noichl.modul2.DevicesInInternetInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.DezToBin;
import de.aachen.rwth.lufgi9.noichl.modul2.DezToBin2;
import de.aachen.rwth.lufgi9.noichl.modul2.HistoryIntro;
import de.aachen.rwth.lufgi9.noichl.modul2.IPIntro;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetEnd;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetQuizInfo;
import de.aachen.rwth.lufgi9.noichl.modul2.InternetStart;
import de.aachen.rwth.lufgi9.noichl.modul2.IpUrlDns;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkCompareHubSwitchRouterActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkConnectedActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkConnectedActivity5;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkInfo5;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkRouter;
import de.aachen.rwth.lufgi9.noichl.modul2.NetworkSwitchConnectedActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.PosttestInformation;
import de.aachen.rwth.lufgi9.noichl.modul2.PretestInformation;
import de.aachen.rwth.lufgi9.noichl.modul2.URL1;
import de.aachen.rwth.lufgi9.noichl.modul2.URL2;
import de.aachen.rwth.lufgi9.noichl.modul2.URLIntro;
import de.aachen.rwth.lufgi9.noichl.modul2.URLVideo;
import de.aachen.rwth.lufgi9.noichl.modul3.Cable;
import de.aachen.rwth.lufgi9.noichl.modul3.CableToNoCable;
import de.aachen.rwth.lufgi9.noichl.modul3.CableVideo;
import de.aachen.rwth.lufgi9.noichl.modul3.CableVideoIntro;
import de.aachen.rwth.lufgi9.noichl.modul3.Connected;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectedSort;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionEnd;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionIntro;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionStart;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionVideo;
import de.aachen.rwth.lufgi9.noichl.modul3.ConnectionVideoIntro;
import de.aachen.rwth.lufgi9.noichl.modul3.DifferenceWLANMobil;
import de.aachen.rwth.lufgi9.noichl.modul3.GameBookIntro;
import de.aachen.rwth.lufgi9.noichl.modul3.InternetWords;
import de.aachen.rwth.lufgi9.noichl.modul3.InternetWordsSolution;
import de.aachen.rwth.lufgi9.noichl.modul3.LanManWan;
import de.aachen.rwth.lufgi9.noichl.modul3.Mobilfunk;
import de.aachen.rwth.lufgi9.noichl.modul3.PreLanManWan;
import de.aachen.rwth.lufgi9.noichl.modul3.WLANQuizInfo;
import de.aachen.rwth.lufgi9.noichl.modul3.WlanWifi;
import de.aachen.rwth.lufgi9.noichl.modul4.CreatePW;
import de.aachen.rwth.lufgi9.noichl.modul4.CreatePWQA;
import de.aachen.rwth.lufgi9.noichl.modul4.CreateSafePW;
import de.aachen.rwth.lufgi9.noichl.modul4.DataDefinition;
import de.aachen.rwth.lufgi9.noichl.modul4.DataMC;
import de.aachen.rwth.lufgi9.noichl.modul4.DataSecEnd;
import de.aachen.rwth.lufgi9.noichl.modul4.DataUsage;
import de.aachen.rwth.lufgi9.noichl.modul4.Datasecurity;
import de.aachen.rwth.lufgi9.noichl.modul4.DatasecurityStart;
import de.aachen.rwth.lufgi9.noichl.modul4.DatasecuritySzenarioInfo;
import de.aachen.rwth.lufgi9.noichl.modul4.DefinitionPW;
import de.aachen.rwth.lufgi9.noichl.modul4.DifferenceDD;
import de.aachen.rwth.lufgi9.noichl.modul4.IsMechanicsSafe;
import de.aachen.rwth.lufgi9.noichl.modul4.IsPWSafe;
import de.aachen.rwth.lufgi9.noichl.modul4.MechanicsInfo;
import de.aachen.rwth.lufgi9.noichl.modul4.PWmcInfo;
import de.aachen.rwth.lufgi9.noichl.modul4.PrePassword;
import de.aachen.rwth.lufgi9.noichl.modul4.SafeMechanics;
import de.aachen.rwth.lufgi9.noichl.modul4.SafePWCreationStratagy;
import de.aachen.rwth.lufgi9.noichl.modul4.SafePWInfo;
import de.aachen.rwth.lufgi9.noichl.modul4.SafePWRequirements;
import de.aachen.rwth.lufgi9.noichl.modul4.Saftymechanics;
import de.aachen.rwth.lufgi9.noichl.modul4.SecurityIntro;

public class AppStart extends MyActivity {


    Context context;
    int currentActivityM1;
    int currentActivityM2;
    int currentActivityM3;
    int currentActivityM4;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        saveSharedPrefs();

        setContentView(R.layout.activity_module_overview);

        try {
            readSharedPrefs();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        context = this;
        boolean mpersonalisation = true;
        boolean mgesten = true;
        boolean micons = true;
        boolean meingaben = true;
        boolean mapps = true;
        boolean mmodul1 = true;
        boolean mmodul2 = true;
        boolean mmodul3 = true;
        boolean mmodul4 = true;

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);

        currentActivityM1 = sharedpreferences.getInt("currentActivityM1", 100);
        int h1 = currentActivityM1-100;
        float g = (float) 25.0;
        float h2 = h1/g;
        int progress1 = Math.round(h2*100);
        ProgressBar progressm1 = findViewById(R.id.progressm1);
        progressm1.setProgress(progress1);

        currentActivityM2 = sharedpreferences.getInt("currentActivityM2", 200);
         h1 = currentActivityM2-200;
         g = (float) 30.0;
         h2 = h1/g;
        int progress2 = Math.round(h2*100);
        ProgressBar progressm2 = findViewById(R.id.progressm2);
        progressm2.setProgress(progress2);

        currentActivityM3 = sharedpreferences.getInt("currentActivityM3", 300);
         h1 = currentActivityM3-300;
         g = (float) 22.0;
         h2 = h1/g;
        int progress3 = Math.round(h2*100);
        ProgressBar progressm3 = findViewById(R.id.progressm3);
        progressm3.setProgress(progress3);

        currentActivityM4 = sharedpreferences.getInt("currentActivityM4", 400);
         h1 = currentActivityM4-400;
         g = (float) 25.0;
         h2 = h1/g;
        int progress4 = Math.round(h2*100);
        ProgressBar progressm4 = findViewById(R.id.progressm4);
        progressm4.setProgress(progress4);

        FileInputStream stream;


        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            try {
                File dirApp = new File(Environment.getExternalStorageDirectory() + "/InfoBiTS/modules.json");
                stream = new FileInputStream(dirApp);
                String jsonStr = null;
                try {
                    FileChannel fc = stream.getChannel();
                    MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                    jsonStr = Charset.defaultCharset().decode(bb).toString();
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                finally {
                    stream.close();
                }



                JSONArray myJson = null;
                try {
                    myJson = new JSONArray(jsonStr);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (myJson != null) {
                    //for (int i = 0; i < myJson.length(); ++i) {
                    JSONObject rec = null;
                    try {
                        rec = myJson.getJSONObject(0);
                        mpersonalisation = rec.getBoolean("personalisation");
                        mgesten = rec.getBoolean("gesten");
                        micons = rec.getBoolean("icons");
                        meingaben = rec.getBoolean("eingaben");
                        mapps = rec.getBoolean("apps");
                        mmodul1 = rec.getBoolean("modul1");
                        mmodul2 = rec.getBoolean("modul2");
                        mmodul3 = rec.getBoolean("modul3");
                        mmodul4 = rec.getBoolean("modul4");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final Button personalization = (Button) findViewById(R.id.personalization);
        if(!mpersonalisation) {
            personalization.setBackgroundColor(getResources().getColor(R.color.grey));
            personalization.setTextColor(getResources().getColor(R.color.black_overlay));
        }else {
            personalization.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, personalization));
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        Intent myIntent = new Intent(context, TextSize.class);
                        startActivity(myIntent);
                        finish();
                    }
                    return true;
                }
            });
        }

        final Button gestures = (Button) findViewById(R.id.gestures);
        if(!mgesten) {
            gestures.setBackgroundColor(getResources().getColor(R.color.grey));
            gestures.setTextColor(getResources().getColor(R.color.black_overlay));
        }else {
            gestures.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, gestures));
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        Intent myIntent = new Intent(context, Tap.class);
                        startActivity(myIntent);
                        finish();
                    }
                    return true;
                }
            });
        }

        final Button icons = (Button) findViewById(R.id.icons);
        if(!micons) {
            icons.setBackgroundColor(getResources().getColor(R.color.grey));
            icons.setTextColor(getResources().getColor(R.color.black_overlay));
        }else {
            icons.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, icons));
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        Intent myIntent = new Intent(context, IconInfo.class);
                        startActivity(myIntent);
                        finish();
                    }
                    return true;
                }
            });
        }

        final Button inputs = (Button) findViewById(R.id.inputs);
        if(!meingaben) {
            inputs.setBackgroundColor(getResources().getColor(R.color.grey));
            inputs.setTextColor(getResources().getColor(R.color.black_overlay));
        }else {
            inputs.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, inputs));
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        Intent myIntent = new Intent(context, TelInput.class);
                        startActivity(myIntent);
                        finish();
                    }
                    return true;
                }
            });
        }

        final Button apps = (Button) findViewById(R.id.apps);
        if(!mapps) {
            apps.setBackgroundColor(getResources().getColor(R.color.grey));
            apps.setTextColor(getResources().getColor(R.color.black_overlay));
        }else {
            apps.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, apps));
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        try
                        {
                        Intent intent = getPackageManager().getLaunchIntentForPackage("de.aachen.rwth.lufgi9.noichl.app");
                        startActivity(intent);
                        finish();
                        }

                        catch (ActivityNotFoundException e)
                        {
                        }
                    }
                    return true;
                }
            });
        }

        final ImageView module1 = (ImageView) findViewById(R.id.module1);
        if(!mmodul1) {
            module1.setBackgroundColor(getResources().getColor(R.color.grey));
            module1.setAlpha(50);
        }else {
            module1.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, module1));
                    if (event.getAction() == MotionEvent.ACTION_UP) {

                        SUS.currentModule = 1;

                        if (currentActivityM1 <= 101) {
                            //Intent myIntent = new Intent(context, CommunicationStart.class);
                            Intent myIntent = nextActivity(101);
                            startActivity(myIntent);
                            finish();
                        } else{
                            showDialog1(getString(R.string.app_start_dialog_title), getString(R.string.app_start_dialog_message));

                        }

                    }
                    return true;
                }
            });
        }

        final ImageView module2 = (ImageView) findViewById(R.id.module2);
        if(!mmodul2) {
            module2.setBackgroundColor(getResources().getColor(R.color.grey));
            module2.setAlpha(50);
        }else {
            module2.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, module2));
                    if (event.getAction() == MotionEvent.ACTION_UP) {


                        SUS.currentModule = 2;


                        if (currentActivityM2 <= 201) {
                            Intent myIntent = new Intent(context, InternetStart.class);
                            startActivity(myIntent);
                            finish();
                        } else{
                            showDialog2(getString(R.string.app_start_dialog_title), getString(R.string.app_start_dialog_message));

                    }

                    }
                    return true;
                }
            });
        }

        final ImageView module3 = (ImageView) findViewById(R.id.module3);
        if(!mmodul3) {
            module3.setBackgroundColor(getResources().getColor(R.color.grey));
            module3.setAlpha(50);
        }else {
            module3.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, module3));
                    if (event.getAction() == MotionEvent.ACTION_UP) {

                        SUS.currentModule = 3;

                        if (currentActivityM3 <= 301) {
                            Intent myIntent = new Intent(context, ConnectionStart.class);
                            startActivity(myIntent);
                            finish();
                        } else{
                            showDialog3(getString(R.string.app_start_dialog_title), getString(R.string.app_start_dialog_message));

                        }
                    }
                    return true;
                }
            });
        }

        final ImageView module4 = (ImageView) findViewById(R.id.module4);
        if(!mmodul4) {
            module4.setBackgroundColor(getResources().getColor(R.color.grey));
            module4.setAlpha(50);
        }else {
            module4.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, module4));
                    if (event.getAction() == MotionEvent.ACTION_UP) {

                        SUS.currentModule = 4;

                        if (currentActivityM4 <= 401) {
                            Intent myIntent = new Intent(context, DatasecurityStart.class);
                            startActivity(myIntent);
                            finish();
                        } else{
                            showDialog4(getString(R.string.app_start_dialog_title), getString(R.string.app_start_dialog_message));

                        }
                    }
                    return true;
                }
            });
        }



    }

    private void showDialog1(String title, String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        myLog("testuser",this.getClass()+"/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));

        // set title
        TextView mytitle = new TextView(context);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("Neustarten",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent myIntent;
                        myIntent = new Intent(context, CommunicationStart.class);
                        myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setPositiveButton("Fortsetzen",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent myIntent;
                        switch (currentActivityM1){
                            case 101: myIntent = new Intent(context, CommunicationStart.class); break;
                            case 102: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul1.PretestInformation.class); break;
                            case 103: myIntent = new Intent(context, TalkLetterIntro.class); break;
                            case 104: myIntent = new Intent(context, VideoTalkAndLetter.class); break;
                            case 105: myIntent = new Intent(context, DifferenceTalkLetter.class); break;
                            case 106: myIntent = new Intent(context, LinkTalkLetter.class); break;
                            case 107: myIntent = new Intent(context, CommunicationUsage.class); break;
                            case 108: myIntent = new Intent(context, SyncVsAsync.class); break;
                            case 109: myIntent = new Intent(context, UsecaseActivity.class); break;
                            case 110: myIntent = new Intent(context, FillTheGapActivity.class); break;
                            case 111: myIntent = new Intent(context, VideoAdressIntro.class); break;
                            case 112: myIntent = new Intent(context, VideoAdressesActivity.class); break;
                            case 113: myIntent = new Intent(context, MessageComponentMatch.class); break;
                            case 114: myIntent = new Intent(context, ImageUseIntro.class); break;
                            case 115: myIntent = new Intent(context, UrheberrechtIntro.class); break;
                            case 116: myIntent = new Intent(context, Urheberrecht.class); break;
                            case 117: myIntent = new Intent(context, CreativeCommonsIntro.class); break;
                            case 118: myIntent = new Intent(context, CreativeCommons.class); break;
                            case 119: myIntent = new Intent(context, OERIntro.class); break;
                            case 120: myIntent = new Intent(context, OER.class); break;
                            case 121: myIntent = new Intent(context, SortPolicy.class); break;
                            case 122: myIntent = new Intent(context, PolicyTrueFalse.class); break;
                            case 123: myIntent = new Intent(context, UsecaseActivity2.class); break;
                            case 124: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul1.PosttestInformation.class); break;
                            case 125: myIntent = new Intent(context, CommunicationEnd.class); break;
                            default: myIntent = new Intent(context, CommunicationStart.class);
                        }
                        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        startActivity(myIntent);
                        finish();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

        TextView textView = alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }

    private void showDialog2(String title, String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        myLog("testuser",this.getClass()+"/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));

        // set title
        TextView mytitle = new TextView(context);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("Neustarten",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent myIntent;
                        myIntent = new Intent(context, InternetStart.class);
                        myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setPositiveButton("Fortsetzen",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent myIntent;
                        switch (currentActivityM2){
                            case 201: myIntent = new Intent(context, InternetStart.class); break;
                            case 202: myIntent = new Intent(context, PretestInformation.class); break;
                            case 203: myIntent = new Intent(context, InternetInfo.class); break;
                            case 204: myIntent = new Intent(context, DataInfo.class); break;
                            case 205: myIntent = new Intent(context, Binaersystem.class); break;
                            case 206: myIntent = new Intent(context, DezToBin.class); break;
                            case 207: myIntent = new Intent(context, DezToBin2.class); break;
                            case 208: myIntent = new Intent(context, Datapackage.class); break;
                            case 209: myIntent = new Intent(context, DPVideo.class); break;
                            case 210: myIntent = new Intent(context, IPIntro.class); break;
                            case 211: myIntent = new Intent(context, Devices.class); break;
                            case 212: myIntent = new Intent(context, NetworkConnectedActivity.class); break;
                            case 213: myIntent = new Intent(context, NetworkInfo5.class); break;
                            case 214: myIntent = new Intent(context, NetworkConnectedActivity5.class); break;
                            case 215: myIntent = new Intent(context, ConnectionpieceInfo.class); break;
                            case 216: myIntent = new Intent(context, NetworkSwitchConnectedActivity.class); break;
                            case 217: myIntent = new Intent(context, NetworkCompareHubSwitchRouterActivity.class); break;
                            case 218: myIntent = new Intent(context, NetworkRouter.class); break;
                            case 219: myIntent = new Intent(context, DevicesInInternetInfo.class); break;
                            case 220: myIntent = new Intent(context, URLIntro.class); break;
                            case 221: myIntent = new Intent(context, URLVideo.class); break;
                            case 222: myIntent = new Intent(context, DNSIntro.class); break;
                            case 223: myIntent = new Intent(context, DNSVideo.class); break;
                            case 224: myIntent = new Intent(context, URL1.class); break;
                            case 225: myIntent = new Intent(context, URL2.class); break;
                            case 226: myIntent = new Intent(context, IpUrlDns.class); break;
                            case 227: myIntent = new Intent(context, HistoryIntro.class); break;
                            case 228: myIntent = new Intent(context, InternetQuizInfo.class); break;
                            case 229: myIntent = new Intent(context, PosttestInformation.class); break;
                            case 230: myIntent = new Intent(context, InternetEnd.class); break;
                            default: myIntent = new Intent(context, InternetStart.class);
                        }
                        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        startActivity(myIntent);
                        finish();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

        TextView textView = alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }

    private void showDialog3(String title, String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        myLog("testuser",this.getClass()+"/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));

        // set title
        TextView mytitle = new TextView(context);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("Neustarten",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent myIntent;
                        myIntent = new Intent(context, ConnectionStart.class);
                        myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setPositiveButton("Fortsetzen",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent myIntent;
                        switch (currentActivityM3){
                            case 301: myIntent = new Intent(context, ConnectionStart.class); break;
                            case 302: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul3.PretestInformation.class); break;
                            case 303: myIntent = new Intent(context, ConnectionIntro.class); break;
                            case 304: myIntent = new Intent(context, Connected.class); break;
                            case 305: myIntent = new Intent(context, ConnectedSort.class); break;
                            case 306: myIntent = new Intent(context, Cable.class); break;
                            case 307: myIntent = new Intent(context, CableVideoIntro.class); break;
                            case 308: myIntent = new Intent(context, CableVideo.class); break;
                            case 309: myIntent = new Intent(context, CableToNoCable.class); break;
                            case 310: myIntent = new Intent(context, InternetWords.class); break;
                            case 311: myIntent = new Intent(context, PreLanManWan.class); break;
                            case 312: myIntent = new Intent(context, LanManWan.class); break;
                            case 313: myIntent = new Intent(context, WlanWifi.class); break;
                            case 314: myIntent = new Intent(context, Mobilfunk.class); break;
                            case 315: myIntent = new Intent(context, InternetWordsSolution.class); break;
                            case 316: myIntent = new Intent(context, ConnectionVideoIntro.class); break;
                            case 317: myIntent = new Intent(context, ConnectionVideo.class); break;
                            case 318: myIntent = new Intent(context, DifferenceWLANMobil.class); break;
                            case 319: myIntent = new Intent(context, GameBookIntro.class); break;
                            case 320: myIntent = new Intent(context, WLANQuizInfo.class); break;
                            case 321: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul3.PosttestInformation.class); break;
                            case 322: myIntent = new Intent(context, ConnectionEnd.class); break;
                            default: myIntent = new Intent(context, ConnectionStart.class);
                        }
                        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        startActivity(myIntent);
                        finish();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

        TextView textView = alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }

    private void showDialog4(String title, String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        myLog("testuser",this.getClass()+"/showed", "showed", this.getClass().toString(), textExtension("Dialog - " + message));

        // set title
        TextView mytitle = new TextView(context);
        mytitle.setText(title);
        mytitle.setPadding(10, 10, 10, 10);
        mytitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        alertDialogBuilder.setCustomTitle(mytitle);


        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("Neustarten",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent myIntent;
                        myIntent = new Intent(context, DatasecurityStart.class);
                        myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setPositiveButton("Fortsetzen",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent myIntent;
                        switch (currentActivityM4){
                            case 401: myIntent = new Intent(context, DatasecurityStart.class); break;
                            case 402: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul4.PosttestInformation.class); break;
                            case 403: myIntent = new Intent(context, PrePassword.class); break;
                            case 404: myIntent = new Intent(context, DefinitionPW.class); break;
                            case 405: myIntent = new Intent(context, SafePWInfo.class); break;
                            case 406: myIntent = new Intent(context, SafePWRequirements.class); break;
                            case 407: myIntent = new Intent(context, CreatePW.class); break;
                            case 408: myIntent = new Intent(context, CreatePWQA.class); break;
                            case 409: myIntent = new Intent(context, SafePWCreationStratagy.class); break;
                            case 410: myIntent = new Intent(context, CreateSafePW.class); break;
                            case 411: myIntent = new Intent(context, IsPWSafe.class); break;
                            case 412: myIntent = new Intent(context, Saftymechanics.class); break;
                            case 413: myIntent = new Intent(context, SafeMechanics.class); break;
                            case 414: myIntent = new Intent(context, MechanicsInfo.class); break;
                            case 415: myIntent = new Intent(context, IsMechanicsSafe.class); break;
                            case 416: myIntent = new Intent(context, PWmcInfo.class); break;
                            case 417: myIntent = new Intent(context, SecurityIntro.class); break;
                            case 418: myIntent = new Intent(context, DataDefinition.class); break;
                            case 419: myIntent = new Intent(context, DataMC.class); break;
                            case 420: myIntent = new Intent(context, DataUsage.class); break;
                            case 421: myIntent = new Intent(context, Datasecurity.class); break;
                            case 422: myIntent = new Intent(context, DifferenceDD.class); break;
                            case 423: myIntent = new Intent(context, DatasecuritySzenarioInfo.class); break;
                            case 424: myIntent = new Intent(context, de.aachen.rwth.lufgi9.noichl.modul4.PosttestInformation.class); break;
                            case 425: myIntent = new Intent(context, DataSecEnd.class); break;
                            default: myIntent = new Intent(context, DatasecurityStart.class);
                        }
                        myLog("testuser",this.getClass()+"/clicked", "clicked", this.getClass().toString(), textExtension("Dialog geschlossen"));
                        startActivity(myIntent);
                        finish();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();


        // show it
        alertDialog.show();

        Objects.requireNonNull(alertDialog.getWindow()).getAttributes();

        TextView textView = alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
        Button btn1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        btn1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);

        new Handler().post(new Runnable() {
            @Override
            public void run() {

                final View info = findViewById(R.id.action_info);
                info.setOnTouchListener((new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            showDialog(getString(R.string.info), getString(R.string.info_noinfo), context);
                        }
                        return true;

                    }}));

                final View recap = findViewById(R.id.action_recap);
                recap.setOnTouchListener((new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;

                    }}));

                final View help = findViewById(R.id.action_help);
                help.setOnTouchListener((new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if(event.getAction() == MotionEvent.ACTION_UP)
                            if(isPlaying){
                                mp.pause();
                                isPlaying = false;

                            }else {
                                mp.start();
                                isPlaying = true;
                                myLog("testuser", this.getClass() + "/neededhelp", "neededhelp", this.getClass().toString(), timeExtension());
                            }
                        return true;
                    }
                }));

            }});
        return true;}
}
