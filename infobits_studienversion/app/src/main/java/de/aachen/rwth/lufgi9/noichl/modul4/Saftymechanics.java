package de.aachen.rwth.lufgi9.noichl.modul4;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class Saftymechanics extends MyActivity {

    Context context;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_s_mytextview_viewstub_s_nextbutton);
        context = this;

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_4_checkbox);
        stub.inflate();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("Saftymechanics"));
        }
        editor.putInt("current", getActivityNumber("Saftymechanics"));
        editor.apply();
        saveSharedPrefs();

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.module4_safemechanics_info);
        CheckBox alt1 = (CheckBox) findViewById(R.id.alt1);
        alt1.setText(R.string.module4_muster);
        CheckBox alt2 = (CheckBox) findViewById(R.id.alt2);
        alt2.setText(R.string.module4_zahlencode);
        CheckBox alt3 = (CheckBox) findViewById(R.id.alt3);
        alt3.setText(R.string.module4_fingerabdruck);
        CheckBox alt4 = (CheckBox) findViewById(R.id.alt4);
        alt4.setText(R.string.module4_gesichtserkennung);

        final Button nextButton = findViewById(R.id.next);



        nextButton.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                    if(event.getAction() == MotionEvent.ACTION_UP) {

                        SharedPreferences Preference = getSharedPreferences("mechanics", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = Preference.edit();
                        editor.putBoolean("1", ((CheckBox) findViewById(R.id.alt1)).isChecked());
                        editor.putBoolean("2", ((CheckBox) findViewById(R.id.alt2)).isChecked());
                        editor.putBoolean("3", ((CheckBox) findViewById(R.id.alt3)).isChecked());
                        editor.putBoolean("4", ((CheckBox) findViewById(R.id.alt4)).isChecked());

                        editor.commit();

                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("1", getString(R.string.module4_muster), ((CheckBox) findViewById(R.id.alt1)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("2", getString(R.string.module4_zahlencode), ((CheckBox) findViewById(R.id.alt2)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("3", getString(R.string.module4_fingerabdruck), ((CheckBox) findViewById(R.id.alt3)).isChecked()+""));
                        myLog("testuser", this.getClass() + "/selected", "selected", this.getClass().toString(), idQAExtension("4", getString(R.string.module4_gesichtserkennung), ((CheckBox) findViewById(R.id.alt4)).isChecked()+""));

                        Intent myIntent = nextActivity(getActivityNumber("Saftymechanics") + 1);
                        startActivity(myIntent);
                        finish();
                    }
                    return false;
            }
        });
    }



}
