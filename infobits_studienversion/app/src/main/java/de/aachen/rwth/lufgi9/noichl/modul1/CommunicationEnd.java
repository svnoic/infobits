package de.aachen.rwth.lufgi9.noichl.modul1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileNotFoundException;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.AppStart;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the end of module 1.
 * This class gives an overview over all texts, videos and excercises of this module and the user can easily jump back to the content he/she wants to repeat.
 * The is also a possibility to watch the results of the pre- and post- "Selbsteinschätzung". If the statsButton is touched the Activity (XYPlotActivity) with the corresponding diagram will be started.
 * If the user press the nextButton the AppStart-Activity will be started.
 * All button touch events will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class CommunicationEnd extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Loads the activities layout (R.layout.viewstub_finalpage_module1).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The recap variable is set to false and the AppStart-Activity will be started.
     * <b>statsButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The XYPlotActivity will be started to show the user a diagram.
     * <b>All other buttons:</b>Loggs time and touch-coordinates of users touch. Start the corresponding Activity with the content the user wants to repeat.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewstub_finalpage_module1);

        context = this;


        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM1", getActivityNumber("CommunicationEnd"));
        }
        editor.putInt("current", getActivityNumber("CommunicationEnd"));
        editor.apply();
        saveSharedPrefs();


        recap = true;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.finish);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setText(R.string.close_module);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                recap = false;
                try {
                    readSharedPrefs();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Intent myIntent = new Intent(getBaseContext(), AppStart.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button statsButton = findViewById(R.id.stats);
        statsButton.setText(R.string.watch_stats);
        statsButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, statsButton));
                Intent myIntent = new Intent(getBaseContext(), XYPlotActivity.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button kutStatsButton = findViewById(R.id.kutstats);
        kutStatsButton.setText(R.string.kut_stats);
        kutStatsButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, kutStatsButton));
                Intent myIntent = new Intent(getBaseContext(), KUT_K_Stats.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button digcompStatsButton = findViewById(R.id.digcompstats);
        digcompStatsButton.setText(R.string.digcomp_stats);
        digcompStatsButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, digcompStatsButton));
                Intent myIntent = new Intent(getBaseContext(), DigComp_Stats.class);
                startActivity(myIntent);
                finish();
                return false;
            }
        });

        final Button recap_video1 = findViewById(R.id.recap_video1);
        recap_video1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    VideoTalkAndLetter.second = false;
                    Intent myIntent = new Intent(getBaseContext(), TalkLetterIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video2 = findViewById(R.id.recap_video2);
        recap_video2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    VideoTalkAndLetter.second = true;
                    Intent myIntent = new Intent(getBaseContext(), VideoTalkAndLetter.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video3 = findViewById(R.id.recap_video3);
        recap_video3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), VideoAdressIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video4 = findViewById(R.id.recap_video4);
        recap_video4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), ImageUseIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video5 = findViewById(R.id.recap_video5);
        recap_video5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), CreativeCommonsIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_video6 = findViewById(R.id.recap_video6);
        recap_video6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), OERIntro.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity1 = findViewById(R.id.recap_activity1);
        recap_activity1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), DifferenceTalkLetter.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity2 = findViewById(R.id.recap_activity2);
        recap_activity2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), CommunicationUsage.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity3 = findViewById(R.id.recap_activity3);
        recap_activity3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), UsecaseActivity.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity4 = findViewById(R.id.recap_activity4);
        recap_activity4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), FillTheGapActivity.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity5 = findViewById(R.id.recap_activity5);
        recap_activity5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity5));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), MessageComponentMatch.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity6 = findViewById(R.id.recap_activity6);
        recap_activity6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity6));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), SortPolicy.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity7 = findViewById(R.id.recap_activity7);
        recap_activity7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity7));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), PolicyTrueFalse.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });

        final Button recap_activity8 = findViewById(R.id.recap_activity8);
        recap_activity8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity8));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent myIntent = new Intent(getBaseContext(), UsecaseActivity2.class);
                    startActivity(myIntent);
                    finish();
                }
                return false;
            }
        });
    }

}
