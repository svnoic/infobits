package de.aachen.rwth.lufgi9.noichl.modul2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.Draw;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.Java8Utilities;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.NetworkComponent;
import de.aachen.rwth.lufgi9.noichl.modul2.utilities.ViewUtilities;

/**
 * This is the second interactive activity of the network topic.
 * Let the user connect components with a switch.
 *
 * @author Karl Ricken
 * @version 1.0
 * @since 1.0
 */
public class NetworkSwitchConnectedActivity extends MyActivity implements OnTouchListener {

    Context context;

    /**
     * The state enum for the current action mode of the
     * This means, that a user can, for example, move a client on screen to another position or connect a client to another.
     */
    public enum ClientActionState {
        /**
         * This is the default case. The user can move a client to another position.
         */
        MOVING,
        /**
         * State for connect the current selected client to another client.
         */
        CONNECTING,
        /**
         * State for connect the current selected client to another client.
         */
        CREATING
    }

    /**
     * The view where the user plays on with clients, server and connections
     */
    private Draw playgroundView;

    /**
     * The area that contains the component bar and the playground view.
     * This is needed to let the user drag and drop a component from the component bar to the playground view
     */
    private ViewGroup paintArea;

    /**
     * The list of clients which can be connected
     */
    private final List<View> clients = new ArrayList<>();

    /**
     * The list of switches which can be connected
     */
    private final List<View> switches = new ArrayList<>();

    /**
     * The list of components which can be connected.
     * The elements are in {@link #clients} or {@link #switches}, too.
     */
    private final List<View> components = new ArrayList<>();

    /**
     * List of connection between component views
     */
    private final List<Pair<View, View>> connectedComponents = new ArrayList<>();

    /**
     * The view of the component that is selected at the moment.
     */
    private View selectedComponent;

    /**
     * State to signalize if the move-event in MotionEvent is to create a connection between two components or to move the current component
     */
    private ClientActionState componentActionState = ClientActionState.MOVING;

    /**
     * The start raw screen position of the user touch when the user touches down
     */
    private final PointF startRawTouch = new PointF();

    /**
     * The start position of the component in his parent view when the user touches down
     */
    private final PointF originComponentPosition = new PointF();

    /**
     * The number of clients that will be initialized per default
     */
    private int installedClients = 4;

    /**
     * Default constructor.
     */

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_viewstub_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM2", getActivityNumber("NetworkSwitchConnectedActivity"));
        }
        editor.putInt("current", getActivityNumber("NetworkSwitchConnectedActivity"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_draw_network_devices);
        stub.inflate();

        playgroundView = findViewById(R.id.network_switch_connected_playground);
        playgroundView.setOnTouchListener(this);

        // initialize the help view
         // The help view for the user with hints what to do

        //TextView helpLayout = findViewById(R.id.my_textview);
        //helpLayout.setText(R.string.module2_switchconnected_info);

        // make the help view to a view click resizing
        /*new HelpTextViewClickResizing("networkSwitchConnected", helpLayout)
                .setTargetWidthRes(R.dimen.width_help_layout_minimized)
                .setTargetHeightRes(R.dimen.height_help_layout_minimized);*/

        playgroundView = findViewById(R.id.network_switch_connected_playground);
        playgroundView.setOnTouchListener(this);

        paintArea = findViewById(R.id.network_switch_connected_paint_area);

        findViewById(R.id.network_switch_connected_components_computer).setOnTouchListener(this);
        findViewById(R.id.network_switch_connected_components_switch).setOnTouchListener(this);


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass()+"/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (switches.size() < 1) {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module2_switchconnected_dialog1), context);
                        return false;
                    }

                    if (switches.size() > 1) {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module2_switchconnected_dialog2), context);

                        return false;
                    }

                    if(switchOccurrences(connectedComponents, switches.get(0)) < clients.size()){
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.module2_switchconnected_dialog3), context);
                        return false;
                    }

                    if (recap) {
                        Intent myIntent = new Intent(context, InternetEnd.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        if(myrecap){
                            restartAfterRecap();
                        }else {
                            Intent myIntent = nextActivity(getActivityNumber("NetworkSwitchConnectedActivity") + 1);
                            startActivity(myIntent);
                            finish();
                        }
                    }
                }
                return false;
            }
        });


        // edit the clients
        if (installedClients >= 1) {
            View client = findViewById(R.id.network_switch_connected_client_1);
            client.setTag("client");
            client.setOnTouchListener(this);
            TextView label = client.findViewById(R.id.network_connected_client_label);
            label.setText(getString(R.string.nameNetworkConnectedClient, "" + (clients.size() + 1)));
            components.add(client);
            clients.add(client);
        }

        // clear the list if the activity is started
        if (savedInstanceState == null) {
            clients.clear();
            connectedComponents.clear();
            componentActionState = ClientActionState.MOVING;

            // install the clients
            editPredefinedClient(R.id.network_switch_connected_client_1, 1);
            editPredefinedClient(R.id.network_switch_connected_client_2, 2);
            editPredefinedClient(R.id.network_switch_connected_client_3, 3);
            editPredefinedClient(R.id.network_switch_connected_client_4, 4);
            editPredefinedClient(R.id.network_switch_connected_client_5, 5);
            editPredefinedClient(R.id.network_switch_connected_client_6, 6);

            // show task as dialog, too

            // show the user the task
            //DialogUtilities.showSimpleOKDialog(this, R.string.titleTask, getResources().getQuantityString(R.plurals.msgNetworkSwitchConnectedTask, 1, 1, installedClients));
        }
    }

    /**
     * Edit the predefined clients (name and visibility) depending on the <code>clientNumber</code>
     *
     * @param layoutId     The layout id to get the view by
     * @param clientNumber The number of the client. Starts by 1.
     */
    private void editPredefinedClient(@IdRes int layoutId, int clientNumber) {
        View client = findViewById(layoutId);

        if (clientNumber > installedClients) {
            ((ViewGroup) client.getParent()).removeView(client);
        } else {
            client.setTag("client");
            client.setOnTouchListener(this);
            TextView label = client.findViewById(R.id.network_connected_client_label);
            label.setText(getString(R.string.nameNetworkConnectedClient, "" + clientNumber));
            components.add(client);
            clients.add(client);
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // install the start position
            startRawTouch.set(event.getRawX(), event.getRawY());

            // check if one of the components is clicked
            if (v.getId() == R.id.network_switch_connected_components_switch) {

                // add only a new switch if the maximum number of switches is not reached.
                if (switches.size() < 1) {
                    RectF paintAreaRect = ViewUtilities.rectScreen(paintArea);
                    final float x = event.getRawX() - paintAreaRect.left;
                    final float y = event.getRawY() - paintAreaRect.top;

                    final View switchView = LayoutInflater.from(this).inflate(R.layout.template_network_connected_switch, playgroundView, false);
                    switchView.setBackgroundResource(R.drawable.background_network_connected_component_selected);
                    // set the tag to signalize the type of the component
                    switchView.setTag("switch");
                    switchView.setOnTouchListener(this);
                    TextView label = switchView.findViewById(R.id.network_connected_switch_label);
                    label.setText(getString(R.string.nameNetworkConnectedSwitch, "" + (switches.size() + 1)));

                    switchView.setX(x);
                    switchView.setY(y);

                    // hide view, since the correct position can not be calculated (because sizes not available at this moment)
                    switchView.setVisibility(View.INVISIBLE);

                    paintArea.addView(switchView);
                    switchView.post(new Runnable() {
                        @Override
                        public void run() {
                            int width = switchView.getWidth();
                            int height = switchView.getHeight();

                            int clientX = Math.max(0, (int) x - width / 2);
                            int clientY = Math.max(0, (int) y - height / 2);

                            // change to correct position
                            switchView.setX(clientX);
                            switchView.setY(clientY);
                            originComponentPosition.set(clientX, clientY);

                            // show the view
                            switchView.setVisibility(View.VISIBLE);
                        }
                    });

                    // set the action state to creating to signalize the creation mode
                    componentActionState = ClientActionState.CREATING;

                    selectedComponent = switchView;

                    //helpLayout.setText(R.string.msgHelpNetworkSwitchConnectedConnectClients);
                }

                return true;
            } else {
                // enable the connection mode so that the user can connect two components by drawing a line
                componentActionState = ClientActionState.CONNECTING;

                // look into components
                if (components.contains(v)) {
                    // set selectable background
                    v.setBackgroundResource(R.drawable.background_network_connected_component_selected);
                    selectedComponent = v;
                    return true;
                }
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            if (selectedComponent != null && componentActionState == ClientActionState.CREATING) {
                // the end of creating mode. Check if position is valid
                if (NetworkConnectedActivity.isValidPosition(playgroundView, components, selectedComponent)) {
                    // add the new component to the component list and to the switch list or client list
                    components.add(selectedComponent);
                    if (selectedComponent.getTag().equals("switch")) {
                        switches.add(selectedComponent);
                    } else if (selectedComponent.getTag().equals("client")) {
                        clients.add(selectedComponent);
                    }

                    // move the component to the playgroundView
                    RectF compRect = ViewUtilities.rectScreen(selectedComponent);
                    RectF playgroundRect = ViewUtilities.rectScreen(playgroundView);
                    paintArea.removeView(selectedComponent);
                    playgroundView.addView(selectedComponent);
                    selectedComponent.setX(compRect.left - playgroundRect.left);
                    selectedComponent.setY(compRect.top - playgroundRect.top);

                    // go to moving mode
                    componentActionState = ClientActionState.CONNECTING;
                } else {
                    paintArea.removeView(selectedComponent);
                    //DialogUtilities.showSimpleOKDialog(this, R.string.titleNetworkConnected, R.string.msgNetworkConnectedInvalidPosition);
                }
                selectedComponent = null;

                // disable the switch component if needed
                NetworkComponent networkComponentSwitch = findViewById(R.id.network_switch_connected_components_switch);
                networkComponentSwitch.setCustomEnabled(switches.size() < 1);
            }

            // make all clients not selected
            for (View component : components) {
                component.setBackgroundResource(R.drawable.background_network_connected_component);
            }

            // if there was a selection before and now the event is inside another component, connect them
            if (selectedComponent != null && componentActionState == ClientActionState.CONNECTING) {
                View connectedComponent = null;
                for (int i = components.size() - 1; i >= 0; i--) { // start at the end... top elements first
                    View component = components.get(i);
                    if (component == selectedComponent) continue;  // skip selected component
                    if (ViewUtilities.containsPoint(component, event.getRawX(), event.getRawY())) {
                        // found a second component
                        connectedComponent = component;
                        break;
                    }
                }

                // if a connected component is found, connect them
                if (connectedComponent != null) {

                    // found a second component. Connect both.
                    playgroundView.addUndirectedViewConnection(selectedComponent, connectedComponent);
                    playgroundView.invalidate();

                    if (!NetworkConnectedActivity.contains(connectedComponents, selectedComponent, connectedComponent)) {

                        // check if one element is a switch. Otherwise, ask if the user want to add a useless connection
                        if (connectedComponent.getTag().equals("client") && selectedComponent.getTag().equals("client")) {
                            final View finalSelectedComponent = selectedComponent;
                            //DialogUtilities.showSimpleYesNoDialog(this, R.string.titleNetworkSwitchConnected, R.string.msgNetworkSwitchConnectedUnnecessaryConnection, new DialogInterface.OnClickListener() {
                            showDialog(getString(R.string.module2_switch_not_needed_connection_dialog_title), getString(R.string.module2_switch_not_needed_connection_dialog_message), context);
                            playgroundView.removeUndirectedViewConnection(finalSelectedComponent, connectedComponent);
                            playgroundView.invalidate();
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    connectedComponents.add(new Pair<>(finalSelectedComponent, finalConnectedComponent));
//                                }
//                            }, new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    playgroundView.removeUndirectedViewConnection(finalSelectedComponent, finalConnectedComponent);
//                                    playgroundView.invalidate();
//                                }
//                            });
                        } else {
                            connectedComponents.add(new Pair<>(selectedComponent, connectedComponent));

                            allDone();// he solved the task
//DialogUtilities.showSimpleOKDialog(this, R.string.titleNetworkSwitchConnected, R.string.msgNetworkConnectedAllDone);
//helpLayout.setText(R.string.msgClickNext);
                        }
                    }
                } else {
                    // show the user a small hint otherwise
                    Toast.makeText(this, R.string.msgNetworkConnectedNoConnectedComponent, Toast.LENGTH_LONG).show();
                }

                // deselect the component
                selectedComponent = null;

                // remove all lines from selectedComponent to current touch position
                playgroundView.clearUndirectedPointConnections();
                playgroundView.invalidate();
            }

            v.performClick();
        } else if (event.getAction() == MotionEvent.ACTION_MOVE && selectedComponent != null) {
            if (componentActionState == ClientActionState.CONNECTING) {
                // check if another component is selected
                for (int i = components.size() - 1; i >= 0; i--) { // start at the end... top elements first
                    View comp = components.get(i);
                    if (comp == selectedComponent) continue;  // skip selected client

                    if (ViewUtilities.containsPoint(comp, event.getRawX(), event.getRawY())) {
                        // highlight the view
                        comp.setBackgroundResource(R.drawable.background_network_connected_component_selected);
                        break;
                    } else {
                        // set normal background otherwise
                        comp.setBackgroundResource(R.drawable.background_network_connected_component);
                    }
                }

                // remove all lines from selectedClient to current touch position
                playgroundView.clearUndirectedPointConnections();
                // add the current position
                playgroundView.addUndirectedPointConnection(
                        selectedComponent.getX() + selectedComponent.getWidth() / 2F,
                        selectedComponent.getY() + selectedComponent.getHeight() / 2F,
                        selectedComponent.getX() + event.getX(),
                        selectedComponent.getY() + event.getY());
                playgroundView.invalidate();
            } else if (componentActionState == ClientActionState.MOVING || componentActionState == ClientActionState.CREATING) {
                float deltaX = event.getRawX() - startRawTouch.x;
                float deltaY = event.getRawY() - startRawTouch.y;
                selectedComponent.setX(originComponentPosition.x + deltaX);
                selectedComponent.setY(originComponentPosition.y + deltaY);
            }
        }

        return false;
    }

    /**
     * Get the number of occurrence for the object. First or second entry does not matter.
     *
     * @param collection The collection that can contain the object
     * @param obj        The object to check
     * @param <E>        The generic type of the objects
     * @return Number of occurrence of the object
     */
    private static <E> int switchOccurrences(Collection<Pair<E, E>> collection, final E obj) {
        return Java8Utilities.filter(collection, new Java8Utilities.Filter<Pair<E, E>>() {
            @Override
            public boolean match(Pair<E, E> pair) {
                return pair.first == obj || pair.second == obj;
            }
        }).size();
    }

    /**
     * Checks if everything is done
     *
     */
    private void allDone() {
        switchOccurrences(connectedComponents, switches.get(0));
        clients.size();
    }
}
