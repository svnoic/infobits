package de.aachen.rwth.lufgi9.noichl.modul3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MainActivity;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is the end of module 1.
 * This class gives an overview over all texts, videos and excercises of this module and the user can easily jump back to the content he/she wants to repeat.
 * The is also a possibility to watch the results of the pre- and post- "Selbsteinschätzung". If the statsButton is touched the Activity (XYPlotActivity) with the corresponding diagram will be started.
 * If the user press the nextButton the AppStart-Activity will be started.
 * All button touch events will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class Recap extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;
    int currentActivity;
    boolean recapMode = false;

    /**
     * Loads the activities layout (R.layout.viewstub_finalpage_module1).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The recap variable is set to false and the AppStart-Activity will be started.
     * <b>statsButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * The XYPlotActivity will be started to show the user a diagram.
     * <b>All other buttons:</b>Loggs time and touch-coordinates of users touch. Start the corresponding Activity with the content the user wants to repeat.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recap_modul3);

        context = this;


        if (myrecap){
            recapMode = true;
        }
        myrecap = true;

        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.recap_text);

        final Button nextButton = findViewById(R.id.next);
        nextButton.setText(R.string.close);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));
                if(recapMode){

                }else {
                    myrecap = false;
                }
                finish();
                return false;
            }
        });

        Context mContext = MainActivity.mycontext;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);

        currentActivity = sharedpreferences.getInt("currentActivityM3", 300);





        final Button recap_video1 = findViewById(R.id.recap_video1);
        if (currentActivity < 307){
            recap_video1.setVisibility(View.GONE);
        }
        if (currentActivity == 307 || currentActivity == 308 || currentActivity == 309){
            recap_video1.setBackgroundResource(R.drawable.color_button);
        }
        recap_video1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 307 || currentActivity == 308 || currentActivity == 309){
                        myrecap = false;
                    }
                        Intent myIntent = new Intent(getBaseContext(), CableVideoIntro.class);
                        startActivity(myIntent);
                        finish();

                }
                return false;
            }
        });

        final Button recap_video2 = findViewById(R.id.recap_video2);
        if (currentActivity < 316){
            recap_video2.setVisibility(View.GONE);
        }
        if (currentActivity == 316 || currentActivity == 317){
            recap_video2.setBackgroundResource(R.drawable.color_button);
        }
        recap_video2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_video2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 316 || currentActivity == 317){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), ConnectionVideoIntro.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });


        final Button recap_text1 = findViewById(R.id.recap_text1);
        if (currentActivity < 303){
            recap_text1.setVisibility(View.GONE);
        }
        if (currentActivity == 303 || currentActivity == 304 || currentActivity == 305 || currentActivity == 306){
            recap_text1.setBackgroundResource(R.drawable.color_button);
        }
        recap_text1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 303 || currentActivity == 304 || currentActivity == 305 || currentActivity == 306){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), ConnectionIntro.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text2 = findViewById(R.id.recap_text2);
        if (currentActivity < 312){
            recap_text2.setVisibility(View.GONE);
        }
        if (currentActivity == 312){
            recap_text2.setBackgroundResource(R.drawable.color_button);
        }
        recap_text2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 312){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), LanManWan.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text3 = findViewById(R.id.recap_text3);
        if (currentActivity < 313){
            recap_text3.setVisibility(View.GONE);
        }
        if (currentActivity == 313){
            recap_text3.setBackgroundResource(R.drawable.color_button);
        }
        recap_text3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 313){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), WlanWifi.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_text4 = findViewById(R.id.recap_text4);
        if (currentActivity < 314){
            recap_text4.setVisibility(View.GONE);
        }
        if (currentActivity == 314){
            recap_text4.setBackgroundResource(R.drawable.color_button);
        }
        recap_text4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_text4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 314){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), Mobilfunk.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });


        final Button recap_activity1 = findViewById(R.id.recap_activity1);
        if (currentActivity < 315){
            recap_activity1.setVisibility(View.GONE);
        }
        if (currentActivity == 315){
            recap_activity1.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity1));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 315){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), InternetWordsSolution.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity2 = findViewById(R.id.recap_activity2);
        if (currentActivity < 318){
            recap_activity2.setVisibility(View.GONE);
        }
        if (currentActivity == 318){
            recap_activity2.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity2));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 318){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), DifferenceWLANMobil.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity3 = findViewById(R.id.recap_activity3);
        if (currentActivity < 319){
            recap_activity3.setVisibility(View.GONE);
        }
        if (currentActivity == 319){
            recap_activity3.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity3));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 319){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), GameBookIntro.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });

        final Button recap_activity4 = findViewById(R.id.recap_activity4);
        if (currentActivity < 320){
            recap_activity4.setVisibility(View.GONE);
        }
        if (currentActivity == 320){
            recap_activity4.setBackgroundResource(R.drawable.color_button);
        }
        recap_activity4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, recap_activity4));
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (currentActivity == 320){
                        myrecap = false;
                    }
                    Intent myIntent = new Intent(getBaseContext(), WLANQuizInfo.class);
                    startActivity(myIntent);
                    finish();

                }
                return false;
            }
        });



    }
}
