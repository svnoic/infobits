package de.aachen.rwth.lufgi9.noichl.basics.gesten;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

/**
 * This class is for learning the Drag and Drop gesture. The user has to move an image into a black surrounded box.
 * If the image is not inside the box a dialog box is shown saying that the user should place the image inside the box.
 * If the image is inside the box the next Activity (End.class) is started.
 * All gestures will be logged with the myLog-method.
 *
 * @author Svenja Noichl
 * @version 1.0
 * @since 1.0
 */

public class DragDrop extends MyActivity {

    /**
     * Represents the context of the current Activity.
     */
    Context context;

    /**
     * Represents the status of the given solution.
     * If the image is inside the box, solution has the state <b>true</b> and on nextButton-touch-event the next activity will be started.
     * If image is not inside the box, solution has the state <b>false</b> and on nextButton-touch-event a dialog will be shown.
     */
    boolean solution;

    /**
     * Loads the activities layout (R.layout.activity_textview_viewstub2_next_button) which uses a viewstub (gestures_main_fragment_dragdrop).
     * <b>nextButton-onTouchListener:</b>Loggs time and touch-coordinates of users touch.
     * Triggered by the ACTION_UP event either the next activity will be started or the dialog will be shown based on the status of the <b>solution</b> variable.
     * If solution is <b>true</b> the next activity (End.class) will be started, otherwise a dialog will be shown.
     * <b>iv_dragdrop-onTouchListener:</b>Loggs that the image view was clicked by the user.
     * If the event is ACTION_UP the position of the image will be evaluated. If the image is inside of the box-background is set to drawable.border_done and solution is set to true.
     * Otherwise the box-background is set to drawable.border and solution is set to false.
     * If the event is ACTION_MOVE the position of the image will be changed according the gestures movement.
     *
     * @param savedInstanceState Android-Parameter
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        context = this;
        setContentView(R.layout.activity_s_mytextview_s_viewstub_nextbutton);

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.basics_gestures_draganddrop);
        stub.inflate();


        TextView tv = findViewById(R.id.my_textview);
        tv.setText(R.string.description_text_dragdrop);

        final ImageView iv = findViewById(R.id.iv_dragdrop);
        iv.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), imageViewEventExtension(event, iv));

                final int X = (int) event.getRawX();
                final int Y = (int) event.getRawY();
                int[] l = new int[2];
                RelativeLayout.LayoutParams lParams;
                RelativeLayout rel = findViewById(R.id.dragdrop);
                int[] i = new int[2];
                rel.getLocationOnScreen(i);
                int xDelta = 0;
                int yDelta = 0;

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        view.getLocationOnScreen(l);
                        lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        if (l[0] < 20) {
                            lParams.leftMargin = 20;
                        }
                        if (l[1] < i[1]) {
                            lParams.topMargin = 0;
                        }
                        if (l[1] > rel.getHeight() + i[1] - 200) {
                            lParams.topMargin = rel.getHeight() - 200;
                        }
                        if (l[0] > rel.getWidth() + i[0] - 200) {
                            lParams.leftMargin = rel.getWidth() - 200;
                        }

                        ImageView border = findViewById(R.id.border);
                        int[] b = new int[2];
                        border.getLocationOnScreen(b);
                        if (l[0] > b[0] && l[1] > b[1] && l[0] + view.getWidth() < b[0] + border.getWidth() && l[1] + view.getHeight() < b[1] + border.getHeight()) {
                            border.setBackground(getResources().getDrawable(R.drawable.border_done));
                            solution = true;
                        } else {
                            border.setBackground(getResources().getDrawable(R.drawable.border));
                            solution = false;
                        }

                        break;
                    case MotionEvent.ACTION_MOVE:
                        view.getLocationOnScreen(l);
                        lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        if (l[0] < 20) {
                            lParams.leftMargin = 20;
                        }
                        if (l[1] < i[1]) {
                            lParams.topMargin = 0;
                        }
                        if (l[1] > rel.getHeight() + i[1] - 200) {
                            lParams.topMargin = rel.getHeight() - 200;
                        }
                        if (l[0] > rel.getWidth() + i[0] - 200) {
                            lParams.leftMargin = rel.getWidth() - 200;
                        }
                        lParams.leftMargin = X - xDelta;
                        lParams.topMargin = Y - yDelta;
                        lParams.rightMargin = 0 - 250;
                        lParams.bottomMargin = 0 - 250;


                        view.setLayoutParams(lParams);


                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + (event.getAction() & MotionEvent.ACTION_MASK));
                }
                @SuppressLint("CutPasteId") ViewGroup mylayout = findViewById(R.id.dragdrop);
                mylayout.invalidate();
                return true;
            }
        });


        final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (solution) {
                        solution = false;
                        Intent myIntent = new Intent(context, End.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        showDialog(getString(R.string.not_yet_ready), getString(R.string.gestures_dragdrop_dialog), context);
                    }
                }

                return false;
            }
        });


    }


}
