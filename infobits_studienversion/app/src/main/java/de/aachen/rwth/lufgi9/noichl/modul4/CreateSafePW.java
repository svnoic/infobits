package de.aachen.rwth.lufgi9.noichl.modul4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import de.aachen.rwth.lufgi9.noichl.R;
import de.aachen.rwth.lufgi9.noichl.general.MyActivity;

public class CreateSafePW extends MyActivity {

    Context context;
    String solution;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_usecasetextview_viewstub_s_nextbutton);
        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences(MyActivity.USERID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (!recap && !myrecap) {
            editor.putInt("currentActivityM4", getActivityNumber("CreateSafePW"));
        }
        editor.putInt("current", getActivityNumber("CreateSafePW"));
        editor.apply();
        saveSharedPrefs();

        ViewStub stub = findViewById(R.id.viewStub);
        stub.setLayoutResource(R.layout.viewstub_textview_edittext);
        stub.inflate();

        solution = getString(R.string.module4_createsafepw_pw);
            TextView tv = findViewById(R.id.my_usecase_textview);
            tv.setText(R.string.moduele4_createsafepw_info);

        TextView tv_ex = findViewById(R.id.tv_ex);
        tv_ex.setText(R.string.module4_createsafepw_ex);

            final Button nextButton = findViewById(R.id.next);
        nextButton.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    EditText et = findViewById(R.id.et);
                    String answer = et.getText().toString();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (answer.equals(solution)) {
                            myLog("testuser", this.getClass() + "/clicked", "clicked", this.getClass().toString(), buttonEventExtension(event, nextButton));

                            if (recap) {
                                Intent myIntent = new Intent(context, DataSecEnd.class);
                                startActivity(myIntent);
                                finish();
                            } else {
                                if(myrecap){
                                    restartAfterRecap();
                                }else {
                                    Intent myIntent = nextActivity(getActivityNumber("CreateSafePW") + 1);
                                    startActivity(myIntent);
                                    finish();
                                }
                            }
                        } else {
                            showDialog(getString(R.string.module4_createsafepw_dialog_title), getString(R.string.module4_createsafepw_dialog
                            ), context);
                        }

                    }
                    return false;
            }});

    }
}
